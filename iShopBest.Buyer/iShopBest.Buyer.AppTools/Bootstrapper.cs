﻿using CapSoft.Identity.Domain.Model;
using CapSoft.Infrastructure.Configuration;
using CapSoft.Infrastructure.Cryptography;
using CapSoft.Infrastructure.Domain;
using CapSoft.Infrastructure.Email;
using System;
using System.ComponentModel.Composition;
using System.Threading;
using System.Web;

namespace iShopBest.Buyer.AppTools
{
    public class Bootstrapper
    {

        public static void Init(string threadName,string contextName)
        {
            DefaultOrganization.Id=29845;
            DefaultOrganization.Name = "Buyer's website";
            DefaultOrganization.WebsiteName = "Buyer's website";
            DefaultOrganization.ClaimsTagPrefix = "buyer";


            if (string.IsNullOrEmpty(Thread.CurrentThread.Name))
                Thread.CurrentThread.Name = threadName + Thread.CurrentThread.ManagedThreadId.ToString();

            /// Wire up MEF
            ValueObjectBase.Container = CompositionContainerFactory.Init();
            CompositionContainerFactory compositor = new CompositionContainerFactory();
            ValueObjectBase.Container.ComposeParts(compositor);
            /// Initialize factories
            ApplicationSettingsFactory.InitializeFactory(ValueObjectBase.Container.GetExportedValue<IApplicationSettings>());
            CryptographyServiceFactory.InitializeFactory(ValueObjectBase.Container.GetExportedValue<ICryptographyService>());
            EmailServiceFactory.InitializeFactory(ValueObjectBase.Container.GetExportedValue<IEmailService>());

            try
            {
                //initialize others
                var initializers = ValueObjectBase.Container.GetExports<IRepositoryInitializer, IRepositoryInitializerMetadata>();
                foreach(var initializer in initializers)
                {
                    switch(initializer.Metadata.InitializerName)
                    {
                        case "Identity":
                            initializer.Value.Init("name=IdentityEntities");
                            break;
                        case "BuyerCatalog":
                            initializer.Value.Init("name=BuyerCatalogEntities");
                            break;
                        default:
                            break;
                    }
                }

                //ValueObjectBase.Container.GetExportedValue<IRepositoryInitializer>().Init(contextName as object);
                //this will keep HttpContext.Current in thread-safe place
                ValueObjectBase.ContextHolder = HttpContext.Current;
            }
            catch (Exception ex)
            {
                if (Console.IsOutputRedirected)
                    Console.WriteLine(ex.ToString());
            }

            // initialize mappings between entities and views
//            Entity2ViewsConfig.InitializeMappings();
        }
    }
}
