﻿using System;
using System.ComponentModel.Composition.Hosting;
using System.Diagnostics;

namespace iShopBest.Buyer.AppTools
{
    internal class CompositionContainerFactory
    {

        public static CompositionContainer Init()
        {

            AggregateCatalog catalog = new AggregateCatalog();

            try
            {
                    catalog.Catalogs.Add(new DirectoryCatalog(System.Web.HttpRuntime.BinDirectory, "iShopBest*.dll"));
                    catalog.Catalogs.Add(new DirectoryCatalog(System.Web.HttpRuntime.BinDirectory, "CapSoft*.dll"));
            }
            catch (Exception ex)
            {
#if DEBUG
                if (Console.IsOutputRedirected)
                    Debug.WriteLine(ex.ToString());
#endif


                DirectoryCatalog dCatalog = new DirectoryCatalog(AppDomain.CurrentDomain.BaseDirectory, "iShopBest*.dll");
                catalog.Catalogs.Add(dCatalog);
                catalog.Catalogs.Add(new DirectoryCatalog(AppDomain.CurrentDomain.BaseDirectory, "CapSoft*.dll"));
                catalog.Catalogs.Add(new DirectoryCatalog(AppDomain.CurrentDomain.BaseDirectory, "iShopBest*.exe"));
            }

            CompositionContainer container = new CompositionContainer(catalog, CompositionOptions.IsThreadSafe);
            return container;
        }
    }
}