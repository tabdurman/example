﻿using iShopBest.Buyer.Domain.Model.ServiceContracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using iShopBest.Buyer.Domain.Model.DataContracts.ProductsService;
using iShopBest.Buyer.Domain.Model.RepositoryContracts;
using CapSoft.Infrastructure.Domain;
using CapSoft.Infrastructure.UnitOfWork;
using CapSoft.Infrastructure.Helpers;
using System.Linq;
using iShopBest.Buyer.Domain.Model.Entities;
using System.Linq.Expressions;
using CapSoft.Infrastructure.Extensions;
using CapSoft.Infrastructure.QueryObject;
using System.Diagnostics;
using System.Threading.Tasks;

namespace iShopBest.Buyer.Domain.Services
{
    /// <summary>
    /// Service to access products
    /// </summary>
    [Export(typeof(IProductsService))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class ProductsService : ServiceBase, IProductsService
    {
        private IProductRepository _productRepository;
        private IProductImageRepository _productImageRepository;

        public ProductsService() : this(
            ValueObjectBase.Container.GetExports<IUnitOfWork, IUnitOfWorkMetadata>(),
            ValueObjectBase.Container.GetExportedValue<IProductRepository>(),
            ValueObjectBase.Container.GetExportedValue<IProductImageRepository>()            )
        {
        }

        [ImportingConstructor]
        public ProductsService(
            [ImportMany] IEnumerable<Lazy<IUnitOfWork, IUnitOfWorkMetadata>> uow,
            [Import] IProductRepository productRepository,
            [Import] IProductImageRepository productImageRepository
            ) : base(uow)
        {
            _productRepository = ArgumentValidator.GetValue<IProductRepository>(() => productRepository);
            _productImageRepository = ArgumentValidator.GetValue<IProductImageRepository>(() => productImageRepository);
        }

        #region IDisposable
        private bool _disposed;
        private object _threadSafety = new object();

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected override void Dispose(bool disposing)
        {
            lock (_threadSafety)
            {
                if (!_disposed)
                {
                    _disposed = true;
                    if (disposing)
                    {
                        base.Dispose(disposing);
                    }
                }

            }

        }
        #endregion

        /// <summary>
        /// Will return products according to requested query
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetProductsResponse GetProducts(GetProductsRequest request)
        {
#if DEBUG
            Debug.WriteLine("Entering Services-GetProducts {0}", DateTime.Now);
#endif

            GetProductsResponse response = new GetProductsResponse();
            Expression<Func<Product, bool>> where;

            //var principal = Thread.CurrentPrincipal;

            response.PageSize = request.PageSize;
            response.PageNumber = request.PageNumber;
            response.OrderBy = request.OrderBy;

            Expression<Func<Product, SortableProperty>>[] orderBy = null;

            if (request.OrderBy.Length > 0)
            {
                List<Expression<Func<Product, SortableProperty>>> orderByList = new List<Expression<Func<Product, SortableProperty>>>();
                foreach (var field in request.OrderBy)
                {

                    bool ascending = true;
                    string sortByField = "";

                    if (field.Substring(0, 1) == "-")
                        ascending = false;
                    if (field.Substring(0, 1) == "-" || field.Substring(0, 1) == "+")
                        sortByField = field.Substring(1, field.Length - 1);
                    else
                        sortByField = field;
                    // keep ascending and sortByField names otherwise extension will not find what to sort by
                    orderByList.Add(x => new SortableProperty(x.GetType().GetProperty(sortByField).GetValue(x, null), ascending));
                }

                orderBy = orderByList.ToArray();
            }

            where = null;

            if (request.CategoryId != null)
            {
                if (request.CategoryId > 100)
                {
                    if ((request.CategoryId % 1000000) == 0)
                        where = (x => x.UNSPSC / 1000000 == request.CategoryId / 1000000);
                    else if ((request.CategoryId % 10000) == 0)
                        where = (x => x.UNSPSC / 10000 == request.CategoryId / 10000);
                    else if ((request.CategoryId % 100) == 0)
                        where = (x => x.UNSPSC / 100 == request.CategoryId / 100);
                    else
                        where = (x => x.UNSPSC == request.CategoryId);
                }
            }

            if (request.Manufacturers != null)
            {
                if (request.Manufacturers.Length > 0)
                {
                    if (where == null)
                        where = (x => request.Manufacturers.Contains(x.ManufacturerId));
                    else
                        where = where.AndAlso<Product>(x => request.Manufacturers.Contains(x.ManufacturerId));
                }
            }

            if (request.PriceMax!=null)
                if (where == null)
                    where = (x => x.PriceMax<=request.PriceMax.Value);
                else
                    where = where.AndAlso<Product>(x => x.PriceMax <= request.PriceMax.Value);

            if (request.PriceMin != null)
                if (where == null)
                    where = (x => x.PriceMin >= request.PriceMin.Value);
                else
                    where = where.AndAlso<Product>(x => x.PriceMin >= request.PriceMin.Value);

            if (!String.IsNullOrEmpty(request.Keywords))
            {
                // FTS Version
                if (request.Keywords.IndexOf(" ")>=0 && request.Keywords.IndexOf("\"") < 0)
                {
                    /*
                    // this will make spaces to be meaning  exact search
                    request.Keywords = "\"" + request.Keywords + "\"";
                    */
                    // all words must be there, so and condition 
                    request.Keywords = request.Keywords.Trim().Replace("  "," ").Replace(" ", " AND ").Replace(" AND AND "," AND ");
                }
                var searchKeywords = "-FTSPREFIX-" + request.Keywords;
                if (where==null)
                    where = (x => x.Keywords.Contains(searchKeywords));
                else
                    where = where .AndAlso<Product>(x => x.Keywords.Contains(searchKeywords));


                // NON FTS Version, uses like for each word in search string
                /*
                    string[] keywords = request.Keywords.Split(' ');
                    foreach(var keyword in keywords)
                    {
                        if (where == null)
                            where = (x => 1 == 1);

                        where = where.AndAlso<Product>(x => x.Keywords.Contains(keyword));
                    }
                */
            }
            if (where == null)
                where = (x =>  true);



#if DEBUG
            Debug.WriteLine("Services-GetProducts, before get count {0}", DateTime.Now);
#endif
            #region async
            //split into paralel tasks
            List<Task> tasks = new List<Task>()
            {
                new Task(()=> {

                    var sums=_productRepository.FindBy(where).AsQueryable().GroupBy(x => 1).Select(x => new { Count = x.Count(), MinPrice= x.Min(y=>y.PriceMin), MaxPrice=x.Max(y=>y.PriceMax) }).FirstOrDefault();
                    if (sums==null)
                    {
                        response.TotalCount = 0;
                        response.MinPrice=0;
                        response.MaxPrice=0;
                    }
                    else
                    {
                        response.TotalCount = sums.Count;
                        response.MinPrice=sums.MinPrice ?? 0.00M;
                        response.MaxPrice=sums.MaxPrice ?? 0.00M;
                    }

                    if (response.PageNumber.Value > response.TotalPages)
                        response.PageNumber = response.TotalPages;

                    if (response.PageNumber.Value <= 0)
                        response.PageNumber = 1;


#if DEBUG
            Debug.WriteLine("Finished sums {0}", DateTime.Now);
#endif

                }),
                new Task(()=> {

                    var manufacturers=_productRepository.FindBy(where).AsQueryable().GroupBy(x => new {x.ManufacturerId,x.ManufacturerName }).Select(x => new ManufacturerSummary() { ManufacturerId= x.Key.ManufacturerId, ManufacturerName=x.Key.ManufacturerName, ProductsCount= x.Count() }).ToList();
                    response.Manufacturers=manufacturers;

#if DEBUG
            Debug.WriteLine("Finished manufacturers {0}", DateTime.Now);
#endif

                })
            };

            Parallel.ForEach(tasks, q => q.RunSynchronously());

            Task.WaitAll(tasks.ToArray());
            #endregion

            #region sync
            /*
            //synchronyous run
            var sums = _productRepository.FindBy(where).AsQueryable().GroupBy(x => 1).Select(x => new { Count = x.Count() }).FirstOrDefault();
            if (sums == null)
                response.TotalCount = 0;
            else
                response.TotalCount = sums.Count;

            if (response.PageNumber.Value > response.TotalPages)
                response.PageNumber = response.TotalPages;

            if (response.PageNumber.Value <= 0)
                response.PageNumber = 1;

            var tempResults = _productRepository
                .FindBy(whereClause: where, orderBy: orderBy, index: (response.PageNumber.Value - 1) * response.PageSize.Value, count: response.PageSize.Value)
                .ToList();

            response.Results = AutoMapper.Mapper.Map<List<Product>, List<ProductView>>(tempResults);
            */
            #endregion

            var tempResults = _productRepository
            .FindBy(whereClause: where, orderBy: orderBy, index: (response.PageNumber.Value - 1) * response.PageSize.Value, count: response.PageSize.Value)
            .ToList();

            response.Results = AutoMapper.Mapper.Map<List<Product>, List<ProductView>>(tempResults);
#if DEBUG
            Debug.WriteLine("Results {0}", response.Results.Count);
            Debug.WriteLine("Finished results {0}", DateTime.Now);
#endif

#if DEBUG
            Debug.WriteLine("Exititng Services-GetProducts {0}", DateTime.Now);
#endif

            return response;
        }

        /// <summary>
        /// Will return product details
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetProductDetailsResponse GetProductDetails(GetProductDetailsRequest request)
        {
            GetProductDetailsResponse response = new GetProductDetailsResponse();
            var prod = _productRepository.FindBy(id: request.ProductId);
            if (prod != null)
                response.ProductDetails = AutoMapper.Mapper.Map<Product, ProductView>(prod);

            return response;
        }

        /// <summary>
        /// Will return product image
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetProductImageResponse GetProductImage(GetProductImageRequest request)
        {
            GetProductImageResponse response=_productImageRepository.FindBy(x => x.Id == request.ProductId).Select(x => new GetProductImageResponse() { DataBin = x.DataBin, MediaType = x.MediaType }).FirstOrDefault();

            if (response== default(GetProductImageResponse))
                response = _productImageRepository.FindBy(x => x.Id == -1).Select(x => new GetProductImageResponse() { DataBin = x.DataBin, MediaType = x.MediaType }).FirstOrDefault();

            return response;
        }
    }

    }
