﻿using CapSoft.Infrastructure.Domain;
using CapSoft.Infrastructure.Helpers;
using CapSoft.Infrastructure.UnitOfWork;
using iShopBest.Buyer.Domain.Model.DataContracts.CategoriesService;
using iShopBest.Buyer.Domain.Model.DataContracts.ProductsService;
using iShopBest.Buyer.Domain.Model.Entities;
using iShopBest.Buyer.Domain.Model.RepositoryContracts;
using iShopBest.Buyer.Domain.Model.ServiceContracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;

namespace iShopBest.Buyer.Domain.Services
{
    /// <summary>
    /// Service to access categories
    /// </summary>
    [Export(typeof(ICategoriesService))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class CategoriesService : ServiceBase, ICategoriesService
    {
        private ICategoryRepository _categoryRepository;
        private ICatalogCategoryRepository _catalogCategoryRepository;
        private ICatalogCategoryToProductRepository _categoryToProductsRepository;
        private IProductRepository _productsRepository;
        private IPriceRepository _pricesRepository;

        public CategoriesService() : this(
            ValueObjectBase.Container.GetExports<IUnitOfWork, IUnitOfWorkMetadata>(),
            ValueObjectBase.Container.GetExportedValue<ICategoryRepository>(),
            ValueObjectBase.Container.GetExportedValue<ICatalogCategoryRepository>(),
            ValueObjectBase.Container.GetExportedValue<ICatalogCategoryToProductRepository>(),
            ValueObjectBase.Container.GetExportedValue<IProductRepository>(),
            ValueObjectBase.Container.GetExportedValue<IPriceRepository>()
            )
        {
        }

        [ImportingConstructor]
        public CategoriesService(
            [ImportMany] IEnumerable<Lazy<IUnitOfWork,IUnitOfWorkMetadata>> uow,
            [Import] ICategoryRepository categoryRepository,
            [Import] ICatalogCategoryRepository catalogCategoryRepository,
            [Import] ICatalogCategoryToProductRepository categoryToProductsRepository,
            [Import] IProductRepository productsRepository,
            [Import] IPriceRepository pricesRepository
             ) : base(uow)
        {
            _categoryRepository = ArgumentValidator.GetValue<ICategoryRepository>(() => categoryRepository);
            _catalogCategoryRepository = ArgumentValidator.GetValue<ICatalogCategoryRepository>(() => catalogCategoryRepository);
            _categoryToProductsRepository = ArgumentValidator.GetValue<ICatalogCategoryToProductRepository>(() =>categoryToProductsRepository);
            _productsRepository = ArgumentValidator.GetValue<IProductRepository>(() => productsRepository);
            _pricesRepository = ArgumentValidator.GetValue<IPriceRepository>(() => pricesRepository);
        }

        #region IDisposable

        private bool _disposed;
        private object _threadSafety = new object();

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected override void Dispose(bool disposing)
        {

            lock (_threadSafety)
            {
                if (!_disposed)
                {
                    _disposed = true;
                    if (disposing)
                    {
                        // TODO: Dispose object state
                        base.Dispose(disposing);
                    }
                }

            }

            
        }
        #endregion


        public GetCategoriesResponse GetCategories(Int64? parentCategoryId)
        {
            GetCategoriesResponse response = new GetCategoriesResponse();
            IEnumerable<CategoryView> qry;

            if (parentCategoryId == null || parentCategoryId == 0)
            {
                qry = _categoryRepository.FindBy(x => x.Level0CategoryId < 10000000)
                    .GroupBy(x => new
                    {
                        ParentCategoryId = parentCategoryId,
                        CategoryId = x.Level0CategoryId.Value,
                        CategoryDescription = x.Level0CategoryDescription,
                        CategoryImage = x.Level0CategoryImage,
                        ProductCount = x.Level0ProdCount.Value
                    }).Select(x => new CategoryView()
                    {
                        ParentCategoryId = x.Key.ParentCategoryId,
                        CategoryId = x.Key.CategoryId,
                        CategoryDescription = x.Key.CategoryDescription,
                        CategoryImage = x.Key.CategoryImage,
                        ProductCount = x.Key.ProductCount
                    }).Distinct();
            }
            else if (parentCategoryId < 10000000)
            {
                qry = _categoryRepository.FindBy(x => x.Level0CategoryId == parentCategoryId)
                    .GroupBy(x => new
                    {
                        ParentCategoryId = parentCategoryId,
                        ParentCategoryDescription = x.Level0CategoryDescription,
                        CategoryId = x.Level1CategoryId.Value,
                        CategoryDescription = x.Level1CategoryDescription,
                        CategoryImage = x.Level1CategoryImage,
                        ProductCount = x.Level1ProdCount.Value
                    }).Select(x => new CategoryView()
                    {
                        ParentCategoryId = x.Key.ParentCategoryId,
                        CategoryId = x.Key.CategoryId,
                        CategoryDescription = x.Key.CategoryDescription,
                        CategoryImage = x.Key.CategoryImage,
                        ProductCount = x.Key.ProductCount,
                        Path = new List<CategoryView>()
                        {
                            new CategoryView() { CategoryId=x.Key.ParentCategoryId.Value, CategoryDescription=x.Key.ParentCategoryDescription }
                        }
                    });
            }
            else if (parentCategoryId % 1000000 == 0)
            {
                qry = _categoryRepository.FindBy(x => x.Level1CategoryId == parentCategoryId && x.Level2ProdCount>0)
                    .GroupBy(x => new
                    {
                        Level0CategoryId = x.Level0CategoryId,
                        Level0CategoryDescription = x.Level0CategoryDescription,
                        ParentCategoryId = parentCategoryId,
                        ParentCategoryDescription = x.Level1CategoryDescription,
                        CategoryId = x.Level2CategoryId.Value,
                        CategoryDescription = x.Level2CategoryDescription,
                        CategoryImage = x.Level2CategoryImage,
                        ProductCount = x.Level2ProdCount.Value
                    }).Select(x => new CategoryView()
                    {
                        ParentCategoryId = x.Key.ParentCategoryId,
                        CategoryId = x.Key.CategoryId,
                        CategoryDescription = x.Key.CategoryDescription,
                        CategoryImage = x.Key.CategoryImage,
                        ProductCount = x.Key.ProductCount,
                        Path = new List<CategoryView>()
                        {
                            new CategoryView() { CategoryId= x.Key.Level0CategoryId.Value, CategoryDescription=x.Key.Level0CategoryDescription },
                            new CategoryView() { CategoryId= x.Key.ParentCategoryId.Value, CategoryDescription=x.Key.ParentCategoryDescription }
                        }
                    }).Distinct();
            }
            else if (parentCategoryId % 10000 == 0)
            {
                qry = _categoryRepository.FindBy(x => x.Level2CategoryId == parentCategoryId && x.Level3ProdCount>0)
                    .GroupBy(x => new
                    {
                        Level0CategoryId = x.Level0CategoryId,
                        Level0CategoryDescription = x.Level0CategoryDescription,
                        Level1CategoryId = x.Level1CategoryId,
                        Level1CategoryDescription = x.Level1CategoryDescription,
                        ParentCategoryId = parentCategoryId,
                        ParentCategoryDescription = x.Level2CategoryDescription,
                        CategoryId = x.Level3CategoryId.Value,
                        CategoryDescription = x.Level3CategoryDescription,
                        CategoryImage = x.Level3CategoryImage,
                        ProductCount = x.Level3ProdCount.Value
                    }).Select(x => new CategoryView()
                    {
                        ParentCategoryId = x.Key.ParentCategoryId,
                        CategoryId = x.Key.CategoryId,
                        CategoryDescription = x.Key.CategoryDescription,
                        CategoryImage = x.Key.CategoryImage,
                        ProductCount = x.Key.ProductCount,
                        Path = new List<CategoryView>()
                        {
                            new CategoryView() { CategoryId= x.Key.Level0CategoryId.Value, CategoryDescription=x.Key.Level0CategoryDescription },
                            new CategoryView() { CategoryId= x.Key.Level1CategoryId.Value, CategoryDescription=x.Key.Level1CategoryDescription },
                            new CategoryView() { CategoryId= x.Key.ParentCategoryId.Value, CategoryDescription=x.Key.ParentCategoryDescription }
                        }
                    }).Distinct();
            }
            else if (parentCategoryId % 100 == 0)
            {
                qry = _categoryRepository.FindBy(x => x.Level3CategoryId == parentCategoryId && x.Level4ProdCount>0)
                    .GroupBy(x => new
                    {
                        Level0CategoryId = x.Level0CategoryId,
                        Level0CategoryDescription = x.Level0CategoryDescription,
                        Level1CategoryId = x.Level1CategoryId,
                        Level1CategoryDescription = x.Level1CategoryDescription,
                        Level2CategoryId = x.Level2CategoryId,
                        Level2CategoryDescription = x.Level2CategoryDescription,
                        ParentCategoryId = parentCategoryId,
                        ParentCategoryDescription = x.Level3CategoryDescription,
                        CategoryId = x.Level4CategoryId,
                        CategoryDescription = x.Level4CategoryDescription,
                        CategoryImage = x.Level4CategoryImage,
                        ProductCount = x.Level4ProdCount.Value
                    }).Select(x => new CategoryView()
                    {
                        ParentCategoryId = x.Key.ParentCategoryId,
                        CategoryId = x.Key.CategoryId ?? 0,
                        CategoryDescription = x.Key.CategoryDescription,
                        CategoryImage = x.Key.CategoryImage,
                        ProductCount = x.Key.ProductCount,
                        Path = new List<CategoryView>()
                        {
                            new CategoryView() { CategoryId= x.Key.Level0CategoryId.Value, CategoryDescription=x.Key.Level0CategoryDescription },
                            new CategoryView() { CategoryId= x.Key.Level1CategoryId.Value, CategoryDescription=x.Key.Level1CategoryDescription },
                            new CategoryView() { CategoryId= x.Key.Level2CategoryId.Value, CategoryDescription=x.Key.Level2CategoryDescription },
                            new CategoryView() { CategoryId= x.Key.ParentCategoryId.Value, CategoryDescription=x.Key.ParentCategoryDescription }
                        }
                    }).Distinct();

            }
            else
            {
                qry = _categoryRepository.FindBy(x => x.Level4CategoryId == parentCategoryId)
                    .GroupBy(x => new
                    {
                        Level0CategoryId = x.Level0CategoryId,
                        Level0CategoryDescription = x.Level0CategoryDescription,
                        Level1CategoryId = x.Level1CategoryId,
                        Level1CategoryDescription = x.Level1CategoryDescription,
                        Level2CategoryId = x.Level2CategoryId,
                        Level2CategoryDescription = x.Level2CategoryDescription,
                        Level3CategoryId = x.Level3CategoryId,
                        Level3CategoryDescription = x.Level3CategoryDescription,
                        ParentCategoryId = x.Level4CategoryId,
                        ParentCategoryDescription = x.Level4CategoryDescription,
                        CategoryId= x.Level4CategoryId,
                        CategoryImage = x.Level4CategoryImage,
                        CategoryDescription = x.Level4CategoryDescription,
                        ProductCount = x.Level4ProdCount.Value
                    }).Select(x => new CategoryView()
                    {
                        ParentCategoryId = x.Key.ParentCategoryId,
                        CategoryId = x.Key.CategoryId ?? 0,
                        CategoryDescription = x.Key.CategoryDescription,
                        CategoryImage = x.Key.CategoryImage,
                        ProductCount = x.Key.ProductCount,
                        Path = new List<CategoryView>()
                        {
                            new CategoryView() { CategoryId= x.Key.Level0CategoryId.Value, CategoryDescription=x.Key.Level0CategoryDescription },
                            new CategoryView() { CategoryId= x.Key.Level1CategoryId.Value, CategoryDescription=x.Key.Level1CategoryDescription },
                            new CategoryView() { CategoryId= x.Key.Level2CategoryId.Value, CategoryDescription=x.Key.Level2CategoryDescription },
                            new CategoryView() { CategoryId= x.Key.Level3CategoryId.Value, CategoryDescription=x.Key.Level3CategoryDescription },
                            new CategoryView() { CategoryId= x.Key.ParentCategoryId ?? 0, CategoryDescription=x.Key.ParentCategoryDescription }
                        }
                    }).Distinct();

            }

            response.Categories = qry.ToList();
            response.Path = null;

            if (response.Categories != null && response.Categories.Count>0 && response.Categories.ElementAt(0).Path!=null && response.Categories.ElementAt(0).Path.Count > 0)
            {
                response.Path = new List<CategoryView>();
                foreach (var elem in response.Categories.ElementAt(0).Path)
                {
                    response.Path.Add(new CategoryView() { CategoryId=elem.CategoryId, CategoryDescription=elem.CategoryDescription });
                }
                foreach(var elem in response.Categories)
                {
                    elem.Path.Clear();
                    elem.Path = null;
                }

                if (response.Categories.ElementAt(0).CategoryId == parentCategoryId)
                {
                    response.Categories.Clear();
                    response.Categories = null;
                }
            }


            return response;
        }

        /// <summary>
        /// thes are shortcuts for Ink4Inky
        /// </summary>
        /// <param name="parentCategoryId"></param>
        /// <returns></returns>
        public List<CatalogCategory> GetCatalogCategories(Int64? parentCategoryId)
        {
            long rootCategoryId = parentCategoryId ?? 0;
            List<CatalogCategory> catalogCategories = _catalogCategoryRepository.FindBy(x => x.ParentCategoryId == rootCategoryId && x.Enabled).OrderBy(x=>x.Name).ToList();

            return catalogCategories;
        }

        public GetProductsByCategoryResponse GetProductsByCategory(GetProductsByCategoryRequest request)
        {
            GetProductsByCategoryResponse response = new GetProductsByCategoryResponse();

            var categoryToProducts = _categoryToProductsRepository.GetTable();
            var products = _productsRepository.GetTable();
            var prices = _pricesRepository.GetTable();

            response.Products = (from c2p in categoryToProducts
                                 from pd in products.Where(x => x.Id == c2p.ProductId)
                                 from pr in prices.Where(x => x.GTINPLUS == pd.Id)
                                 where c2p.CategoryId == request.CategoryId
                                 group pr by new {pd}  into g
                                 select new ProductAggregate() { Product=g.Key.pd, Prices= g.Select(x=>x).OrderBy(x=>x.Cost).ToList() }
                                 ).OrderBy(x=>x.Product.PriceMin).ToList();

            return response;
        }

    }
}
