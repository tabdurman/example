﻿using iShopBest.Buyer.Domain.Model.ServiceContracts;
using System;
using System.Linq;
using iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService;
using System.ComponentModel.Composition;
using iShopBest.Buyer.Domain.Model.RepositoryContracts;
using CapSoft.Infrastructure.Domain;
using CapSoft.Infrastructure.UnitOfWork;
using System.Collections.Generic;
using CapSoft.Infrastructure.Helpers;
using iShopBest.Buyer.Domain.Model.Entities;
using System.Diagnostics;
using CapSoft.Infrastructure.DataStructures;

namespace iShopBest.Buyer.Domain.Services
{
    /// <summary>
    /// Service to do vendorselection tasks
    /// </summary>
    [Export(typeof(IVendorSelectionService))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class VendorSelectionService : ServiceBase, IVendorSelectionService
    {
        private IProductRepository _productRepository;
        private IPriceRepository _priceRepository;
        private IInventoryRepository _inventoryRepository;
        private IZipCodeRepository _zipCodeRepository;

        public VendorSelectionService() : this(
            ValueObjectBase.Container.GetExports<IUnitOfWork, IUnitOfWorkMetadata>(),
            ValueObjectBase.Container.GetExportedValue<IProductRepository>(),
            ValueObjectBase.Container.GetExportedValue<IPriceRepository>(),
            ValueObjectBase.Container.GetExportedValue<IInventoryRepository>(),
            ValueObjectBase.Container.GetExportedValue<IZipCodeRepository>()
            )
        {

        }

        [ImportingConstructor]
        public VendorSelectionService(
            [ImportMany] IEnumerable<Lazy<IUnitOfWork, IUnitOfWorkMetadata>> uow,
            [Import] IProductRepository productRepository,
            [Import] IPriceRepository priceRepository,
            [Import] IInventoryRepository inventoryRepository,
            [Import] IZipCodeRepository zipCodeRepository
            ) : base(uow)
        {
            _productRepository = ArgumentValidator.GetValue<IProductRepository>(() => productRepository);
            _priceRepository = ArgumentValidator.GetValue<IPriceRepository>(() => priceRepository);
            _inventoryRepository = ArgumentValidator.GetValue<IInventoryRepository>(() => inventoryRepository);
            _zipCodeRepository = ArgumentValidator.GetValue<IZipCodeRepository>(() => zipCodeRepository);
        }

        #region IDisposable
        private bool _disposed;
        private object _threadSafety = new object();

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected override void Dispose(bool disposing)
        {
            lock (_threadSafety)
            {
                if (!_disposed)
                {
                    _disposed = true;
                    if (disposing)
                    {
                        base.Dispose(disposing);
                    }
                }

            }

        }
        #endregion
        public RunVendorSelectionResponse RunVendorSelection(RunVendorSelectionRequest request)
        {
#if DEBUG
            Debug.WriteLine("Starting RunVendorSelection ...");
#endif
            int startTicks = Environment.TickCount & Int32.MaxValue;

            RunVendorSelectionResponse response = new RunVendorSelectionResponse();
            GetMatchesResponse mResponse = GetMatches(new GetMatchesRequest() { ShoppingCart = request.ShoppingCart, IncludeInventory=request.IncludeInventory });


            GenerateRoutesResponse rResponse = GenerateRoutes(new GenerateRoutesRequest() {
                MatchResults = mResponse,
                IncludeInventory= request.IncludeInventory,
                Preferences=request.Preferences,
                ShoppingCart=request.ShoppingCart
            });

            if (rResponse.RoutesPerGtin != null)
            {
                #region Generating and scoring possible orders
                Node<RouteIndex> combinationsTree = new Node<RouteIndex>(new RouteIndex() { WeightedCost = 0M, RouteAxisI = -1, RouteAxisJ = -1 });

                //Build tree graph
                for (int i = 0; i < rResponse.RoutesPerGtin.Count; i++)
                {
                    foreach (var combinations in combinationsTree.GetNodesAtLevel(i))
                    {
                        for (int j = 0; j < rResponse.RoutesPerGtin[i].Routes.Count; j++)
                        {
                            combinations.Add(new RouteIndex() { WeightedCost = 0M, RouteAxisI = i, RouteAxisJ = j });
                        }
                    }
                }

                response.Orders = new List<Order>();
                //walk thru each graph and generate possible orders
                foreach (var combination in combinationsTree.GetNodesAtLevel(rResponse.RoutesPerGtin.Count))
                {
                    Order order = new Order() { OrderLines = new List<OrderLine>() };
                    Node<RouteIndex> walker = combination;
                    //order.Preferences = rResponse.RoutesPerGtin[walker.Value.RouteAxisI].Routes[walker.Value.RouteAxisJ].Path[0].Preferences;
                    while (!walker.IsRoot)
                    {
                        foreach (RouteItem item in rResponse.RoutesPerGtin[walker.Value.RouteAxisI].Routes[walker.Value.RouteAxisJ].Path)
                        {
                            var tmp1 = request.Preferences.Where(x => x.Name.Contains("SupplierRating")).FirstOrDefault();
                            decimal SupplierRating = 0M;
                            decimal WarehouseRating = 0M;
                            if (tmp1 != null)
                                SupplierRating = decimal.Parse(tmp1.Value);
                            tmp1 = request.Preferences.Where(x => x.Name.Contains("WarehouseRating")).FirstOrDefault();
                            if (tmp1 != null)
                                WarehouseRating = decimal.Parse(tmp1.Value);

                            order.OrderLines.Add(
                                new OrderLine()
                                {
                                    GTINPLUS = item.Requirement.GtinPlus,
                                    SupplierId = item.MatchPrice.SupplierId,
                                    SupplierName = item.MatchPrice.SupplierName,
                                    SupplierRating = item.MatchPrice.SupplierRating ?? 0,
                                    WarehouseId = item.MatchInventory.WarehouseId,
                                    WarehouseName = item.MatchInventory.WarehouseName,
                                    WarehouseRating = item.MatchInventory.WarehouseRating,
                                    Sku = item.MatchPrice.Sku,
                                    Quantity = item.OrderedQuantity,
                                    RequestedQuantity = item.Requirement.Quantity,
                                    Price = item.MatchPrice.Cost,
                                    Coo = item.MatchPrice.CountryOfOrigin,
                                    Distance = item.Distance,
                                    EstimatedFreight = item.FreigthCost,
                                    Weighter = (1 +
                                    //(1 - ProductRating / 5M) * decimal.Parse(prefs.Where(x => x.Name.Contains("ProductRating")).FirstOrDefault().Value) +
                                    (1 - (item.MatchPrice.SupplierRating ?? 0) / 5M) * SupplierRating / 100M +
                                    (1 - (item.MatchInventory.WarehouseRating ?? 0) / 5M) * WarehouseRating / 100M
                                    )
                                });
                        }
                        //step up
                        walker = walker.Parent;
                    }
                    order.Preferences = request.Preferences;
                    response.Orders.Add(order);
                }

                if (response.Orders != null)
                {
                    var maxVendors = response.Orders.Max(x => x.OrderingVendors.Count);
                    var maxFreightCost = response.Orders.Max(x => x.TotalEstimatedFreight);
                    var maxDeliveryDays = response.Orders.Max(x => x.MaxDeliveryDays);



                    response.Orders.ToList().ForEach((elem) =>
                    {

                        decimal coof = 0M;
                        elem.OrderLines.ForEach((elem2) =>
                        {
                            coof += elem2.WeightedCost * (1 +
                            (decimal)(elem2.DeliveryDays / maxDeliveryDays) * decimal.Parse(request.Preferences.Where(x => x.Name.Contains("DeliverySpeed")).FirstOrDefault().Value) / 100M +
                            (elem2.EstimatedFreight / maxFreightCost) * decimal.Parse(request.Preferences.Where(x => x.Name.Contains("FreightCost")).FirstOrDefault().Value) / 100M);

                        });
#if DEBUG
                        Debug.WriteLine("coof set to {0}.", coof);
#endif

                        elem.WeightedCostTotal = coof *
                            (1 + elem.OrderingVendors.Count / maxVendors *
                             (decimal.Parse(request.Preferences.Where(x => x.Name.Contains("SingleSupplier")).FirstOrDefault().Value) / 100M));
#if DEBUG
                        Debug.WriteLine("WeightedCostTotal set to {0}.", elem.WeightedCostTotal);
#endif
                    });

                    // get max min to calculate scores
                    var maxCost = response.Orders.Max(x => x.WeightedCostTotal);
                    var minCost = response.Orders.Min(x => x.WeightedCostTotal);

                    //calsulate scores
                    if (maxCost > minCost)
                    {
                        response.Orders.ToList().ForEach((elem) =>
                        {
                            elem.Score = ((decimal)((maxCost - elem.WeightedCostTotal) / (maxCost - minCost) * 100));
                        });
                    }
                    else
                    {
                        response.Orders[0].Score = 100;
                    }
                    #endregion
                }

                response.Orders = response.Orders.OrderByDescending(x => x.Score).ToList();

                response.ExecutionSpeedTicks = (Environment.TickCount & Int32.MaxValue) - startTicks;
#if DEBUG
                Debug.WriteLine("Finished RunVendorSelection ... time {0} ticks.", response.ExecutionSpeedTicks);
#endif
#if DEBUG
                foreach (var order in response.Orders.OrderByDescending(x => x.Score))
                {
                    Debug.WriteLine("Order score: {0}, OrderTotal: {1}, WeightedCostTotal {2}", order.Score, order.OrderTotal, order.WeightedCostTotal);

                    foreach (var line in order.OrderLines)
                    {
                        Debug.WriteLine("--- Line GTINPLUS: {0}, Vendorid: {1}, InventoryLocationId: {2}, DeliveryDays: {3}, Price: {4}, Quantity: {5}/{6}, EstimatedFreight: {7}, WeightedCost: {8}", line.GTINPLUS, line.SupplierId, line.WarehouseId, line.DeliveryDays, line.Price, line.Quantity, line.RequestedQuantity, line.EstimatedFreight, line.WeightedCost);
                    }
                }
#endif
            }

            if (mResponse != null)
            {
                response.Matches = mResponse.Matches;
                response.Inventories = mResponse.Inventories;
                response.Products = mResponse.Products;
            }

            return response;
        }

        /// <summary>
        /// Will load matches for each line item
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetMatchesResponse GetMatches(GetMatchesRequest request)
        {
#if DEBUG
            Debug.WriteLine("Starting GetMatches ...");
#endif
            int startTicks = Environment.TickCount & Int32.MaxValue;
            GetMatchesResponse response = new GetMatchesResponse();

            List<Int64> GtinList = (from l in request.ShoppingCart
                                   select l.GtinPlus).ToList();

            List<int> DCodesList = (from l in request.ShoppingCart
                                       select int.Parse(l.DestZipCode)).ToList();

            response.Products = _productRepository.FindBy(x => GtinList.Contains(x.Id)).ToList();
            response.Matches = _priceRepository.FindBy(x => GtinList.Contains(x.GTINPLUS)).ToList();

            var zipCodes = _zipCodeRepository.FindBy(x => DCodesList.Contains(x.ZipCodeInt.Value)).ToList();

            foreach(var elem in request.ShoppingCart)
            {
                var val=(from l in zipCodes where l.ZipCode1==elem.DestZipCode select new { l.Longitude, l.Latitude }).FirstOrDefault();
                elem.DestLatitude = val.Latitude;
                elem.DestLongitude = val.Longitude;
            }

            if (request.IncludeInventory)
                response.Inventories = _inventoryRepository.FindBy(x => GtinList.Contains(x.GTINPLUS)).ToList();
            else
                response.Inventories = new List<Inventory>();

            response.ExecutionSpeedTicks = (Environment.TickCount & Int32.MaxValue) - startTicks;
#if DEBUG
            Debug.WriteLine("Finished GetMatches ... time {0} ticks.", response.ExecutionSpeedTicks);
#endif

            return response;
        }

        /// <summary>
        /// Will generate ordering routes for each line item within order
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GenerateRoutesResponse GenerateRoutes(GenerateRoutesRequest request)
        {
#if DEBUG
            Debug.WriteLine("Starting GenerateRoutes ...");
#endif
            int startTicks = Environment.TickCount & Int32.MaxValue;
            GenerateRoutesResponse response = new GenerateRoutesResponse();
            response.RoutesPerGtin = new List<RoutesList>();

            #region Loop each line item in shopping cart
            foreach (ItemRequirement lineItem in request.ShoppingCart.OrderBy(x=>x.GtinPlus))
            {
#if DEBUG
                Debug.WriteLine("*CHECKING: GtinPlus: {0}, OrderedQuantity: {1}.", lineItem.GtinPlus, lineItem.Quantity);
#endif
                RoutesList routesList = new RoutesList()
                {
                    GtinPlus = lineItem.GtinPlus,
                    Routes = new List<Route>(),
                    BestRouteSequenceNumber = 1
                };

                if (request.IncludeInventory && request.MatchResults.Inventories!=null && request.MatchResults.Inventories.Count>0) // include inventory
                {
                    var singleWarehouseFillUps = (
                        from m in request.MatchResults.Matches
                        from i in request.MatchResults.Inventories.Where(x=>x.GTINPLUS==m.GTINPLUS && x.SupplierId==m.SupplierId).DefaultIfEmpty()
                        select new RouteItem() { MatchInventory=i, MatchPrice=m, OrderedQuantity=lineItem.Quantity, Requirement=lineItem }
                        );

                    #region Loop all matched vendors
                    foreach (var supplierMatch in request.MatchResults.Matches.Where(x=>x.GTINPLUS==lineItem.GtinPlus).Select(x=>new { SupplierId=x.SupplierId, SupplierName=x.SupplierName, Sku=x.Sku }).Distinct().ToList())
                    {

                        Route route = new Route()
                        {
                            SequenceNumber = 1,
                            Path = new List<RouteItem>()
                        };
#if DEBUG
                        Debug.WriteLine("**SUPPLIER MATCH: SupplierId: {0}, SupplierName: {1}, Sku: {2}.", supplierMatch.SupplierId, supplierMatch.SupplierName,supplierMatch.Sku);
#endif

                        int sequenceNumber = 1;
                        // add all what can be delivered from closest single warehouse
                        foreach (RouteItem routeItem in singleWarehouseFillUps
                            .Where(x => x.MatchPrice.GTINPLUS == lineItem.GtinPlus 
                            && x.MatchPrice.SupplierId==supplierMatch.SupplierId 
                            && (x.MatchInventory.Available ?? 0) >= lineItem.Quantity)
                            .OrderBy(x => x.Distance)
                            .Take(1).ToList())
                        {
#if DEBUG
                            Debug.WriteLine("***WHOLE ORDER FROM SINGLE WAREHOUSE ROUTE ITEM: WarehouseId: {0}, WarehouseName: {1}, Distance: {2}, OrderedQuantity: {3}.", routeItem.MatchInventory.WarehouseId, routeItem.MatchInventory.WarehouseName , routeItem.Distance, lineItem.Quantity );
#endif
                            routeItem.OrderedQuantity = lineItem.Quantity;
                            routeItem.SequenceNumber = sequenceNumber++;
                            route.Path.Add(routeItem);

#if DEBUG
                            Debug.WriteLine("**ROUTE: Paths: {0}, Ordered Quantity: {1}, Cost: {2}, AvailableQuantity: {3}.", route.Path.Count, route.OrderedQuantity, route.RouteCost, route.SequenceNumber);
#endif

                            routesList.Routes.Add(route);

                            route = new Route()
                            {
                                SequenceNumber = 1,
                                Path = new List<RouteItem>()
                            };

                        }

                        int totalAllocatedQty = 0, totalInStock=0;
                        bool hasStockInfo = false;

                        var tmp = singleWarehouseFillUps
                            .Where(x => x.MatchPrice.GTINPLUS == lineItem.GtinPlus
                            && x.MatchPrice.SupplierId == supplierMatch.SupplierId
                            && (x.MatchInventory.Available ?? 0) > 0)
                            .OrderBy(x => x.Distance).ToList();

                        #region Fill partially from warehouses of that vendor
                        // fill from closest warehouses until requested quantity filled up
                        foreach (RouteItem routeItem in tmp)
                        {
                            routeItem.MatchInventory.Available=routeItem.MatchInventory.Available ?? 0;
                            totalInStock = routeItem.MatchInventory.TotalAvailable ?? 0;
                            if (routeItem.MatchInventory.WarehouseId != null)
                                hasStockInfo = true;

                            if (totalAllocatedQty+routeItem.MatchInventory.Available>=lineItem.Quantity && totalAllocatedQty==0) // first item and complete fill - already there, skip
                            {
#if DEBUG
                                Debug.WriteLine("***SKIPPED - COZ ALREADY ADDED - WHOLE ORDER FROM SINGLE WAREHOUSE ROUTE ITEM: WarehouseId: {0}, WarehouseName: {1}, Distance: {2}, OrderedQuantity: {3}.", routeItem.MatchInventory.WarehouseId, routeItem.MatchInventory.WarehouseName, routeItem.Distance, lineItem.Quantity);
#endif
                                totalAllocatedQty = lineItem.Quantity;
                            }
                            else if (totalAllocatedQty + routeItem.MatchInventory.Available >= lineItem.Quantity && totalAllocatedQty > 0) // not first
                            {
                                routeItem.OrderedQuantity = lineItem.Quantity - totalAllocatedQty;
                                routeItem.SequenceNumber = sequenceNumber++;
#if DEBUG
                                Debug.WriteLine("***PARTIAL ORDER FROM WAREHOUSE ROUTE ITEM: WarehouseId: {0}, WarehouseName: {1}, Distance: {2}, OrderedQuantity: {3}.", routeItem.MatchInventory.WarehouseId, routeItem.MatchInventory.WarehouseName, routeItem.Distance, routeItem.OrderedQuantity);
#endif
                                totalAllocatedQty += routeItem.OrderedQuantity;
                                route.Path.Add(routeItem);
                            }
                            else if (totalAllocatedQty + routeItem.MatchInventory.Available < lineItem.Quantity && routeItem.MatchInventory.Available.Value>0) // get all stock from next warehouse, if available qty>0
                            {

                                routeItem.SequenceNumber = sequenceNumber++;
                                routeItem.OrderedQuantity = routeItem.MatchInventory.Available.Value;
#if DEBUG
                                Debug.WriteLine("***PARTIAL ORDER FROM ADDITIONAL WAREHOUSE ROUTE ITEM: WarehouseId: {0}, WarehouseName: {1}, Distance: {2}, OrderedQuantity: {3}.", routeItem.MatchInventory.WarehouseId, routeItem.MatchInventory.WarehouseName, routeItem.Distance, routeItem.OrderedQuantity);
#endif
                                totalAllocatedQty += routeItem.OrderedQuantity;
                                route.Path.Add(routeItem);
                            }
                            else
                            {

#if DEBUG
                                Debug.WriteLine("***SKIPPING - ROUTE ITEM: WarehouseId: {0}, WarehouseName: {1}, Distance: {2}, AvailableQuantity: {3}.", routeItem.MatchInventory.WarehouseId, routeItem.MatchInventory.WarehouseName, routeItem.Distance, routeItem.MatchInventory.Available);
#endif

                            }

                            //exit if completely ordered
                            if (totalAllocatedQty >= lineItem.Quantity)
                                break;
                        }
                        #endregion

                        if (totalAllocatedQty< lineItem.Quantity)
                        {
                            /// not compete qiuantity will be added as backordered with warehouseid=0
                            var routeItem = request.MatchResults.Matches
                                .Where(x => x.GTINPLUS == lineItem.GtinPlus && x.SupplierId == supplierMatch.SupplierId)
                                .Select(x => new RouteItem()
                                {
                                    MatchPrice = x,
                                    MatchInventory = new Inventory()
                                    {
                                        GTINPLUS =lineItem.GtinPlus,
                                        SupplierId =supplierMatch.SupplierId,
                                        Sku =supplierMatch.Sku,
                                        WarehouseName ="Backorder",
                                        WarehouseId=0,
                                        WarehouseRating=0,
                                        SrcLatitude = lineItem.DestLatitude>0 ?  -90M : 90M, // north pole for south people, south pole for north people
                                        SrcLongitude = 0M
                                    },
                                    OrderedQuantity = lineItem.Quantity- totalAllocatedQty,
                                    Requirement = lineItem,
                                    SequenceNumber = sequenceNumber
                                })
                                .FirstOrDefault();
                            if (routeItem != null)
                            {
#if DEBUG
                                Debug.WriteLine("***ROUTE ITEM: WarehouseId: {0}, WarehouseName: {1}, Distance: {2}, Ordered Quantity: {3}.", routeItem.MatchInventory.WarehouseId, routeItem.MatchInventory.WarehouseName, routeItem.Distance, routeItem.OrderedQuantity);
#endif
                                route.Path.Add(routeItem);
                            }

                        }


                        route.NoStockAvailable = !hasStockInfo;
                        if (totalInStock>0 && totalAllocatedQty==0)
                        {
#if DEBUG
                            Debug.WriteLine("***ALL WAREHOUSES OUT OF STOCK BUT TOTAL ON HAND>0");
#endif
                        }
                        if (route.Path.Count > 0)
                        {
#if DEBUG
                            Debug.WriteLine("**ROUTE: Paths: {0}, Ordered Quantity: {1}, Cost: {2}, AvailableQuantity: {3}.", route.Path.Count, route.OrderedQuantity, route.RouteCost, route.SequenceNumber);
#endif
                            routesList.Routes.Add(route);
                        }
                    }
                    #endregion
                }
                else // do not include inventory
                {
                    #region simply add matches ordered by cost
                    foreach(Price match in request.MatchResults.Matches.Where(x=>x.GTINPLUS==lineItem.GtinPlus).OrderBy(x=>x.Cost))
                    {
#if DEBUG
                        Debug.WriteLine("NO INVENTORY REQUIRED MATCH: GTINPLUS: {0}, VendorId: {1}, OrderedQuantity: {2}.", lineItem.GtinPlus, match.SupplierName, lineItem.Quantity);
#endif
                        routesList.Routes.Add(
                            new Route()
                            {
                                SequenceNumber = 1,
                                Path = new List<RouteItem>()
                                {
                                    new RouteItem()
                                    {
                                        Requirement = lineItem,
                                        MatchInventory = new Inventory()
                                        {
                                            GTINPLUS =lineItem.GtinPlus,
                                            SupplierId =match.SupplierId,
                                            Sku =match.Sku,
                                            WarehouseName ="N/A",
                                            WarehouseId =-1,
                                            WarehouseRating =0,
                                            SrcLatitude=lineItem.DestLatitude,
                                            SrcLongitude=lineItem.DestLongitude
                                        },
                                        MatchPrice=match,
                                        OrderedQuantity=lineItem.Quantity,
                                        SequenceNumber=1
                                    }
                                }
                            });

                    }

                    #endregion
                }

                response.RoutesPerGtin.Add(routesList);
            }
            #endregion

            response.ExecutionSpeedTicks = (Environment.TickCount & Int32.MaxValue) - startTicks;
#if DEBUG
            Debug.WriteLine("Finished GenerateRoutes ... time {0} ticks.", response.ExecutionSpeedTicks);
#endif
            return response;
        }

    }
}
