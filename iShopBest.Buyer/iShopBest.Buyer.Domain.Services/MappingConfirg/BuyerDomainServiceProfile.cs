﻿using AutoMapper;
using iShopBest.Buyer.Domain.Model.DataContracts.CategoriesService;
using iShopBest.Buyer.Domain.Model.DataContracts.ProductsService;
using iShopBest.Buyer.Domain.Model.Entities;

namespace iShopBest.Buyer.Domain.Services.MappingConfirg
{
    public class BuyerDomainServiceProfile : Profile
    {
        public BuyerDomainServiceProfile()
        {
            CreateMap<Category, CategoryView>();
            CreateMap<Product, ProductView>();
        }
    }
}
