﻿using CapSoft.Identity.Domain.Model;
using CapSoft.Identity.Domain.Model.Entities;
using CapSoft.Infrastructure.Domain;
using CapSoft.Infrastructure.Helpers;
using CapSoft.Infrastructure.UnitOfWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Security.Principal;

namespace iShopBest.Buyer.Domain.Services
{
    public abstract class ServiceBase : IDisposable
    {
        protected IUnitOfWork _uow;

        private IPrincipal _principal;

        public IPrincipal Principal
        {
            get
            {
                return _principal;
            }
            set
            {
                _principal = value;
            }
        }

        public User OnBehalfOfUser
        {
            get
            {
                return _onBehalfOfUser;
            }

            set
            {
                _onBehalfOfUser = value;
            }
        }

        private User _onBehalfOfUser;


        /*public ServiceBase()
            : this(ValueObjectBase.Container.GetExports<IUnitOfWork, IUnitOfWorkMetadata>())
        { }
        */

        public ServiceBase()
        : this(ValueObjectBase.Container.GetExports<IUnitOfWork, IUnitOfWorkMetadata>())
        { }

        [ImportingConstructor]
        public ServiceBase([ImportMany] IEnumerable<Lazy<IUnitOfWork, IUnitOfWorkMetadata>> uow)
        {
            var tUow = ArgumentValidator.GetValue<IEnumerable<Lazy<IUnitOfWork, IUnitOfWorkMetadata>>>(() => uow);
            _uow = uow.Where(x => x.Metadata.UowType == "BuyerCatalog").FirstOrDefault().Value;
        }

        #region Shared
        protected void WrappedExecute(Action<IdentityClaimsPrincipal, User> actionToExecute)
        {
            if (this.Principal == default(IPrincipal))
                throw new ApplicationException("Unauthorized");

            IdentityClaimsPrincipal resolvedPrincipal = this.Principal as IdentityClaimsPrincipal;

            if (resolvedPrincipal == default(IdentityClaimsPrincipal))
                throw new ApplicationException("Unauthorized");

            // if impersonated, main principal must be administrator
            if (OnBehalfOfUser.Id == default(Int64) || (resolvedPrincipal.UserId != OnBehalfOfUser.Id && !resolvedPrincipal.IsInRole("Administrators")))
                throw new ApplicationException("Unauthorized");

            actionToExecute.Invoke(resolvedPrincipal, OnBehalfOfUser);
        }
        #endregion

        #region IDisposable
        /* IDisposable */
        private bool _disposed;
        private object _threadSafety = new object();
        public bool Disposed { get { return _disposed; } }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            lock (_threadSafety)
            {
                if (!_disposed)
                {
                    _disposed = true;
                    if (disposing)
                    {
                        // TODO: Dispose object state
                    }
                }

            }
        }
        /* IDisposable */
        #endregion

    }
}

