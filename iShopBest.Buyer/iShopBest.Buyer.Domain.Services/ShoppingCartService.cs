﻿using iShopBest.Buyer.Domain.Model.ServiceContracts;
using System;
using iShopBest.Buyer.Domain.Model.DataContracts.ShoppingCartService;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using CapSoft.Infrastructure.UnitOfWork;
using iShopBest.Buyer.Domain.Model.RepositoryContracts;
using CapSoft.Infrastructure.Helpers;
using CapSoft.Infrastructure.Domain;
using System.Linq;
using iShopBest.Buyer.Domain.Model.Entities;
using CapSoft.Infrastructure.Extensions;
using System.Linq.Expressions;

namespace iShopBest.Buyer.Domain.Services
{
    /// <summary>
    /// Service to deal with shopping cart
    /// </summary>
    [Export(typeof(IShoppingCartService))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class ShoppingCartService : ServiceBase, IShoppingCartService
    {
        IShoppingCartRepository _shoppingCartRepository;
        IShoppingCartItemRepository _shoppingCartItemRepository;

        [ImportingConstructor]
        public ShoppingCartService(
            [ImportMany] IEnumerable<Lazy<IUnitOfWork, IUnitOfWorkMetadata>> uow,
            [Import] IShoppingCartRepository shoppingCartRepository,
            [Import] IShoppingCartItemRepository shoppingCartItemRepository
            ) : base(uow)
        {
            _shoppingCartRepository = ArgumentValidator.GetValue<IShoppingCartRepository>(() => shoppingCartRepository);
            _shoppingCartItemRepository = ArgumentValidator.GetValue<IShoppingCartItemRepository>(() => shoppingCartItemRepository);
        }

        public ShoppingCartService() : this(
            ValueObjectBase.Container.GetExports<IUnitOfWork, IUnitOfWorkMetadata>(),
            ValueObjectBase.Container.GetExportedValue<IShoppingCartRepository>(),
            ValueObjectBase.Container.GetExportedValue<IShoppingCartItemRepository>()
            )
        {
        }

        #region IDisposable
        private bool _disposed;
        private object _threadSafety = new object();

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected override void Dispose(bool disposing)
        {
            lock (_threadSafety)
            {
                if (!_disposed)
                {
                    _disposed = true;
                    if (disposing)
                    {
                        base.Dispose(disposing);
                    }
                }

            }

        }
        #endregion

        public GetCartsListResponse GetCartsList(GetCartsListRequest request)
        {
            GetCartsListResponse response = new GetCartsListResponse();
            response.ShoppingCarts = _shoppingCartRepository.FindBy(x => x.OwnerUserId == request.ForUserId).OrderBy(x => x.CartName).ToList();
            return response;
        }

        public LoadCartResponse LoadCart(LoadCartRequest request)
        {
            LoadCartResponse resp = new LoadCartResponse();
            if (request.CartId == 0)
            {
                resp.Cart = _shoppingCartRepository.FindBy(x => x.CartName == "Temp Cart" && x.OwnerUserId==request.ForUserId).FirstOrDefault();
                if (resp.Cart == null)
                {
                    ShoppingCart cart = new ShoppingCart() { CartName = "Temp Cart", OwnerUserId = request.ForUserId, DateSaved = DateTime.Now };
                    _shoppingCartRepository.Add(cart);
                    _uow.CommitAndFlush();
                    resp.Cart = _shoppingCartRepository.FindBy(x => x.CartName == "Temp Cart" && x.OwnerUserId == request.ForUserId).FirstOrDefault();
                }

            }
            else
                resp.Cart = _shoppingCartRepository.FindBy(x => x.Id == request.CartId).FirstOrDefault();

            if (resp.Cart!=null)
                resp.ShoppingCartItems = _shoppingCartItemRepository.FindBy(x => x.ShoppingCartId == resp.Cart.Id).ToList();
            return resp;
        }

        public SaveCartResponse SaveCart(SaveCartRequest request)
        {

            if (request.ShoppingCartItems==null || request.ShoppingCartItems.Count == 0)
                throw new ApplicationException("There are no shopping cart items attached to cart.");

            SaveCartResponse response = new SaveCartResponse();
            var cart = _shoppingCartRepository.FindBy(x => x.OwnerUserId == request.UserId && x.CartName.ToUpper() == request.CartName.ToUpper()).FirstOrDefault();
            if (cart == null)
            {
                cart = new ShoppingCart() { CartName=request.CartName, OwnerUserId=request.UserId, DateSaved=DateTime.Now };
                _shoppingCartRepository.Add(cart);
                _uow.CommitAndFlush();
            }
            else if (!request.OverrideExisting)
            {
                response.Cart = cart;
                throw new ApplicationException("Cart with same name already exists. Set override existing check box to verride it.");
                //return response;
            }
            else
            {
                // remove all lines under shopping cart
                var existingItems = _shoppingCartItemRepository.FindBy(x => x.ShoppingCartId == cart.Id).ToList();
                if (existingItems != null && existingItems.Count>0)
                {
                    //_uow.CommitAndFlush();
                    foreach (var elem in existingItems)
                        _shoppingCartItemRepository.Remove(elem);
                    _uow.CommitAndFlush();
                }

            }

            foreach(var elem in request.ShoppingCartItems)
            {
                elem.ShoppingCartId = cart.Id;
                _shoppingCartItemRepository.Add(elem);
            }
            _uow.CommitAndFlush();

            response.Cart = cart;
            return response;

        }
    }
}
