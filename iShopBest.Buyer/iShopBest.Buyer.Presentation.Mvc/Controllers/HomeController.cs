﻿using CapSoft.Identity.Domain.Model;
using CapSoft.Identity.Domain.Model.Entities;
using CapSoft.Identity.Presentation.Mvc;
using CapSoft.Identity.Presentation.Mvc.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace iShopBest.Buyer.Presentation.Mvc.Controllers
{
    public class HomeController : BaseController
    {

        private async Task SendEmailConfirmationTokenAsync(long userId,string emailSubject)
        {
            // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
            // Send an email with this link
            string token = await UserManager.GenerateEmailConfirmationTokenAsync(userId);
            var callbackUrl = Url.Action("ConfirmEmail", "", new { userId = userId, token = token }, protocol: Request.Url.Scheme);
            await UserManager.SendEmailAsync(userId, emailSubject, "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

        }

        public ActionResult Index()
        {
            return View();
        }

        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl = "/")
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToLocal(returnUrl);
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }


        //[AuthorizeWithClaims("WebSiteUser", "Yes")]
        [AllowAnonymous]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                model.Email = model.Email.Trim();
                model.Password = model.Password.Trim();

                var user = await UserManager.FindAsync(model.Email, model.Password);
                if (user != null)
                {
                    bool checkForClaim = true;
                    var userRoles = UserManager.GetRoles(user.Id);
                    if (userRoles != null)
                    {
                        //admins have access without claim check
                        if (userRoles.Contains("Administrators"))
                            checkForClaim = false;
                    }

                    if (checkForClaim)
                    {
                        var userCLaims = UserManager.GetClaims(user.Id);
                        if (userCLaims != null)
                        {
                            var claim = userCLaims.Where(x => x.Type == DefaultOrganization.ClaimsTagPrefix + "." + "WebSiteUser" && x.Value == "Yes").FirstOrDefault();
                            if (claim==default(Claim))
                            {
                                ModelState.AddModelError("", "You have no rights to use this resourse.");
                            }
                        }

                    }

                    if (!await UserManager.IsEmailConfirmedAsync(user.Id))
                    {
                        await SendEmailConfirmationTokenAsync(user.Id, "Confirm your account - Resend");

                        // Uncomment to debug locally  
                        // ViewBag.Link = callbackUrl;
                        //ViewBag.errorMessage = "You must have a confirmed email to log on. "
                        //                     + "The confirmation token has been resent to your email account.";
                        //return View("Error");
                        ModelState.AddModelError("", "You must confirm your account via email.");
                    }

                    if (ModelState.IsValid)
                    {
                        await SignInAsync(user, model.RememberMe);
                        if (!String.IsNullOrEmpty(returnUrl) && returnUrl.IndexOf("?") >= 0)
                            returnUrl=returnUrl.Replace("?", "#");
                        return RedirectToLocal(returnUrl);
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Invalid username or password.");
                }
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            model.UserName = model.Email;
            if (ModelState.IsValid)
            {
                var user = new User() { UserName = model.UserName, Email = model.Email, OrganizationId = DefaultOrganization.Id };
                IdentityResult result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await UserManager.AddClaimAsync(user.Id, new Claim(DefaultOrganization.ClaimsTagPrefix + ".WebSiteUser", "Yes"));

                    //await SignInAsync(user, isPersistent: false);

                    await SendEmailConfirmationTokenAsync(user.Id, "Confirm your account");

                    //return RedirectToAction("Index", "Home");
                    AddErrors(result);
                }
                else
                {
                    AddErrors(result);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }


        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(long userId, string token)
        {
            IdentityResult result = await UserManager.ConfirmEmailAsync(userId,token);
            if (result.Succeeded)
            {
                User user = await UserManager.FindByIdAsync(userId);
                    user.EmailConfirmed = true;
                    await UserManager.UpdateAsync(user);
                    await SignInAsync(user, isPersistent: false);
                    return RedirectToAction("Index", "Home", new { ConfirmedEmail = user.Email });
            }
            else
            {
                return View("Error", new { result });
            }

        }

        public ActionResult Confirm()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = await UserManager.FindByEmailAsync(model.Email);
                if (user != null)
                {
                    string token = UserManager.GeneratePasswordResetToken(user.Id);
                    var callbackUrl = Url.Action("ResetPasswordFromEmail", "", new { userId = user.Id, token = token }, protocol: Request.Url.Scheme);
                    await UserManager.SendEmailAsync(user.Id, "Password reset request.", "Please follow link to reset your password <a href=\"" + callbackUrl + "\">here</a>");
                }
                else
                {
                    ModelState.AddModelError("", "Unknown email.");
                }
            }
            return View(model);
        }

        [AuthorizeWithClaims("WebSiteUser", "Yes")]
        public ActionResult ResetPassword()
        {
            string token = UserManager.GeneratePasswordResetToken(Int64.Parse(User.Identity.GetUserId()));
            PasswordResetViewModel model = new PasswordResetViewModel() { UserId = Int64.Parse(User.Identity.GetUserId()), Token = token };
            return View(model);
        }

        [AllowAnonymous]
        public async Task<ActionResult> ResetPasswordFromEmail(long userId,string token)
        {
            User user = await UserManager.FindByIdAsync(userId);
            if (user!=null)
            {
                PasswordResetViewModel model = new PasswordResetViewModel() { UserId = userId, Token = token };
                return View("ResetPassword",model);
            }
            else
            {
                return View("Error");
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ResetPassword(PasswordResetViewModel model)
        {
            if (ModelState.IsValid)
            {
                IdentityResult result=await UserManager.ResetPasswordAsync(model.UserId, model.Token, model.NewPassword);
                if (result.Succeeded)
                {
                    User user = await UserManager.FindByIdAsync(model.UserId);
                    await SignInAsync(user, isPersistent: false);
                    return RedirectToAction("Index", "Home", new { PasswordResetSuccess = true });

                }
                else
                {
                    AddErrors(result);
                }
            }
            return View(model);
        }


        [AuthorizeWithClaims("WebSiteUser", "Yes")]
        public ActionResult Do()
        {
            return View();
        }
    }
}
