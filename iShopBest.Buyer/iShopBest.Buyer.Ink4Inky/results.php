﻿<?php
$method = $_SERVER['REQUEST_METHOD'];
if ($method!="POST"){
	header("Location: /index.html");
}
	

ini_set('display_errors', 'On');

include('globals.php');
include('authorization/authorize.php');
session_start();

if (authorize($_SERVER['REQUEST_URI']))
{

	$render_html=false;
	if(isset($_POST["Basket"])){
		$request=json_decode($_POST["Basket"],true);
		$render_html=true;
}
	else {
		$str_json=file_get_contents('php://input');
		$request=json_decode($str_json, true);
	}

	$shopping_cart=array();
	foreach($request as $item) {
		$obj=new stdClass();
		$obj->GtinPlus=$item['gtinplus'];
		$obj->Quantity=1;
		$obj->DestZipCode="33065";
		array_push($shopping_cart,$obj);
	};

	$vsRequest2= [
	'IncludeInventory'=>'false',
	'Preferences'=>json_decode('[{"Name":"DeliverySpeedImportance", "Value": 50 }, { "Name": "FreightCostImportance", "Value": 50 }, { "Name": "SingleSupplierImportance", "Value": 50 }]',true),
	'ShoppingCart'=>$shopping_cart
	];


	//echo($vsRequest);
	$channel=curl_init($apiServer."/VendorSelection/QuickRun");

	curl_setopt_array($channel,array(
		CURLOPT_SSL_VERIFYPEER => 0,
		CURLOPT_POST=> TRUE,
		CURLOPT_RETURNTRANSFER => TRUE,
		CURLOPT_HTTPHEADER => array(
			"Authorization: Bearer ".$_SESSION['authToken'],
			'Content-Type: application/x-www-form-urlencoded;charset=UTF-8'
		),
		CURLOPT_POSTFIELDS => http_build_query(
			//['vensorSelectionQuery'=>$vsRequest]
			$vsRequest2
		)
	));

	$response=curl_exec($channel);
	if (curl_error($channel)) {
		//report error
		$errorMessage=curl_error($channel);
		curl_close($channel);
		header("HTTP/1.0 500 Internal Server Error**");
		die(json_encode(array([
				"error"=>$errorMessage,
				"token"=>$token
			])));
	}
	curl_close($channel);
	if ($render_html) {
		//do render html attach results as json value
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="robots" content="noindex, nofollow">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="article">
    <meta property="og:title" content="Who is Inky">
    <meta property="og:description" content="Major websites like Staples, Office Depot, and Rakuten as well as lesser known eco-friendly websites.">
    <meta property="og:site_name" content="Who is Inky?">
    <meta property="og:url" content="http://www.ink4inky.com">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" id="mobile-viewport">

    <title>Inky</title>

    <!--link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700"-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" />
    <link rel="stylesheet" href="/content/bootstrap.min.css">
    <link rel="stylesheet" href="/content/font-awesome.min.css">
    <link rel="stylesheet" href="/content/normalize.css">
    <link rel="stylesheet" href="/content/animate.min.css">
    <link rel="stylesheet" href="/content/results.css">
</head>
<body class="ink4inky-page animated fadeIn">
    <section>
        <div class="container move-down">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"></div>
                <!--div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 header-text blur-around" id="left"><h2>Inky analyzed 100+ scenarios & these are the 
top choices.</h2></div-->
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 header-text blur-around" id="left"><h2 id="productsSummary" style="min-height:85px;">loading ...</h2></div>
            </div>
            <div class="row white-panel animated zoomInDown">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="container">
                        <div class="row centralize">
                            <div class="col-lg-12 text-center" style="min-height: 50px; margin-top: 25px;">
                                <button class="dynamic-button-inversed pull-right" id="startOverButton">Start over...</button>
                            </div>
                        </div>
                        <div class="row centralize">
                            <div class="col-lg-12 text-left" style="min-height: 0px;" id="gridStart"></div>
                        </div>

                        <div class="row centralize">
                            <div class="col-lg-12 text-left" style="min-height: 40px;">&nbsp;</div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <h1 style="margin-bottom: 0px;"><p style="margin-bottom: 0px;"><font color="#BABABA" style="font-size: 24px; font-weight: bold;">Results provided by</font></p></h1>
                                <a href="http://www.ishopbest.com"><img src="/images/iShopBest-logo-only.png" style="width: 239px; height: 63px;" title="Results provided by iShopBest.com." alt="iShopBest.com" /></a>
                                <div style="position: absolute; bottom: -75px; right: 20px;"><img src="/images/1498061672-16298491-160x136-splat-magenta.png" title="Inky was here!" /></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div style="min-height: 50px;"></div>
    </section>
    <footer>
        <div class="container-fluid">
            <div class="row">
                <div class="col col-xs-12">
                    <p style="line-height: 22.4px; font-size: 16px; margin-top: 10px;">
                        <font color="#ffffff">
                            Made with &nbsp;<img src="/images/pinkheart.png" style="height:16px; width: 17px; margin-top: 0px;" alt="Love, Peace" title="Love, Peace!">&nbsp;in FL
                        </font>
                    </p>
                </div>
            </div>
        </div>
    </footer>

    <div id="feedback" title="leave feedback"><i class="fa fa-envelope-square"></i></div>

    <div id="modal-responsive" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal animated fadeIn">
        <div class="modal-dialog">
            <div class="modal-content dialog-width">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close-form"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Feedback</h4>
                </div>
                <div class="modal-body" style="max-height: 600px;">
                    <div id="signin-page" style="height: auto;">
                        <div class="page-form">
                            <form action="#" class="form" id="provide-feedback">
                                <!--div class="header-content">
                                    <h2>Please enter your feedback below.</h2>
                                </div-->
                                <div class="body-content">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <i class="fa fa-user"></i>&nbsp;<label for="name" class="control-label">Name <span class="require">*</span></label><span class="alert hide" id="name-alert">empty or wrong!</span>
                                                <div class="input-icon right">
                                                    <input type="text" placeholder="Name" id="name" name="name" class="form-control" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <i class="fa fa-phone"></i>&nbsp;<label for="phone" class="control-label">Phone </label><span class="alert hide" id="phone-alert">empty or wrong!</span>
                                                <div class="input-icon right">
                                                    <input type="text" placeholder="Phone" name="phone" id="phone" class="form-control" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <i class="fa fa-envelope"></i>&nbsp;<label for="email" class="control-label">E-Mail Address </label><span class="alert hide" id="email-alert">empty or wrong!</span>
                                                <div class="input-icon right">
                                                    <input type="email" placeholder="E-Mail" name="email" id="email" class="form-control" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <i class="fa fa-cube"></i>&nbsp;<label for="package" class="control-label">Subject <span class="require">*</span></label>
                                                <div class="input-icon right">
                                                    <input type="text" placeholder="Subject" name="subject" id="subject" class="form-control" required>
                                                </div>
                                                <span class="alert hide">subject is required!</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <i class="fa fa-comment"></i>&nbsp;<label for="feedback-body" class="control-label">Feedback </label><span class="alert hide" id="phone-alert">empty or wrong!</span>
                                            <div class="input-icon right">
                                                <textarea placeholder="your feedback here" name="feedback-body" id="feedback-body" class="form-control" required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="dynamic-button-inversed" id="submit-form">Submit&nbsp;<i class="fa fa-chevron-circle-right"></i></button>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript" src="/scripts/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="/scripts/globals.js"></script>
    <script type="text/javascript" src="/scripts/main.js"></script>
    <script type="text/javascript">
        $(function () { app.resultsScript.init(
		<?php
		echo("{request: ".json_encode($vsRequest2).", response: ".$response."}");
		?>
		); });
    </script>
    <script>
    (function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-101736277-1', 'auto');
    ga('send', 'pageview');

    </script>
</body>
</html>
<?php	
	}
	else 
	{
		header("Content-type: application/json");
		//do not render just pass json as response
		echo($response);
	}
}
else 
{
	header("HTTP/1.0 500 Internal Server Error*");
	die("failed to process results");
}
?>
