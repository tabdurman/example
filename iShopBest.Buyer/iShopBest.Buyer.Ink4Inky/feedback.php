﻿<?php
include('globals.php');
include('/authorization/authorize.php');
session_start();

if (authorize($_SERVER['REQUEST_URI']))
{
	$str_json=file_get_contents('php://input');
	$request=json_decode($str_json, true);

	$to = "feedback@ishopbest.com";

	$subject = $request["subject"];
	
	if ($subject=="")
		$subject="Ink4Inky feedback.";
	
	$body = "You have new feedback:\r\n";
	$body .="Name: ".$request["name"]."\r\n";
	$body .="Phone: ".$request["phone"]."\r\n";
	$body .="Email: ".$request["email"]."\r\n";
	$body .=$request["message"];


	$channel=curl_init($apiServer."/ExtraServices/SendEmail");

	curl_setopt_array($channel,array(
		CURLOPT_SSL_VERIFYPEER => 0,
		CURLOPT_POST=> TRUE,
		CURLOPT_RETURNTRANSFER => TRUE,
		CURLOPT_HTTPHEADER => array(
			"Authorization: Bearer ".$_SESSION['authToken'],
			'Content-Type: application/x-www-form-urlencoded;charset=UTF-8'
		),
		CURLOPT_POSTFIELDS => http_build_query(
			['name'=>$request["name"],
			'phoneNumber'=>$request["phone"],
			'email'=>$request["email"],
			'subject'=>$request["subject"],
			'message'=>$body]
		)
	));

	$response=curl_exec($channel);

	$token=$_SESSION['authToken'];

	if (curl_error($channel)) {
		//report error
		$errorMessage=curl_error($channel);
		curl_close($channel);
		header("HTTP/1.0 500 Internal Server Error");
		die(json_encode(array([
				"error"=>$errorMessage,
				"token"=>$token
			])));
	}
	curl_close($channel);
	//echo($response);
}
else 
{
	header("HTTP/1.0 500 Internal Server Error");
	die("failed send feedback");
}
?>