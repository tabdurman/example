﻿<?php

function do_logout() {
	global $apiServer;
	if (ISSET($_SESSION['authToken'])) {
		$channel=curl_init($apiServer."/Account/Logout");
		curl_setopt_array($channel,array(
			CURLOPT_SSL_VERIFYPEER => 0,
			CURLOPT_POST=> TRUE,
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLOPT_HTTPHEADER => array(
				"Authorization: Bearer ".$_SESSION['authToken']
			)
		));
		
		$response=curl_exec($channel);
		if (curl_error($channel)) {
			//report error
			$errorMessage=curl_error($channel);
			curl_close($channel);
			die($errorMessage);
		}

		curl_close($channel);
		//echo("logged out");
	}
}

function do_login() {

	global $apiServer;
	do_logout();

	// check if access token exists, logout
	//clear session
	$_SESSION['authToken']=NULL;

	//user/pass
	$user='apipublic';
	$password='iShopBest2017_';

	//echo($apiServer."/Account/Token");

	//login
	$channel=curl_init($apiServer."/Account/Token");
	curl_setopt_array($channel,array(
		CURLOPT_SSL_VERIFYPEER => 0,
		CURLOPT_POST=> TRUE,
		CURLOPT_RETURNTRANSFER => TRUE,
		CURLOPT_HTTPHEADER => array(
			'Content-Type: application/x-www-form-urlencoded;charset=UTF-8'
		),
		CURLOPT_POSTFIELDS => http_build_query(
			['email'=>'',
			 'password'=>$password,
			 'confirmPassword'=>'',
			 'userName'=>$user,
			 'grant_type'=>'password'])
	));

	$response=curl_exec($channel);
	if (curl_error($channel)) {
		//report error
		$errorMessage=curl_error($channel);
		curl_close($channel);
		die($errorMessage);
	}
	
	curl_close($channel);

	$accessInfo=json_decode($response, true);

	// store token
	$_SESSION['authToken']=$accessInfo["access_token"];
};
?>