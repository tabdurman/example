﻿var apiRoot = 'https://localhost:44303';

var app = {
    apiRoot: 'https://localhost:44303',
    destinationCategory: "0",
    destinationGtinplus: 0,
    urlParams: {},
    reverse: function (s) {
        return s.split('').reverse().join('');
    }
};