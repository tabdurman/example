﻿var app = (function (app, $) {

    var addAnotherTonerButton = $("#add-another-toner");
    var showBestPrice = $("#show-best-price");

    var editBasketButton = $("#edit-basket-button");
    var closeBasketButton = $("#close-basket-button");
    var doneWithBasket = $("#done-with-basket");

    var selectBrand = $("#select-brand");
    var selectSeries = $("#select-series");
    var selectModel = $("#select-model");
    var selectProduct = $("#select-product");
    var basketItemsCount = $("#basket-items-count");

    var basketPopupTitle = $("#basket-popup-title");

    var basketDetailsPopup = $("#basket-details");
    var Basket = [];

    var toggleElement = function (elm, state) {
        if (elm && elm.attr) {
            if (state && state == 'disable') {
                elm.attr('disabled', 'disabled');
            }
            else {
                elm.removeAttr('disabled');
            }
        }
    };

    //for index page
    app.indexScript = {
        closeBasketPopUp : function () {
            if (basketDetailsPopup) {
                basketDetailsPopup.addClass("hide");
            }
        },

        onClickHandlerButton : function (e) {
            if (e.target && e.target.id) {
                parseId = e.target.id.split('-');
                if (parseId.length > 1) {
                    $.each(Basket, function (idx, elm) {
                        if (elm.gtinplus == parseId[parseId.length - 1]) {
                            Basket.splice(Basket.indexOf(elm), 1);
                            return false;
                        }
                    });
                    this.rebuldBasketView();
                }
            }
            return false;
        },

        onClickHandler : function (e) {
            if (e.target && e.target.parentElement && e.target.parentElement.id) {
                parseId = e.target.parentElement.id.split('-');
                if (parseId.length > 1) {
                    $.each(Basket, function (idx, elm) {
                        if (elm.gtinplus == parseId[parseId.length - 1]) {
                            Basket.splice(Basket.indexOf(elm), 1);
                            return false;
                        }
                    });
                    this.rebuldBasketView();
                }
            }
            return false;
        },

        resetForm : function (level) {
            if (level == 3) {
                selectBrand.empty();
                toggleElement(selectBrand, 'disable');
                destinationBrand = undefined;
            }
            if (level >= 2) {
                selectSeries.empty();
                toggleElement(selectSeries, 'disable');
                destinationSeries = undefined;
            }
            if (level >= 1) {
                selectModel.empty();
                toggleElement(selectModel, 'disable');
                destinationModel = undefined;
                destinationCategory = undefined;
            }
            if (level >= 0) {
                selectProduct.empty();
                toggleElement(selectProduct, 'disable');
                destinationTitle = undefined;
                destinationGtinplus = undefined;
            }
        },

        rebuldBasketView : function () {
            $("#cart-start").empty();

            var cnt = 0;
            if (Basket.length > 0) {

                var addHtml = "<div class='containder-fluid'><table class='table table-striped table-hover'><thead><tr><th width='50px'></th><th align='center'></th><th align='center'>Brand</th><th>Series</th><th align='center'>Model</th></tr></thead><tbody>";

                $.each(Basket, function (idx, elm) {
                    addHtml += '<tr><td><button class="btn btn-small btn-success hidden-sm hidden-md hidden-lg" id="deleteFromCart-' + elm.gtinplus + '">delete</button><a href="#" id="basket-delete-item-' + elm.gtinplus + '"><i class="fa fa-close h1 pull-right close-button hidden-xs" style="margin-top: 0px; margin-right: 0px; padding-right: 0px; color: darkred"></i></a></td><td>' + elm.title + '</td><td>' + elm.brand + '</td><td>' + elm.series + '</td><td>' + elm.model + '</td></tr>';
                    cnt++;
                });
                addHtml += "</tbody></table></div>";
                $("#cart-start").append(addHtml);

                $("button[id^='deleteFromCart'").each(function (idx, elm) {
                    elm.addEventListener('click', this.onClickHandlerButton);
                    elm.addEventListener('tap', this.onClickHandlerButton);
                });

                $("a[id^='basket-delete-item'").each(function (idx, elm) {
                    elm.addEventListener('click', this.onClickHandler);
                    elm.addEventListener('tap', this.onClickHandler);
                });

            }
            if (basketPopupTitle) {
                basketPopupTitle.text('You have ' + cnt + ' item(s) in your basket.');
            };
            if (basketItemsCount) {
                basketItemsCount.text(cnt);
            };


        },

        initVisuals: function () {
            if (selectBrand && selectBrand[0] && selectBrand[0].options && selectBrand[0].options.selectedIndex) {
                if (selectBrand[0].options.length > 0) {
                    selectBrand[0].options[selectBrand[0].options.selectedIndex].selected = false;
                    selectBrand[0].options[0].selected = true;
                }
                selectBrand[0].selectedIndex = 0;
            }
        },

        init: function () {
            this.initVisuals();
            this.rebuldBasketView();

            // show best price button click
            if (showBestPrice) {
                showBestPrice.bind('click', function () {
                    if (Basket.length > 0) {
                        var gtinPlusList = "";
                        $.each(Basket, function (idx, elm) {
                            gtinPlusList += gtinPlusList == "" ? elm.gtinplus : "," + elm.gtinplus;
                            if (destinationGtinplus == elm.gtinplus)
                                destinationGtinplus = undefined;
                        });
//                        window.location.href = "/results2.html?gtinplus=" + gtinPlusList;
                    }
                    else if (destinationGtinplus) {
//                        window.location.href = "/results2.html?gtinplus=" + destinationGtinplus;
                    }

                    if (destinationGtinplus)
                        Basket.push({
                            brand: destinationBrand,
                            series: destinationSeries,
                            model: destinationModel,
                            gtinplus: destinationGtinplus,
                            title: destinationTitle
                        });


                    var form = $(document.createElement('form'));
                    //$(form).attr("action", "https://localhost:9292/results.php");
                    $(form).attr("action", "results.php");
                    $(form).attr("method", "POST");
                    $(form).css("display", "none");

                    var input_employee_name = $("<input>")
                        .attr("type", "text")
                        .attr("name", "Basket")
                        .val(JSON.stringify(Basket));
                    $(form).append($(input_employee_name));


                    form.appendTo(document.body);
                    $(form).submit();


                    return false;
                });
            };

            // add another toner button click
            if (addAnotherTonerButton) {
                addAnotherTonerButton.bind('click', function () {
                    if (!Basket) {
                        Basket = [];
                    }

                    var doContinue = true;
                    $.each(Basket, function (idx, elm) {
                        if (destinationGtinplus == elm.gtinplus) {
                            doContinue = false;
                            return false;
                        }
                    });

                    if (doContinue) {
                        Basket.push({
                            brand: destinationBrand,
                            series: destinationSeries,
                            model: destinationModel,
                            gtinplus: destinationGtinplus,
                            title: destinationTitle
                        });
                    }

                    app.indexScript.rebuldBasketView();
                    app.indexScript.initVisuals();
                    toggleElement(addAnotherTonerButton, 'disable');
                    app.indexScript.resetForm(2);

                    return false;
                });
            };

            // edit basket button
            if (editBasketButton) {
                editBasketButton.bind('click', function () {
                    if (basketDetailsPopup) {
                        if (basketDetailsPopup.is(":visible"))
                            basketDetailsPopup.addClass("hide");
                        else
                            basketDetailsPopup.removeClass("hide");
                    }
                    return false;
                });
            };

            // done with basket button
            if (doneWithBasket) {
                doneWithBasket.bind('click', function () {
                    app.indexScript.closeBasketPopUp();
                    return false;
                });
            };

            // edit basket button
            if (closeBasketButton) {
                closeBasketButton.bind('click', function () {
                    app.indexScript.closeBasketPopUp();
                    return false;
                });
            };

            //select brand functionality
            if (selectBrand) {

                app.indexScript.resetForm(3);
                toggleElement(selectBrand, 'enable');
                selectBrand.append('<option value=0>loading brands</option>');
                selectBrand.bind('change', function (e) {
                    if (e && e.target && e.target.value && e.target.value !== "0") {

                        destinationBrand = e.target.options[e.target.selectedIndex].text;
                        app.indexScript.resetForm(2);

                        //$.get(apiRoot + '/api/Catalogcategories/' + e.target.value)
                        $.get('/categories.php?Id=' + e.target.value)
                        .done(function (data) {
                            selectSeries.empty();
                            selectSeries.append('<option selected=true value=0 disabled>Select Series</option>');
                            $.each(data, function (idx, elm) {
                                selectSeries.append('<option value="' + elm.Id + '">' + elm.Name + '</option>');
                            });

                            //enable series select
                            toggleElement(selectSeries, 'enable');

                        }).fail(function (reason) {
                            console.log('Plase report this to techsupport: ' + JSON.stringify(reason));
                        });
                    }
                });
            };

            //select series functionality
            if (selectSeries) {
                selectSeries.bind('change', function (e) {
                    if (e && e.target && e.target.value) {

                        destinationSeries = e.target.options[e.target.selectedIndex].text;
                        app.indexScript.resetForm(1);

                        //$.get(apiRoot + '/api/Catalogcategories/' + e.target.value)
                        $.get('/categories.php?Id=' + e.target.value)
                            .done(function (data) {
                            selectModel.empty();
                            selectModel.append('<option selected=true value=0 disabled>Select Model</option>');
                            $.each(data, function (idx, elm) {
                                selectModel.append('<option value="' + elm.Id + '">' + elm.Name + '</option>');
                            });

                            //enable series select
                            toggleElement(selectModel, 'enable');
                        }).fail(function (reason) {
                            console.log('Plase report this to techsupport: ' + JSON.stringify(reason));
                        });
                    }
                });
            };

            //select model functionality
            if (selectModel) {

                selectModel.bind('change', function (e) {
                    if (e && e.target && e.target.value) {

                        destinationModel = e.target.options[e.target.selectedIndex].text;
                        app.indexScript.resetForm(0);


                        //$.get(apiRoot + '/api/CategoryProducts/Summary/' + e.target.value)
                         $.get('/products.php?Id=' + e.target.value)
                            .done(function (data) {
                                selectProduct.append('<option selected=true value=0 disabled>Select Product</option>');
                                $.each(data, function (idx, elm) {
                                    var color = "black";
                                    var icon = "";

                                    selectProduct.append('<option value="' + elm.Product.Id + '" class="' + color + '">' + icon + ' ' + elm.Product.ProductTitle + '</option>');
                                });

                                toggleElement(selectProduct, 'enable');
                            })
                            .fail(function (reason) {
                                console.log('Plase report this to techsupport: ' + JSON.stringify(reason));
                            });
                    }
                });
            };

            if (selectProduct) {

                selectProduct.bind('change', function (e) {
                    destinationGtinplus = e.target.value;
                    destinationTitle = e.target.options[e.target.selectedIndex].text;
                    toggleElement(addAnotherTonerButton, 'enable');
                    toggleElement(showBestPrice, 'enable');
                });

            }

            toggleElement(addAnotherTonerButton, 'disable');
            toggleElement(showBestPrice, 'disable');

            // initiate brands load
           // $.get(apiRoot + '/api/CatalogCategories/0')
            $.get('/categories.php?Id=0')
                .done(function (data) {
                    //get select for brand
                    if (selectBrand) {

                        //if good clear options
                        selectBrand.empty();
                        selectBrand.append('<option selected=true value=0 disabled>Select Brand</option>');

                        $.each(data, function (idx, elm) {
                            selectBrand.append('<option value="' + elm.Id + '">' + elm.Name + '</option>');
                        });
                    }
                })
                .fail(function (reason) {
                    console.log('Plase report this to techsupport: ' + JSON.stringify(reason));
                });

        }
    };

    //for results page
    app.resultsScript = {
        getProductTitle: function (products, gtinplus) {
            var foundTitle = "";
            if (products && gtinplus) {
                $.each(products, function (idx, elm) {
                    if (elm.ProductTitle && elm.Id) {
                        if (elm.Id == gtinplus) {
                            foundTitle = elm.ProductTitle;
                            return false;
                        }
                    };
                });
            }
            return foundTitle;
        },
        getImageAddress: function (supplier) {

            var imageUrl = "";

            switch (supplier) {
                case "Staples, Inc.":
                    imageUrl = "/images/1498061668-16200041-176x86-staples-logo-69924DC.png";
                    break;
                case "Office Depot, Inc.":
                    imageUrl = "/images/1498061669-16200056-175x57-PNGPIX-COM-Office-De.png";
                    break;
                case "GovGroup.com":
                    imageUrl = "/images/govgroup-logo.svg";
                    break;
                case "TomatoInk.com":
                    imageUrl = "/images/tomatoink.png";
                    break;
                case "InkJets.com":
                    imageUrl = "/images/inkjets.png";
                    break;
                case "InkCartridges.com":
                    imageUrl = "/images/inkcartridges.png";
                    break;
                case "3R Inkjets and Toners, LLC":
                    imageUrl = "/images/3rinkandtoner.jpg";
                    break;
                case "QUILL CORPORATION":
                    imageUrl = "/images/quill.png";
                    break;
                default:
                    imageUrl = "/images/1498061668-16200041-176x86-staples-logo-69924DC.png";
                    break;
            }
            return imageUrl;
        },
        init: function (data) {

            $("#modal-responsive").hide();

            $("#feedback").bind("click", function () {
                $("#modal-responsive").show();
            });

            $("#close-form").bind("click", function () {
                $("#modal-responsive").hide();
            });

            $('#startOverButton').bind('click', function () {
                window.location.href = "/index.html";
            })

            $("#submit-form").bind("click", function () {
                // validate form



                $.post("feedback.php", JSON.stringify({
                    name: $("#name").val(),
                    phone: $("#phone").val(),
                    email: $("#email").val(),
                    subject: $("#subject").val(),
                    message: $("#feedback-body").val()
                })).done(function (data) {
                    $("#modal-responsive").hide();
                }).fail(function (reason) {
                    console.log("Please report this to support: " + JSON.stringify(reason));
                });
            });


            $("#productsSummary").text("loading ...");
            $("#tableHeader").hide();

            var processData = function (data) {

                var insertAfter = $("#gridStart");
                var counter = 1;
                if (data.Orders.length == 1)
                    $("#productsSummary").text("Inky analyzed " + data.Orders.length + " basket.");
                else
                    $("#productsSummary").text("Inky analyzed " + data.Orders.length + " baskets.");

                $("#tableHeader").show();
                // set this to how many orders you want to show
                var ordersToShow = 3;
                if (data.Orders && data.Orders.length > 0) {

                    //do not let show more than having
                    if (data.Orders.length < ordersToShow)
                        ordersToShow = data.Orders.length;

                    $.each(data.Orders, function (idx, elm) {
                        //var elm = data.Orders[0];
                        var score = elm.Score;
                        var orderStatus = "";
                        if (idx == 0)
                            orderStatus = "Top Ranked Basket";
                        else if (idx == 1)
                            orderStatus = "Second Ranked Basket";
                        else
                            orderStatus = "Third Ranked Basket";

                        //start new order view
                        var elmText =
                            '                    <div class="header-text text-center">' +
                            '                        <h1>' + orderStatus + '</h1>' +
                            '                    </div>' +

                            '                <div>' +
                            '                <table class="header-text" style="width: 100%;">' +
                            '                        <thead>' +
                            '                            <tr>' +
                            '                                <th style="width: 45%;"></th>' +
                            '                                <th style="width: 15%;"></th>' +
                            '                                <th style="width: 15%;"></th>' +
                            '                                <th></th>' +
                            '                            </tr>' +
                            '                        </thead>' +
                            '                        <tbody>'

                        if (elm.OrderLines && elm.OrderLines.length && elm.OrderLines.length > 0) {
                            $.each(elm.OrderLines, function (subIdx, subElm) {
                                var leadTime = 4;
                                var linkUrl = "";

                                //load lead time and link url
                                $.each(data.Matches, function (cmpIdx, cmpElm) {
                                    if (cmpElm.GTINPLUS == subElm.GTINPLUS && cmpElm.SupplierId == subElm.SupplierId && cmpElm.Sku == subElm.Sku) {
                                        leadTime = cmpElm.LeadTime;
                                        linkUrl = cmpElm.Url;
                                        return false;
                                    };
                                });

                                //add next orderLine
                                elmText +=
                                    "                        <tr>" +
                                "                            <td colspan='4'><h2>" + app.resultsScript.getProductTitle(data.Products, subElm.GTINPLUS) + ".</h2></td>" +
                                    "                        </tr>" +
                                    "                        <tr>" +
                                "                            <td><img src='" + app.resultsScript.getImageAddress(subElm.SupplierName) + "' class='image-responsive' style='top: 15px; width: 175px;' alt=''></td>" +
                                    "                                <td><h2>$" + subElm.Price + "</h2></td>" +
                                    "                                <td><h3>" + leadTime + " days shipping</h3></td>" +
                                    "                                <td class='visit-button-container'><button class='dynamic-button' onclick='window.open(\"" + linkUrl + "\",\"_blank\");'>Visit</button></td>" +
                                    "                        </tr>"

                            });
                            counter++;

                        }

                        //compelete order view
                        elmText +=
                            "                                   </tbody >" +
                            "                                </table > " +
                            "                            </div > ";
                        insertAfter.append(elmText);
                        ordersToShow--;

                        //quit on showing enough
                        if (ordersToShow == 0)
                            return false;
                    });
                }

            };

            if (data && data.request) {
                var VSRequest = data.request;
            }

            if (data && data.response) {
                processData(data.response);
            }
            else {

                $.post(app.apiRoot + 'results.php', VSRequest).done(function (data) {
                    processData(data);
                }).fail(function (reason) {
                    $("#productsSummary").text("oops, something terrible happened, please try again later ... or click start over button.");
                    console.log("Please report this to techsupport: " + JSON.stringify(reason));
                });
            }
        },
    };

    return app;
})(app || {}, jQuery);

(window.onpopstate = function () {
    var match,
        pl = /\+/g,  // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
        query = window.location.search.substring(1);

    app.urlParams = {};
    while (match = search.exec(query))
        app.urlParams[decode(match[1])] = decode(match[2]);
})();
