﻿(window.onpopstate = function () {
    var match,
        pl = /\+/g,  // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
        query = window.location.search.substring(1);

    app.urlParams = {};
    while (match = search.exec(query))
        app.urlParams[decode(match[1])] = decode(match[2]);
})();

$(function () {
    var getProductTitle = function (products, gtinplus) {
        var foundTitle = "";
        if (products && gtinplus) {
            $.each(products, function (idx, elm) {
                if (elm.ProductTitle && elm.Id) {
                    if (elm.Id == gtinplus) {
                        foundTitle = elm.ProductTitle;
                        return false;
                    }
                };
            });
        }
        return foundTitle;
    };
    var getImageAddress = function (supplier) {

        var imageUrl = "";

        switch (supplier) {
            case "Staples, Inc.":
                imageUrl = "/images/1498061668-16200041-176x86-staples-logo-69924DC.png";
                break;
            case "Office Depot, Inc.":
                imageUrl = "/images/1498061669-16200056-175x57-PNGPIX-COM-Office-De.png";
                break;
            case "GovGroup.com":
                imageUrl = "/images/govgroup-logo.svg";
                break;
            case "TomatoInk.com":
                imageUrl = "/images/tomatoink.png";
                break;
            case "InkJets.com":
                imageUrl = "/images/inkjets.png";
                break;
            case "InkCartridges.com":
                imageUrl = "/images/inkcartridges.png";
                break;
            case "3R Inkjets and Toners, LLC":
                imageUrl = "/images/3rinkandtoner.jpg";
                break;
            case "QUILL CORPORATION":
                imageUrl = "/images/quill.png";
                break;
            default:
                imageUrl = "/images/1498061668-16200041-176x86-staples-logo-69924DC.png";
                break;
        }
        return imageUrl;
    };

    $("#modal-responsive").hide();

    $("#feedback").bind("click", function () {
        $("#modal-responsive").show();
    });

    $("#close-form").bind("click", function () {
        $("#modal-responsive").hide();
    });

    $("#submit-form").bind("click", function () {
        // validate form

        $.post("feedback.php", JSON.stringify({
                name: $("#name").val(),
                phone: $("#phone").val(),
                email: $("#email").val(),
                subject: $("#subject").val(),
                message: $("#feedback-body").val()
        })).done(function (data) {
                $("#modal-responsive").hide();
            }).fail(function (reason) {
                console.log("Please report this to support: " + JSON.stringify(reason));
            });
    });


    var gtinplus = '';
    if (app.urlParams && app.urlParams["gtinplus"])
        gtinplus = app.urlParams["gtinplus"];
    if (!gtinplus)
        window.location.href = "/index.html";


    $('#startOverButton').bind('click', function () {
        window.location.href = "/index.html";
    })


    var VSRequest = {
        IncludeInventory: false,
        Preferences: [{ "Name": "DeliverySpeedImportance", "Value": 50 }, { "Name": "FreightCostImportance", "Value": 50 }, { "Name": "SingleSupplierImportance", "Value": 50 }],
        ShoppingCart: []
    };

    var arrToGo = gtinplus.split(",");
    $.each(arrToGo, function (idx, elm) {
        VSRequest.ShoppingCart.push({ "GtinPlus": elm, "Quantity": 1, "DestZipCode": "33324" });
    });

    $("#productsSummary").text("loading ...");
    $("#tableHeader").hide();
    $.post(apiRoot + '/api/VendorSelection/QuickRun', VSRequest).done(function (data) {
        var insertAfter = $("#gridStart");
        var counter = 1;
        if (data.Orders.length == 1)
            $("#productsSummary").text("Inky analyzed " + data.Orders.length + " basket.");
        else
            $("#productsSummary").text("Inky analyzed " + data.Orders.length + " baskets.");

        $("#tableHeader").show();
        // set this to how many orders you want to show
        var ordersToShow = 3;
        if (data.Orders && data.Orders.length > 0) {

            //do not let show more than having
            if (data.Orders.length < ordersToShow)
                ordersToShow = data.Orders.length;

            $.each(data.Orders, function (idx, elm) {
                //var elm = data.Orders[0];
                var score = elm.Score;
                var orderStatus = "";
                if (idx == 0)
                    orderStatus = "Top Ranked Basket";
                else if (idx==1)
                    orderStatus = "Second Ranked Basket";
                else
                    orderStatus = "Third Ranked Basket";

                //start new order view
                var elmText =
'                    <div class="header-text text-center">'+
'                        <h1>' + orderStatus+'</h1>'+
'                    </div>'+

'                <div>'+
'                <table class="header-text" style="width: 100%;">'+
'                        <thead>'+
'                            <tr>'+
'                                <th style="width: 45%;"></th>'+
'                                <th style="width: 15%;"></th>'+
'                                <th style="width: 15%;"></th>'+
'                                <th></th>'+
'                            </tr>'+
'                        </thead>'+
'                        <tbody>'

                if (elm.OrderLines && elm.OrderLines.length && elm.OrderLines.length > 0) {
                    $.each(elm.OrderLines, function (subIdx, subElm) {
                        var leadTime = 4;
                        var linkUrl = "";

                        //load lead time and link url
                        $.each(data.Matches, function (cmpIdx, cmpElm) {
                            if (cmpElm.GTINPLUS == subElm.GTINPLUS && cmpElm.SupplierId == subElm.SupplierId && cmpElm.Sku==subElm.Sku) {
                                leadTime = cmpElm.LeadTime;
                                linkUrl = cmpElm.Url;
                                return false;
                            };
                        });

                        //add next orderLine
                        elmText +=
    "                        <tr>"+
    "                            <td colspan='4'><h2>"+ getProductTitle(data.Products, subElm.GTINPLUS) + ".</h2></td>"+
    "                        </tr>"+
    "                        <tr>"+
    "                            <td><img src='" + getImageAddress(subElm.SupplierName)+"' class='image-responsive' style='top: 15px; width: 175px;' alt=''></td>"+
    "                                <td><h2>$" + subElm.Price + "</h2></td>"+
    "                                <td><h3>"+ leadTime + " days shipping</h3></td>"+
    "                                <td class='visit-button-container'><button class='dynamic-button' onclick='window.open(\"" + linkUrl +"\",\"_blank\");'>Visit</button></td>"+
    "                        </tr>"

                    });
                    counter++;

                }

                //compelete order view
                elmText +=
"                                   </tbody >"+
"                                </table > "+
"                            </div > ";
                insertAfter.append(elmText);
                ordersToShow--;

                //quit on showing enough
                if (ordersToShow == 0)
                    return false;
            });
        }

    }).fail(function (reason) {
        $("#productsSummary").text("oops, something terrible happened, please try again later ... or click start over button.");
        console.log("Please report this to techsupport: "+JSON.stringify(reason));
    });
});
