﻿<?php
ini_set('display_errors', 'On');

include('globals.php');
include('authorization/authorize.php');
session_start();

if (authorize($_SERVER['REQUEST_URI']))
{
	if(isset($_GET["Id"])) {
	
		$channel=curl_init($apiServer."/Catalogcategories/".$_GET["Id"]);

		curl_setopt_array($channel,array(
			CURLOPT_SSL_VERIFYPEER => 0,
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLOPT_HTTPHEADER => array(
				"Authorization: Bearer ".$_SESSION['authToken']
			) 
		));

		$response=curl_exec($channel);
		if (curl_error($channel)) {
			//report error
			$errorMessage=curl_error($channel);
			curl_close($channel);
			header("HTTP/1.0 500 Internal Server Error**");
			die("{ error : { code: 500, subcode: 0, message: '$errorMessage'}}");
		}
		curl_close($channel);
		header("Content-type: application/json");

		if (strpos($response, '404.0 - Not Found') !== false) { 
			die("{ error : { code: 500, subcode: 3, message: 'failed to process results'}}");
		}

		if (strpos($response, 'No HTTP resource was found') !== false) { 
			die("{ error : { code: 500, subcode: 4, message: 'failed to process results'}}");
		}
		
		
		echo($response);
	}
	else {
		header("Content-type: application/json");
		header("HTTP/1.0 500 Internal Server Error**");
		die("{ error : { code: 500, subcode: 1, message: 'failed to process results'}}");
	}
}
else 
{
	header("Content-type: application/json");
	header("HTTP/1.0 500 Internal Server Error*");
	die("{ error : { code: 500, subcode: 2, message: 'failed to process results'}}");
}
?>