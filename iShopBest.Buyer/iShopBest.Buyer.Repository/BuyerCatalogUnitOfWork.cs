﻿using CapSoft.Infrastructure.Domain;
using CapSoft.Infrastructure.UnitOfWork;
using System;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace iShopBest.Buyer.Repository
{
    [Export(typeof(IUnitOfWork))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    [ExportMetadata("UowType", "BuyerCatalog")]
    public class BuyerCatalogUnitOfWork : IUnitOfWork
    {
        public BuyerCatalogUnitOfWork()
        {
            var x = 1;
        }

        public void RegisterAmended(IAggregateRoot entity, IUnitOfWorkRepository unitofWorkRepository)
        {
            unitofWorkRepository.PersistUpdateOf(entity);
        }

        public void RegisterNew(IAggregateRoot entity, IUnitOfWorkRepository unitofWorkRepository)
        {
            unitofWorkRepository.PersistCreationOf(entity);
        }

        public void RegisterRemoved(IAggregateRoot entity, IUnitOfWorkRepository unitofWorkRepository)
        {
            unitofWorkRepository.PersistDeletionOf(entity);
        }

        public void RegisterRemoved(object predicate, IUnitOfWorkRepository unitofWorkRepository)
        {
            unitofWorkRepository.PersistDeletionOf(predicate);
        }

        public void Commit()
        {
            try
            {
                DbContext storageContext = BuyerCatalogDbContextFactory.GetDbContext() as DbContext;
                //DbContextFactory.GetDbContext().SaveChanges();
                storageContext.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                EntityValidationException newEx = new EntityValidationException("Entity Business Rules Broken.");

                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        newEx.AddValidationException(new ViolatedEntityBusinessRule(eve.Entry.Entity.GetType().Name, string.Format("{0}", eve.Entry.State), ve.PropertyName, ve.ErrorMessage));
                    }
                }
            }
            catch (Exception ex)
            {
                //DbContextFactory.ResetDbContext();
#if DEBUG
                if (Console.IsOutputRedirected)
                    Debug.WriteLine(ex.ToString());
#endif
            }
            finally
            {
                BuyerCatalogDbContextFactory.ResetDbContext();
            }
        }

        public async Task CommitAsync()
        {
            try
            {
                DbContext storageContext = BuyerCatalogDbContextFactory.GetDbContext() as DbContext;
                //await DbContextFactory.GetDbContext().SaveChangesAsync().ConfigureAwait(false);
                await storageContext.SaveChangesAsync().ConfigureAwait(false);
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder errorMessageBuilder = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    errorMessageBuilder.Append(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:\r\n",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errorMessageBuilder.Append(string.Format("- Property: \"{0}\", Error: \"{1}\"\r\n",
                            ve.PropertyName, ve.ErrorMessage));
                    }
                }
                throw new ApplicationException(errorMessageBuilder.ToString());
            }
            catch(Exception ex)
            {
#if DEBUG
                if (Console.IsOutputRedirected)
                    Debug.WriteLine(ex.ToString());
#endif
            }
            finally
            {
                BuyerCatalogDbContextFactory.ResetDbContext();
            }
        }

        public void CommitAndFlush()
        {
            DbContext storageContext = null;
            try
            {
                storageContext = BuyerCatalogDbContextFactory.GetDbContext() as DbContext;
                //DbContextFactory.GetDbContext().SaveChanges();
                storageContext.SaveChanges();

                //storageContext.Dispose();
            }
            catch (DbEntityValidationException e)
            {
                EntityValidationException newEx = new EntityValidationException("Entity Business Rules Broken.");

                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        newEx.AddValidationException(new ViolatedEntityBusinessRule(eve.Entry.Entity.GetType().Name, string.Format("{0}", eve.Entry.State), ve.PropertyName, ve.ErrorMessage));
                    }
                }
                /*
                StringBuilder errorMessageBuilder=new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    errorMessageBuilder.Append(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:\r\n",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errorMessageBuilder.Append(string.Format("- Property: \"{0}\", Error: \"{1}\"\r\n",
                            ve.PropertyName, ve.ErrorMessage));
                    }
                }
                
                throw new ApplicationException(errorMessageBuilder.ToString());
                 */
                throw newEx;
            }
            catch(Exception ex)
            {
#if DEBUG
                if (Console.IsOutputRedirected)
                    Debug.WriteLine(ex.ToString());
#endif

            }
            finally
            {
                BuyerCatalogDbContextFactory.ResetDbContext();
                //storageContext = null;
            }
        }

        #region IDisposable
        /* IDisposable */
        private bool _disposed;
        private object _threadSafety = new object();
        public bool Disposed { get { return _disposed; } }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            lock (_threadSafety)
            {
                if (!_disposed)
                {
                    _disposed = true;
                    if (disposing)
                    {
                        // TODO: Dispose object state
                    }
                }

            }
        }
        /* IDisposable */
        #endregion

    }
}
