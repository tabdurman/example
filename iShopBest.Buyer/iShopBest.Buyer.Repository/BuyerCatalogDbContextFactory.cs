﻿using CapSoft.Infrastructure.DbContextStorage;
using CapSoft.Infrastructure.Domain;
using CapSoft.Infrastructure.TypeConversion;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Text;

namespace iShopBest.Buyer.Repository
{
    public class BuyerCatalogDbContextFactory
    {
        protected static string _connection;
        private static object _threadSafety = new object();

        public static void Init(string connection)
        {
            _connection = connection;
        }

        public static IStorageContext GetDbContext()
        {
            IDbContextStorageContainer _dbContextStorageContainer = DbContextStorageFactory.CreateStorageContainer();
            IStorageContext storageContext = _dbContextStorageContainer.GetDbContext("BuyerCatalog");

            lock (_threadSafety)
            {

                var dbContext = storageContext as DbContext;
                if (dbContext != null)
                {
                    if (dbContext.Database.Connection.State == ConnectionState.Closed || dbContext.Database.Connection.State == ConnectionState.Broken)
                    {
                        dbContext.Database.Connection.Open();
                    }
                }
            }

            return storageContext;
        }

        public static void ResetDbContext()
        {
            IDbContextStorageContainer _dbContextStorageContainer = DbContextStorageFactory.CreateStorageContainer();
            _dbContextStorageContainer.ResetDbContext("BuyerCatalog");
        }


        /// <summary>
        /// Will call sql server directly via SqlCommand and return DataSet
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static DataSet DirectSqlCall(string commandText, IDictionary<string, ParameterDescriptor> parameters)
        {
            DataSet dataSet = default(DataSet);
            //using (SqlConnection sqlConnection = new SqlConnection(GetDbContext().Database.Connection.ConnectionString))
            using (SqlConnection sqlConnection = new SqlConnection(_connection))
            {
                using (SqlCommand sqlCommand = new SqlCommand())
                {

                    StringBuilder commandTextBulder = new StringBuilder(commandText);
                    bool firstParam = true;

                    if (parameters != null)
                    {
                        foreach (var param in parameters)
                        {
                            if (firstParam)
                            {
                                commandTextBulder.Append(" ");
                                commandTextBulder.Append(param.Key);
                                firstParam = false;
                            }
                            else
                            {
                                commandTextBulder.Append(",");
                                commandTextBulder.Append(param.Key);
                            }

                            sqlCommand.Parameters.Add(new SqlParameter() { ParameterName = param.Key, SqlDbType = TypeConvertor.ToSqlDbType(param.Value.ObjectType), Size = param.Value.Length, Value = param.Value.ObjectHolder ?? DBNull.Value, Direction = ParameterDirection.Input });
                        }
                    }

                    sqlConnection.Open();
                    sqlCommand.Connection = sqlConnection;
                    sqlCommand.CommandText = commandTextBulder.ToString();

                    try
                    {
                        using (SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand))
                        {
                            dataSet = new DataSet();
                            dataAdapter.Fill(dataSet);
                            // sort by score
                            dataSet.Tables[0].DefaultView.Sort = "Score ASC";
                        }
                    }
                    catch (Exception)
                    {

                    }

                }
            }

            return dataSet;
        }

    }
}
