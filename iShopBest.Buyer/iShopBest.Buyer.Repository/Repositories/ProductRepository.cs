//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace iShopBest.Buyer.Repository.Repositories
{
    using CapSoft.Infrastructure.UnitOfWork;
    using CapSoft.Infrastructure.QueryObject;
    using Domain.Model.RepositoryContracts;
    using Domain.Model.Entities;
    
    using System;
    using System.Collections.Generic;
    
    using System.Linq;
    using System.Linq.Expressions;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity;
    using System.ComponentModel.Composition;
    using System.Threading.Tasks;
    
    [Export(typeof(IProductRepository))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public partial class ProductRepository : Repository<Product,Int64>,IProductRepository
    {
    
    		[ImportingConstructor]
    //        public ProductRepository([Import] IUnitOfWork uow) : base(uow)
            public ProductRepository([ImportMany] IEnumerable<Lazy<IUnitOfWork, IUnitOfWorkMetadata>> uow) : base(uow)
    		
            {
            }
    
    		/*
            public override IQueryable<Product> GetDbSet()
            {
               // return DbContextFactory.GetDbContext().Products;
               return DbContextFactory.GetDbContext().Products.AsQueryable<Product>();
            }
    		*/
    
            public override string GetEntitySetName()
            {
                return "Products";
            }
    
            public override Product FindBy(Int64 Id)
            {
                return GetTable().FirstOrDefault(x => x.Id == Id);
            }
    
    		public override Product FindBy(Int64 Id,params Expression<Func<Product, object>>[] includes)
            {
                return GetTable(includes).FirstOrDefault(x => x.Id == Id);
            }
    
    		public async override Task<Product> FindByAsync(Int64 Id)
            {
                return await GetTable().FirstOrDefaultAsync(x => x.Id == Id).ConfigureAwait(false);
            }
    
            public override DbQuery<Product> TranslateIntoObjectQueryFrom(Query query)
            {
                DbQuery<Product> xProductQuery = null; // GetDbSet().All();
                return xProductQuery;
            }
    
            public override Product FindBy(Product entity)
            {
                return GetTable().FirstOrDefault(x => x.Id == entity.Id);
            }
    
    
            public override void PersistUpdateOf(Product entity)
            {
                var existingProductEntity = FindBy(entity);
                //var dbContext = DbContextFactory.GetDbContext();
                var dbContext = BuyerCatalogDbContextFactory.GetDbContext() as DbContext; 
                if (existingProductEntity != default(Product))
                {
                    dbContext.Entry(existingProductEntity).CurrentValues.SetValues(entity);
                    dbContext.Entry(existingProductEntity).State = EntityState.Modified;
                }
    
            }
    
    }
}
