﻿using CapSoft.Infrastructure.Domain;
using CapSoft.Infrastructure.QueryObject;
using CapSoft.Infrastructure.TypeConversion;
using CapSoft.Infrastructure.UnitOfWork;
using CapSoft.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CapSoft.Infrastructure.Helpers;
using System.Diagnostics;

namespace iShopBest.Buyer.Repository
{
    public abstract class Repository<T, EntityKey> : IUnitOfWorkRepository where T : class, IAggregateRoot
    {
        private IUnitOfWork _uow;

        public Repository()
        {

        }

        public Repository(IEnumerable<Lazy<IUnitOfWork, IUnitOfWorkMetadata>> uow)
        {
            var tUow = ArgumentValidator.GetValue<IEnumerable<Lazy<IUnitOfWork, IUnitOfWorkMetadata>>>(() => uow);
            _uow = tUow.Where(x => x.Metadata.UowType == "BuyerCatalog").FirstOrDefault().Value; ;
        }

        #region IRepository implementation
        public void Add(T entity)
        {
            _uow.RegisterNew(entity, this);
        }


        public void Remove(T entity)
        {
            _uow.RegisterRemoved(entity, this);
        }

        public virtual void Remove(Expression<Func<T, bool>> predicate)
        {
            _uow.RegisterRemoved(predicate as object, this);
        }
        public void Save(T entity)
        {
            _uow.RegisterAmended(entity, this);
            // Do nothing as EF tracks changes
        }
        #endregion

        #region Helpers
        /// <summary>
        /// Obtaining Set&lt;T&gt;
        /// </summary>
        /// <returns>Returns set of entity, usable in your LINQ or Lambdas</returns>
        public IQueryable<T> GetTable()
        {
            DbContext dbContext = BuyerCatalogDbContextFactory.GetDbContext() as DbContext;
            if (dbContext == null)
                return null;
            else
                return dbContext.Set<T>();//.AsQueryable<T>();
            //return DbContextFactory.GetDbContext().Set<T>();//.AsQueryable<T>();
        }

        /// <summary>
        /// Obtaining Set&lt;T&gt; with includes
        /// </summary>
        /// <returns>Returns set of entity, usable in your LINQ or Lambdas</returns>
        public IQueryable<T> GetTable(params Expression<Func<T, object>>[] includes)
        {
            //IQueryable<T> dbSet = DbContextFactory.GetDbContext().Set<T>();
            IQueryable<T> dbSet = GetTable();
            foreach (var include in includes)
            {
                dbSet = dbSet.Include(include);
            }

            return dbSet;
        }

        /// <summary>
        /// Obtaining Set&lt;T&gt;
        /// </summary>
        /// <returns>Returns set of entity, usable in your LINQ or Lambdas, with not tracking, changes will not be tracked</returns>
        public IQueryable<T> GetTableAsNoTracking()
        {
            //            return DbContextFactory.GetDbContext().Set<T>().AsNoTracking();//.AsQueryable<T>();
            DbContext dbContext = BuyerCatalogDbContextFactory.GetDbContext() as DbContext;

            if (dbContext == null)
                return null;
            else
                return dbContext.Set<T>().AsNoTracking<T>();//.AsQueryable<T>();

        }

        /// <summary>
        /// Obtaining Set&lt;T&gt; with includes
        /// </summary>
        /// <returns>Returns set of entity, usable in your LINQ or Lambdas, with not tracking, changes will not be tracked</returns>
        public IQueryable<T> GetTableAsNoTracking(params Expression<Func<T, object>>[] includes)
        {
            //     IQueryable<T> dbSet = DbContextFactory.GetDbContext().Set<T>().AsNoTracking();
            IQueryable<T> dbSet = GetTableAsNoTracking();
            foreach (var include in includes)
            {
                dbSet = dbSet.Include(include).AsNoTracking();
            }

            return dbSet;
        }


        public abstract string GetEntitySetName();

        public abstract DbQuery<T> TranslateIntoObjectQueryFrom(Query query);
        #endregion


        #region IReadOnlyRepository implementation

        #region FindBy(EntityKey Id)
        public abstract T FindBy(EntityKey Id);
        public abstract T FindBy(EntityKey Id, params Expression<Func<T, object>>[] includes);

        public abstract Task<T> FindByAsync(EntityKey Id);

        public abstract T FindBy(T entity);
        #endregion

        #region FindAll

        public IQueryable<T> FindAll()
        {
            return GetTableAsNoTracking();
        }


        public async Task<IQueryable<T>> FindAllAsync()
        {
            return await Task.Run(() => { return GetTableAsNoTracking(); }).ConfigureAwait(false);
        }

        /// <summary>
        /// FindAll paged
        /// </summary>
        /// <param name="index">strat position</param>
        /// <param name="count">count of items</param>
        /// <returns></returns>
        public IQueryable<T> FindAll(Expression<Func<T, SortableProperty>>[] orderBy, int index, int count)
        {
            return GetTableAsNoTracking().OrderBy(orderBy).Skip(index).Take(count);
        }

        /// <summary>
        /// Async version of FindAll paged
        /// </summary>
        /// <param name="index">strat position</param>
        /// <param name="count">count of items</param>
        /// <returns></returns>
        public async Task<IQueryable<T>> FindAllAsync(Expression<Func<T, SortableProperty>>[] orderBy, int index, int count)
        {
            return await Task.Run(() => { return FindAll(orderBy, index, count); }).ConfigureAwait(false);
        }


        /// <summary>
        /// Find all existing entities and includes specified entities
        /// example FindAll(includeProperties: new Expression<Func<User, object>>[] {i=>i.List_Position, i=>i.List_Department , i=>i.UserAddresses, i=>i.UserTypes.Select(j=>j.List_UserType) })
        /// </summary>
        /// <returns>List of instances of existing entities</returns>
        public IEnumerable<T> FindAll(params Expression<Func<T, object>>[] includes)
        {
            return GetTable(includes).ToList<T>();
        }

        /// <summary>
        /// Async Find all existing entities and includes specified entities
        /// example FindAll(includeProperties: new Expression<Func<User, object>>[] {i=>i.List_Position, i=>i.List_Department , i=>i.UserAddresses, i=>i.UserTypes.Select(j=>j.List_UserType) })
        /// </summary>
        /// <returns>List of instances of existing entities</returns>
        public async Task<IEnumerable<T>> FindAllAsync(params Expression<Func<T, object>>[] includes)
        {
            return await GetTable(includes).ToListAsync<T>().ConfigureAwait(false);
        }

        /// <summary>
        /// Find all existing entities and includes specified entities paged
        /// example FindAll(1,10,includeProperties: new Expression<Func<User, object>>[] {i=>i.List_Position, i=>i.List_Department , i=>i.UserAddresses, i=>i.UserTypes.Select(j=>j.List_UserType) })
        /// </summary>
        /// <returns>List of instances of existing entities</returns>
        public IEnumerable<T> FindAll(int index, int count, params Expression<Func<T, object>>[] includes)
        {
            return GetTable(includes).Skip(index).Take(count).ToList<T>();
        }

        /// <summary>
        /// Async Find all existing entities and includes specified entities paged
        /// example FindAll(1,10,includeProperties: new Expression<Func<User, object>>[] {i=>i.List_Position, i=>i.List_Department , i=>i.UserAddresses, i=>i.UserTypes.Select(j=>j.List_UserType) })
        /// </summary>
        /// <returns>List of instances of existing entities</returns>
        public async Task<IEnumerable<T>> FindAllAsync(int index, int count, params Expression<Func<T, object>>[] includes)
        {
            return await GetTable(includes).Skip(index).Take(count).ToListAsync<T>().ConfigureAwait(false);
        }
        #endregion

        #region FindBy(Query query)
        /// <summary>
        /// Find all matching query
        /// </summary>
        /// <param name="query">Query iteself</param>
        /// <returns>List of matches</returns>
        public IEnumerable<T> FindBy(Query query)
        {
            DbQuery<T> efQuery = TranslateIntoObjectQueryFrom(query);
            return efQuery.ToList<T>();
        }

        /// <summary>
        /// Async version of Find all matching query
        /// </summary>
        /// <param name="query">Query iteself</param>
        /// <returns>List of matches</returns>
        public async Task<IEnumerable<T>> FindByAsync(Query query)
        {
            DbQuery<T> efQuery = TranslateIntoObjectQueryFrom(query);
            return await efQuery.ToListAsync<T>().ConfigureAwait(false);
        }

        /// <summary>
        /// Find all matching query paged
        /// </summary>
        /// <param name="query">Query iteself</param>
        /// <returns>List of matches</returns>
        public IEnumerable<T> FindBy(Query query, int index, int count)
        {
            DbQuery<T> efQuery = TranslateIntoObjectQueryFrom(query);
            return efQuery.Skip(index).Take(count).ToList<T>();
        }

        /// <summary>
        /// Async version of Find all matching query paged
        /// </summary>
        /// <param name="query">Query iteself</param>
        /// <returns>List of matches</returns>
        public async Task<IEnumerable<T>> FindByAsync(Query query, int index, int count)
        {
            DbQuery<T> efQuery = TranslateIntoObjectQueryFrom(query);
            return await efQuery.Skip(index).Take(count).ToListAsync<T>().ConfigureAwait(false);
        }
        #endregion

        #region FindBy(Func<IQueryable<T>,IQueryable<T>> query)
        /// <summary>
        /// Find all matching Linq to entity query
        /// </summary>
        /// <param name="query">Query iteself: (x=>x.Where(y=>y.Id==1))</param>
        /// <returns>List of matches</returns>
        public IEnumerable<T> FindBy(Func<IQueryable<T>, IQueryable<T>> query)
        {
            return query(GetTable());
        }

        /// <summary>
        /// Async ver Find all matching Linq to entity query
        /// </summary>
        /// <param name="query">Query iteself: (x=>x.Where(y=>y.Id==1))</param>
        /// <returns>List of matches</returns>
        public async Task<IEnumerable<T>> FindByAsync(Func<IQueryable<T>, IQueryable<T>> query)
        {
            return await query(GetTable()).ToListAsync<T>().ConfigureAwait(false);
        }

        /// <summary>
        /// Find all matching Linq to entity query
        /// </summary>
        /// <param name="query">Query iteself: (x=>x.Where(y=>y.Id==1))</param>
        /// <returns>List of matches</returns>
        public IEnumerable<T> FindBy(Func<IQueryable<T>, IQueryable<T>> query, int index, int count)
        {
            return query(GetTable()).Skip(index).Take(count);
        }

        /// <summary>
        /// Async ver Find all matching Linq to entity query
        /// </summary>
        /// <param name="query">Query iteself: (x=>x.Where(y=>y.Id==1))</param>
        /// <returns>List of matches</returns>
        public async Task<IEnumerable<T>> FindByAsync(Func<IQueryable<T>, IQueryable<T>> query, int index, int count)
        {
            return await query(GetTable()).Skip(index).Take(count).ToListAsync<T>().ConfigureAwait(false);
        }

        /// <summary>
        /// Find all matching Linq to entity query, if you need to include navigation properties this one to use
        /// includeProperties: new Expression<Func<User, object>>[] {i=>i.List_Position, i=>i.List_Department , i=>i.UserAddresses, i=>i.UserTypes.Select(j=>j.List_UserType) }
        /// </summary>
        /// <param name="query">Query iteself: (x=>x.Where(y=>y.Id==1))</param>
        /// <param name="includes">string name of entity you want include</param>
        /// <returns>List of matches</returns>
        public IEnumerable<T> FindBy(Func<IQueryable<T>, IQueryable<T>> query, params Expression<Func<T, object>>[] includes)
        {
            return query(GetTable(includes));
        }

        /// <summary>
        /// Async version of Find all matching Linq to entity query, if you need to include navigation properties this one to use
        /// includeProperties: new Expression<Func<User, object>>[] {i=>i.List_Position, i=>i.List_Department , i=>i.UserAddresses, i=>i.UserTypes.Select(j=>j.List_UserType) }
        /// </summary>
        /// <param name="query">Query iteself: (x=>x.Where(y=>y.Id==1))</param>
        /// <param name="includes">Expression<Func<T, object>>[] {x=&lt;x.Include}</param>
        /// <returns>List of matches</returns>
        public async Task<IEnumerable<T>> FindByAsync(Func<IQueryable<T>, IQueryable<T>> query, params Expression<Func<T, object>>[] includes)
        {
            return await query(GetTable(includes)).ToListAsync<T>().ConfigureAwait(false);
        }

        /// <summary>
        /// Paged Find all matching Linq to entity query, if you need to include navigation properties this one to use
        /// includeProperties: new Expression<Func<User, object>>[] {i=>i.List_Position, i=>i.List_Department , i=>i.UserAddresses, i=>i.UserTypes.Select(j=>j.List_UserType) }
        /// </summary>
        /// <param name="query">Query iteself: (x=>x.Where(y=>y.Id==1))</param>
        /// <param name="includes">string name of entity you want include</param>
        /// <returns>List of matches</returns>
        public IEnumerable<T> FindBy(Func<IQueryable<T>, IQueryable<T>> query, int index, int count, params Expression<Func<T, object>>[] includes)
        {
            return query(GetTable(includes)).Skip(index).Take(count);
        }

        /// <summary>
        /// Async version of paged Find all matching Linq to entity query, if you need to include navigation properties this one to use
        /// includeProperties: new Expression<Func<User, object>>[] {i=>i.List_Position, i=>i.List_Department , i=>i.UserAddresses, i=>i.UserTypes.Select(j=>j.List_UserType) }
        /// </summary>
        /// <param name="query">Query iteself: (x=>x.Where(y=>y.Id==1))</param>
        /// <param name="includes">string name of entity you want include</param>
        /// <returns>List of matches</returns>
        public async Task<IEnumerable<T>> FindByAsync(Func<IQueryable<T>, IQueryable<T>> query, int index, int count, params Expression<Func<T, object>>[] includes)
        {
            return await query(GetTable(includes)).Skip(index).Take(count).ToListAsync<T>().ConfigureAwait(false);
        }
        #endregion

        #region FindBy(IDictionary<string, ParameterDescriptor> parameters)
        /// <summary>
        /// Allowing usage of parameters for stored procedure calls
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public virtual T FindBy(IDictionary<string, ParameterDescriptor> parameters)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Async version of Allowing usage of parameters for stored procedure calls
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public async virtual Task<T> FindByAsync(IDictionary<string, ParameterDescriptor> parameters)
        {
            return await new Task<T>(() =>
            {
                return default(T);
            }).ConfigureAwait(false);
        }

        #endregion

        #region FindBy(Expression<Func<T,bool>> whereClause)
        /// <summary>
        /// Find all matching Linq to entity query
        /// </summary>
        /// <param name="query">Query iteself: (x=>x.Where(y=>y.Id==1))</param>
        /// <returns>List of matches</returns>
        public IEnumerable<T> FindBy(Expression<Func<T, bool>> whereClause)
        {
            return GetTable().Where(whereClause);
        }

        /// <summary>
        /// Async ver Find all matching Linq to entity query
        /// </summary>
        /// <param name="query">Query iteself: (x=>x.Where(y=>y.Id==1))</param>
        /// <returns>List of matches</returns>
        public async Task<IEnumerable<T>> FindByAsync(Expression<Func<T, bool>> whereClause)
        {
            return await GetTable().Where(whereClause).ToListAsync<T>().ConfigureAwait(false);
        }

        /// <summary>
        /// Find all matching Linq to entity query
        /// </summary>
        /// <param name="query">Query iteself: (x=>x.Where(y=>y.Id==1))</param>
        /// <returns>List of matches</returns>
        public IQueryable<T> FindBy(Expression<Func<T, bool>> whereClause, Expression<Func<T, SortableProperty>>[] orderBy, int index, int count)
        {
            return GetTableAsNoTracking().Where(whereClause).OrderBy(orderBy).Skip(index).Take(count);
        }

        /// <summary>
        /// Async ver Find all matching Linq to entity query
        /// </summary>
        /// <param name="query">Query iteself: (x=>x.Where(y=>y.Id==1))</param>
        /// <returns>List of matches</returns>
        public async Task<IQueryable<T>> FindByAsync(Expression<Func<T, bool>> whereClause, Expression<Func<T, SortableProperty>>[] orderBy, int index, int count)
        {
            return await Task.Run(() => { return FindBy(whereClause, orderBy, index, count); }).ConfigureAwait(false);
        }

        /// <summary>
        /// Find all matching Linq to entity query, if you need to include navigation properties this one to use
        /// includeProperties: new Expression<Func<User, object>>[] {i=>i.List_Position, i=>i.List_Department , i=>i.UserAddresses, i=>i.UserTypes.Select(j=>j.List_UserType) }
        /// </summary>
        /// <param name="query">Query iteself: (x=>x.Where(y=>y.Id==1))</param>
        /// <param name="includes">string name of entity you want include</param>
        /// <returns>List of matches</returns>
        public IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includes)
        {
            return GetTable(includes).Where(predicate);
        }

        /// <summary>
        /// Async version of Find all matching Linq to entity query, if you need to include navigation properties this one to use
        /// includeProperties: new Expression<Func<User, object>>[] {i=>i.List_Position, i=>i.List_Department , i=>i.UserAddresses, i=>i.UserTypes.Select(j=>j.List_UserType) }
        /// </summary>
        /// <param name="query">Query iteself: (x=>x.Where(y=>y.Id==1))</param>
        /// <param name="includes">Expression<Func<T, object>>[] {x=&lt;x.Include}</param>
        /// <returns>List of matches</returns>
        public async Task<IEnumerable<T>> FindByAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includes)
        {
            return await GetTable(includes).Where(predicate).ToListAsync<T>().ConfigureAwait(false);
        }

        /// <summary>
        /// Paged Find all matching Linq to entity query, if you need to include navigation properties this one to use
        /// includeProperties: new Expression<Func<User, object>>[] {i=>i.List_Position, i=>i.List_Department , i=>i.UserAddresses, i=>i.UserTypes.Select(j=>j.List_UserType) }
        /// </summary>
        /// <param name="query">Query iteself: (x=>x.Where(y=>y.Id==1))</param>
        /// <param name="includes">string name of entity you want include</param>
        /// <returns>List of matches</returns>
        public IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate, int index, int count, params Expression<Func<T, object>>[] includes)
        {
            return GetTable(includes).Where(predicate).Skip(index).Take(count);
        }

        /// <summary>
        /// Async version of paged Find all matching Linq to entity query, if you need to include navigation properties this one to use
        /// includeProperties: new Expression<Func<User, object>>[] {i=>i.List_Position, i=>i.List_Department , i=>i.UserAddresses, i=>i.UserTypes.Select(j=>j.List_UserType) }
        /// </summary>
        /// <param name="query">Query iteself: (x=>x.Where(y=>y.Id==1))</param>
        /// <param name="includes">string name of entity you want include</param>
        /// <returns>List of matches</returns>
        public async Task<IEnumerable<T>> FindByAsync(Expression<Func<T, bool>> predicate, int index, int count, params Expression<Func<T, object>>[] includes)
        {
            return await GetTable(includes).Where(predicate).Skip(index).Take(count).ToListAsync<T>().ConfigureAwait(false);
        }

        /// <summary>
        /// Paged Find all matching Linq to entity query, if you need to include navigation properties this one to use
        /// includeProperties: new Expression<Func<User, object>>[] {i=>i.List_Position, i=>i.List_Department , i=>i.UserAddresses, i=>i.UserTypes.Select(j=>j.List_UserType) }
        /// </summary>
        /// <param name="query">Query iteself: (x=>x.Where(y=>y.Id==1))</param>
        /// <param name="includes">string name of entity you want include</param>
        /// <returns>List of matches</returns>
        public IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate, Expression<Func<T, SortableProperty>>[] orderBy, int index, int count, params Expression<Func<T, object>>[] includes)
        {
            return GetTable(includes).Where(predicate).OrderBy(orderBy).Skip(index).Take(count);
        }

        /// <summary>
        /// Async version of paged Find all matching Linq to entity query, if you need to include navigation properties this one to use
        /// includeProperties: new Expression<Func<User, object>>[] {i=>i.List_Position, i=>i.List_Department , i=>i.UserAddresses, i=>i.UserTypes.Select(j=>j.List_UserType) }
        /// </summary>
        /// <param name="query">Query iteself: (x=>x.Where(y=>y.Id==1))</param>
        /// <param name="includes">string name of entity you want include</param>
        /// <returns>List of matches</returns>
        public async Task<IEnumerable<T>> FindByAsync(Expression<Func<T, bool>> predicate, Expression<Func<T, SortableProperty>>[] orderBy, int index, int count, params Expression<Func<T, object>>[] includes)
        {
            return await GetTable(includes).Where(predicate).OrderBy(orderBy).Skip(index).Take(count).ToListAsync<T>().ConfigureAwait(false);
        }
        #endregion


        /// <summary>
        /// something like that
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        public IEnumerable<T> FindBy(IDbSet<T> entity, Func<IQueryable<T>, IQueryable<T>> query)
        {
            return query(entity);
        }
        #endregion

        #region IUnitOfWorkReposioty
        public virtual void PersistCreationOf(IAggregateRoot entity)
        {
            PersistCreationOf(entity as T);
        }

        public virtual void PersistUpdateOf(IAggregateRoot entity)
        {
            PersistUpdateOf(entity as T);
        }

        public virtual void PersistDeletionOf(IAggregateRoot entity)
        {
            if (entity as object != null)
                PersistDeletionOf(entity as T);
        }

        public virtual void PersistDeletionOf(object predicate)
        {
            PersistDeletionOf(predicate as Expression<Func<T, bool>>);
        }
        #endregion

        public virtual void PersistUpdateOf(T entity)
        {
        }

        public virtual void PersistCreationOf(T entity)
        {
            DbContext dbContext = BuyerCatalogDbContextFactory.GetDbContext() as DbContext;
            try
            {
                dbContext.Set<T>().Add(entity);
                //dbContext.Set<T>().Add(entity);
                //dbContext.Entry<T>(entity).State= EntityState.Detached;
                //DbContextFactory.GetDbContext().Set<T>().Add(entity);
            }
            catch(Exception ex)
            {
#if DEBUG
                if (Console.IsOutputRedirected)
                    Debug.WriteLine(ex.ToString());
#endif

            }
        }

        public virtual void PersistDeletionOf(T entity)
        {
            if (entity as object != null)
            {
                var t = BuyerCatalogDbContextFactory.GetDbContext();
                DbContext dbContext = t as DbContext;
                //var u= dbContext.Entry<T>(entity).State;
                try
                {
                    var x = dbContext.Set<T>();
                    x.Attach(entity);
                    x.Remove(entity);
                }
                catch (Exception ex)
                {
#if DEBUG
                    if (Console.IsOutputRedirected)
                        Debug.WriteLine(ex.ToString());
#endif
                }
            }
            //DbContextFactory.GetDbContext().Set<T>().Remove(entity);
        }

        public virtual void PersistDeletionOf(Expression<Func<T, bool>> predicate)
        {
            if (predicate as object != null)
            {
                DbContext dbContext = BuyerCatalogDbContextFactory.GetDbContext() as DbContext;

                dbContext.Set<T>().RemoveRange(dbContext.Set<T>().Where(predicate));

            }
            //DbContextFactory.GetDbContext().Set<T>().RemoveRange(DbContextFactory.GetDbContext().Set<T>().Where(predicate));
        }

    }
    }
