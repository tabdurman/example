﻿using CapSoft.Infrastructure.Domain;
using System;
using System.ComponentModel.Composition;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Infrastructure.Interception;

namespace iShopBest.Buyer.Repository
{
    [Export(typeof(IStorageContext))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    [ExportMetadata("ContextName", "BuyerCatalog")]
    public partial class BuyerCatalogEntities : IDisposable, IStorageContext
    {

        #region IDisposable
        /* IDisposable */
        private bool _disposed;
        private object _threadSafety = new object();
        public bool Disposed { get { return _disposed; } }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            lock (_threadSafety)
            {
                if (!_disposed)
                {
                    _disposed = true;
                    if (disposing)
                    {
                        // TODO: Dispose object state
                        base.Dispose(disposing);
                    }
                }

            }
        }
        /* IDisposable */
        #endregion


        public BuyerCatalogEntities(DbConnection existingConnection, DbCompiledModel model, bool contextOwnsConnection)
        { }

        public BuyerCatalogEntities()
            : this("name=BuyerCatalogEntities")
        {
        }

        public BuyerCatalogEntities(string connectionString)
            : base(connectionString)
        {
            // without this EF6 raises exception
            var type = typeof(System.Data.Entity.SqlServer.SqlProviderServices);

            // disable tables recreation
            //Database.SetInitializer<iShopBestEntities>(null);
            Database.SetInitializer<BuyerCatalogEntities>(new BuyerCatalogEntitiesInitializer());
            DbInterception.Add(new FtsInterceptor());

            this.SetCommandTimeOut(300);
            this.Configuration.ProxyCreationEnabled = false;
            this.Configuration.LazyLoadingEnabled = true;
        }

        /// <summary>
        /// Allows setting of command timeout for connection
        /// </summary>
        /// <param name="Timeout"></param>
        public void SetCommandTimeOut(int Timeout)
        {
            var objectContext = (this as IObjectContextAdapter).ObjectContext;
            objectContext.CommandTimeout = Timeout;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }

    }
}
