﻿using CapSoft.Infrastructure.Domain;
using System;
using System.ComponentModel.Composition;

namespace iShopBest.Buyer.Repository
{
    [Export(typeof(IRepositoryInitializer))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    [ExportMetadata("InitializerName", "BuyerCatalog")]
    public class BuyerCatalogRepositoryInitializer : IRepositoryInitializer
    {
        public void Init(object initParam)
        {
            try
            {
                string connectionString = initParam as string;
                if (connectionString != null)
                    BuyerCatalogDbContextFactory.Init(connectionString);
            }
            catch (Exception)
            {
            }
        }
    }
}
