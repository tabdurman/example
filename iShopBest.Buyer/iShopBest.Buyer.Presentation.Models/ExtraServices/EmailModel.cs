﻿using System.Runtime.Serialization;

namespace iShopBest.Buyer.Presentation.Models.ExtraServices
{
    [DataContract]
    public class EmailModel
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string PhoneNumber { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Subject { get; set; }
        [DataMember]
        public string Message { get; set; }
    }
}
