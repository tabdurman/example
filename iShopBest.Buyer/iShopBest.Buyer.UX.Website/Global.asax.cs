﻿using iShopBest.Buyer.AppTools;
using iShopBest.Buyer.Presentation.Api.Config;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace iShopBest.Buyer.UX.Website
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Bootstrapper.Init("iShopBest.Buyer.App", "name=BuyerCatalogEntities");

            ModelMapping.InitializeMappings(); //initialize webapi class mappingd

            // rquire HTTPS for all endpoints
            GlobalFilters.Filters.Add(new RequireHttpsAttribute());

            AreaRegistration.RegisterAllAreas();

            GlobalConfiguration.Configure(WebApiConfig.Register);

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
