﻿using CapSoft.Identity.Presentation.Mvc;
using System.Web.Mvc;

namespace iShopBest.Buyer.UX.Website.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
    }
}
