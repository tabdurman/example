﻿(function (angular) {

    var app;

    try {
        app = angular.module('rootApp');
    }
    catch (err)
    {
        angular.module("rootApp", ["ui.router", "ngResource","ngSanitize"]);
        app = angular.module('rootApp');
    }

        app.controller('RootAppCtrl',
            ['$scope','$rootScope','$stateParams','$http', '$log', '$filter','$sce','infrastructure','shoppingCart','runningMode',
                function ($scope, $rootScope, $stateParams, $http, $log,$filter,$sce , infrastructure, shoppingCart, runningMode) {
                    var that = this;
                    //infrastructure.clearError();
                    $scope.WaitMessage = infrastructure.waitMessage.text;

                    $scope.config = {
                        Views: {
                            LeftBar: {
                                visible: true,
                                width: 0
                            },
                            MainContent: {
                                visible: true,
                                width: 12
                            },
                            RightBar: {
                                visible: false,
                                width: 0
                            },
                        },
                        Data: {
                            FirstLoad: true,
                            CategoryPath: {},
                            TopLevel: true,
                            BottomLevel: false,
                            ShowCategories: true,
                            ShowProducts: false
                        },
                        State: {
                            InitializationCompleted: false,
                            ShowList: true,
                            Current: 'home'
                        },
                        Infrastructure: infrastructure,
                        ShoppingCart: shoppingCart
                    };

                    //$scope.shoppingCartLink=$sce.trustAsHtml('<i class="glyphicon glyphicon-shopping-cart"></i> Cart ('+$scope.config.ShoppingCart.shoppingCartItems.length+')</a>');
                    //$scope.shoppingCartLink = $sce.trustAsHtml('<i class="glyphicon glyphicon-shopping-cart"></i> Cart ({{config.ShoppingCart.shoppingCartItems.length}})</a>');
                    //$scope.config.Data.TopLevel = false;
                    //$scope.config.Data.TopLevel = true;

                    //$scope.config.Events.OnReloadCategories = true;

                    $rootScope.$on('$stateChangeStart',
                        function (event, toState, toParams, fromState, fromParams) {
                            // do something

                            $scope.config.State.Current = toState.name;
                            if (typeof toParams.keywords !== 'undefined')
                                $scope.searchKeywords = toParams.keywords;

                            if (toState.name == "home") {
                                $scope.config.Data.FirstLoad = false;

                                $scope.config.Data.CurrentCategoryId = toParams.rootCategoryId;
                                $scope.config.Data.CurrentKeywords = toParams.keywords;
                                $scope.searchKeywords = toParams.keywords
                                //$scope.config.Views.MainContent.width = 9;
                                $scope.config.Views.MainContent.width = 12;
                                infrastructure.setSupplierId(0);
                            }
                            else if (toState.name == "home.product") {
                                $scope.config.Data.FirstLoad = false;

                                $scope.config.Data.CurrentProductId = toParams.productId;
                                $scope.config.Views.MainContent.width = 12;

                            }
                            else if (toState.name == "home.cart") {
                                $scope.config.Views.MainContent.width = 12;
                            }
                            else if (toState.name == "home.results") {
                                $scope.config.Views.MainContent.width = 12;
                            }

                        });

                    $rootScope.$on('$stateNotFound',
                        function (event, unfoundState, fromState, fromParams) {
                            // do something
                            event.preventDefault();
                        });


                    $scope.config.State.InitializationCompleted = true;
                    $scope.localMode=false;

                    if (runningMode == 'LOCAL')
                        $scope.localMode = true;

                    $scope.$watch(function () {
                        return infrastructure.validationErrors
                    }, function (newValue, oldValue, scope) {
                        //console.log("newValue=" + JSON.stringify(newValue) + " oldValue=" + JSON.stringify(oldValue));
                        $scope.validationErrors = newValue;
                    });

                    $scope.$watch(function () {
                        return infrastructure.waitMessage.text;
                    }, function (newValue, oldValue, scope) {
                        //console.log("newValue=" + JSON.stringify(newValue) + " oldValue=" + JSON.stringify(oldValue));
                        $scope.WaitMessage = newValue;
                    });
                }
            ]);
})(window.angular);