﻿(function (angular) {
    var app;

    try {
        app = angular.module('rootApp');
    }
    catch (err)
    {
        angular.module("rootApp", ["ui.router", "ngResource"]);
        app = angular.module('rootApp');
    }

    app.controller("SearchBoxCtrl", ['$scope', '$rootScope','$state','infrastructure', function ($scope, $rootScope,$state,infrastructure) {

        $scope.onSearchClick = function () {
            // apply keywords on click
            if ($scope.searchKeywords.length < 3)
                infrastructure.validationErrors = { status: "Search parameters too short.", data: { Message: "Please enter better keywords." } }
            else {
                infrastructure.waitMessage.text = "Searching products";
                $state.go('home', { rootCategoryId: $scope.config.Data.CurrentCategoryId, keywords: $scope.searchKeywords });
            }

        };
    }]);

})(window.angular);