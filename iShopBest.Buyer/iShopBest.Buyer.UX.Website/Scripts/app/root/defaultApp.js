﻿(function (angular) {

'use strict';

    try {
        app = angular.module('rootApp');
    }
    catch (err) {
        angular.module("rootApp", ["ui.router", "ngResource","ngSanitize"]);

        var app = angular.module("rootApp");

        app.factory('quickInterceptor', function ($q, $injector) {

            return {
                'request': function (config) {
                    $('#grayout').show();
                    $('#loading').show();
                    return config;
                },
                'requestError': function (rejection) {
                    var $http = $injector.get('$http');
                    if ($http.pendingRequests.length < 1) {
                        $('#loading').hide();
                        $('#grayout').hide();
                    }
                    return $q.reject(rejection);
                },
                'response': function (response) {
                    var $http = $injector.get('$http');
                    if ($http.pendingRequests.length < 1) {
                        $('#loading').hide();
                        $('#grayout').hide();
                    }
                    return response;
                },
                'responseError': function (rejection) {
                    var $http = $injector.get('$http');
                    if ($http.pendingRequests.length < 1) {
                        $('#loading').hide();
                        $('#grayout').hide();
                    }
                    return $q.reject(rejection);

                }
            };

        });

        app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function ($stateProvider, $urlRouterProvider, $httpProvider) {
            $httpProvider.interceptors.push('quickInterceptor');
        }]);

        app.factory('infrastructure', ['$rootScope', '$filter', function ($rootScope, $filter) {
            return {};
        }]);
    }

})(window.angular);