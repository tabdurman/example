﻿ProductDetail = {
    "$id": "2",
    "ProductId": 1000084001029932,
    "ReferenceId": 269597,
    "ManufacturerPartNumber": "102993",
    "ProductTitle": "PAPER,CONST,9X12,HOLIDYRD",
    "ShortDescription": "Tru-Ray Construction Paper, 76 Lbs., 9 X 12, Holiday Red, 50 Sheets/pack",
    "LongDescription": "Heavyweight 100% vat-dyed sulphite with longer, stronger fibers. Tough enough to take scoring, folding and curling without cracking or tearing. Superior fade-resistance keeps projects brighter longer. Acid-free for archival quality.",
    "Keywords": "00084001029932 Pacon Corporation 102993 PAPER,CONST,9X12,HOLIDYRD",
    "GTIN": "00084001029932",
    "Dimensions": "12L x 9W x 1H",
    "Weight": 0.94,
    "ListPrice": 3.59,
    "Rating": null,
    "ManufacturerName": "Pacon Corporation",
    "Unit": "PK",
    "CountryOfOrigin": "US",
    "UNSPSC": 14111610,
    "ImageUrl": "/api/Products/Images/1000084001029932",
    "PriceMin": 1.39,
    "PriceMax": 1.72,
    "QtyRequested": 1
};

Match = {
    "$id": "2",
    "GTINPLUS": 1000084001029932,
    "ReferenceId": 269597,
    "SupplierId": 8,
    "SupplierName": "United Stationers, Inc.",
    "SupplierRating": 5,
    "LeadTime": 2,
    "EffectiveDate": "2016-10-01T00:00:00",
    "ExpirationDate": "2017-01-01T00:00:00",
    "MinimumQty": 1,
    "Sku": "PAC102993",
    "Cost": 1.72,
    "PackSku": null,
    "PackQty": null,
    "PackPrice": null,
    "PackRequired": false,
    "Unit": "PK",
    "PackUnit": null,
    "CountryOfOrigin": "US",
    "TAA": true,
    "LowestPrice": null,
    "LowestTAAPrice": null,
    "Id": 397884
};

shoppingCartItem = {
    "ProductId": 1000084001029932,
    "ReferenceId": 269597,
    "ManufacturerPartNumber": "102993",
    "ProductTitle": "PAPER,CONST,9X12,HOLIDYRD",
    "ShortDescription": "Tru-Ray Construction Paper, 76 Lbs., 9 X 12, Holiday Red, 50 Sheets/pack",
    "LongDescription": "Heavyweight 100% vat-dyed sulphite with longer, stronger fibers. Tough enough to take scoring, folding and curling without cracking or tearing. Superior fade-resistance keeps projects brighter longer. Acid-free for archival quality.",
    "Keywords": "",
    "GTIN": "00084001029932",
    "Dimensions": "12L x 9W x 1H",
    "Weight": 0.94,
    "ListPrice": 3.59,
    "Rating": null,
    "ManufacturerName": "Pacon Corporation",
    "Unit": "PK",
    "CountryOfOrigin": "US",
    "UNSPSC": 14111610,
    "ImageUrl": "/api/Products/Images/1000084001029932",
    "PriceMin": 1.39,
    "PriceMax": 1.72,
    "Quantity": 1,
    "DestZipCode": "33065",
    "ProductRating": 0,
    "SupplierId": 8,
    "SupplierName": "United Stationers, Inc.",
    "SupplierRating": 5,
    "Sku": "PAC102993",
    "Coo": "US",
    "WarehouseId": 81,
    "WarehouseName": "ORL",
    "WarehouseRating": null,
    "RequestedQuantity": 1,
    "QuantityAllocated": 1,
    "Price": 1.72,
    "EstimatedFreight": 1,
    "Distance": 167.80667210769715,
    "DeliveryDays": 1,
    "Weighter": 1.3,
    "WeightedCost": 2.236
};

preference = {
    "name": "ProductRatingImportance",
    "value": 30,
    "type": "slider",
    "minValue": 0,
    "maxValue": 100,
    "text": "Product Rating"
};

vendorSelectionQueryRequest=
{
    "IncludeInventory": true,
	"Preferences": 
    [
		{
		    "Name": "ProductRatingImportance",
		    "Value": "30"
		}, 
		{
		    "Name": "SupplierRatingImportance",
		    "Value": "30"
		}, 
		{
		    "Name": "WarehouseRatingImportance",
		    "Value": "30"
		}, 
		{
		    "Name": "DeliverySpeedImportance",
		    "Value": "30"
		}, 
		{
		    "Name": "FreightCostImportance",
		    "Value": "30"
		}, 
		{
		    "Name": "PackagesCountImportance",
		    "Value": "30"
		}, 
		{
		    "Name": "SingleSupplierImportance",
		    "Value": "30"
		} 
    ], 
	"ShoppingCart": 
    [
		{
		    "DestZipCode": "33065",
		    "GtinPlus": "1000084001029932",
		    "Quantity": 50
		} 
    ] 
}

