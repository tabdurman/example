﻿(function (angular) {

    var app = angular.module('rootApp');

    app.factory('vendorSelectionRepository', function ($resource) {
        var baseUrl = window.appGlobals.apiLocation + 'VendorSelection';
        /*
        var params = {
            Preferences: [{Name:"", Value:""}],
            ShoppingCart: [{GtinPlus:"", Quantity:0, DestZipCode:""}]
        }
        */

        return $resource(baseUrl , { },
            {
                'run': {
                    url: baseUrl + '/Run',
                    method: "POST",
                    isArray: false
                },
                'getMatches': {
                    url: baseUrl + '/Matches',
                    method: "POST",
                    isArray: false
                }

            });
    });

})(window.angular);