﻿(function (angular) { 

    var app = angular.module('rootApp');

    app.factory('categoriesRepository', function ($resource) {
        var baseUrl = window.appGlobals.apiLocation + 'Categories';

        return $resource(baseUrl + '/:rootCategoryId', { rootCategoryId: '@rootCategoryId' },
            {
                'findBy': {
                    method: "GET"
                    , isArray: false
                }
            });
    });

})(window.angular);