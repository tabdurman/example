﻿(function (angular) {
    angular
        .module("rootApp").factory('infrastructure', ['$rootScope', '$filter', function ($rootScope, $filter) {
            var baseValue = 0;

            var vendorSelectionParamsWithAuth = {
                VendorSelectionParams: {
                    ItemBuyingRequirements: [
                        //       { GTINPLUS: 1000076174104998, Quantity: 1, DestinationZip: "33065" }
                    ],
                    Preferences: {
                        ProductRatingImportance: "30",
                        VendorRatingImportance: "30",
                        InventoryLocationRatingImportance: "30",
                        DeliverySpeedImportance: "30",
                        FreightCostImportance: "30",
                        LessPackagesImportance: "30",
                        SkipStockCheck: true
                    }
                },
                AuthData: {
                    UserName: '',
                    Password: ''
                }
            };

            var supplierId = 0;

            var VendorSelectionResults = null;

            var WaitMessage = "Please Wait";

            var SetError = function (errorObject) {
                ValidationErrors = errorObject
            };

            var infrastructure= {
                /*'nullError': {
                    config: null,
                    data: null,
                    headers: null,
                    status: null,
                    statusText: null

                },
                'buildError': function (configParam, dataParam, headersParam, statusParam, statusTextParam) {
                    return {
                        config: configParam,
                        data: dataParam,
                        headers: headersParam,
                        status: statusParam,
                        statusText: statusTextParam
                    }

                },
                'error': function (errorObject) {
                    var errorObj = errorObject;
                    return errorObj;
                },
                'range': function (start, end) {
                    var ret = [];
                    if (!end) {
                        end = start;
                        start = 0;
                    }
                    for (var i = start; i < end; i++) {
                        ret.push(i);
                    }
                    return ret;
                },
                */
                'vendorSelectionParams': vendorSelectionParamsWithAuth.VendorSelectionParams,
                'vendorSelectionParamsWithAuth': vendorSelectionParamsWithAuth,
                'vendorSelectionResults': VendorSelectionResults,
                waitMessage: { text: "Please Wait" },
                validationErrors: undefined,
                setSupplierId: function (supId) {
                    supplierId = supId;
                },
                getSupplierId: function () {
                    return supplierId;
                }
            }

            return infrastructure;
    }]);
})(window.angular);