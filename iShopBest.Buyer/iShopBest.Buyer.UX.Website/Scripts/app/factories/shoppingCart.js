﻿(function (angular) {
    angular
        .module("rootApp").factory('shoppingCart', ['$rootScope', '$filter','$cookies', function ($rootScope, $filter,$cookies) {

            var shoppingCartItems = [];
            //var shoppingCartItems = $cookies.getObject("shoppingCartItems");
            //if (shoppingCartItems == null)
            //    shoppingCartItems = [];

            var shoppingCartChanged = [{ Dirty: false }];

            //method can be 'Update' will increase quantity or 'Replace' will set quantity
            var addItemToCart = function (tempCartItem,method) {

                if (typeof method === 'undefined')
                    method = 'Update';

                tempCartItem.Keywords = '';
                tempCartItem.$id = undefined;

                var cartItem = {
                    "ShoppingCartId":  tempCartItem.ShoppingCartId,
                    "ProductId": tempCartItem.ProductId,
                    "ReferenceId": tempCartItem.ReferenceId,
                    "ManufacturerId": tempCartItem.ManufacturerId,
                    "ManufacturerName": tempCartItem.ManufacturerName,
                    "ManufacturerPartNumber": tempCartItem.ManufacturerPartNumber,
                    "ManufacturerRating": tempCartItem.ManufacturerRating,
                    "ProductTitle": tempCartItem.ProductTitle,
                    "ShortDescription": tempCartItem.ShortDescription,
                    "LongDescription": tempCartItem.LongDescription,
                    "Keywords": "",
                    "GTIN": tempCartItem.GTIN,
                    "Dimensions": tempCartItem.Dimensions,
                    "Weight": tempCartItem.Weight,
                    "ListPrice": tempCartItem.ListPrice,
                    "ProductRating": tempCartItem.ProductRating,
                    "Unit": tempCartItem.Unit,
                    "CountryOfOrigin": tempCartItem.CountryOfOrigin,
                    "UNSPSC": tempCartItem.UNSPSC,
                    "ImageUrl": tempCartItem.ImageUrl,
                    "PriceMin": tempCartItem.PriceMin,
                    "PriceMax": tempCartItem.PriceMax,
                    "Quantity": tempCartItem.Quantity,
                    "DestZipCode": tempCartItem.DestZipCode,
                    "SupplierId": tempCartItem.SupplierId,
                    "SupplierName":  tempCartItem.SupplierName,
                    "SupplierRating":  tempCartItem.SupplierRating,
                    "LeadTime":  tempCartItem.LeadTime,
                    "EffectiveDate":  tempCartItem.EffectiveDate,
                    "ExpirationDate":  tempCartItem.ExpirationDate,
                    "MinimumQty":  tempCartItem.MinimumQty,
                    "Sku":  tempCartItem.Sku,
                    "Cost":  tempCartItem.Cost,
                    "PackSku":  tempCartItem.PackSku,
                    "PackQty":  tempCartItem.PackQty,
                    "PackPrice":  tempCartItem.PackPrice,
                    "PackRequired":  tempCartItem.PackRequired,
                    "SupplierUnit":  tempCartItem.SupplierUnit,
                    "SupplierPackUnit":  tempCartItem.SupplierPackUnit,
                    "SupplierCountryOfOrigin":  tempCartItem.SupplierCountryOfOrigin,
                    "TAA":  tempCartItem.TAA,
                    "WarehouseId":  tempCartItem.WarehouseId,
                    "WarehouseName":  tempCartItem.WarehouseName,
                    "WarehouseRating":  tempCartItem.WarehouseRating,
                    "AllocatedQuantity":  tempCartItem.AllocatedQuantity,
                    "DeliveryDays":  tempCartItem.DeliveryDays,
                    "EstimatedFreight":  tempCartItem.EstimatedFreight,
                    "Score":  tempCartItem.Score,
                    "Id":  tempCartItem.Id
                };

                var found = $filter('filter')(shoppingCartItems, { ProductId: cartItem.ProductId }, true);
                if (found != null && found.length > 0) {
                    if (method == 'Add')
                        shoppingCartItems[shoppingCartItems.indexOf(found[0])].Quantity = parseInt(shoppingCartItems[shoppingCartItems.indexOf(found[0])].Quantity) + parseInt(cartItem.Quantity);

                    if (shoppingCartItems[shoppingCartItems.indexOf(found[0])].Quantity == 'NaN' || method=='Replace')
                        shoppingCartItems[shoppingCartItems.indexOf(found[0])].Quantity = parseInt(cartItem.Quantity);
                }
                else {
                    shoppingCartItems.push(cartItem);
                }
                /*
                $cookies.remove("shoppingCartItems");
                if (shoppingCartItems.length > 0)
                    $cookies.putObject("shoppingCartItems", angular.copy(shoppingCartItems));
                */
                shoppingCartChanged[0].Dirty = true;
            };

            var deleteItemFromCart = function (id) {
                if (shoppingCartItems && shoppingCartItems.length > 0) {
                    var found = $filter('filter')(shoppingCartItems, { ProductId: id }, true);
                    if (found != null && found.length > 0) {
                        shoppingCartItems.splice(shoppingCartItems.indexOf(found[0]), 1);
                    }
                    /*
                    $cookies.remove("shoppingCartItems");
                    if (shoppingCartItems.length>0)
                        $cookies.putObject("shoppingCartItems", angular.copy(shoppingCartItems));
                    */
                    shoppingCartChanged[0].Dirty = true;
                }
            };

            var getOrderTotal = function () {
                var orderTotal = 0.000;
                angular.forEach(shoppingCartItems, function (val, key) {
                    orderTotal += val.PriceMin * val.Quantity;
                });
                return orderTotal;
            };

            var preferences= [
                     { name: 'ProductRatingImportance', value: 30, type: "slider", minValue: 0, maxValue: 100, text: 'Product Rating' },
                     { name: 'SupplierRatingImportance', value: 30, type: "slider", minValue: 0, maxValue: 100, text: 'Supplier Rating' },
                     { name: 'WarehouseRatingImportance', value: 30, type: "slider", minValue: 0, maxValue: 100, text: 'Warehouse Rating' },
                     { name: 'DeliverySpeedImportance', value: 30, type: "slider", minValue: 0, maxValue: 100, text: 'Delivery Speed' },
                     { name: 'FreightCostImportance', value: 30, type: "slider", minValue: 0, maxValue: 100, text: 'Freight Cost' },
                     { name: 'PackagesCountImportance', value: 30, type: "slider", minValue: 0, maxValue: 100, text: 'Packages Count' },
                     { name: 'SingleSupplierImportance', value: 30, type: "slider", minValue: 0, maxValue: 100, text: 'Single Supplier' }
            ];

            var shoppingCart = {
                shoppingCartItems: shoppingCartItems,
                'shoppingCartDirty': shoppingCartChanged[0],

                'clearShoppingCartChanged': function () {
                    shoppingCartChanged[0].Dirty = false;
                },
                'addToCart': function (cartItem, quantity, destZipCode, method) {
                    if (typeof quantity !== 'undefined')
                        cartItem.Quantity = quantity;
                    if (typeof destZipCode !== 'undefined')
                        cartItem.DestZipCode = destZipCode;
                    if (typeof method === 'undefined')
                        method='Add';
                    
                    addItemToCart(cartItem, method);
                },
                'deleteFromCart': function (GtinPlus) {
                    deleteItemFromCart(GtinPlus);
                },
                'clearCart': function () {
                    shoppingCartItems = [];
                },
                'orderTotal': getOrderTotal,
                'vendorSelectionPreferences': preferences,
                'name': "New Cart",
                'overrideExisting': false

            };

            return shoppingCart;

    }]);
})(window.angular);