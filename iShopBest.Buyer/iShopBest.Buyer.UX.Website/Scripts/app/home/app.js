﻿(function (angular) {

    var app = angular.module("rootApp");
    app.value('runningMode', 'LOCAL');

    app.factory('quickInterceptor', function ($q, $injector) {

        return {
            'request': function (config) {
                $('#grayout').show();
                $('#loading').show();
                return config;
            },
            'requestError': function (rejection) {
                var $http = $injector.get('$http');
                if ($http.pendingRequests.length < 1) {
                    $('#loading').hide();
                    $('#grayout').hide();
                }
                return $q.reject(rejection);
            },
            'response': function (response) {
                var $http = $injector.get('$http');
                if ($http.pendingRequests.length < 1) {
                    $('#loading').hide();
                    $('#grayout').hide();
                }
                return response;
            },
            'responseError': function (rejection) {
                var $http = $injector.get('$http');
                if ($http.pendingRequests.length < 1) {
                    $('#loading').hide();
                    $('#grayout').hide();
                }
                return $q.reject(rejection);

            }
        };

    });

    app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function ($stateProvider, $urlRouterProvider, $httpProvider) {
        //        app.config(['$stateProvider', '$urlRouterProvider', '$qProvider', '$httpProvider', function ($stateProvider, $urlRouterProvider, $qProvider,$httpProvider) {

        //1.5.9 fix for possible unhandled rejection
        //$qProvider.errorOnUnhandledRejections(false);

        //route definitions
        $urlRouterProvider
                .when('/', 's')
                .when('//', 's')
                .otherwise('');

        $stateProvider.state('home', {
            //url: '/s/:rootCategoryId/:keywords',
            url: '/s/:keywords',
            views: {
                'generalInformation': {
                    templateUrl: '/Scripts/app/home/views/generalInformationView.html',
                    controller: 'GeneralInformationCtrl'
                },
                /*
                'categoriesList': {
                    templateUrl: '/Scripts/app/home/views/categoriesListView.html',
                    controller: 'CategoriesListCtrl'
                },*/
                'productsList': {

                    templateUrl: '/Scripts/app/home/views/productsListView.html',
                    controller: 'ProductsListCtrl'
                },
                'searchBox': {
                    controller: 'SearchBoxCtrl',
                    templateUrl: '/Scripts/app/root/views/searchBoxView.html'
                }/*,
                'productsFilter': {
                    controller: 'ProductsFilterCtrl',
                    templateUrl: '/Scripts/app/home/views/productsFilterView.html'
                }*/
            }
        }).state('home.product', {
            url: '/product/:productId',
            views: {
                'productsList@': {
                    templateUrl: '/Scripts/app/home/views/productDetailsView.html',
                    controller: 'ProductDetailsCtrl'
                }/*,
                'searchBox@': {
                    controller: 'SearchBoxCtrl',
                    templateUrl: '/Scripts/app/root/views/searchBoxView.html'
                }*/
            }
        }).state('home.cart', {
            url: '/cart',
            views: {
                'productsList@': {
                    templateUrl: '/Scripts/app/home/views/cartView.html',
                    controller: 'CartCtrl'
                }/*,
                'searchBox@': {
                    controller: 'SearchBoxCtrl',
                    templateUrl: '/Scripts/app/root/views/searchBoxView.html'
                }*/
            }
        }).state('home.results', {
            url: '/results',
            views: {
                'productsList@': {
                    templateUrl: '/Scripts/app/home/views/vendorSelectionResultsView.html',
                    controller: 'VendorSelectionResultsCtrl'
                }/*,
                'searchBox@': {
                    controller: 'SearchBoxCtrl',
                    templateUrl: '/Scripts/app/root/views/searchBoxView.html'
                }*/
            }
        });

        $httpProvider.interceptors.push('quickInterceptor');

    }]);



    app.run(['$state', '$stateParams', '$rootScope', function ($state, $stateParams, $rootScope) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        $state.go('home');
    }]);


})(window.angular);
