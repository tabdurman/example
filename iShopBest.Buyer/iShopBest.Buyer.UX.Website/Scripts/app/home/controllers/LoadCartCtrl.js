﻿(function (angular) {
    var app=angular.module('rootApp');

    app.controller('LoadCartCtrl', ['$scope', '$uibModalInstance','$rootScope','$state','$filter','infrastructure','shoppingCart','shoppingCartRepository',
        function ($scope, $modalInstance, $rootScope, $state,$filter, infrastructure, shoppingCart, shoppingCartRepository) {

            if (typeof shoppingCart.cartId === 'undefined')
                $scope.tempCartId = 0;
            else
                $scope.tempCartId = shoppingCart.cartId;
            
            infrastructure.validationErrors = undefined;
            infrastructure.waitMessage = {text: "Loading Carts List"};

            shoppingCartRepository.getList({}
                , function (data) {
                    $scope.carts = data.ShoppingCarts;
                    infrastructure.waitMessage = { text: "Please Wait" };
                }, function (reason) {
                    infrastructure.validationErrors = reason;
                });


            $scope.ok = function () {
                if (shoppingCart.cartId != $scope.tempCartId || (shoppingCart.shoppingCartItems && shoppingCart.shoppingCartItems.length == 0)) {
                    var found = $filter('filter')($scope.carts, { Id: $scope.tempCartId }, true);
                    if (typeof found!== 'undefined' && found!=null && found.length==1)
                    {

                        infrastructure.validationErrors = undefined;
                        infrastructure.waitMessage = { text: "Loading Cart" };

                        shoppingCartRepository.loadCart({ cartId: $scope.tempCartId }
                            ,function (data) {
                                infrastructure.waitMessage = { text: "Please Wait" };
                                shoppingCart.cartId = data.Cart.Id;
                                shoppingCart.cartName = data.Cart.CartName

                                angular.forEach(shoppingCart.shoppingCartItems, function (elm, idx) {
                                    shoppingCart.deleteFromCart(elm.ProductId);
                                });

                                angular.forEach(data.ShoppingCartItems, function (elm, idx) {
                                    shoppingCart.addToCart(elm)
                                });
                                //shoppingCart.shoppingCartItems = data.ShoppingCartItems;
                            }
                            ,function (reason) {
                                infrastructure.validationErrors = reason;
                            });
                    }

                }

                $modalInstance.close();
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };

        }]);
})(window.angular);