﻿(function (angular) {
    var app=angular.module('rootApp');

    app.controller('ProductDetailsCtrl',
        ['$scope','$rootScope', '$http', '$log', '$filter', 'infrastructure','shoppingCart','productsRepository','vendorSelectionRepository',
            function ($scope,$rootScope, $http, $log, $filter, infrastructure,shoppingCart,productsRepository,vendorSelectionRepository) {
                var that = this;

                $scope.currentProduct= { ProductRating: 0, ManufacturerRating: 0}
                $scope.pickedVendorIndex = 0;


                var refillDetails = function (currentProduct,match) {
                            currentProduct.SupplierId = match.SupplierId;
                            currentProduct.SupplierName = match.SupplierName;
                            currentProduct.SupplierRating = match.SupplierRating;
                            currentProduct.LeadTime = match.LeadTime;
                            currentProduct.EffectiveDate = match.EffectiveDate;
                            currentProduct.ExpirationDate = match.ExpirationDate;
                            currentProduct.MinimumQty = match.MinimnumQty;
                            currentProduct.Sku = match.Sku;
                            currentProduct.Cost = match.Cost;
                            currentProduct.PackSku = match.PackSku;
                            currentProduct.PackQty = match.PackQty;
                            currentProduct.PackPrice = match.PackPrice;
                            currentProduct.PackRequired = match.PackRequired;
                            currentProduct.SupplierUnit = match.Unit;
                            currentProduct.SupplierCountryOfOrigin = match.CountryOfOrigin;
                            currentProduct.SupplierPackUnit = match.PackUnit;
                            currentProduct.TAA = match.TAA;
                            return currentProduct;
                };

                var setSupplier = function () {
                    if ($scope.currentProduct) {
                        var found = [];
                        var defaultSupplierId = infrastructure.getSupplierId();

                        if (!$scope.currentProduct.SupplierId && defaultSupplierId)
                            $scope.currentProduct.SupplierId = defaultSupplierId;

                        if ($scope.currentProduct.SupplierId)
                            found = $filter('filter')($scope.currentMatches, { SupplierId: $scope.currentProduct.SupplierId }, true);
                        else {
                            found = $filter('filter')($scope.currentMatches, function (value) { return value.MinimumQty <= $scope.qtyRequired }, true);
                            if (found.length > 0)
                                found = $filter('orderBy')($scope.currentMatches, 'Cost'); // get the lowest price match
                            else
                                found = [];
                        }

                        if (found.length > 0) {
                            $scope.currentProduct = refillDetails($scope.currentProduct, found[0]);
                        }
                    }
                }

                $scope.addProductToCart = function (currentProduct, qtyRequired, destZip, Method) {
                    setSupplier();
                    shoppingCart.addToCart(currentProduct, qtyRequired, destZip, Method);

                };



                infrastructure.validationErrors = undefined;
                infrastructure.waitMessage = {text: "Loading Details"};

                productsRepository.getDetails(
                    { productId: $scope.config.Data.CurrentProductId },
                    function (data) {
                        if (typeof data !== 'undefined' && typeof data.ProductDetails !== 'undefined') {
                            $scope.currentProduct = data.ProductDetails;
                            $scope.currentProduct.QtyRequested = 1;

                            infrastructure.waitMessage = { text: "Loading Matches" };

                            vendorSelectionRepository.getMatches({
                                ShoppingCart:
                                    [{ GtinPlus: $scope.config.Data.CurrentProductId, Quantity: 1, DestZipCode: "33065" }],
                                IncludeInventory: false
                            },
                                function (data) {
                                    $scope.currentMatches = data.Matches;
                                    setSupplier();
                                    infrastructure.waitMessage = { text: "Please Wait" };
                                },
                                function(reason) {
                                    infrastructure.validationErrors = reason;
                                }
                                );
                        }

                    },
                    function (reason) {
                        infrastructure.validationErrors = reason;
                    }
                    );
                $scope.qtyRequired = 1;

            }
        ]);
})(window.angular);