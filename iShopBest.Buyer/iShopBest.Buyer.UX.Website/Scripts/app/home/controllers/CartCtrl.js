﻿(function (angular) {
    var app=angular.module('rootApp');

    app.controller('CartCtrl', ['$scope', '$rootScope', '$http', '$log', '$filter','$state', 'infrastructure', 'shoppingCart', 'productsRepository','vendorSelectionRepository', '$uibModal',
        function ($scope, $rootScope, $http, $log, $filter,$state, infrastructure, shoppingCart, productsRepository,vendorSelectionRepository,$modal) {
            var that = this;
            $scope.domLoaded = false;

            $scope.shoppingCart = shoppingCart;

            $scope.preferencesPopOver = {
                isOpen: false
            };

            $scope.open = function (size) {

                var modalInstance = $modal.open({
                    templateUrl: 'preferencesAsModal.html',
                    controller: 'PreferencesCtrl',
                    size: size,
                    resolve: {
                        items: function () {
                            return $scope.items;
                        }
                    }
                });

                modalInstance.result.then(function (selectedItem) {
                    $scope.selected = selectedItem;
                }, function () {
                    //$log.info('Modal dismissed at: ' + new Date());
                });
            };

            $scope.openLoadCart = function (size) {

                var modalInstance = $modal.open({
                    templateUrl: '/Scripts/app/home/views/loadCartView.html',
                    controller: 'LoadCartCtrl',
                    size: size,
                    resolve: {
                        items: function () {
                            return $scope.items;
                        }
                    }
                });

                modalInstance.result.then(function (selectedItem) {

                    $scope.shoppingCart = shoppingCart;

                }, function () {
                    //$log.info('Modal dismissed at: ' + new Date());
                });
            };

            $scope.openSaveCart = function (size) {

                var modalInstance = $modal.open({
                    templateUrl: '/Scripts/app/home/views/saveCartView.html',
                    controller: 'SaveCartCtrl',
                    size: size,
                    resolve: {
                        items: function () {
                            return $scope.items;
                        }
                    }
                });

                modalInstance.result.then(function (selectedItem) {
                    $scope.selected = selectedItem;
                }, function () {
                    //$log.info('Modal dismissed at: ' + new Date());
                });
            };

            $scope.deleteItemFromCart = function (productId) {
                shoppingCart.deleteFromCart(productId);
                $scope.shoppingCart = shoppingCart;
            };

            $scope.goToDetails = function (prodId, supId) {
                infrastructure.setSupplierId(supId);
                $state.go('home.product',{ productId: prodId });
            };

            $scope.runVendorSelection = function () {
                var params = {
                    Preferences: [],
                    ShoppingCart: [],
                    IncludeInventory: true
                }


                angular.forEach(shoppingCart.vendorSelectionPreferences, function (val, idx) {
                    params.Preferences.push({ Name: val.name, Value: val.value });
                });

                angular.forEach(shoppingCart.shoppingCartItems, function (val, idx) {
                    params.ShoppingCart.push({ GtinPlus: val.ProductId, Quantity: val.Quantity, DestZipCode: val.DestZipCode });
                });

                if (params.ShoppingCart.length == 0)
                    infrastructure.validationErrors = { status: "No items in shopping cart." }
                else {

                    vendorSelectionRepository.run(params,
                        function (data) {
                            infrastructure.vendorSelectionResults = data;
                            infrastructure.validationErrors = undefined;
                            $state.go('home.results');

                        },
                        function (reason) {
                            infrastructure.validationErrors = reason;
                            infrastructure.vendorSelectionResults = null;
                        });
                }
            };

        }]);


})(window.angular);