﻿(function (angular) {
    var app=angular.module('rootApp');

    app.controller('ProductsListCtrl',
        ['$scope','$rootScope', '$http', '$log', '$filter', 'infrastructure','productsRepository',
            function ($scope,$rootScope, $http, $log, $filter, infrastructure,productsRepository) {
                var that = this;

                $scope.productsQuery = {
                    CategoryId: $scope.config.Data.CurrentCategoryId,
                    Keywords: $scope.config.Data.CurrentKeywords,
                    PageNumber: 1,
                    PageSize: "50",
                    OrderBy: 'PriceMin'
                };



                reloadProducts = function () {
                    $scope.queryResults = {};

                    infrastructure.validationErrors = undefined;
                    infrastructure.waitMessage.text = "Searching Products";

                    //console.log("Search for: " + JSON.stringify($scope.productsQuery))

                    productsRepository.findBy(
                        $scope.productsQuery,
                        function (data) {
                            $scope.products = data.Results;
                            $scope.queryResults = {
                                OrderBy: data.OrderBy,
                                ProductsCount: data.TotalCount,
                                PagesCount: data.TotalPages,
                                PageSize: data.PageSize,
                                PageNumber: data.PageNumber,
                                ResultsMessage: data.TotalCount + ' result(s), showing from ' + ((data.PageNumber - 1) * data.PageSize + 1) + ' to ' + ((data.PageNumber * data.PageSize) > data.TotalCount ? data.TotalCount : data.PageNumber * data.PageSize)
                            };


                            if (typeof data.Results === 'undefined' || data.Results.length<1)
                                infrastructure.validationErrors = { status: 'No results found for "' + $scope.productsQuery.Keywords + '"' };

                            infrastructure.waitMessage.text = "Please Wait";
                            //console.log("Success: " + JSON.stringify(infrastructure.validationErrors))
                        },
                        function (reason) {
                            infrastructure.validationErrors = reason;
                            //infrastructure.validationErrors.data.Message += " " + $scope.productsQuery.Keywords
                            infrastructure.waitMessage.text = "Please Wait";
                            //console.log("Failed: " + JSON.stringify(infrastructure.validationErrors))
                        }
                        );
                };

                $scope.goToPage = function(pageNumber) {
                    $scope.productsQuery.PageNumber = pageNumber;
                    reloadProducts();
                }


                $scope.$watch(function () {
                    return $scope.config.Data.ShowProducts
                }, function (newValue, oldValue,scope) {
                    if ($scope.config.State.Current == "home" && $scope.config.State.InitializationCompleted && newValue) {
                        reloadProducts();
                    }
                });

                $scope.$watch(function () {
                    return $scope.config.Data.CurrentKeywords
                }, function (newValue, oldValue,scope) {
                    if ($scope.config.State.Current == "home" && $scope.config.State.InitializationCompleted && typeof newValue !== 'undefined' && newValue != "") {
                        $scope.config.Data.ShowProducts = true;
                    }
                });

            }
        ]);
})(window.angular);