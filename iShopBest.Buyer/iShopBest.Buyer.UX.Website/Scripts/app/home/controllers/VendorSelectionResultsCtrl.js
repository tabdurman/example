﻿(function (angular) {
    var app=angular.module('rootApp');

    app.controller('VendorSelectionResultsCtrl',
        ['$scope','$rootScope', '$http', '$log', '$filter', 'infrastructure','shoppingCart',
            function ($scope,$rootScope, $http, $log, $filter, infrastructure,shoppingCart) {
                var that = this;
                $scope.VendorSelectionResults = infrastructure.vendorSelectionResults;


                $scope.showOrders = true;
                $scope.showMatches = false;
                $scope.showInventories = false;


                $scope.toggleOrders = function () {
                    $scope.showOrders = !$scope.showOrders;
                };
                $scope.toggleMatches = function () {
                    $scope.showMatches = !$scope.showMatches;
                };
                $scope.toggleInventories = function () {
                    $scope.showInventories = !$scope.showInventories;
                };


            }
        ]);
})(window.angular);