﻿(function (angular) {
    var app=angular.module('rootApp');

    app.controller('PreferencesCtrl', ['$scope', '$uibModalInstance','$rootScope','$state','infrastructure','shoppingCart',
        function ($scope, $modalInstance, $rootScope,$state, infrastructure, shoppingCart) {

            $scope.shoppingCart = shoppingCart;

            $scope.ok = function () {


                $modalInstance.close();
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };

        }]);
})(window.angular);