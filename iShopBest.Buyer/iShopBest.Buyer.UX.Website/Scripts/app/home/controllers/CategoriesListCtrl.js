﻿(function (angular) {
    var app=angular.module('rootApp');

    app.controller('CategoriesListCtrl',
        ['$scope','$rootScope', '$http', '$log', '$filter', '$stateParams','infrastructure', 'categoriesRepository',
            function ($scope,$rootScope, $http, $log, $filter,$stateParams, infrastructure, categoriesRepository) {
                var that = this;

                $scope.$watch(function () { return $scope.config.Data.CurrentCategoryId }, function (newValue, oldValue,scope) {
                    if ($scope.config.State.Current=="home" && $scope.config.State.InitializationCompleted && $scope.config.Data.ShowCategories) {
                        categoriesRepository
                            .findBy(
                            { rootCategoryId: $scope.config.Data.CurrentCategoryId },
                            function (data) {
                                $scope.validationErrors = infrastructure.nullError;
                                $scope.categories = data.Categories;
                                $scope.config.Data.CategoryPath = data.Path;
                                //$rootScope.$emit('categoryPathChanged', data.Path);
                                var topLevel = false;
                                if ($scope.config.Data.CurrentCategoryId == null || $scope.config.Data.CurrentCategoryId == "" || typeof $scope.config.Data.CurrentCategoryId === 'undefined' || (data != null && data.Path == null && data.Categories != null))
                                    topLevel = true
                                var bottomLevel = false;
                                if (data != null && data.Path != null && data.Categories == null)
                                    bottomLevel = true

                                $scope.config.Data.TopLevel = topLevel;
                                $scope.config.Data.BottomLevel = bottomLevel;

                                if ($scope.config.Data.BottomLevel) {
                                    $scope.config.Data.ShowProducts = true;
                                    //$scope.config.Data.ShowCategories = false;
                                }
                                else {
                                    $scope.config.Data.ShowCategories = true;
                                    if (typeof $scope.config.Data.CurrentKeywords ==='undefined' || $scope.config.Data.CurrentKeywords=="")
                                        $scope.config.Data.ShowProducts = false;

                                }

                            },
                            function (reason) {
                                infrastructure.validationErrors = reason;
                            });
                    }
                });
            }]);
})(window.angular);