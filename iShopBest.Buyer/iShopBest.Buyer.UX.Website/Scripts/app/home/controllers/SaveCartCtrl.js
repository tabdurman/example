﻿(function (angular) {
    var app=angular.module('rootApp');

    app.controller('SaveCartCtrl', ['$scope', '$uibModalInstance','$rootScope','$state','infrastructure','shoppingCart','shoppingCartRepository',
        function ($scope, $modalInstance, $rootScope,$state, infrastructure, shoppingCart,shoppingCartRepository) {

            //$scope.shoppingCart = shoppingCart;
            $scope.saveCartViewModel = {
                cartName: shoppingCart.cartName,
                overrideExisting: shoppingCart.overrideExisting,
                shoppingCartItems: shoppingCart.shoppingCartItems
            }


            $scope.ok = function () {

                infrastructure.validationErrors = undefined;
                infrastructure.waitMessage.text = "Saving cart";

                shoppingCartRepository.saveCart($scope.saveCartViewModel,
                    function (data) {
                        var x = data;
                        infrastructure.waitMessage.text = "Please wait";
                    },
                    function (reason) {
                        infrastructure.validationErrors = reason;
                        infrastructure.waitMessage.text = "Please wait";
                    }
                );

                $modalInstance.close();
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };

        }]);
})(window.angular);