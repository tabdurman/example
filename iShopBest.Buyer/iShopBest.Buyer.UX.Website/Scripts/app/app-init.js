﻿'use strict';
(function (angular) {

    angular.module("rootApp", ["ui.router", "ngResource", "ui.bootstrap", "ngCookies", "ui.slider"]);
    var app = angular.module("rootApp");
    app.value('runningMode', 'GLOBAL');

})(window.angular);