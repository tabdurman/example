﻿(function (angular) {
    var app = angular.module('rootApp');


    app.directive('validationErrors', ['infrastructure',
        function (infrastructure) {
            return {
                restrict: 'EA',
                template: '<div class="container">'+
                          '<div class="row validation-summary-valid theme-error" data-valmsg-summary="true" ng-hide="!validationErrors.status">' +
                          ' <div class="col-lg-12 col-sm-12 col-md-12 text-center">' +
                          '  Oops, something went wrong:'+
                          ' </div>' +
                          '</div>'+
                          '<div class="row validation-summary-valid theme-error" data-valmsg-summary="true" ng-hide="!validationErrors.status">' +
                          ' <div class="col-lg-12 col-sm-12 col-md-12 text-center">' +
                          '     <div ng-hide="!validationErrors.status" class="row" ng-bind="validationErrors.status"></div>' +
                          '     <div ng-hide="!validationErrors.statusText" class="row" ng-bind="validationErrors.statusText"></div>' +
                          '     <span id="toggleErrorButton" ng-click="toggleError()"><i ng-class="{\'glyphicon glyphicon-eye-open\' : !config.ErrorDetails.Visible, \'glyphicon glyphicon-eye-close\' : config.ErrorDetails.Visible}"></i>{{config.ErrorDetails.Visible ? " hide" : " show"}} error details</span>' +
                          '     <div ng-hide="!config.ErrorDetails.Visible">'+
                          '         <div ng-hide="!validationErrors.data" class="row">'+
                          '             <div ng-hide="!validationErrors.data.ExceptionMessage" ng-bind="validationErrors.data.ExceptionMessage"></div>'+
                          '             <div ng-hide="!validationErrors.data.ExceptionType" ng-bind="validationErrors.data.ExceptionType"></div>' +
                          '             <div ng-hide="!validationErrors.data.Message" ng-bind="validationErrors.data.Message"></div>' +
                          '             <div ng-hide="!validationErrors.data.StackTrace" class="error"><pre ng-bind="validationErrors.data.StackTrace"></pre></div>' +
                          '         </div>'+
                          '         <div ng-hide="!validationErrors.data" class="row">' +
                          '             <div ng-hide="!validationErrors.data.status" class="error"><pre ng-bind="validationErrors.data.status"></pre></div>' +
                          '             <div ng-hide="!validationErrors.data.statusText" class="error"><pre ng-bind="validationErrors.data.statusText"></pre></div>' +
                          '             <div ng-hide="!validationErrors.data.data">' +
                          '                 <div ng-hide="!validationErrors.data.data.ExceptionMessage" class="error"><pre ng-bind="validationErrors.data.data.ExceptionMessage"></pre></div>' +
                          '                 <div ng-hide="!validationErrors.data.data.ExceptionType" class="error"><pre ng-bind="validationErrors.data.data.ExceptionType"></pre></div>' +
                          '                 <div ng-hide="!validationErrors.data.data.Message" class="error"><pre ng-bind="validationErrors.data.data.Message"></pre></div>' +
                          '                 <div ng-hide="!validationErrors.data.data.StackTrace" class="error"><pre ng-bind="validationErrors.data.data.StackTrace"></pre></></div>' +
                          '             </div>'+
                          '         </div>'+
                          '         <div ng-hide="!validationErrors" class="row text-left">'+
                          '         <pre ng-bind="validationErrors | json">' +
                          '         </pre>' +
                          '         </div>' +
                          '     </div>' +
                          ' </div>'+
                          '</div>' +
                          '</div>',
                link: function ($scope, element, attrs) {
                    if (typeof $scope.config === 'undefined') {
                        $scope.config = {
                            ErrorDetails: {
                                Visible: false
                            }
                        };
                    }
                    else if (typeof $scope.config.ErrorDetails === 'undefined') {
                        $scope.config.ErrorDetails = { Visible: false };
                    }
                    else
                        $scope.config.ErrorDetails.Visible = false;

                    
                    $scope.toggleError = function () {
                        $scope.config.ErrorDetails.Visible = !$scope.config.ErrorDetails.Visible;
                    };

                }
            }
        }
        ]);
})(window.angular);