﻿using Owin;

namespace iShopBest.Buyer.UX.Website
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

        }
    }
}