﻿using CapSoft.Infrastructure.Domain;
using iShopBest.Buyer.Domain.Model.DataContracts.ProductsService;
using System.Runtime.Serialization;

namespace iShopBest.Buyer.Presentation.Api.Models
{
    [DataContract(IsReference = true, Namespace = "")]
    public class ProductQueryResults : CommonPagedResponse<ProductView>
    {

    }
}
