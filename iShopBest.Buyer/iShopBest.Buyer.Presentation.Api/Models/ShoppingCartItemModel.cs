﻿using System.Runtime.Serialization;

namespace iShopBest.Buyer.Presentation.Api.Models
{
    [DataContract(IsReference=true, Namespace ="")]
    public class ShoppingCartItemModel
    {
        /// <summary>
        /// GTINPLUS
        /// </summary>
        [DataMember]
        public string GtinPlus { get; set; }
        /// <summary>
        /// Required quantity
        /// </summary>
        [DataMember]
        public int Quantity { get; set; }
        /// <summary>
        /// Destination zip code
        /// </summary>
        [DataMember]
        public string DestZipCode { get; set; }
    }
}
