﻿using System.Runtime.Serialization;

namespace iShopBest.Buyer.Presentation.Api.Models
{
    [DataContract(IsReference = true, Namespace = "")]
    public class AuthData
    {
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string Password { get; set; }

    }
}
