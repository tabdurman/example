﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace iShopBest.Buyer.Presentation.Api.Models
{
    [DataContract(IsReference = true)]
    public class SaveCartViewModel
    {
        /// <summary>
        /// Name of cart
        /// </summary>
        [DataMember]
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 5)]
        [Display(Name = "Cart Name")]
        public string CartName { get; set; }

        /// <summary>
        /// Override if cart exists
        /// </summary>
        [DataMember]
        public bool OverrideExisting { get; set; }

        /// <summary>
        /// List of items under cart
        /// </summary>
        [DataMember]
        public List<ShoppingCartItemViewModel> ShoppingCartItems { get; set; }
    }
}