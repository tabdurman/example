﻿using iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace iShopBest.Buyer.Presentation.Api.Models
{
    [DataContract(IsReference=true, Namespace ="")]
    public class VendorSelectionQuery : MatchesQuery
    {
        /// <summary>
        /// List of preferences
        /// </summary>
        [DataMember]
        public List<Parameter> Preferences { get; set; }
    }
}
