﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace iShopBest.Buyer.Presentation.Api.Models
{
    [DataContract(IsReference=true)]
    public class ShoppingCartItemViewModel
    {
        [Required]
        [DataMember]
        public long ProductId { get; set; }
        [DataMember]
        public Nullable<long> ReferenceId { get; set; }
        [DataMember]
        public Nullable<long> ManufacturerId { get; set; }
        [DataMember]
        public string ManufacturerName { get; set; }
        [DataMember]
        public string ManufacturerPartNumber { get; set; }
        [DataMember]
        public Nullable<byte> ManufacturerRating { get; set; }
        [DataMember]
        public string ProductTitle { get; set; }
        [DataMember]
        public string ShortDescription { get; set; }
        [DataMember]
        public string LongDescription { get; set; }
        [DataMember]
        public string Keywords { get; set; }
        [DataMember]
        public string GTIN { get; set; }
        [DataMember]
        public string Dimensions { get; set; }
        [DataMember]
        public Nullable<decimal> Weight { get; set; }
        [DataMember]
        public Nullable<decimal> ListPrice { get; set; }
        [DataMember]
        public Nullable<byte> ProductRating { get; set; }
        [DataMember]
        public string Unit { get; set; }
        [DataMember]
        public string CountryOfOrigin { get; set; }
        [DataMember]
        public Nullable<long> UNSPSC { get; set; }
        [DataMember]
        public string ImageUrl { get; set; }
        [DataMember]
        public Nullable<decimal> PriceMin { get; set; }
        [DataMember]
        public Nullable<decimal> PriceMax { get; set; }
        [Required]
        [DataMember]
        public int Quantity { get; set; }
        [Required]
        [DataType(DataType.PostalCode)]
        [DataMember]
        public string DestZipCode { get; set; }
        [DataMember]
        public long SupplierId { get; set; }
        [DataMember]
        public string SupplierName { get; set; }
        [DataMember]
        public Nullable<byte> SupplierRating { get; set; }
        [DataMember]
        public Nullable<int> LeadTime { get; set; }
        [DataMember]
        public Nullable<System.DateTime> EffectiveDate { get; set; }
        [DataMember]
        public Nullable<System.DateTime> ExpirationDate { get; set; }
        [DataMember]
        public int MinimumQty { get; set; }
        [DataMember]
        public string Sku { get; set; }
        [DataMember]
        public decimal Cost { get; set; }
        [DataMember]
        public string PackSku { get; set; }
        [DataMember]
        public Nullable<int> PackQty { get; set; }
        [DataMember]
        public Nullable<decimal> PackPrice { get; set; }
        [DataMember]
        public bool PackRequired { get; set; }
        [DataMember]
        public string SupplierUnit { get; set; }
        [DataMember]
        public string SupplierPackUnit { get; set; }
        [DataMember]
        public string SupplierCountryOfOrigin { get; set; }
        [DataMember]
        public Nullable<bool> TAA { get; set; }
        [DataMember]
        public Nullable<long> WarehouseId { get; set; }
        [DataMember]
        public string WarehouseName { get; set; }
        [DataMember]
        public Nullable<byte> WarehouseRating { get; set; }
        [DataMember]
        public Nullable<int> AllocatedQuantity { get; set; }
        [DataMember]
        public Nullable<int> DeliveryDays { get; set; }
        [DataMember]
        public Nullable<decimal> EstimatedFreight { get; set; }
        [DataMember]
        public Nullable<decimal> Score { get; set; }
    }
}
