﻿using System.Runtime.Serialization;

namespace iShopBest.Buyer.Presentation.Api.Models
{
    [DataContract(IsReference = true, Namespace = "")]
    public class PagingData
    {
        /// <summary>
        /// Records per page
        /// </summary>
        [DataMember]
        public int PageSize { get; set; }
        /// <summary>
        /// 1 based page number
        /// </summary>
        [DataMember]
        public int PageNumber { get; set; }
        /// <summary>
        /// comma separated Order by list, "-" prefix makes it DESC
        /// </summary>
        [DataMember]
        public string OrderBy { get; set; }
    }
}
