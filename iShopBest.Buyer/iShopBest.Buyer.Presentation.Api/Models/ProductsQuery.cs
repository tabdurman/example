﻿using System;
using System.Runtime.Serialization;

namespace iShopBest.Buyer.Presentation.Api.Models
{
    [DataContract(IsReference = true, Namespace = "")]
    public class ProductsQuery : PagingData
    {

        /// <summary>
        /// Ctegory ID, UNSPSC code
        /// </summary>
        [DataMember]
        public long? CategoryId { get; set; }

        /// <summary>
        /// Keywords, see SQL Server Full Search CONTAINS function
        /// </summary>
        [DataMember]
        public string Keywords { get; set; }

        /// <summary>
        /// Price minimum border
        /// </summary>
        [DataMember]
        public Nullable<decimal> PriceMin { get; set; }

        /// <summary>
        /// Price maximum border
        /// </summary>
        [DataMember]
        public Nullable<decimal> PriceMax { get; set; }

        /// <summary>
        /// List of manufacturer Ids
        /// </summary>
        [DataMember]
        public long[] Manufacturers { get; set; }
    }
}
