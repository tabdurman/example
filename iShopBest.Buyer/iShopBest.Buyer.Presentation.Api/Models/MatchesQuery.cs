﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace iShopBest.Buyer.Presentation.Api.Models
{
    [DataContract(IsReference=true, Namespace ="")]
    public class MatchesQuery
    {
        /// <summary>
        /// List of shopping cart requirements per item
        /// </summary>
        [DataMember]
        public List<ShoppingCartItemModel> ShoppingCart { get; set; }
        /// <summary>
        /// Include inventory levels
        /// </summary>
        [DataMember]
        public bool IncludeInventory { get; set; }
    }
}
