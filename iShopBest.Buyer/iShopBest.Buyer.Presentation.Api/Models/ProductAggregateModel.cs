﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace iShopBest.Buyer.Presentation.Api.Models
{
    [DataContract(IsReference = true)]
    public class ProductAggregateSummaryModel
    {
        [DataMember]
        public SimpleProductView Product { get; set; }
    }

    [DataContract(IsReference=true)]
    public class ProductAggregateModel
    {
        [DataMember]
        public SimpleProductView Product { get; set; }
        [DataMember]
        public List<SimplePriceView> Prices { get; set; }
    }

    [DataContract(IsReference = true)]
    public class  SimpleProductView
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string ProductTitle { get; set; }
        [DataMember]
        public string ShortDescription { get; set; }
    }

    [DataContract(IsReference = true)]
    public class SimplePriceView
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Sku { get; set; }

        [DataMember]
        public decimal Cost { get; set; }
        [DataMember]
        public long SupplierId { get; set; }
        [DataMember]
        public string SupplierName { get; set; }
        [DataMember]
        public Nullable<int> LeadTime { get; set; }
        [DataMember]
        public string Url { get; set; }

    }
}
