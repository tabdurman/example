﻿using AutoMapper;
using iShopBest.Buyer.Domain.Model.DataContracts.CategoriesService;
using iShopBest.Buyer.Domain.Model.DataContracts.ProductsService;
using iShopBest.Buyer.Domain.Model.DataContracts.ShoppingCartService;
using iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService;
using iShopBest.Buyer.Domain.Model.Entities;
using iShopBest.Buyer.Presentation.Api.Models;
using System;

namespace iShopBest.Buyer.Presentation.Api.Config
{
    public class ModelMapping
    {
        public static void InitializeMappings()
        {

            // this not used nomore
            Mapper.Initialize(cfg => {

                cfg.CreateMap<ProductsQuery,GetProductsRequest>().ForMember(x=>x.OrderBy,specs=>specs.Ignore());
                cfg.CreateMap<Category, CategoryView>();
                cfg.CreateMap<Product, ProductView>()
                    // map Id to ProductId
                    .ForMember(x => x.ProductId, specs => specs.MapFrom(src => src.Id));
                cfg.CreateMap<ShoppingCartItemModel, ItemRequirement>().ForMember(x=>x.GtinPlus,specs=>specs.MapFrom(src=>Int64.Parse(src.GtinPlus)));
                cfg.CreateMap<VendorSelectionQuery, RunVendorSelectionRequest>();
                cfg.CreateMap<MatchesQuery, GetMatchesRequest>();
                cfg.CreateMap<SaveCartViewModel, SaveCartRequest>();
                cfg.CreateMap<ShoppingCartItemViewModel, ShoppingCartItem>();
                cfg.CreateMap<Price, SimplePriceView>();
                cfg.CreateMap<Product, SimpleProductView>();
                cfg.CreateMap<ProductAggregate, ProductAggregateModel>();
            });

        }
    }

}
