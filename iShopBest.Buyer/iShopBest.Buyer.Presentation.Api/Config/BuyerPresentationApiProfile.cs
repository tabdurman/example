﻿using AutoMapper;
using iShopBest.Buyer.Domain.Model.DataContracts.CategoriesService;
using iShopBest.Buyer.Domain.Model.DataContracts.ProductsService;
using iShopBest.Buyer.Domain.Model.DataContracts.ShoppingCartService;
using iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService;
using iShopBest.Buyer.Domain.Model.Entities;
using iShopBest.Buyer.Presentation.Api.Models;
using System;

namespace iShopBest.Buyer.Presentation.Api.Config
{
    public class BuyerPresentationApiProfile : Profile
    {
        public BuyerPresentationApiProfile()
        {
            CreateMap<ProductsQuery, GetProductsRequest>().ForMember(x => x.OrderBy, specs => specs.Ignore());
            CreateMap<Category, CategoryView>();
            CreateMap<Product, ProductView>()
                // map Id to ProductId
                .ForMember(x => x.ProductId, specs => specs.MapFrom(src => src.Id));
            CreateMap<ShoppingCartItemModel, ItemRequirement>().ForMember(x => x.GtinPlus, specs => specs.MapFrom(src => Int64.Parse(src.GtinPlus)));
            CreateMap<VendorSelectionQuery, RunVendorSelectionRequest>();
            CreateMap<MatchesQuery, GetMatchesRequest>();
            CreateMap<SaveCartViewModel, SaveCartRequest>();
            CreateMap<ShoppingCartItemViewModel, ShoppingCartItem>();
            CreateMap<Price, SimplePriceView>();
            CreateMap<Product, SimpleProductView>();
            CreateMap<ProductAggregate, ProductAggregateModel>();
            CreateMap<ProductAggregate, ProductAggregateSummaryModel>();


        }
    }
}
