﻿using AutoMapper;
using CapSoft.Identity.Presentation.Api;
using CapSoft.Identity.Presentation.Api.Controllers;
using CapSoft.Infrastructure.Domain;
using CapSoft.Infrastructure.Helpers;
using iShopBest.Buyer.Domain.Model.DataContracts.ShoppingCartService;
using iShopBest.Buyer.Domain.Model.ServiceContracts;
using iShopBest.Buyer.Presentation.Api.Attributes;
using iShopBest.Buyer.Presentation.Api.Models;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace iShopBest.Buyer.Presentation.Api.Controllers
{
    [WebApiAuthorizeWithClaims("WebSiteUser", "Yes")]
    public class ShoppingCartApiController : BaseApiController
    {
        private IShoppingCartService _shoppingCartService;

        /// <summary>
        /// proper constructor
        /// </summary>
        /// <param name="shoppingCartService"></param>
        public ShoppingCartApiController(IShoppingCartService shoppingCartService)
        {
            _shoppingCartService = ArgumentValidator.GetValue<IShoppingCartService>(() => shoppingCartService);
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public ShoppingCartApiController() : this(ValueObjectBase.Container.GetExportedValue<IShoppingCartService>())
        {
        }

        /// <summary>
        /// Gets all carts belong to loggedin user
        /// </summary>
        /// <param name="httpRequestMessage"></param>
        /// <returns></returns>
        [HttpGet]
        [RequiresHttps]
        [Route("api/ShoppingCart")]
        public async Task<HttpResponseMessage> Get(HttpRequestMessage httpRequestMessage)
        {
            return await Task.Run(() => ProcessWithVerification(httpRequestMessage, () =>
            {
                //GetProductsRequest getProductsRequest =ProductsQuery new GetProductsRequest();

                GetCartsListRequest getCartsListRequest = new GetCartsListRequest() {  ForUserId= CurrentUser.UserId };


                GetCartsListResponse response = _shoppingCartService.GetCartsList(getCartsListRequest);
                return httpRequestMessage.CreateResponse(HttpStatusCode.OK, response);
            }));

        }

        /// <summary>
        /// Retrieves cart details by id
        /// </summary>
        /// <param name="httpRequestMessage"></param>
        /// <param name="cartId">Id of cart</param>
        /// <returns></returns>
        [HttpGet]
        [RequiresHttps]
        [Route("api/ShoppingCart/{cartId}")]
        public async Task<HttpResponseMessage> Get(HttpRequestMessage httpRequestMessage,long cartId)
        {
            return await Task.Run(() => ProcessWithVerification(httpRequestMessage, () =>
            {
                //GetProductsRequest getProductsRequest =ProductsQuery new GetProductsRequest();

                LoadCartRequest loadCartRequest = new LoadCartRequest() { ForUserId = CurrentUser.UserId, CartId=cartId };


                LoadCartResponse response = _shoppingCartService.LoadCart(loadCartRequest);
                return httpRequestMessage.CreateResponse(HttpStatusCode.OK, response);
            }));

        }

        /// <summary>
        /// Saves cart
        /// </summary>
        /// <param name="httpRequestMessage"></param>
        /// <param name="saveCartModel"></param>
        /// <returns></returns>
        [HttpPost]
        [RequiresHttps]
        [Route("api/ShoppingCart")]
        public async Task<HttpResponseMessage> Save(HttpRequestMessage httpRequestMessage, [FromBody] SaveCartViewModel saveCartModel)
        {
            return await Task.Run(() => ProcessWithVerification(httpRequestMessage, () =>
            {
                //GetProductsRequest getProductsRequest =ProductsQuery new GetProductsRequest();
                    SaveCartRequest saveCartRequest = Mapper.Map<SaveCartViewModel,SaveCartRequest>(saveCartModel);
                    saveCartRequest.UserId = CurrentUser.UserId;

                    SaveCartResponse response = _shoppingCartService.SaveCart(saveCartRequest);
                    return httpRequestMessage.CreateResponse(HttpStatusCode.OK, response);
            }));

        }

    }
}
