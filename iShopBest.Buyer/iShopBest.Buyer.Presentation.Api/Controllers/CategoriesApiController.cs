﻿using AutoMapper;
using CapSoft.Identity.Presentation.Api;
using CapSoft.Identity.Presentation.Api.Controllers;
using CapSoft.Infrastructure.Domain;
using CapSoft.Infrastructure.Helpers;
using iShopBest.Buyer.Domain.Model.DataContracts.CategoriesService;
using iShopBest.Buyer.Domain.Model.DataContracts.ProductsService;
using iShopBest.Buyer.Domain.Model.ServiceContracts;
using iShopBest.Buyer.Presentation.Api.Attributes;
using iShopBest.Buyer.Presentation.Api.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace iShopBest.Buyer.Presentation.Api.Controllers
{
    public class CategoriesApiController : BaseApiController
    {
        ICategoriesService _service;
        public CategoriesApiController() : this(
            ValueObjectBase.Container.GetExportedValue<ICategoriesService>()
            )
        {
        }

        [ImportingConstructor]
        public CategoriesApiController([Import] ICategoriesService categoriesService)
        {
            _service = ArgumentValidator.GetValue<ICategoriesService>(() => categoriesService);
        }

        #region IDisposable
        private bool _disposed;
        private object _threadSafety = new object();

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected override void Dispose(bool disposing)
        {
            lock (_threadSafety)
            {
                if (!_disposed)
                {
                    _disposed = true;
                    if (disposing)
                    {
                        _service.Dispose();
                        base.Dispose(disposing);
                    }
                }

            }

        }
        #endregion

        /// <summary>
        /// Load categories, children of rootCategoryId
        /// </summary>
        /// <param name="httpRequestMessage"></param>
        /// <param name="rootCategoryId">Send null for root categories</param>
        /// <returns></returns>
        [HttpGet]
        [WebApiAuthorizeWithClaims("WebSiteUser", "Yes")]
        [Route("api/Categories/{rootCategoryId:long?}")]
        [RequiresHttps]
        public async Task<HttpResponseMessage> Get(HttpRequestMessage httpRequestMessage, Int64? rootCategoryId=null)
        {
            return await Task.Run(() => ProcessWithVerification(httpRequestMessage, () => {
                return httpRequestMessage.CreateResponse(HttpStatusCode.OK,_service.GetCategories(rootCategoryId));
            }));
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("api/CatalogCategories/{rootCategoryId:long?}")]
        [RequiresHttps]
        public async Task<HttpResponseMessage> GetCatalogCategories(HttpRequestMessage httpRequestMessage, Int64? rootCategoryId = null)
        {
            return await Task.Run(() => ProcessWithVerification(httpRequestMessage, () => {
                return httpRequestMessage.CreateResponse(HttpStatusCode.OK, _service.GetCatalogCategories(rootCategoryId));
            }));
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("api/CategoryProducts/{categoryId:long?}")]
        [RequiresHttps]
        public async Task<HttpResponseMessage> GetCategoryProducts(HttpRequestMessage httpRequestMessage, Int64? categoryId = null)
        {
            return await Task.Run(() => ProcessWithVerification(httpRequestMessage, () => {
                if ((categoryId ?? 0) == 0)
                    return httpRequestMessage.CreateResponse(HttpStatusCode.OK, new[] {
                        new { ProductId=1, Price=75.99, Shipping="Free 2 Day Shipping",Image="/images/1498061669-16200056-175x57-PNGPIX-COM-Office-De.png" },
                        new { ProductId=2, Price=76.59, Shipping="Free 2 Day Shipping",Image="/images/1498061667-16200046-166x69-inktonersandcartridg.jpg" },
                        new { ProductId=3, Price=79.47, Shipping="Free 3-5 Day Shipping",Image="/images/1498061671-16200051-166x59-740ab9-6059ec6f0a174.png" },
                        new { ProductId=4, Price=79.47, Shipping="$7.68 Shipping Cost 3 Days Shipping",Image="/images/1498061668-16200041-176x86-staples-logo-69924DC.png" }
                    });
                else
                {
                    GetProductsByCategoryResponse response = _service.GetProductsByCategory(new GetProductsByCategoryRequest() { CategoryId= categoryId ?? 0 });
                    return httpRequestMessage.CreateResponse(HttpStatusCode.OK, Mapper.Map<List<ProductAggregate>,List<ProductAggregateModel>>(response.Products));
                }
            }));
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("api/CategoryProducts/Summary/{categoryId:long?}")]
        [RequiresHttps]
        public async Task<HttpResponseMessage> GetCategoryProductsSummary(HttpRequestMessage httpRequestMessage, Int64? categoryId = null)
        {
            return await Task.Run(() => ProcessWithVerification(httpRequestMessage, () =>
            {
                GetProductsByCategoryResponse response = _service.GetProductsByCategory(new GetProductsByCategoryRequest() { CategoryId = categoryId ?? 0 });
                return httpRequestMessage.CreateResponse(HttpStatusCode.OK, Mapper.Map<List<ProductAggregate>, List<ProductAggregateSummaryModel>>(response.Products));
            }));
        }



    }
}
