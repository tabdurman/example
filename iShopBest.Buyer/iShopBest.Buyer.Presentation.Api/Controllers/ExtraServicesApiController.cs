﻿using CapSoft.Identity.Presentation.Api;
using CapSoft.Identity.Presentation.Api.Controllers;
using CapSoft.Infrastructure.Domain;
using CapSoft.Infrastructure.Email;
using CapSoft.Infrastructure.Helpers;
using iShopBest.Buyer.Presentation.Api.Attributes;
using iShopBest.Buyer.Presentation.Models.ExtraServices;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace iShopBest.Buyer.Presentation.Api.Controllers
{
    public class ExtraServicesApiController : BaseApiController
    {
        IEmailService _emailService;

        public ExtraServicesApiController() : this(ValueObjectBase.Container.GetExportedValue<IEmailService>())
        {
        }

        [ImportingConstructor]
        public ExtraServicesApiController([Import] IEmailService emailService)
        {
            _emailService = ArgumentValidator.GetValue<IEmailService>(() => emailService);
            EmailServiceFactory.InitializeFactory(_emailService);
        }

        [HttpPost]
        [WebApiAuthorizeWithClaims("UseApi", "Yes")]
        [Route("api/ExtraServices/SendEmail")]
        [RequiresHttps]
        public async Task<HttpResponseMessage> SendEmail(HttpRequestMessage httpRequestMessage, [FromBody] EmailModel email)
        {

            return await Task.Run(() => ProcessWithVerification(httpRequestMessage, () =>
            {
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress("awssmtp@capitoldevelopment.com");
                mailMessage.To.Add( "tabdurman@capitoldevelopment.com");
                mailMessage.Subject = email.Subject;
                mailMessage.Body = email.Message;
                EmailServiceFactory.GetService().Send("smtp.office365.com", 587, "awssmtp@capitoldevelopment.com", "iShopBest2015!", true, mailMessage);
                return httpRequestMessage.CreateResponse(HttpStatusCode.OK);
            }));
        }


    }
}
