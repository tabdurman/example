﻿using AutoMapper;
using CapSoft.Identity.Presentation.Api;
using CapSoft.Identity.Presentation.Api.Controllers;
using CapSoft.Infrastructure.Domain;
using CapSoft.Infrastructure.Helpers;
using iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService;
using iShopBest.Buyer.Domain.Model.ServiceContracts;
using iShopBest.Buyer.Presentation.Api.Attributes;
using iShopBest.Buyer.Presentation.Api.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace iShopBest.Buyer.Presentation.Api.Controllers
{
    public class VendorSelectionApiController : BaseApiController
    {
        IVendorSelectionService _vendorSelectionService;

        public VendorSelectionApiController() : this(
            ValueObjectBase.Container.GetExportedValue<IVendorSelectionService>()
            )
        {
        }

        [ImportingConstructor]
        public VendorSelectionApiController([Import] IVendorSelectionService vendorSelectionService)
        {
            _vendorSelectionService = ArgumentValidator.GetValue<IVendorSelectionService>(() => vendorSelectionService);
        }


        /// <summary>
        /// Runs vendor selection based on selection query passed to endpoint
        /// </summary>
        /// <param name="httpRequestMessage"></param>
        /// <param name="vendorSelectionQuery">vendor selection parameters and requirements</param>
        /// <returns></returns>
        [HttpPost]
        [WebApiAuthorizeWithClaims("WebSiteUser","Yes")]
        [Route("api/VendorSelection/Run")]
        [RequiresHttps]
        public async Task<HttpResponseMessage> RunVendorSelection(HttpRequestMessage httpRequestMessage, [FromBody] VendorSelectionQuery vendorSelectionQuery)
        {

            return await Task.Run(() => ProcessWithVerification(httpRequestMessage, () =>
            {
                //GetProductsRequest getProductsRequest =ProductsQuery new GetProductsRequest();

                RunVendorSelectionRequest runVendorSelectionRequest = Mapper.Map<VendorSelectionQuery, RunVendorSelectionRequest>(vendorSelectionQuery);

                RunVendorSelectionResponse runVendorSelectionResponse = _vendorSelectionService.RunVendorSelection(runVendorSelectionRequest);
                return httpRequestMessage.CreateResponse(HttpStatusCode.OK, runVendorSelectionResponse);
            }));
        }

        /// <summary>
        /// Runs vendor selection based on selection query passed to endpoint
        /// </summary>
        /// <param name="httpRequestMessage"></param>
        /// <param name="productsList">list of gtins</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/VendorSelection/QuickRun")]
        [RequiresHttps]
        public async Task<HttpResponseMessage> QuickRunVendorSelection(HttpRequestMessage httpRequestMessage, [FromBody] VendorSelectionQuery vendorSelectionQuery)
        {

            return await Task.Run(() => ProcessWithVerification(httpRequestMessage, () =>
            {
                
                //GetProductsRequest getProductsRequest =ProductsQuery new GetProductsRequest();
                RunVendorSelectionRequest runVendorSelectionRequest = Mapper.Map<VendorSelectionQuery, RunVendorSelectionRequest>(vendorSelectionQuery);

                RunVendorSelectionResponse runVendorSelectionResponse = _vendorSelectionService.RunVendorSelection(runVendorSelectionRequest);
                return httpRequestMessage.CreateResponse(HttpStatusCode.OK, runVendorSelectionResponse);
            }));
        }

        /// <summary>
        /// Shows matches based on data passed as match query
        /// </summary>
        /// <param name="httpRequestMessage"></param>
        /// <param name="matchesQuery">match requirements</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/VendorSelection/Matches")]
        [WebApiAuthorizeWithClaims("WebSiteUser", "Yes")]
        [RequiresHttps]
        public async Task<HttpResponseMessage> GetMatches(HttpRequestMessage httpRequestMessage, [FromBody] MatchesQuery matchesQuery)
        {

            return await Task.Run(() => ProcessWithVerification(httpRequestMessage, () =>
            {

                //GetProductsRequest getProductsRequest =ProductsQuery new GetProductsRequest();

                GetMatchesRequest getMatchesRequest = Mapper.Map<MatchesQuery, GetMatchesRequest>(matchesQuery);

                GetMatchesResponse getMatchesResponse = _vendorSelectionService.GetMatches(getMatchesRequest);
                return httpRequestMessage.CreateResponse(HttpStatusCode.OK, getMatchesResponse);
            }));
        }

        /// <summary>
        /// Shows matches based on data passed as match query
        /// </summary>
        /// <param name="httpRequestMessage"></param>
        /// <param name="productsList">comma separated gtin pluses list</param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/VendorSelection/QuickMatches/{productsList}")]
        [RequiresHttps]
        public async Task<HttpResponseMessage> GetQuickMatches(HttpRequestMessage httpRequestMessage, [FromUri] string productsList)
        {

            return await Task.Run(() => ProcessWithVerification(httpRequestMessage, () =>
            {
                if (string.IsNullOrEmpty(productsList))
                    return httpRequestMessage.CreateResponse(HttpStatusCode.NotAcceptable, new ExceptionModel() { status = "Parameters erro", statusText = "Please check and provide parameters in proper way." });

                //GetProductsRequest getProductsRequest =ProductsQuery new GetProductsRequest();
                string[] listOfProducts = productsList.Split(',');
                MatchesQuery matchesQuery = new MatchesQuery() {
                    ShoppingCart=new List<ShoppingCartItemModel>(),
                    IncludeInventory = false
                };

                foreach (var elm in listOfProducts)
                {
                    ShoppingCartItemModel model = new ShoppingCartItemModel() {
                        GtinPlus=elm,
                        Quantity=1,
                        DestZipCode="33324"
                    };
                    matchesQuery.ShoppingCart.Add(model);
                }

                GetMatchesRequest getMatchesRequest = Mapper.Map<MatchesQuery, GetMatchesRequest>(matchesQuery);

                GetMatchesResponse getMatchesResponse = _vendorSelectionService.GetMatches(getMatchesRequest);
                return httpRequestMessage.CreateResponse(HttpStatusCode.OK, getMatchesResponse);
            }));
        }

    }
}
