﻿using AutoMapper;
using CapSoft.Identity.Presentation.Api;
using CapSoft.Identity.Presentation.Api.Controllers;
using CapSoft.Infrastructure.Domain;
using CapSoft.Infrastructure.Helpers;
using iShopBest.Buyer.Domain.Model.DataContracts.ProductsService;
using iShopBest.Buyer.Domain.Model.ServiceContracts;
using iShopBest.Buyer.Presentation.Api.Attributes;
using iShopBest.Buyer.Presentation.Api.Models;
using System;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

namespace iShopBest.Buyer.Presentation.Api.Controllers
{
    public class ProductsApiController : BaseApiController
    {
        IProductsService _service;
        public ProductsApiController() : this(
            ValueObjectBase.Container.GetExportedValue<IProductsService>()
            )
        {
        }

        [ImportingConstructor]
        public ProductsApiController([Import] IProductsService productsService)
        {
            _service = ArgumentValidator.GetValue<IProductsService>(() => productsService);
        }

        #region IDisposable
        private bool _disposed;
        private object _threadSafety = new object();

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected override void Dispose(bool disposing)
        {
            lock (_threadSafety)
            {
                if (!_disposed)
                {
                    _disposed = true;
                    if (disposing)
                    {
                        _service.Dispose();
                        base.Dispose(disposing);
                    }
                }

            }

        }
        #endregion
        /// <summary>
        /// Get list of products based on products query
        /// </summary>
        /// <param name="httpRequestMessage"></param>
        /// <param name="productsQuery">send OrderBy in format -field1,field2,+field3, - means desc, + or nothing means asc (default)</param>
        /// <returns></returns>
        /// 
        [HttpGet]
        [Route("api/Products")]
        [WebApiAuthorizeWithClaims("WebSiteUser", "Yes")]
        [RequiresHttps]
        public async Task<HttpResponseMessage> Get(HttpRequestMessage httpRequestMessage, [FromUri] ProductsQuery productsQuery)
        {

            return await Task.Run(() => ProcessWithVerification(httpRequestMessage, () =>
            {
                //GetProductsRequest getProductsRequest =ProductsQuery new GetProductsRequest();
#if DEBUG
                Debug.WriteLine("Entering api/Products {0}",DateTime.Now);
#endif

                if (String.IsNullOrEmpty(productsQuery.Keywords))
                    return httpRequestMessage.CreateResponse(HttpStatusCode.NotAcceptable, new { statusText = "Please enter keywords for search" });
                if (productsQuery.Keywords.Length<3)
                    return httpRequestMessage.CreateResponse(HttpStatusCode.NotAcceptable, new { statusText = "Pease enter at least 3 characters" });

                GetProductsRequest getProductsRequest = Mapper.Map<ProductsQuery, GetProductsRequest>(productsQuery);

                string[] sortFields;
                if (productsQuery.OrderBy == null)
                    sortFields = "Id".Split(',');
                else
                    sortFields = productsQuery.OrderBy.Split(',');

                getProductsRequest.OrderBy = sortFields;

                GetProductsResponse getProductsResponse = _service.GetProducts(getProductsRequest);
#if DEBUG
                Debug.WriteLine("Exiting api/Products {0}", DateTime.Now);
#endif
                return httpRequestMessage.CreateResponse(HttpStatusCode.OK, getProductsResponse);
            }));
        }

        /// <summary>
        /// Get product details based on GTINPLUS
        /// </summary>
        /// <param name="httpRequestMessage"></param>
        /// <param name="productId">GTINPLUS of product</param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/Products/Details/{productId}")]
        [WebApiAuthorizeWithClaims("WebSiteUser", "Yes")]
        [RequiresHttps]
        public async Task<HttpResponseMessage> GetProductDetails(HttpRequestMessage httpRequestMessage, [FromUri] long? productId)
        {

            return await Task.Run(() => ProcessWithVerification(httpRequestMessage, () =>
            {
                if (productId == null)
                    return httpRequestMessage.CreateResponse(HttpStatusCode.OK, "{}");

                GetProductDetailsRequest request = new GetProductDetailsRequest() { ProductId = productId ?? 0 };

                GetProductDetailsResponse response = _service.GetProductDetails(request);

                return httpRequestMessage.CreateResponse(HttpStatusCode.OK, response);
            }));
        }
        /// <summary>
        /// Get product details based on GTINPLUS for anonymous access
        /// </summary>
        /// <param name="httpRequestMessage"></param>
        /// <param name="productId">GTINPLUS of product</param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/Products/Details/Anonymous/{productId}")]
        [RequiresHttps]
        public async Task<HttpResponseMessage> GetProductDetails4Anonymous(HttpRequestMessage httpRequestMessage, [FromUri] long? productId)
        {

            return await Task.Run(() => ProcessWithVerification(httpRequestMessage, () =>
            {
                if (productId == null)
                    return httpRequestMessage.CreateResponse(HttpStatusCode.OK, "{}");

                GetProductDetailsRequest request = new GetProductDetailsRequest() { ProductId = productId ?? 0 };

                GetProductDetailsResponse response = _service.GetProductDetails(request);

                return httpRequestMessage.CreateResponse(HttpStatusCode.OK, response);
            }));
        }

        /// <summary>
        /// Get product image based on GTINPLUS
        /// </summary>
        /// <param name="httpRequestMessage"></param>
        /// <param name="productId">GTINPLUS of product</param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/Products/Images/{productId}")]
        [WebApiAuthorizeWithClaims("WebSiteUser", "Yes")]
        [RequiresHttps]
        public async Task<HttpResponseMessage> GetProductImage(HttpRequestMessage httpRequestMessage, [FromUri] long? productId)
        {

            return await Task.Run(() => ProcessWithVerification(httpRequestMessage, () =>
            {
                GetProductImageRequest request = new GetProductImageRequest() { ProductId = productId ?? 0 };

                GetProductImageResponse response = _service.GetProductImage(request);

                if (response as object == null)
                    return httpRequestMessage.CreateResponse(HttpStatusCode.InternalServerError, new { ErrorMessage = "Invalid parameeters." });

                HttpResponseMessage resp = new HttpResponseMessage(HttpStatusCode.OK);
                resp.Content = new ByteArrayContent(response.DataBin);
                resp.Content.Headers.ContentType = new MediaTypeHeaderValue(response.MediaType);

                return resp;
            }));
        }
    }
}
