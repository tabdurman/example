﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace iShopBest.Buyer.Presentation.Api.Handlers
{
    public class PepelineHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            //UploadStringCompletedEventArgs this handler to debug incoming data
            return base.SendAsync(request, cancellationToken);
            
            /*
            // Create the response.
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent("Hello!")
            };

            // Note: TaskCompletionSource creates a task that does not contain a delegate.
            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response);   // Also sets the task state to "RanToCompletion"
            return tsc.Task;
            */
        }
    }
}
