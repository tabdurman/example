﻿(function (angular) {

    var app = angular.module('app');

    app.factory('shoppingCartRepository', function ($resource) {
        var baseUrl = window.appGlobals.apiLocation + 'ShoppingCart';
        /*
        var params = {
            Preferences: [{Name:"", Value:""}],
            ShoppingCart: [{GtinPlus:"", Quantity:0, DestZipCode:""}]
        }
        */

        return $resource(baseUrl, {},
            {
                'getList': {
                    url: baseUrl + '/',
                    method: "GET",
                    isArray: false
                },
                'loadCart': {
                    url: baseUrl + '/:cartId',
                    method: "GET",
                    isArray: false,
                    cartId: '@cartId'
                },
                'saveCart': {
                    url: baseUrl + '/',
                    method: "POST",
                    isArray: false
                }

            });
    });

})(window.angular);