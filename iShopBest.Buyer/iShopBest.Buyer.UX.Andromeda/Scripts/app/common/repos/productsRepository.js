﻿(function (angular) {

    var app = angular.module('app');

    app.factory('productsRepository', function ($resource) {
        var baseUrl = window.appGlobals.apiLocation + 'Products';

        return $resource(baseUrl + '/:productsQuery', { CategoryId: '@CategoryId', PageSize: '@PageSize', PageNumber: '@PageNumber', OrderBy: '@OrderBy' },
            {
                'findBy': {
                    method: "GET"
                    , isArray: false
                },

                'getDetails': {
                    method: "GET",
                    isArray: false,
                    url: baseUrl + '/Details/:productId',
                    params: { productId: '@productId' }
                }
            });
    });

})(window.angular);