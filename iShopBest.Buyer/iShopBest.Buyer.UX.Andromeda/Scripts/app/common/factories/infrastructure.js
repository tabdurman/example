﻿(function (angular) {
    angular
        .module("app").factory('infrastructure', ['$rootScope', '$filter', function ($rootScope, $filter) {

            var preferences = [];

            var searchLog = [{
                CategoryId: 0,
                Keywords: "",
                PageNumber: 1,
                PageSize: "8",
                OrderBy: 'PriceMin',
                Results: undefined
            }];

            var lastSearch = undefined;

            var allocateInSearchLog = function (categoryId, keywords, pageNumber, pageSize, orderBy) {
                return $filter('filter')(searchLog, {
                    CategoryId: categoryId,
                    Keywords: keywords,
                    PageNumber: pageNumber,
                    PageSize: pageSize,
                    OrderBy: orderBy
                },true);
            };

            var supplierId = 0;

            return {
                'setSupplierId': function (supId) {
                    supplierId = supId;
                },
                'getSupplierId': function () {
                    return supplierId;
                },
                'range': function (start, end) {
                    var ret = [];
                    if (!end) {
                        end = start;
                        start = 1;
                    }
                    for (var i = start; i <= end; i++) {
                        ret.push(i);
                    }
                    return ret;
                },
                'buildChoices' : function(minQty,packQty,packRequired) {
                    if (packRequired) {
                        return this.packsRange(packQty, 10);
                    }
                    else if (minQty>1) {
                        return this.valuesRange(minQty, minQty + 100);
                    }
                    else {
                        return this.valuesRange(1, 100);
                    }
                },
                'packsRange': function (packSize,iterCount) {
                    var ret = [];
                    for (var i = 1; i <= iterCount; i++) {
                        ret.push({ value: i * packSize, text: "" + (i * packSize) });
                    }
                    return ret;
                },
                'valuesRange': function (start, end) {
                    var ret = [];
                    if (!end) {
                        end = start;
                        start = 1;
                    }
                    for (var i = start; i <= end; i++) {
                        ret.push({ value: i, text: ""+i });
                    }
                    return ret;
                },
                'pricesRange': function (start, end, step) {
                    var ret = [];
                    if (!start)
                        start = 0.00;

                    if (!end) {
                        end = start+step;
                    }

                    var minValue = 0, maxValue = 0;
                    for (var i = start; i <= end; i+=step) {
                        minValue = i;
                        maxValue = i + step;
                        ret.push({ minValue: minValue, maxValue: maxValue, text: "" + minValue+"-"+maxValue });
                    }
                    return ret;
                },
                'add2SearchLog': function (categoryId, keywords, pageNumber, pageSize, orderBy, results) {

                    var res = allocateInSearchLog(categoryId, keywords, pageNumber, pageSize, orderBy);

                    if ((res != null || typeof res === 'undefined' || res.length === 0) && keywords && keywords.length>=3) {
                        searchLog.push({
                            CategoryId: categoryId,
                            Keywords: keywords,
                            PageNumber: pageNumber,
                            PageSize: pageSize,
                            OrderBy: orderBy,
                            Results: results
                        });
                    }
                },
                'findInSearchLog': function (categoryId, keywords, pageNumber, pageSize, orderBy) {
                    return allocateInSearchLog(categoryId, keywords, pageNumber, pageSize, orderBy);
                },
                'getLastSearchParams': function () {
                    return lastSearch;
                },
                'setLastSearchParams': function (lastSearchParams) {
                    lastSearch = lastSearchParams;
                },
                'getPreferences': function () {
                    return preferences;
                },
                'setPreferences': function (newPreferences) {
                    preferences = newPreferences;
                },
                'initPreferences': function () {
                    preferences = [
                        //{ name: 'ProductRatingImportance', value: 30, type: "slider", minValue: 0, maxValue: 100, text: 'Product Rating' },
                        //{ name: 'SupplierRatingImportance', value: 30, type: "slider", minValue: 0, maxValue: 100, text: 'Supplier Rating' },
                        //{ name: 'WarehouseRatingImportance', value: 30, type: "slider", minValue: 0, maxValue: 100, text: 'Warehouse Rating' },
                        {
                            name: 'SingleSupplierImportance',
                            value: 50,
                            type: "mutualex",
                            minValue: 0,
                            maxValue: 100,
                            choices: [
                                { value: 100, text: 'High' },
                                { value: 50, text: 'Dont know' },
                                { value: 0, text: 'Low' }
                            ],
                            text: 'I would like all items from SINGLE seller',
                            label: 'Single seller',
                            sequenceNumber: 1
                        },
                        {
                            name: 'DeliverySpeedImportance',
                            value: 50,
                            type: "mutualex",
                            minValue: 0,
                            maxValue: 100,
                            choices: [
                              { value: 100, text: 'High' },
                              { value: 50, text: 'Dont know' },
                              { value: 0, text: 'Low' }
                            ],
                            text: 'I care about the speed of DELIVERY.',
                            label: 'Delivery speed',
                            sequenceNumber: 2

                        },
                        {
                            name: 'FreightCostImportance',
                            value: 50,
                            type: "mutualex",
                            minValue: 0,
                            maxValue: 100,
                            choices: [
                                { value: 100, text: 'High' },
                                { value: 50, text: 'Dont know' },
                                { value: 0, text: 'Low' }
                            ],
                            text: 'I wold like FREIGHT to be as low as possible.',
                            label: 'Low freight',
                            sequenceNumber: 3
                        },


                        /*{
                            name: 'PackagesCountImportance', value: 50, type: "mutualex", minValue: 0, maxValue: 100, choices: [
                              { value: 100, text: 'Important' },
                              { value: 50, text: 'Neutral' },
                              { value: 0, text: 'Not Important' }
                        ], text: 'I would like to have as LESS PACKAGES as possible.' }, */

                    ];
                }

            };
        }]);
}(window.angular))