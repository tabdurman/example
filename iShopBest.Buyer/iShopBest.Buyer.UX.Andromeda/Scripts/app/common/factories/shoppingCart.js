﻿(function (angular) {
    angular
        .module("app").factory('shoppingCart', ['$rootScope', '$filter', 'infrastructure','shoppingCartRepository', function ($rootScope, $filter, infrastructure, shoppingCartRepository) {


            var saveCartViewModel = {
                cartName: "not loaded",
                overrideExisting: false,
                shoppingCartItems: []
            }

            var shoppingCartChanged = [{ Dirty: false }];

            var persistCartToServer = function () {
                shoppingCartRepository.saveCart(
                    saveCartViewModel,
                    function (data) {
                        var x = data;
                    },
                    function (reason) {
                        var x = reason;
                    });

            };

            //method can be 'Update' will increase quantity or 'Replace' will set quantity
            var addItemToCart = function (tempCartItem,method) {

                if (typeof method === 'undefined')
                    method = 'Update';

                tempCartItem.Keywords = '';
                tempCartItem.$id = undefined;

                var cartItem = {
                    "ShoppingCartId":  tempCartItem.ShoppingCartId,
                    "ProductId": tempCartItem.ProductId,
                    "ReferenceId": tempCartItem.ReferenceId,
                    "ManufacturerId": tempCartItem.ManufacturerId,
                    "ManufacturerName": tempCartItem.ManufacturerName,
                    "ManufacturerPartNumber": tempCartItem.ManufacturerPartNumber,
                    "ManufacturerRating": tempCartItem.ManufacturerRating,
                    "ProductTitle": tempCartItem.ProductTitle,
                    "ShortDescription": tempCartItem.ShortDescription,
                    "LongDescription": tempCartItem.LongDescription,
                    "Keywords": "",
                    "GTIN": tempCartItem.GTIN,
                    "Dimensions": tempCartItem.Dimensions,
                    "Weight": tempCartItem.Weight,
                    "ListPrice": tempCartItem.ListPrice,
                    "ProductRating": tempCartItem.ProductRating,
                    "Unit": tempCartItem.Unit,
                    "CountryOfOrigin": tempCartItem.CountryOfOrigin,
                    "UNSPSC": tempCartItem.UNSPSC,
                    "ImageUrl": tempCartItem.ImageUrl,
                    "PriceMin": tempCartItem.PriceMin,
                    "PriceMax": tempCartItem.PriceMax,
                    "Quantity": tempCartItem.Quantity,
                    "DestZipCode": tempCartItem.DestZipCode,
                    "SupplierId": tempCartItem.SupplierId,
                    "SupplierName":  tempCartItem.SupplierName,
                    "SupplierRating":  tempCartItem.SupplierRating,
                    "LeadTime":  tempCartItem.LeadTime,
                    "EffectiveDate":  tempCartItem.EffectiveDate,
                    "ExpirationDate":  tempCartItem.ExpirationDate,
                    "MinimumQty":  tempCartItem.MinimumQty,
                    "Sku":  tempCartItem.Sku,
                    "Cost":  tempCartItem.Cost,
                    "PackSku":  tempCartItem.PackSku,
                    "PackQty":  tempCartItem.PackQty,
                    "PackPrice":  tempCartItem.PackPrice,
                    "PackRequired":  tempCartItem.PackRequired,
                    "SupplierUnit":  tempCartItem.SupplierUnit,
                    "SupplierPackUnit":  tempCartItem.SupplierPackUnit,
                    "SupplierCountryOfOrigin":  tempCartItem.SupplierCountryOfOrigin,
                    "TAA":  tempCartItem.TAA,
                    "WarehouseId":  tempCartItem.WarehouseId,
                    "WarehouseName":  tempCartItem.WarehouseName,
                    "WarehouseRating":  tempCartItem.WarehouseRating,
                    "AllocatedQuantity":  tempCartItem.AllocatedQuantity,
                    "DeliveryDays":  tempCartItem.DeliveryDays,
                    "EstimatedFreight":  tempCartItem.EstimatedFreight,
                    "Score":  tempCartItem.Score,
                    "Id":  tempCartItem.Id
                };

                var found = $filter('filter')(saveCartViewModel.shoppingCartItems, { ProductId: cartItem.ProductId }, true);
                if (found != null && found.length > 0) {
                    //find item on server

                    if (method == 'Add')
                        saveCartViewModel.shoppingCartItems[saveCartViewModel.shoppingCartItems.indexOf(found[0])].Quantity = parseInt(saveCartViewModel.shoppingCartItems[saveCartViewModel.shoppingCartItems.indexOf(found[0])].Quantity) + parseInt(cartItem.Quantity);

                    if (saveCartViewModel.shoppingCartItems[saveCartViewModel.shoppingCartItems.indexOf(found[0])].Quantity == 'NaN' || method == 'Replace')
                        saveCartViewModel.shoppingCartItems[saveCartViewModel.shoppingCartItems.indexOf(found[0])].Quantity = parseInt(cartItem.Quantity);

                    // set quantity
                }
                else {
                    // add item to cart
                    saveCartViewModel.shoppingCartItems.push(cartItem);
                }

                /*
                $cookies.remove("shoppingCartItems");
                if (shoppingCartItems.length > 0)
                    $cookies.putObject("shoppingCartItems", angular.copy(shoppingCartItems));
                */
                //saveCartViewModel.shoppingCartChanged[0].Dirty = true;
                persistCartToServer();
            };

            var deleteItemFromCart = function (id) {
                if (saveCartViewModel.shoppingCartItems && saveCartViewModel.shoppingCartItems.length > 0) {
                    var found = $filter('filter')(saveCartViewModel.shoppingCartItems, { ProductId: id }, true);
                    if (found != null && found.length > 0) {
                        saveCartViewModel.shoppingCartItems.splice(saveCartViewModel.shoppingCartItems.indexOf(found[0]), 1);
                    }
                    /*
                    $cookies.remove("shoppingCartItems");
                    if (shoppingCartItems.length>0)
                        $cookies.putObject("shoppingCartItems", angular.copy(shoppingCartItems));
                    */
                    //saveCartViewModel.shoppingCartChanged[0].Dirty = true;
                    persistCartToServer();
                }
            };

            var getOrderTotal = function () {
                var orderTotal = 0.000;
                angular.forEach(saveCartViewModel.shoppingCartItems, function (val, key) {
                    orderTotal += val.PriceMin * val.Quantity;
                });
                return orderTotal;
            };

            var preferences= [
                     { name: 'ProductRatingImportance', value: 30, type: "slider", minValue: 0, maxValue: 100, text: 'Product Rating' },
                     { name: 'SupplierRatingImportance', value: 30, type: "slider", minValue: 0, maxValue: 100, text: 'Supplier Rating' },
                     { name: 'WarehouseRatingImportance', value: 30, type: "slider", minValue: 0, maxValue: 100, text: 'Warehouse Rating' },
                     { name: 'DeliverySpeedImportance', value: 30, type: "slider", minValue: 0, maxValue: 100, text: 'Delivery Speed' },
                     { name: 'FreightCostImportance', value: 30, type: "slider", minValue: 0, maxValue: 100, text: 'Freight Cost' },
                     { name: 'PackagesCountImportance', value: 30, type: "slider", minValue: 0, maxValue: 100, text: 'Packages Count' },
                     { name: 'SingleSupplierImportance', value: 30, type: "slider", minValue: 0, maxValue: 100, text: 'Single Supplier' }
            ];

            var shoppingCart = {
                'shoppingCartDirty': shoppingCartChanged[0],

                'clearShoppingCartChanged': function () {
                    shoppingCartChanged[0].Dirty = false;
                },
                'addToCart': function (cartItem, quantity, destZipCode, method) {
                    if (typeof quantity !== 'undefined')
                        cartItem.Quantity = quantity;
                    if (typeof destZipCode !== 'undefined')
                        cartItem.DestZipCode = destZipCode;
                    if (typeof method === 'undefined')
                        method='Add';
                    
                    addItemToCart(cartItem, method);
                },
                'deleteFromCart': function (GtinPlus) {
                    deleteItemFromCart(GtinPlus);
                },
                'clearCart': function () {
                    shoppingCartItems = [];
                },
                'orderTotal': getOrderTotal,
                'getCurrentCartName': function () {
                    return saveCartViewModel.cartName;
                },
                'getSaveCartViewModel': function () {
                    return saveCartViewModel;
                },
                'loadCart': function (loadCartId, onSuccess, onFailure) {
                    shoppingCartRepository.loadCart(
                        { cartId: loadCartId },
                        onSuccess,
                        onFailure
                        );
                },
                'setSaveCartViewModel': function (cart,overrideExisting) {
                    saveCartViewModel.cartName = cart.Cart.CartName;
                    saveCartViewModel.dateSaved = cart.Cart.DateSaved;
                    saveCartViewModel.shoppingCartItems = cart.ShoppingCartItems;
                    saveCartViewModel.overrideExisting = overrideExisting;
                },
                'getItemsCount': function () {
                    if (saveCartViewModel && saveCartViewModel.shoppingCartItems && saveCartViewModel.shoppingCartItems.length)
                        return saveCartViewModel.shoppingCartItems.length
                    else
                        return 0;
                },
                'refillDetails' : function (currentProduct, match) {
                    currentProduct.SupplierId = match.SupplierId;
                    currentProduct.SupplierName = match.SupplierName;
                    currentProduct.SupplierRating = match.SupplierRating;
                    currentProduct.LeadTime = match.LeadTime;
                    currentProduct.EffectiveDate = match.EffectiveDate;
                    currentProduct.ExpirationDate = match.ExpirationDate;
                    currentProduct.MinimumQty = match.MinimnumQty;
                    currentProduct.Sku = match.Sku;
                    currentProduct.Cost = match.Cost;
                    currentProduct.PackSku = match.PackSku;
                    currentProduct.PackQty = match.PackQty;
                    currentProduct.PackPrice = match.PackPrice;
                    currentProduct.PackRequired = match.PackRequired;
                    currentProduct.SupplierUnit = match.Unit;
                    currentProduct.SupplierCountryOfOrigin = match.CountryOfOrigin;
                    currentProduct.SupplierPackUnit = match.PackUnit;
                    currentProduct.TAA = match.TAA;
                    return currentProduct;
                },
                'setSupplier': function (productInformation, matchesInformation, quantity) {
                    var response = {
                        productInformation: productInformation,
                        quantityChoices: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                        addToCartQuantity: quantity
                    }

                    if (productInformation) {
                        var found = [];
                        var defaultSupplierId = infrastructure.getSupplierId();

                        if (!productInformation.SupplierId && defaultSupplierId)
                            productInformation.SupplierId = defaultSupplierId;

                        if (productInformation.SupplierId)
                            found = $filter('filter')(matchesInformation, { SupplierId: productInformation.SupplierId }, true);
                        else {
                            found = $filter('filter')(matchesInformation, function (value) { return value.MinimumQty >= quantity }, true);
                            if (found.length > 0)
                                found = $filter('orderBy')(matchesInformation, 'Cost'); // get the lowest price match
                            else
                                found = [];
                        }

                        if (found.length > 0) {
                            response.productInformation = this.refillDetails(productInformation, found[0]);
                            response.quantityChoices = infrastructure.buildChoices(found[0].MinimumQty, found[0].PackQty, found[0].PackRequired);
                            response.addToCartQuantity = quantity < found[0].MinimumQty ? found[0].MinimumQty : quantity;

                        }
                    }
                    return response;
                }
            };

            return shoppingCart;

    }]);
})(window.angular);