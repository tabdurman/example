﻿(function (angular) {
    var app=angular.module('app');
    
    app.directive('paginate', ['infrastructure', function (infrastructure) {
    return {
        restrict: 'EA',
        replace: true,
        scope: {
            totalPages: "=",
            currentPage: "=",
            onNavigate: "&"
        },
        template: '<ul class="pagination pagination-primary">' +
                  ' <li ng-if="showFirst()" class="lightgrayclr lheight35"><span ng-click="onNavigate({toPage:1,fromPage: currentPage})">&nbsp; &lt;&lt; &nbsp;</span></li>' +
                  ' <li ng-if="showFirst()" class="lightgrayclr lheight35"><span class="plain-text">&nbsp; ... &nbsp;</span></li>' +
                  ' <li ng-if="showPrev()" class="lightgrayclr lheight35"><span ng-click="onNavigate({toPage:currentPage-1,fromPage:currentPage})">&nbsp; &lt; &nbsp;</span></li>' +
                  ' <li ng-if="totalPages>1" ng-repeat="page in getPages(1,totalPages)" ng-class="{\'active\' : currentPage==page, \'\': !(currentPage==page)}"><a href="javascript:void(0);" ng-click="onNavigate({toPage:page,fromPage:currentPage})">{{page}}</a></li>' +
                  ' <li ng-if="showNext()"><span ng-click="onNavigate({toPage:currentPage+1,fromPage:currentPage})">&nbsp; &gt; &nbsp;</span></li>' +
                  ' <li ng-if="showLast()" class="lightgrayclr lheight35"><span class="plain-text">&nbsp; ... &nbsp;</span></li>' +
                  ' <li ng-if="showLast()" class="lightgrayclr lheight35"><span ng-click="onNavigate({toPage:totalPages,fromPage:currentPage})">&nbsp; &gt;&gt; &nbsp;</span></li>' +
                  '</ul>',
        controller: function ($scope, $element, $attrs) {
            var x = 1;

            $scope.showFirst = function () {
                if (!$scope.currentPage || isNaN($scope.currentPage) || parseInt($scope.currentPage) > parseInt($scope.totalPages) || parseInt($scope.currentPage) < 1)
                    return false;
                return parseInt($scope.currentPage) > 3;
            };

            $scope.showPrev = function () {
                if (!$scope.currentPage || isNaN($scope.currentPage) || parseInt($scope.currentPage) > parseInt($scope.totalPages) || parseInt($scope.currentPage) < 1)
                    return false;
                return parseInt($scope.currentPage) > 3;
            };

            $scope.showNext = function () {
                if (!$scope.currentPage || isNaN($scope.currentPage) || parseInt($scope.currentPage) > parseInt($scope.totalPages) || parseInt($scope.currentPage) < 1)
                    return false;
                return parseInt($scope.currentPage) <= parseInt($scope.totalPages) - 3
            };

            $scope.showLast = function () {
                if (!$scope.currentPage || isNaN($scope.currentPage) || parseInt($scope.currentPage) > parseInt($scope.totalPages) || parseInt($scope.currentPage) < 1)
                    return false;
                return parseInt($scope.currentPage) <= parseInt($scope.totalPages) - 3
            };

            $scope.getPages = function (start, end) {
                if (!$scope.currentPage || isNaN($scope.currentPage) || parseInt($scope.currentPage) > parseInt($scope.totalPages) || parseInt($scope.currentPage) < 1)
                    return undefined;

                var _start = 1;
                var _end = 5;
                var _currentPage = parseInt($scope.currentPage);
                var _maxPages = parseInt($scope.totalPages);

                if (_currentPage < 3 && _maxPages <= 5)
                    _end = _maxPages;
                else if (_currentPage < 3 && _maxPages > 5)
                    _end = 5;
                else if (_currentPage >= 3 && _currentPage <= _maxPages && _maxPages <= 5) {
                    _start = _currentPage - 2;
                    _end = _maxPages;
                }
                else if (_currentPage >= 3 && _currentPage <= _maxPages - 2 && _maxPages > 5) {
                    _start = _currentPage - 2;
                    _end = _currentPage + 2;
                }
                else if (_currentPage >= _maxPages - 2 && _maxPages > 5) {
                    _start = _maxPages - 4;
                    _end = _maxPages;
                }


                /*
                if (parseInt($scope.currentPage) > 3)
                    start = parseInt($scope.currentPage) - 2;
                if (parseInt($scope.currentPage) < parseInt($scope.totalPages) - 2)
                    end = parseInt($scope.currentPage) + 2;
                    */
                return infrastructure.range(_start, _end);
            };
        },
        link: function (scope, element, attrs, controller) {
            var y = 2;
            scope.$watch(scope.currentPage, function () {
                var z = 3;
            })
        }
    };
   }]);
})(window.angular);
