﻿(function (angular) {
    var app = angular.module('app');

    app.directive('iCheck', function ($timeout, $parse) {
        return {
            require: 'ngModel',
            link: function ($scope, element, $attrs, ngModel) {
                return $timeout(function () {
                    var value;
                    value = $attrs['value'];

                    $scope.$watch($attrs['ngModel'], function (newValue) {
                        $(element).iCheck('update');
                    })

                    return $(element).iCheck({
                        checkboxClass: $attrs['checkboxClass'],
                        radioClass: $attrs['radioClass']

                    }).on('ifChanged', function (event) {
                        if ($(element).attr('type') === 'checkbox' && $attrs['ngModel']) {
                            $scope.$apply(function () {
                                return ngModel.$setViewValue(event.target.checked);
                            });
                        }
                        if ($(element).attr('type') === 'radio' && $attrs['ngModel']) {
                            return $scope.$apply(function () {
                                return ngModel.$setViewValue(value);
                            });
                        }
                    });
                });
            }
        };
    });

})(window.angular);

/*
https://github.com/fronteed/iCheck/

var rebuidJQueries = function () {
    setTimeout(function () {
        /*
        $('.icheck-input').iCheck(
            {
                handle: 'checkbox',
                checkboxClass: 'icheckbox_minimal-orange',
            }
        );


        $('input.icheck-input').on('ifChecked', function (evt) {
            console.log('Checked ' + JSON.stringify(evt.currentTarget.id));
        });
        $('input.icheck-input').on('ifUnchecked', function (evt) {
            console.log('Unchecked ' + JSON.stringify(evt.currentTarget.id));
        });
        */
        /*
        $('.icheck-ng-sort-input').iCheck(
            {
                handle: 'radio',
                radioClass: 'iradio_minimal-orange',
            }
        );
        $('input.icheck-ng-sort-input').on('ifChecked', function (evt) {
            console.log('Checked ' + JSON.stringify(evt.currentTarget.id));
        });
        $('input.icheck-ng-sort-input').on('ifUnchecked', function (evt) {
            console.log('Unchecked ' + JSON.stringify(evt.currentTarget.id));
        });


    }, 100);
};
*/
