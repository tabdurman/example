﻿(function (angular) {

    var app = angular.module("app");
    app.value('runningMode', 'LOCAL');

    
    app.factory('quickInterceptor', function ($q, $injector) {

        return {
            'request': function (config) {
                $('#grayout').show();
                $('#loading').show();
                return config;
            },
            'requestError': function (rejection) {
                var $http = $injector.get('$http');
                if ($http.pendingRequests.length < 1) {
                    $('#loading').hide();
                    $('#grayout').hide();
                }
                return $q.reject(rejection);
            },
            'response': function (response) {
                var $http = $injector.get('$http');
                if ($http.pendingRequests.length < 1) {
                    $('#loading').hide();
                    $('#grayout').hide();
                }
                return response;
            },
            'responseError': function (rejection) {
                var $http = $injector.get('$http');
                if ($http.pendingRequests.length < 1) {
                    $('#loading').hide();
                    $('#grayout').hide();
                }
                return $q.reject(rejection);

            }
        };

    });
    
    app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider','$locationProvider', function ($stateProvider, $urlRouterProvider, $httpProvider,$locationProvider) {
        $locationProvider.hashPrefix('');
        //$locationProvider.html5Mode(true);
        //route definitions
        $urlRouterProvider
                .when('/', '/0')
                .when('//', '/0')
                .otherwise('');

        $stateProvider.state('search', {
            url: '/search/{pageNumber:int}/{categoryId:int}/{keyword}',
            params: {
                keyword: { value: "" },
                categoryId: {value: 0},
                pageNumber: { value: 1 }
            },
            views: {
                'productsList': {
                    templateUrl: '/Scripts/app/search/views/productsListView.html',
                    controller: 'ProductsListCtrl'
                }
                ,'searchBox': {
                templateUrl: '/Scripts/app/search/views/searchBoxView.html',
                controller: 'SearchBoxCtrl'
                }
                ,'basketSummary': {
                    templateUrl: '/Scripts/app/search/views/basketSummaryView.html',
                    controller: 'BasketSummaryCtrl'
                }
            }
        }).state('product', {
            url: '/product/{productId:int}',
            params: {
                productId: { value: 0 }
            },
            views: {
                'productsList@': {
                    templateUrl: '/Scripts/app/search/views/productDetailView.html',
                    controller: 'ProductDetailCtrl'
                }
                , 'searchBox@': {
                    templateUrl: '/Scripts/app/search/views/searchBoxView.html',
                    controller: 'SearchBoxCtrl'
                }
                , 'basketSummary@': {
                    templateUrl: '/Scripts/app/search/views/basketSummaryView.html',
                    controller: 'BasketSummaryCtrl'
                }

            }
        }).state('basket', {
            url: '/basket',
            params: {
                productId: { value: 0 }
            },
            views: {
                'productsList@': {
                    templateUrl: '/Scripts/app/search/views/basketView.html',
                    controller: 'BasketCtrl'
                }
                , 'searchBox@': {
                    templateUrl: '/Scripts/app/search/views/searchBoxView.html',
                    controller: 'SearchBoxCtrl'
                }
                , 'basketSummary@': {
                    templateUrl: '/Scripts/app/search/views/basketSummaryView.html',
                    controller: 'BasketSummaryCtrl'
                }

            }
        }).state('pagination', {
            url: '/pagination',
            params: {
                productId: { value: 0 }
            },
            views: {
                'productsList@': {
                    templateUrl: '/Scripts/app/search/views/paginationTestView.html',
                    controller: 'PaginationTestCtrl'
                }
                , 'searchBox@': {
                    templateUrl: '/Scripts/app/search/views/searchBoxView.html',
                    controller: 'SearchBoxCtrl'
                }
                , 'basketSummary@': {
                    templateUrl: '/Scripts/app/search/views/basketSummaryView.html',
                    controller: 'BasketSummaryCtrl'
                }

            }
        });

       $httpProvider.interceptors.push('quickInterceptor');

    }]);



    app.run(['$state', '$stateParams', '$rootScope', function ($state, $stateParams, $rootScope) {

        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        $state.go('search');
    }]);


})(window.angular);