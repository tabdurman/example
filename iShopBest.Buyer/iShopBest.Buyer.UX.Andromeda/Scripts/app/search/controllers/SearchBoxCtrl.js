﻿(function (angular) {
    var app = angular.module('app');

    app.controller('SearchBoxCtrl', ['$scope', '$state','infrastructure',
        function ($scope, $state,infrastructure) {
            var that = this;

            //get last search parameters if user is not in search mode, or set them to defaults otherwise
            var lastSearchParams = infrastructure.getLastSearchParams();
            if (!lastSearchParams)
                lastSearchParams={CategoryId: 0, Keywords: "", PageNumber: 1, PageSize: "8", OrderBy: "PriceMin"};


            if ($scope.$stateParams.keyword)
                $scope.keyword = $scope.$stateParams.keyword;
            else 
                $scope.keyword=lastSearchParams.Keywords;

            if ($scope.$stateParams.categoryId)
                $scope.categoryId = $scope.$stateParams.categoryId;
            else
                $scope.categoryId = lastSearchParams.CategoryId;

            if ($scope.$stateParams.pageNumber)
                $scope.pageNumber = $scope.$stateParams.pageNumber;
            else
                $scope.pageNumber = lastSearchParams.PageNumber;


            $scope.requestSearch = function (evt) {
                if (typeof evt.keyCode === 'undefined' || evt.keyCode == 13) {
                    $state.go('search', { pageNumber: 1, categoryId: $scope.categoryId, 'keyword': $scope.keyword });
                }
            };
        }]);
})(window.angular);