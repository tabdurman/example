﻿(function (angular) {
    var app=angular.module('app');

    app.filter('productsRow', function () {
        return function (data, parameter) {
            var filtered = [];
            if (data && data.length) {
                for (var i = 0; i < data.length; i++)
                    if (Math.floor(i / 4) == parameter)
                        filtered.push(data[i]);
            }
            return filtered;
        }
    });


    app.controller('ProductsListCtrl', ['$scope', '$rootScope','$state','infrastructure','shoppingCart','productsRepository','categoriesRepository','vendorSelectionRepository',
        function ($scope, $rootScope, $state, infrastructure, shoppingCart, productsRepository, categoriesRepository, vendorSelectionRepository) {
            var that = this;

            $scope.quantity = [];

            $scope.config = {
                manufacturersExpanded: false,
                filterMode: false
            };

            $scope.filter = {
                Manufacturers: [],
                MinPrice: 0.00,
                MaxPrice: 0.00
            };

            $scope.summaryMessage = "";
            $scope.orderByChoices = [
                { value: 'PriceMin', text: 'price'},
                { value: '-PriceMin', text: 'price desc' },
                { value: 'ManufacturerName', text: 'manufacturer' },
                { value: '-ManufacturerName', text: 'manufacturer desc' },
                { value: 'GTIN', text: 'gtin' },
                { value: '-GTIN', text: 'gtin desc' },
            ];

            var buildSummaryMessage = function (productsQuery) {
                if ($scope.queryResults && $scope.queryResults.TotalCount > 0) {
                    var firstN = $scope.queryResults.PageSize * ($scope.queryResults.PageNumber - 1) + 1;
                    var lastN = $scope.queryResults.PageSize * $scope.queryResults.PageNumber;
                    if (lastN > $scope.queryResults.TotalCount)
                        lastN = $scope.queryResults.TotalCount;
                    $scope.summaryMessage = firstN + '-' + lastN + ' of ' + $scope.queryResults.TotalCount + ' results for "' + productsQuery.Keywords + '"';
                }
                else {
                    $scope.summaryMessage = 'No results for "' + productsQuery.Keywords + '"';
                }
            };

            var reloadResults = function (productsQuery) {

                //$scope.queryResults = undefined;


                var cache = infrastructure.findInSearchLog(productsQuery.CategoryId,
                    productsQuery.Keywords,
                    productsQuery.PageNumber,
                    productsQuery.PageSize,
                    productsQuery.OrderBy,

                );

                if ($scope.config.filterMode || typeof cache === 'undefined' || cache.length == 0) {

                    if ($scope.config.filterMode) {
                        productsQuery.PriceMin = undefined;
                        if ($scope.filter && typeof $scope.filter.MinPrice !== 'undefined' && !isNaN($scope.filter.MinPrice) && $scope.filter.MinPrice > 0)
                            productsQuery.PriceMin = $scope.filter.MinPrice;

                        productsQuery.PriceMax = undefined;
                        if ($scope.filter && typeof $scope.filter.MaxPrice !== 'undefined' && !isNaN($scope.filter.MaxPrice) && $scope.filter.MaxPrice > 0)
                            productsQuery.PriceMax = $scope.filter.MaxPrice;

                        productsQuery.Manufacturers = undefined;
                        if ($scope.filter && typeof $scope.filter.Manufacturers !== 'undefined' && $scope.filter.Manufacturers.length && $scope.filter.Manufacturers.length > 0) {
                            // get all input checkboxes from manufacturers list
                            var existingManufacturers = angular.element(".icheck-input");
                            if (existingManufacturers && existingManufacturers != null) {
                                productsQuery.Manufacturers = [];
                                angular.forEach($scope.filter.Manufacturers, function (elm, idx) {
                                    if (elm)
                                        productsQuery.Manufacturers.push(existingManufacturers[idx].id);
                                });
                            }
                        }
                    }


                    productsRepository.findBy(
                        productsQuery,
                        function (data) {

                            if (!$scope.config.filterMode)
                                $scope.queryResults = data;
                            else {
                                
                                $scope.queryResults.OrderBy = data.OrderBy;
                                $scope.queryResults.PageNumber = data.PageNumber;
                                $scope.queryResults.PageSize = data.PageSize;
                                $scope.queryResults.Results = data.Results;
                                $scope.queryResults.TotalCount = data.TotalCount;
                                $scope.queryResults.TotalPages = data.TotalPages;
                            }

                            if (productsQuery.PageNumber != data.PageNumber)
                                productsQuery.PageNumber = data.PageNumber;


                            buildSummaryMessage(productsQuery);

                            infrastructure.add2SearchLog(
                                productsQuery.CategoryId,
                                productsQuery.Keywords,
                                productsQuery.PageNumber,
                                productsQuery.PageSize,
                                productsQuery.OrderBy,
                                data
                            );

                        }
                        , function (reason) {
                            $scope.queryResults = reason;
                            buildSummaryMessage(productsQuery);
                        });
                }
                else {
                    $scope.queryResults = cache[0].Results;
                    buildSummaryMessage(productsQuery);
                }

                infrastructure.setLastSearchParams(productsQuery);
            };

            $scope.toggleManufacturersList = function () {
                $scope.config.manufacturersExpanded = !$scope.config.manufacturersExpanded;
            };

            if ($state.params && $state.params.keyword && $state.params.keyword.length>=3 && ($state.params.categoryId || $state.params.categoryId === 0) && ($state.params.pageNumber || $state.params.pageNumber === 0)) {

                $scope.productsQuery = {
                    CategoryId: $state.params.categoryId,
                    Keywords: $state.params.keyword,
                    PageNumber: $state.params.pageNumber,
                    PageSize: "8",
                    OrderBy: 'PriceMin'
                };

                $scope.config.filterMode = false;
                reloadResults($scope.productsQuery);

            }
            else {
                $scope.summaryMessage ="please enter proper search keywords.";
            };

            $scope.resortResults = function () {
                reloadResults($scope.productsQuery);
            };

            $scope.refilterResults = function () {
                reloadResults($scope.productsQuery);
            };

            $scope.sortByChanged = function () {
                $scope.resortResults();
            };

            $scope.manufacturersChanged = function () {
                $scope.config.filterMode = true;
                $scope.refilterResults();
            };

            $scope.onSliderFinish = function () {
                $scope.config.filterMode = true;
                $scope.refilterResults();
            };

            //#region  pagination

            // designed to show 2 rows by 4, 8 products per page
            $scope.Groups = [{ mod4: 0 }, { mod4: 1 }];

            $scope.navigateToPage = function (pageNum,currPage) {
                if (pageNum != currPage) {

                    $scope.productsQuery.PageNumber = pageNum;
                    reloadResults($scope.productsQuery);
                    //$state.go('search', { pageNumber: pageNum, categoryId: $scope.productsQuery.CategoryId, keyword: $scope.productsQuery.Keywords });

                }
            };
            //#endregion  pagination

            $scope.addToBasket = function (gtinplus, idx) {
                if (!$scope.quantity || typeof $scope.quantity[idx] === 'undefined')
                    $scope.quantity[idx] = 1;

                console.log('gtinplus: ' + gtinplus + ', qty: ' + $scope.quantity[idx] + ', zip: 33065');

                vendorSelectionRepository.getMatches({
                    ShoppingCart:
                    [{ GtinPlus: gtinplus, Quantity: $scope.quantity[idx], DestZipCode: "33065" }],
                    IncludeInventory: false
                },
                    function (data) {
                        var response = shoppingCart.setSupplier($scope.queryResults.Results[idx], data.Matches, $scope.quantity[idx]);

                        //$scope.productInformation = response.productInformation;
                        //$scope.addToCart.quantity = response.addToCartQuantity;
                        //$scope.quantityChoices = response.quantityChoices;

                        shoppingCart.addToCart($scope.queryResults.Results[idx], response.addToCartQuantity, "33065", "Add");
                        console.log(JSON.stringify(data));
                    },
                    function (reason) {
                        console.log(JSON.stringify(reason));
                    }
                );
            };


            $scope.categories = {};
            $scope.currentCategory = [{ CategoryDescription: "AllCategories", CategoryId: 0, Path: undefined, SubCategories: [] }];

            /*
            categoriesRepository.findBy({ rootCategoryId: $scope.productsQuery.CategoryId }
                , function (data) {
                    $scope.currentCategory[0].SubCategories = data.Categories;
                    $scope.currentCategory[0].Path = data.Path;
                }
                , function (reason) {
                    $scope.categories = reason;
                })
                */

        }]);
})(window.angular);