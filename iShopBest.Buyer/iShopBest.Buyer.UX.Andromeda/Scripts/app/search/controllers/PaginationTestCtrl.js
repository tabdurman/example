﻿(function (angular) {
    var app=angular.module('app');

    app.controller('PaginationTestCtrl', ['$scope',
        function ($scope) {
            var that = this;

            $scope.currentPageX = 1;
            $scope.totalPagesX = 100;
            $scope.navigateToPage = function (toPage, fromPage) {
                if (toPage != fromPage) {

                    // $scope.productsQuery.PageNumber = pageNum;

                    // reloadResults($scope.productsQuery);
                    $state.go('search', { pageNumber: toPage, categoryId: $scope.productsQuery.CategoryId, keyword: $scope.productsQuery.Keywords });

                }

            };


        }]);
})(window.angular);