﻿(function (angular) {
    var app=angular.module('app');
    app.controller('ProductDetailCtrl', ['$scope', '$rootScope','$state','$filter','infrastructure','shoppingCart','productsRepository','vendorSelectionRepository',
        function ($scope, $rootScope, $state, $filter, infrastructure, shoppingCart, productsRepository, vendorSelectionRepository) {
            var that = this;


            if ($state.params && ($state.params.productId || $state.params.productId > 0)) {
                $scope.addToCart = { quantity: 1, gtinplus: $state.params.productId };
            }



            var refillDetails = function (currentProduct, match) {
                return shoppingCart.refillDetails(currentProduct, match);
                /*
                currentProduct.SupplierId = match.SupplierId;
                currentProduct.SupplierName = match.SupplierName;
                currentProduct.SupplierRating = match.SupplierRating;
                currentProduct.LeadTime = match.LeadTime;
                currentProduct.EffectiveDate = match.EffectiveDate;
                currentProduct.ExpirationDate = match.ExpirationDate;
                currentProduct.MinimumQty = match.MinimnumQty;
                currentProduct.Sku = match.Sku;
                currentProduct.Cost = match.Cost;
                currentProduct.PackSku = match.PackSku;
                currentProduct.PackQty = match.PackQty;
                currentProduct.PackPrice = match.PackPrice;
                currentProduct.PackRequired = match.PackRequired;
                currentProduct.SupplierUnit = match.Unit;
                currentProduct.SupplierCountryOfOrigin = match.CountryOfOrigin;
                currentProduct.SupplierPackUnit = match.PackUnit;
                currentProduct.TAA = match.TAA;
                return currentProduct;
                */
            };


            var setSupplier = function () {
                var response = shoppingCart.setSupplier($scope.productInformation, $scope.matchesInformation, $scope.addToCart.quantity);
                $scope.productInformation = response.productInformation;
                $scope.addToCart.quantity = response.addToCartQuantity;
                $scope.quantityChoices = response.quantityChoices;

                /*
                if ($scope.productInformation) {
                    var found = [];
                    var defaultSupplierId = infrastructure.getSupplierId();

                    if (!$scope.productInformation.SupplierId && defaultSupplierId)
                        $scope.productInformation.SupplierId = defaultSupplierId;

                    if ($scope.productInformation.SupplierId)
                        found = $filter('filter')($scope.matchesInformation, { SupplierId: $scope.productInformation.SupplierId }, true);
                    else {
                        found = $filter('filter')($scope.matchesInformation, function (value) { return value.MinimumQty >= $scope.addToCart.quantity }, true);
                        if (found.length > 0)
                            found = $filter('orderBy')($scope.matchesInformation, 'Cost'); // get the lowest price match
                        else
                            found = [];
                    }

                    if (found.length > 0) {
                        $scope.productInformation = refillDetails($scope.productInformation, found[0]);
                        $scope.quantityChoices=infrastructure.buildChoices(found[0].MinimumQty, found[0].PackQty, found[0].PackRequired);
                        $scope.addToCart.quantity = found[0].MinimumQty;

                    }
                }
                */
            }

            productsRepository.getDetails(
                { productId: $scope.addToCart.gtinplus}
                , function (data) {
                    $scope.productInformation = data.ProductDetails;
                    vendorSelectionRepository.getMatches({
                        ShoppingCart:
                            [{ GtinPlus: $scope.productInformation.ProductId, Quantity: 1, DestZipCode: "33065" }],
                        IncludeInventory: false
                    },
                    function (data) {
                        $scope.matchesInformation = data.Matches;
                        setSupplier();
                    },
                    function (reason) {
                        var x = reason;
                    });
                }
                , function (reason) {
                    $scope.productInformation = undefined;
                }
                );


            $scope.$watch(function (scope) {
                if (scope.productInformation)
                    return scope.productInformation.SupplierId;
                else
                    return undefined;
            },
              function (newValue, oldValue) {
                  setSupplier();
              }
             );

            $scope.addToBasket = function () {
                var response = shoppingCart.setSupplier($scope.productInformation, $scope.matchesInformation, $scope.addToCart.quantity);

                $scope.productInformation = response.productInformation;
                $scope.addToCart.quantity = response.addToCartQuantity;
                $scope.quantityChoices = response.quantityChoices;

                shoppingCart.addToCart($scope.productInformation, $scope.addToCart.quantity, "33065", "Add");
            };
        }]);
})(window.angular);