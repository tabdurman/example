﻿(function (angular) {
    var app = angular.module('app');
    app.controller('BasketCtrl', ['$scope', '$rootScope', '$state', '$filter', 'infrastructure', 'shoppingCart', 'vendorSelectionRepository',
        function ($scope, $rootScope, $state, $filter, infrastructure, shoppingCart, vendorSelectionRepository) {
            var that = this;


            $scope.scenariosPaginationConfig = {
                PageSize: 6,
                TotalPages: 1,
                CurrentPage: 1,
                StartFrom: 1
            };

            $scope.filter = {
                sellers: []
            };

            $scope.values2 = ['Low', 'Dont know', 'High'];

            $scope.onSliderFinish = function () {
                $scope.config.filterMode = true;
                $scope.refilterResults();
            };

            $scope.navigateToPage = function (toPage, fromPage) {
                if (toPage != fromPage) {
                    $scope.scenariosPaginationConfig.CurrentPage = toPage;
                    $scope.scenariosPaginationConfig.StartFrom = ($scope.scenariosPaginationConfig.CurrentPage - 1) * $scope.scenariosPaginationConfig.PageSize + 1;

                    $scope.scenariosToShow = [];

                    for (var i = $scope.scenariosPaginationConfig.StartFrom - 1; (i < $scope.scenariosPaginationConfig.StartFrom + $scope.scenariosPaginationConfig.PageSize - 1 && i < $scope.vendorSelectionResults.length) ; i++) {
                        $scope.scenariosToShow.push($scope.vendorSelectionResults[i]);
                    };
                }
            };

            $scope.toggleHeaderContainer = function (idx) {
                if ($scope.vendorSelectionResults) {

                    if (!$scope.vendorSelectionResults[idx].collapse)
                        $scope.vendorSelectionResults[idx].collapse = true;
                    else
                        $scope.vendorSelectionResults[idx].collapse = false;

                }
            };

            $scope.getPrice = function (result, gtinplus) {
                var found = $filter('filter')(result.OrderLines, { GTINPLUS: gtinplus }, true);
                if (found && found.length > 0)
                    return found[0].Price;
            };

            $scope.getValue = function (result, gtinplus, property) {
                var found = $filter('filter')(result.OrderLines, { GTINPLUS: gtinplus }, true);
                if (found && found.length > 0) {
                    switch (property)
                    {
                        case 'supplier':
                            return found[0].SupplierName;
                        case 'sku':
                            return found[0].Sku;
                        case 'distance':
                            return found[0].WarehouseName + ((found[0].WarehouseName == 'Backorder') ? '' : ' ( ' + $filter('number')(found[0].Distance,2) + ' miles away)');
                        case 'price':
                        default:
                            return found[0].Price;
                    }
                }
            };

            var prepareScenarios = function () {
                if (!$scope.vendorSelectionResults || $scope.vendorSelectionResults.length < 1)
                    return;

                $scope.scenariosPaginationConfig.TotalPages = ($scope.vendorSelectionResults.length / $scope.scenariosPaginationConfig.PageSize);
                if (($scope.vendorSelectionResults.length % $scope.scenariosPaginationConfig.PageSize) > 0)
                    $scope.scenariosPaginationConfig.TotalPages++;
                $scope.navigateToPage(1, 0);
            }

            var initiateSellers = function (matches) {
                $scope.sellers = [];
                if (matches && matches.length && matches.length > 0) {
                    angular.forEach(matches, function (elm, idx) {
                        if (elm.SupplierName) {
                            var found = $filter('filter')($scope.sellers, { name: elm.SupplierName }, true);
                            if (!found || found.length == 0)
                                $scope.sellers.push({ name: elm.SupplierName, id: elm.SupplierId  });                            
                        };
                    });
                };
                var x = $scope.sellers.length;
            };

            var initiateClassification = function () {
                $scope.classifications = [
                    { name: 'Small Business', id: 1 },
                    { name: 'Alaska Native Corp.', id: 2 },
                    { name: 'HUB Zone', id: 3 },
                ];
            }

            $scope.applySellersFilter = function () {

            };

            $scope.$watch(function (scope) {
                return shoppingCart.getItemsCount()
            },
                function (newValue, oldValue) {
                if (newValue > 0) {
                    $scope.itemsCount = newValue;
                    $scope.basket = shoppingCart.getSaveCartViewModel();
                    angular.forEach($scope.basket.shoppingCartItems, function (elem, index) {
                        elem.quantityChoices = infrastructure.buildChoices(elem.MinimumQty, elem.PackQty, elem.PackRequired);
                    });
                    $scope.go2Scenarios();
                    }
                }
            );
            
            $scope.removeItem = function (gtinplus) {
                shoppingCart.deleteFromCart(gtinplus);
            };

            $scope.prefs=infrastructure.getPreferences();
            if (!$scope.prefs || $scope.prefs.length === 0)
                infrastructure.initPreferences();
            $scope.prefs = infrastructure.getPreferences();

            $scope.switch2Scenarios = function () {
                $scope.showScenarios = true;
                $scope.showResults = undefined;
            }

            $scope.go2Scenarios = function () {

                //$scope.switch2Scenarios();

                var params = {
                    Preferences: [],
                    ShoppingCart: [],
                    IncludeInventory: true
                }

                angular.forEach($scope.prefs, function (val, idx) {
                    params.Preferences.push({ Name: val.name, Value: val.value });
                });

                angular.forEach($scope.basket.shoppingCartItems, function (val, idx) {
                    params.ShoppingCart.push({ GtinPlus: val.ProductId, Quantity: val.Quantity, DestZipCode: val.DestZipCode });
                });

                $scope.vendorSelectionResults = undefined;

                vendorSelectionRepository.run(params,
                    function (data) {
                        $scope.vendorSelectionResults = $filter('orderBy')(data.Orders, '-Score');
                        if ($scope.vendorSelectionResults && $scope.vendorSelectionResults.length>0)
                            $scope.vendorSelectionResults[0].collapse = true;
                        prepareScenarios();
                        initiateSellers(data.Matches);
                        initiateClassification();
                    },
                    function (reason) {
                        $scope.vendorSelectionResults = undefined;
                    });
            };

            $scope.switch2Results = function () {
                $scope.showResults = true;
                $scope.showScenarios = undefined;
            };

            $scope.go2Results = function (idx) {
                $scope.switch2Results();
                if (idx < $scope.vendorSelectionResults.length) {
                    //get result data
                    $scope.scenarioDetailsToShow = $scope.vendorSelectionResults[idx].OrderLines;

                    angular.forEach($scope.scenarioDetailsToShow, function (elm, idx) {
                        //find additional data
                        var found = $filter('filter')($scope.basket.shoppingCartItems, { ProductId: elm.GTINPLUS }, true);
                        if (found && found.length > 0) {
                            elm.ImageUrl = found[0].ImageUrl;
                            elm.ProductTitle = found[0].ProductTitle;
                            elm.SupplierUnit = found[0].SupplierUnit
                        }
                    });
                }

            };

            $scope.go2Basket = function () {
                $scope.showScenarios = undefined;
                $scope.showResults = undefined;
            }

            $scope.go2ProductDetails = function (gtinplus) {
                $state.go('product', { productId:gtinplus });

            }


        }]);
})(window.angular);