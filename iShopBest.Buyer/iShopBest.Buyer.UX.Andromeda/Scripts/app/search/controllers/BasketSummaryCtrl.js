﻿(function (angular) {
    var app = angular.module('app');

    app.controller('BasketSummaryCtrl', ['$scope', '$state','shoppingCart',
        function ($scope, $state, shoppingCart) {
            var that = this;


            $scope.$watch(function (scope) {
                return shoppingCart.getItemsCount()
            },
            function (newValue, oldValue) {
                $scope.itemsCount = newValue;
            });


            //Load temp shopping cart, if no shopping cart loaded yet
            if (shoppingCart.getCurrentCartName() === 'not loaded')
                shoppingCart.loadCart(0,
                    function (data) {
                        shoppingCart.setSaveCartViewModel(data, true);
                        $scope.itemsCount = shoppingCart.getItemsCount();
                    },
                    function (reason) {
                        var x = reason;
                    });




        }]);
})(window.angular);