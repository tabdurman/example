﻿'use strict';
(function (angular) {

    angular.module("app", ["ui.router", "ngResource"]); //, "ui.bootstrap", "ngCookies", "ui.slider"]);
    var app = angular.module("app");
    app.value('runningMode', 'GLOBAL');

})(window.angular);