﻿using Owin;

namespace iShopBest.Buyer.UX.Andromeda
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

        }
    }
}