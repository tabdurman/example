﻿using AutoMapper;
using CapSoft.Identity.Presentation.Api.Config;
using iShopBest.Buyer.Presentation.Api.Config;

namespace iShopBest.Buyer.UX.Andromeda
{
    public class MappingConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg => {
                cfg.AddProfile<BuyerPresentationApiProfile>();
                cfg.AddProfile<IdentityPresentationApiProfile>();
            });
        }
    }
}