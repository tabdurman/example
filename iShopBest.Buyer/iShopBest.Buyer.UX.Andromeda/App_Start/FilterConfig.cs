﻿using System.Web.Mvc;

namespace iShopBest.Buyer.UX.Andromeda
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}