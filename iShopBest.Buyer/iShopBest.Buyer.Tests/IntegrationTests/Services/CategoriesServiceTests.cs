﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using iShopBest.Buyer.AppTools;
using CapSoft.Infrastructure.Domain;
using iShopBest.Buyer.Domain.Model.ServiceContracts;
using iShopBest.Buyer.Domain.Model.DataContracts.CategoriesService;

namespace iShopBest.Buyer.Tests.IntegrationTests.Services
{
    [TestClass]
    public class CategoriesServiceTests
    {
        [TestInitialize]
        public void Initialize()
        {
            Bootstrapper.Init("iShopBest.Buyer.App", "name=BuyerCatalogEntities");
        }

        [TestMethod]
        public void CallGetTopLevelCategories()
        {
            Bootstrapper.Init("iShopBest.Buyer.App", "name=BuyerCatalogEntities");
            ICategoriesService service = ValueObjectBase.Container.GetExportedValue<ICategoriesService>();
            GetCategoriesResponse topCategories = service.GetCategories(null);
            Assert.IsNotNull(topCategories);
        }

        [TestMethod]
        public void CallFirstLevelCategories()
        {
            Bootstrapper.Init("iShopBest.Buyer.App", "name=BuyerCatalogEntities");
            ICategoriesService service = ValueObjectBase.Container.GetExportedValue<ICategoriesService>();
            GetCategoriesResponse topCategories = service.GetCategories(71);
            Assert.IsNotNull(topCategories);
        }

        [TestMethod]
        public void CallSecondLevelCategories()
        {
            Bootstrapper.Init("iShopBest.Buyer.App", "name=BuyerCatalogEntities");
            ICategoriesService service = ValueObjectBase.Container.GetExportedValue<ICategoriesService>();
            GetCategoriesResponse topCategories = service.GetCategories(43000000);
            Assert.IsNotNull(topCategories);
        }

        [TestMethod]
        public void CallThirdLevelCategories()
        {
            Bootstrapper.Init("iShopBest.Buyer.App", "name=BuyerCatalogEntities");
            ICategoriesService service = ValueObjectBase.Container.GetExportedValue<ICategoriesService>();
            GetCategoriesResponse topCategories = service.GetCategories(43220000);
            Assert.IsNotNull(topCategories);
        }

        [TestMethod]
        public void CallFourthLevelCategories()
        {
            Bootstrapper.Init("iShopBest.Buyer.App", "name=BuyerCatalogEntities");
            ICategoriesService service = ValueObjectBase.Container.GetExportedValue<ICategoriesService>();
            GetCategoriesResponse topCategories = service.GetCategories(43221500);
            Assert.IsNotNull(topCategories);
        }

        [TestMethod]
        public void CallGetProductsByCategory()
        {
            Bootstrapper.Init("iShopBest.Buyer.App", "name=BuyerCatalogEntities");
            ICategoriesService service = ValueObjectBase.Container.GetExportedValue<ICategoriesService>();
            GetProductsByCategoryResponse prods = service.GetProductsByCategory(new GetProductsByCategoryRequest() { CategoryId = 1368 });
            Assert.IsNotNull(prods);
        }
    }
}
