﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using iShopBest.Buyer.AppTools;
using iShopBest.Buyer.Domain.Model.ServiceContracts;
using CapSoft.Infrastructure.Domain;
using iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService;
using iShopBest.Buyer.Tests.IntegrationTests.Services.VendorSelectionTestParams;

namespace iShopBest.Buyer.Tests.IntegrationTests.Services
{
    [TestClass]
    public class VendorSelectionServiceTests
    {
        private IVendorSelectionService _testingService;

        [TestInitialize]
        public void Init()
        {
            Bootstrapper.Init("iShopBest.Buyer.App", "name=BuyerCatalogEntities");
            _testingService = ValueObjectBase.Container.GetExportedValue<IVendorSelectionService>();
        }

        [TestMethod]
        public void TestGetMatches()
        {
            GetMatchesRequest request = new GetMatchesRequest()
            {
                IncludeInventory = true,
                ShoppingCart = VendorSelectionServiceTestRequests.ShoppingCart
            };

            GetMatchesResponse response= _testingService.GetMatches(request);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void TestGenerateRoutes()
        {
            GenerateRoutesResponse response = null;

            GenerateRoutesRequest request = new GenerateRoutesRequest()
            {
                ShoppingCart = VendorSelectionServiceTestRequests.ShoppingCart,
                Preferences = VendorSelectionServiceTestRequests.Preferences,
                MatchResults=new GetMatchesResponse()
                {
                    Inventories = VendorSelectionServiceTestRequests.Inventories,
                    Matches = VendorSelectionServiceTestRequests.Matches
                },
                IncludeInventory = true
            };

            response = _testingService.GenerateRoutes(request);

            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void TestRunVendorSelection()
        {
            RunVendorSelectionResponse response = null;

            RunVendorSelectionRequest request = new RunVendorSelectionRequest()
            {
                ShoppingCart = VendorSelectionServiceTestRequests.ShoppingCart,
                Preferences = VendorSelectionServiceTestRequests.Preferences,
                IncludeInventory = true
            };

            response = _testingService.RunVendorSelection(request);

            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void RunVendorSelection4Cartridges()
        {
            VSSRunVendorSelectionInput input = new VSSRunVendorSelectionInput();
            
            var response = _testingService.RunVendorSelection(input.GetSecondRequest());
            Assert.IsNotNull(response);
        }
    }
}


