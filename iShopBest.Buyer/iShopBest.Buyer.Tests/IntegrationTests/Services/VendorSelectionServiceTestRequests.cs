﻿using iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService;
using System.Collections.Generic;
using iShopBest.Buyer.Domain.Model.Entities;

namespace iShopBest.Buyer.Tests.IntegrationTests.Services
{
    public static class VendorSelectionServiceTestRequests
    {
        private static List<Parameter> _preferences;
        private static List<ItemRequirement> _shoppingCart;
        private static List<Inventory> _inventories;
        private static List<Price> _matches;

        public static List<Parameter> Preferences
        {

            get
            {
                if (_preferences == null)
                    _preferences = new List<Parameter>
                    {
                        #region preferences
                        new Parameter
                        {
                            Name = "ProductRatingImportance",
                            Value = "30"
                        },
                        new Parameter
                        {
                            Name = "SupplierRatingImportance",
                            Value = "30"
                        },
                        new Parameter
                        {
                            Name = "WarehouseRatingImportance",
                            Value = "30"
                        },
                        new Parameter
                        {
                            Name = "DeliverySpeedImportance",
                            Value = "30"
                        },
                        new Parameter
                        {
                            Name = "FreightCostImportance",
                            Value = "30"
                        },
                        new Parameter
                        {
                            Name = "PackagesCountImportance",
                            Value = "30"
                        },
                        new Parameter
                        {
                            Name = "SingleSupplierImportance",
                            Value = "30"
                        }
                        #endregion
                    };
                return _preferences;
            }
        }

        public static List<ItemRequirement> ShoppingCart
        {
            get
            {
                if (_shoppingCart == null)
                    _shoppingCart = new List<ItemRequirement>
                    {
                        new ItemRequirement()
                        {
                            GtinPlus = 1000043859543403,
                            Quantity =5,
                            DestLatitude = 26.2729m,
                            DestLongitude = -80.2603m,
                            DestZipCode ="33065"
                        },
                        new ItemRequirement()
                        {
                            GtinPlus = 1000023942491712,
                            DestLatitude = 26.1520m,
                            DestLongitude = -80.3165m,
                            Quantity =2,
                            DestZipCode ="33323"
                        },
                        new ItemRequirement()
                        {
                            GtinPlus = 1000023942986645,
                            Quantity = 6,
                            DestLatitude = 26.2729m,
                            DestLongitude = -80.2603m,
                            DestZipCode ="33065"
                        },
                        new ItemRequirement()
                        {
                            GtinPlus = 1000013803134971,
                            Quantity =1,
                            DestLatitude = 26.2729m,
                            DestLongitude = -80.2603m,
                            DestZipCode ="33065"
                        },
                    };

                return _shoppingCart;
            }

        }

        public static List<Inventory> Inventories
        {
            get
            {
                if (_inventories == null)
                    _inventories = new List<Inventory>
                    {
                        #region inventories
                        new Inventory
               {
                   Available = 6,

                   GTINPLUS = 1000013803134971,
                   Id = 5399,
                   Sku = "501447",
                   SrcLatitude = 32.7678M,
                   SrcLongitude = -96.6082M,
                   SrcZipCode = "75149",
                   SupplierId = 3,
                   TotalAvailable = 75,
                   Unit = "EA",
                   WarehouseId = 4,
                   WarehouseName = "TX",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 25,

                   GTINPLUS = 1000013803134971,
                   Id = 5755,
                   Sku = "501447",
                   SrcLatitude = 36.6207M,
                   SrcLongitude = -119.7308M,
                   SrcZipCode = "93725",
                   SupplierId = 3,
                   TotalAvailable = 75,
                   Unit = "EA",
                   WarehouseId = 1,
                   WarehouseName = "CA",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 5979,
                   Sku = "501447",
                   SrcLatitude = 40.2417M,
                   SrcLongitude = -77.1983M,
                   SrcZipCode = "17013",
                   SupplierId = 3,
                   TotalAvailable = 75,
                   Unit = null,
                   WarehouseId = 3,
                   WarehouseName = "PA",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 44,

                   GTINPLUS = 1000013803134971,
                   Id = 6915,
                   Sku = "501447",
                   SrcLatitude = 38.7809M,
                   SrcLongitude = -90.3669M,
                   SrcZipCode = "63042",
                   SupplierId = 3,
                   TotalAvailable = 75,
                   Unit = "EA",
                   WarehouseId = 2,
                   WarehouseName = "MO",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 66777,
                   Sku = "3097682",
                   SrcLatitude = 33.9381M,
                   SrcLongitude = -84.1972M,
                   SrcZipCode = "30071",
                   SupplierId = 4,
                   TotalAvailable = 57,
                   Unit = null,
                   WarehouseId = 12,
                   WarehouseName = "4",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 68562,
                   Sku = "3097682",
                   SrcLatitude = 36.2115M,
                   SrcLongitude = -115.1241M,
                   SrcZipCode = "89030",
                   SupplierId = 4,
                   TotalAvailable = 57,
                   Unit = null,
                   WarehouseId = 10,
                   WarehouseName = "2",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 6,

                   GTINPLUS = 1000013803134971,
                   Id = 69735,
                   Sku = "3097682",
                   SrcLatitude = 41.9205M,
                   SrcLongitude = -88.0793M,
                   SrcZipCode = "60139",
                   SupplierId = 4,
                   TotalAvailable = 57,
                   Unit = "EA",
                   WarehouseId = 14,
                   WarehouseName = "6",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 8,

                   GTINPLUS = 1000013803134971,
                   Id = 69736,
                   Sku = "3097682",
                   SrcLatitude = 38.8867M,
                   SrcLongitude = -77.4457M,
                   SrcZipCode = "20151",
                   SupplierId = 4,
                   TotalAvailable = 57,
                   Unit = "EA",
                   WarehouseId = 17,
                   WarehouseName = "9",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 6,

                   GTINPLUS = 1000013803134971,
                   Id = 70927,
                   Sku = "3097682",
                   SrcLatitude = 45.4560M,
                   SrcLongitude = -122.7996M,
                   SrcZipCode = "97008",
                   SupplierId = 4,
                   TotalAvailable = 57,
                   Unit = "EA",
                   WarehouseId = 5,
                   WarehouseName = "10",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 70928,
                   Sku = "3097682",
                   SrcLatitude = 25.7735M,
                   SrcLongitude = -80.3572M,
                   SrcZipCode = "33172",
                   SupplierId = 4,
                   TotalAvailable = 57,
                   Unit = null,
                   WarehouseId = 9,
                   WarehouseName = "16",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 13,

                   GTINPLUS = 1000013803134971,
                   Id = 70929,
                   Sku = "3097682",
                   SrcLatitude = 34.9441M,
                   SrcLongitude = -89.8544M,
                   SrcZipCode = "38654",
                   SupplierId = 4,
                   TotalAvailable = 57,
                   Unit = "EA",
                   WarehouseId = 15,
                   WarehouseName = "7",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 71422,
                   Sku = "3097682",
                   SrcLatitude = 34.0183M,
                   SrcLongitude = -117.8546M,
                   SrcZipCode = "91789",
                   SupplierId = 4,
                   TotalAvailable = 57,
                   Unit = null,
                   WarehouseId = 6,
                   WarehouseName = "11",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 4,

                   GTINPLUS = 1000013803134971,
                   Id = 72072,
                   Sku = "3097682",
                   SrcLatitude = 39.8814M,
                   SrcLongitude = -83.0839M,
                   SrcZipCode = "43123",
                   SupplierId = 4,
                   TotalAvailable = 57,
                   Unit = "EA",
                   WarehouseId = 8,
                   WarehouseName = "14",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 11,

                   GTINPLUS = 1000013803134971,
                   Id = 72475,
                   Sku = "3097682",
                   SrcLatitude = 40.5192M,
                   SrcLongitude = -74.3021M,
                   SrcZipCode = "08832",
                   SupplierId = 4,
                   TotalAvailable = 57,
                   Unit = "EA",
                   WarehouseId = 16,
                   WarehouseName = "8",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 3,

                   GTINPLUS = 1000013803134971,
                   Id = 72755,
                   Sku = "3097682",
                   SrcLatitude = 32.9462M,
                   SrcLongitude = -96.7058M,
                   SrcZipCode = "75081",
                   SupplierId = 4,
                   TotalAvailable = 57,
                   Unit = "EA",
                   WarehouseId = 13,
                   WarehouseName = "5",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 72807,
                   Sku = "3097682",
                   SrcLatitude = 37.5176M,
                   SrcLongitude = -121.9287M,
                   SrcZipCode = "94539",
                   SupplierId = 4,
                   TotalAvailable = 57,
                   Unit = null,
                   WarehouseId = 11,
                   WarehouseName = "3",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 3,

                   GTINPLUS = 1000013803134971,
                   Id = 73578,
                   Sku = "3097682",
                   SrcLatitude = 33.8193M,
                   SrcLongitude = -118.2325M,
                   SrcZipCode = "90810",
                   SupplierId = 4,
                   TotalAvailable = 57,
                   Unit = "EA",
                   WarehouseId = 7,
                   WarehouseName = "12",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 1129437,
                   Sku = "CL241XL",
                   SrcLatitude = 36.6207M,
                   SrcLongitude = -119.7308M,
                   SrcZipCode = "93725",
                   SupplierId = 5,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 92,
                   WarehouseName = "California",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 1143801,
                   Sku = "CL241XL",
                   SrcLatitude = 33.3958M,
                   SrcLongitude = -84.7121M,
                   SrcZipCode = "30265",
                   SupplierId = 5,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 18,
                   WarehouseName = "Atlanta",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 1143802,
                   Sku = "CL241XL",
                   SrcLatitude = 41.6790M,
                   SrcLongitude = -88.1403M,
                   SrcZipCode = "60490",
                   SupplierId = 5,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 20,
                   WarehouseName = "Chicago",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 1252600,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 27.9951M,
                   SrcLongitude = -82.4046M,
                   SrcZipCode = "33610",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 53,
                   WarehouseName = "004",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 1257493,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 40.3039M,
                   SrcLongitude = -74.5065M,
                   SrcZipCode = "08512",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 54,
                   WarehouseName = "044",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 1259446,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 39.9683M,
                   SrcLongitude = -74.9533M,
                   SrcZipCode = "08057",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 35,
                   WarehouseName = "023",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 1259447,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 39.8682M,
                   SrcLongitude = -86.2123M,
                   SrcZipCode = "46268",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 36,
                   WarehouseName = "024",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 1260246,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 30.3415M,
                   SrcLongitude = -81.7358M,
                   SrcZipCode = "32254",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 39,
                   WarehouseName = "027",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 1260247,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 25.8856M,
                   SrcLongitude = -80.2292M,
                   SrcZipCode = "33167",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 42,
                   WarehouseName = "003",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 1260248,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 33.4564M,
                   SrcLongitude = -112.1284M,
                   SrcZipCode = "85009",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 49,
                   WarehouseName = "036",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 1260249,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 33.0033M,
                   SrcLongitude = -96.8820M,
                   SrcZipCode = "75007",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 56,
                   WarehouseName = "005",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 1260552,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 39.9046M,
                   SrcLongitude = -82.9703M,
                   SrcZipCode = "43207",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 60,
                   WarehouseName = "009",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 1263357,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 40.7498M,
                   SrcLongitude = -111.9260M,
                   SrcZipCode = "84104",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 46,
                   WarehouseName = "033",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 1264302,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 33.8631M,
                   SrcLongitude = -84.5382M,
                   SrcZipCode = "30082",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 22,
                   WarehouseName = "001",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 1264303,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 29.5790M,
                   SrcLongitude = -98.2778M,
                   SrcZipCode = "78154",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 28,
                   WarehouseName = "015",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 1265792,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 26.2729M,
                   SrcLongitude = -80.2603M,
                   SrcZipCode = "33065",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 55,
                   WarehouseName = "046",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 1267239,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 38.7229M,
                   SrcLongitude = -90.4474M,
                   SrcZipCode = "63043",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 31,
                   WarehouseName = "018",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 1270433,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 37.5748M,
                   SrcLongitude = -77.4267M,
                   SrcZipCode = "23222",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 25,
                   WarehouseName = "012",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 2131420,
                   Sku = "CNM5208B001",
                   SrcLatitude = 35.0231M,
                   SrcLongitude = -89.8492M,
                   SrcZipCode = "38141",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = null,
                   WarehouseId = 95,
                   WarehouseName = "MTN",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 2136214,
                   Sku = "CNM5208B001",
                   SrcLatitude = 39.8682M,
                   SrcLongitude = -86.2123M,
                   SrcZipCode = "46268",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = null,
                   WarehouseId = 94,
                   WarehouseName = "IDY",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 18,

                   GTINPLUS = 1000013803134971,
                   Id = 2137054,
                   Sku = "CNM5208B001",
                   SrcLatitude = 43.0073M,
                   SrcLongitude = -85.7255M,
                   SrcZipCode = "49544",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = "EA",
                   WarehouseId = 73,
                   WarehouseName = "GMI",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 2145320,
                   Sku = "CNM5208B001",
                   SrcLatitude = 39.1479M,
                   SrcLongitude = -94.5680M,
                   SrcZipCode = "64116",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = null,
                   WarehouseId = 76,
                   WarehouseName = "KAN",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 61,

                   GTINPLUS = 1000013803134971,
                   Id = 2145321,
                   Sku = "CNM5208B001",
                   SrcLatitude = 44.8471M,
                   SrcLongitude = -93.1543M,
                   SrcZipCode = "55121",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = "EA",
                   WarehouseId = 77,
                   WarehouseName = "MIN",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 32,

                   GTINPLUS = 1000013803134971,
                   Id = 2145322,
                   Sku = "CNM5208B001",
                   SrcLatitude = 36.0127M,
                   SrcLongitude = -86.5600M,
                   SrcZipCode = "37086",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = "EA",
                   WarehouseId = 80,
                   WarehouseName = "NSH",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 27,

                   GTINPLUS = 1000013803134971,
                   Id = 2145323,
                   Sku = "CNM5208B001",
                   SrcLatitude = 42.4829M,
                   SrcLongitude = -71.1574M,
                   SrcZipCode = "01801",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = "EA",
                   WarehouseId = 91,
                   WarehouseName = "WOB",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 12,

                   GTINPLUS = 1000013803134971,
                   Id = 2148875,
                   Sku = "CNM5208B001",
                   SrcLatitude = 39.1551M,
                   SrcLongitude = -76.7215M,
                   SrcZipCode = "21076",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = "EA",
                   WarehouseId = 63,
                   WarehouseName = "BAL",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 89,

                   GTINPLUS = 1000013803134971,
                   Id = 2155585,
                   Sku = "CNM5208B001",
                   SrcLatitude = 35.2072M,
                   SrcLongitude = -80.9568M,
                   SrcZipCode = "28278",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = "EA",
                   WarehouseId = 67,
                   WarehouseName = "CNC",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 2155586,
                   Sku = "CNM5208B001",
                   SrcLatitude = 47.4483M,
                   SrcLongitude = -122.2731M,
                   SrcZipCode = "98188",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = null,
                   WarehouseId = 97,
                   WarehouseName = "SWA",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 41,

                   GTINPLUS = 1000013803134971,
                   Id = 2155993,
                   Sku = "CNM5208B001",
                   SrcLatitude = 39.9648M,
                   SrcLongitude = -83.1260M,
                   SrcZipCode = "43228",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = "EA",
                   WarehouseId = 64,
                   WarehouseName = "CBS",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 56,

                   GTINPLUS = 1000013803134971,
                   Id = 2155994,
                   Sku = "CNM5208B001",
                   SrcLatitude = 40.3039M,
                   SrcLongitude = -74.5065M,
                   SrcZipCode = "08512",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = "EA",
                   WarehouseId = 68,
                   WarehouseName = "CNJ",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 90,

                   GTINPLUS = 1000013803134971,
                   Id = 2155995,
                   Sku = "CNM5208B001",
                   SrcLatitude = 38.8933M,
                   SrcLongitude = -89.4052M,
                   SrcZipCode = "62246",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = "EA",
                   WarehouseId = 88,
                   WarehouseName = "STL",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 44,

                   GTINPLUS = 1000013803134971,
                   Id = 2159379,
                   Sku = "CNM5208B001",
                   SrcLatitude = 39.7388M,
                   SrcLongitude = -104.4083M,
                   SrcZipCode = "80238",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = "EA",
                   WarehouseId = 70,
                   WarehouseName = "DCO",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 2169994,
                   Sku = "CNM5208B001",
                   SrcLatitude = 45.4840M,
                   SrcLongitude = -122.6365M,
                   SrcZipCode = "97202",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = null,
                   WarehouseId = 83,
                   WarehouseName = "POR",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 7994141,
                   Sku = "CL241XL",
                   SrcLatitude = 36.6207M,
                   SrcLongitude = -119.7308M,
                   SrcZipCode = "93725",
                   SupplierId = 5,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 19,
                   WarehouseName = "California",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 8005134,
                   Sku = "CL241XL",
                   SrcLatitude = 40.2910M,
                   SrcLongitude = -76.8203M,
                   SrcZipCode = "17109",
                   SupplierId = 5,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 21,
                   WarehouseName = "Harrisburg",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 8110743,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 35.2522M,
                   SrcLongitude = -80.8265M,
                   SrcZipCode = "28206",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 33,
                   WarehouseName = "002",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 8113652,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 39.1484M,
                   SrcLongitude = -76.7922M,
                   SrcZipCode = "20794",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 32,
                   WarehouseName = "019",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 8113653,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 40.4322M,
                   SrcLongitude = -80.1021M,
                   SrcZipCode = "15205",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 51,
                   WarehouseName = "038",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 8113654,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 42.8802M,
                   SrcLongitude = -85.5352M,
                   SrcZipCode = "49512",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 52,
                   WarehouseName = "039",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 8118312,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 38.6406M,
                   SrcLongitude = -121.4440M,
                   SrcZipCode = "95838",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 43,
                   WarehouseName = "030",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 8124728,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 39.1222M,
                   SrcLongitude = -94.5487M,
                   SrcZipCode = "64120",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 58,
                   WarehouseName = "007",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 8128666,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 36.2019M,
                   SrcLongitude = -79.7101M,
                   SrcZipCode = "27214",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 34,
                   WarehouseName = "022",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 8128667,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 33.9938M,
                   SrcLongitude = -117.5236M,
                   SrcZipCode = "91752",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 44,
                   WarehouseName = "031",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 8135404,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 29.7971M,
                   SrcLongitude = -95.4958M,
                   SrcZipCode = "77055",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 59,
                   WarehouseName = "008",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 8136731,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 39.7340M,
                   SrcLongitude = -105.0259M,
                   SrcZipCode = "80204",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 24,
                   WarehouseName = "011",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 8136732,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 42.0076M,
                   SrcLongitude = -87.9931M,
                   SrcZipCode = "60007",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 48,
                   WarehouseName = "035",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 8144840,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 29.9871M,
                   SrcLongitude = -90.1695M,
                   SrcZipCode = "70001",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 30,
                   WarehouseName = "017",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 8144841,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 42.7717M,
                   SrcLongitude = -71.5132M,
                   SrcZipCode = "03063",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 47,
                   WarehouseName = "034",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 8151159,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 35.4444M,
                   SrcLongitude = -97.6164M,
                   SrcZipCode = "73128",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 26,
                   WarehouseName = "013",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 8151160,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 41.9099M,
                   SrcLongitude = -72.6029M,
                   SrcZipCode = "06088",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 27,
                   WarehouseName = "014",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 8151161,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 47.4483M,
                   SrcLongitude = -122.2731M,
                   SrcZipCode = "98188",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 40,
                   WarehouseName = "028",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 8151568,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 42.2768M,
                   SrcLongitude = -83.3758M,
                   SrcZipCode = "48184",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 45,
                   WarehouseName = "032",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 8151569,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 35.0231M,
                   SrcLongitude = -89.8492M,
                   SrcZipCode = "38141",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 57,
                   WarehouseName = "006",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 8154711,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 41.4886M,
                   SrcLongitude = -74.3450M,
                   SrcZipCode = "10941",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 50,
                   WarehouseName = "037",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 8155854,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 36.1901M,
                   SrcLongitude = -86.8053M,
                   SrcZipCode = "37228",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 38,
                   WarehouseName = "026",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 8164176,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 33.4816M,
                   SrcLongitude = -86.8590M,
                   SrcZipCode = "35211",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 29,
                   WarehouseName = "016",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 8189443,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 45.0132M,
                   SrcLongitude = -93.0297M,
                   SrcZipCode = "55109",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 23,
                   WarehouseName = "010",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 8189444,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 43.1624M,
                   SrcLongitude = -87.9898M,
                   SrcZipCode = "53223",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 37,
                   WarehouseName = "025",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 105,

                   GTINPLUS = 1000013803134971,
                   Id = 8906013,
                   Sku = "CNM5208B001",
                   SrcLatitude = 34.0425M,
                   SrcLongitude = -84.0262M,
                   SrcZipCode = "30024",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = "EA",
                   WarehouseId = 62,
                   WarehouseName = "ATL",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 635,

                   GTINPLUS = 1000013803134971,
                   Id = 8906014,
                   Sku = "CNM5208B001",
                   SrcLatitude = 41.9178M,
                   SrcLongitude = -88.1370M,
                   SrcZipCode = "60188",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = "EA",
                   WarehouseId = 65,
                   WarehouseName = "CHI",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 8906015,
                   Sku = "CNM5208B001",
                   SrcLatitude = 40.4344M,
                   SrcLongitude = -80.0248M,
                   SrcZipCode = "15086",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = null,
                   WarehouseId = 84,
                   WarehouseName = "PTS",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 8906016,
                   Sku = "CNM5208B001",
                   SrcLatitude = 40.3039M,
                   SrcLongitude = -74.5065M,
                   SrcZipCode = "08512",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = null,
                   WarehouseId = 93,
                   WarehouseName = "CNJ",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 6,

                   GTINPLUS = 1000013803134971,
                   Id = 9007247,
                   Sku = "CNM5208B001",
                   SrcLatitude = 29.8744M,
                   SrcLongitude = -95.5278M,
                   SrcZipCode = "77040",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = "EA",
                   WarehouseId = 74,
                   WarehouseName = "HOU",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 35,

                   GTINPLUS = 1000013803134971,
                   Id = 9007248,
                   Sku = "CNM5208B001",
                   SrcLatitude = 28.4522M,
                   SrcLongitude = -81.4678M,
                   SrcZipCode = "32819",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = "EA",
                   WarehouseId = 81,
                   WarehouseName = "ORL",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 30,

                   GTINPLUS = 1000013803134971,
                   Id = 9023299,
                   Sku = "CNM5208B001",
                   SrcLatitude = 33.3917M,
                   SrcLongitude = -111.9249M,
                   SrcZipCode = "85282",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = "EA",
                   WarehouseId = 82,
                   WarehouseName = "PAZ",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 107,

                   GTINPLUS = 1000013803134971,
                   Id = 9054580,
                   Sku = "CNM5208B001",
                   SrcLatitude = 33.7866M,
                   SrcLongitude = -118.2987M,
                   SrcZipCode = "91749",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = "EA",
                   WarehouseId = 69,
                   WarehouseName = "COI",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 29,

                   GTINPLUS = 1000013803134971,
                   Id = 9054581,
                   Sku = "CNM5208B001",
                   SrcLatitude = 40.7498M,
                   SrcLongitude = -111.9260M,
                   SrcZipCode = "84104",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = "EA",
                   WarehouseId = 87,
                   WarehouseName = "SLK",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 37,

                   GTINPLUS = 1000013803134971,
                   Id = 9092486,
                   Sku = "CNM5208B001",
                   SrcLatitude = 42.3501M,
                   SrcLongitude = -73.8199M,
                   SrcZipCode = "12051",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = "EA",
                   WarehouseId = 61,
                   WarehouseName = "ALB",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 26,

                   GTINPLUS = 1000013803134971,
                   Id = 9092487,
                   Sku = "CNM5208B001",
                   SrcLatitude = 41.3289M,
                   SrcLongitude = -81.4559M,
                   SrcZipCode = "44087",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = "EA",
                   WarehouseId = 66,
                   WarehouseName = "CLV",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 107,

                   GTINPLUS = 1000013803134971,
                   Id = 9092488,
                   Sku = "CNM5208B001",
                   SrcLatitude = 38.6865M,
                   SrcLongitude = -121.3494M,
                   SrcZipCode = "95842",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = "EA",
                   WarehouseId = 85,
                   WarehouseName = "SAC",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 53,

                   GTINPLUS = 1000013803134971,
                   Id = 9100553,
                   Sku = "CNM5208B001",
                   SrcLatitude = 32.8267M,
                   SrcLongitude = -96.9633M,
                   SrcZipCode = "75061",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = "EA",
                   WarehouseId = 71,
                   WarehouseName = "DFW",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 9148427,
                   Sku = "CNM5208B001",
                   SrcLatitude = 29.9511M,
                   SrcLongitude = -90.2060M,
                   SrcZipCode = "70123",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = null,
                   WarehouseId = 79,
                   WarehouseName = "NOL",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 9183579,
                   Sku = "CNM5208B001",
                   SrcLatitude = 40.7498M,
                   SrcLongitude = -111.9260M,
                   SrcZipCode = "84104",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = null,
                   WarehouseId = 96,
                   WarehouseName = "SLK",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 44,

                   GTINPLUS = 1000013803134971,
                   Id = 9217474,
                   Sku = "CNM5208B001",
                   SrcLatitude = 35.0231M,
                   SrcLongitude = -89.8492M,
                   SrcZipCode = "38141",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = "EA",
                   WarehouseId = 78,
                   WarehouseName = "MTN",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 13,

                   GTINPLUS = 1000013803134971,
                   Id = 9217475,
                   Sku = "CNM5208B001",
                   SrcLatitude = 47.4483M,
                   SrcLongitude = -122.2731M,
                   SrcZipCode = "98188",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = "EA",
                   WarehouseId = 89,
                   WarehouseName = "SWA",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 18,

                   GTINPLUS = 1000013803134971,
                   Id = 9217476,
                   Sku = "CNM5208B001",
                   SrcLatitude = 36.1398M,
                   SrcLongitude = -96.0297M,
                   SrcZipCode = "74158",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = "EA",
                   WarehouseId = 90,
                   WarehouseName = "TUL",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 197,

                   GTINPLUS = 1000013803134971,
                   Id = 9239217,
                   Sku = "CNM5208B001",
                   SrcLatitude = 26.0480M,
                   SrcLongitude = -80.3749M,
                   SrcZipCode = "33331",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = "EA",
                   WarehouseId = 72,
                   WarehouseName = "FTL",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 11510915,
                   Sku = "804397",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 15499,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = null,
                   WarehouseName = null,
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 14097710,
                   Sku = "S15208B001",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 29539,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = null,
                   WarehouseName = null,
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 14676719,
                   Sku = "CNMCL241XL",
                   SrcLatitude = 43.0734M,
                   SrcLongitude = -76.0558M,
                   SrcZipCode = "13057",
                   SupplierId = 7,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 41,
                   WarehouseName = "029",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 12,

                   GTINPLUS = 1000013803134971,
                   Id = 14735474,
                   Sku = "CNM5208B001",
                   SrcLatitude = 39.8682M,
                   SrcLongitude = -86.2123M,
                   SrcZipCode = "46268",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = "EA",
                   WarehouseId = 75,
                   WarehouseName = "IDY",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000013803134971,
                   Id = 14735475,
                   Sku = "CNM5208B001",
                   SrcLatitude = 29.5395M,
                   SrcLongitude = -98.4194M,
                   SrcZipCode = "78217",
                   SupplierId = 8,
                   TotalAvailable = 1989,
                   Unit = null,
                   WarehouseId = 86,
                   WarehouseName = "SAT",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 6,

                   GTINPLUS = 1000023942491712,
                   Id = 4776,
                   Sku = "912417",
                   SrcLatitude = 32.7678M,
                   SrcLongitude = -96.6082M,
                   SrcZipCode = "75149",
                   SupplierId = 3,
                   TotalAvailable = 6,
                   Unit = "EA",
                   WarehouseId = 4,
                   WarehouseName = "TX",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 5497,
                   Sku = "912417",
                   SrcLatitude = 36.6207M,
                   SrcLongitude = -119.7308M,
                   SrcZipCode = "93725",
                   SupplierId = 3,
                   TotalAvailable = 6,
                   Unit = null,
                   WarehouseId = 1,
                   WarehouseName = "CA",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 5546,
                   Sku = "912417",
                   SrcLatitude = 38.7809M,
                   SrcLongitude = -90.3669M,
                   SrcZipCode = "63042",
                   SupplierId = 3,
                   TotalAvailable = 6,
                   Unit = null,
                   WarehouseId = 2,
                   WarehouseName = "MO",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 6555,
                   Sku = "912417",
                   SrcLatitude = 40.2417M,
                   SrcLongitude = -77.1983M,
                   SrcZipCode = "17013",
                   SupplierId = 3,
                   TotalAvailable = 6,
                   Unit = null,
                   WarehouseId = 3,
                   WarehouseName = "PA",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 80332,
                   Sku = "3729282",
                   SrcLatitude = 41.9205M,
                   SrcLongitude = -88.0793M,
                   SrcZipCode = "60139",
                   SupplierId = 4,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 14,
                   WarehouseName = "6",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 81373,
                   Sku = "3729282",
                   SrcLatitude = 33.8193M,
                   SrcLongitude = -118.2325M,
                   SrcZipCode = "90810",
                   SupplierId = 4,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 7,
                   WarehouseName = "12",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 81603,
                   Sku = "3729282",
                   SrcLatitude = 40.5192M,
                   SrcLongitude = -74.3021M,
                   SrcZipCode = "08832",
                   SupplierId = 4,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 16,
                   WarehouseName = "8",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 81842,
                   Sku = "3729282",
                   SrcLatitude = 38.8867M,
                   SrcLongitude = -77.4457M,
                   SrcZipCode = "20151",
                   SupplierId = 4,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 17,
                   WarehouseName = "9",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 85457,
                   Sku = "3729282",
                   SrcLatitude = 39.8814M,
                   SrcLongitude = -83.0839M,
                   SrcZipCode = "43123",
                   SupplierId = 4,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 8,
                   WarehouseName = "14",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 86347,
                   Sku = "3729282",
                   SrcLatitude = 33.9381M,
                   SrcLongitude = -84.1972M,
                   SrcZipCode = "30071",
                   SupplierId = 4,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 12,
                   WarehouseName = "4",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 86496,
                   Sku = "3729282",
                   SrcLatitude = 34.9441M,
                   SrcLongitude = -89.8544M,
                   SrcZipCode = "38654",
                   SupplierId = 4,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 15,
                   WarehouseName = "7",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 87475,
                   Sku = "3729282",
                   SrcLatitude = 37.5176M,
                   SrcLongitude = -121.9287M,
                   SrcZipCode = "94539",
                   SupplierId = 4,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 11,
                   WarehouseName = "3",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 87476,
                   Sku = "3729282",
                   SrcLatitude = 32.9462M,
                   SrcLongitude = -96.7058M,
                   SrcZipCode = "75081",
                   SupplierId = 4,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 13,
                   WarehouseName = "5",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 89034,
                   Sku = "3729282",
                   SrcLatitude = 25.7735M,
                   SrcLongitude = -80.3572M,
                   SrcZipCode = "33172",
                   SupplierId = 4,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 9,
                   WarehouseName = "16",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 89442,
                   Sku = "3729282",
                   SrcLatitude = 34.0183M,
                   SrcLongitude = -117.8546M,
                   SrcZipCode = "91789",
                   SupplierId = 4,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 6,
                   WarehouseName = "11",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 89443,
                   Sku = "3729282",
                   SrcLatitude = 36.2115M,
                   SrcLongitude = -115.1241M,
                   SrcZipCode = "89030",
                   SupplierId = 4,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 10,
                   WarehouseName = "2",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 89591,
                   Sku = "3729282",
                   SrcLatitude = 45.4560M,
                   SrcLongitude = -122.7996M,
                   SrcZipCode = "97008",
                   SupplierId = 4,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 5,
                   WarehouseName = "10",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 72,

                   GTINPLUS = 1000023942491712,
                   Id = 1299310,
                   Sku = "VER49171",
                   SrcLatitude = 37.5748M,
                   SrcLongitude = -77.4267M,
                   SrcZipCode = "23222",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = "EA",
                   WarehouseId = 25,
                   WarehouseName = "012",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 93,

                   GTINPLUS = 1000023942491712,
                   Id = 1299311,
                   Sku = "VER49171",
                   SrcLatitude = 40.4322M,
                   SrcLongitude = -80.1021M,
                   SrcZipCode = "15205",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = "EA",
                   WarehouseId = 51,
                   WarehouseName = "038",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 1299729,
                   Sku = "VER49171",
                   SrcLatitude = 36.2019M,
                   SrcLongitude = -79.7101M,
                   SrcZipCode = "27214",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = null,
                   WarehouseId = 34,
                   WarehouseName = "022",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 72,

                   GTINPLUS = 1000023942491712,
                   Id = 1301095,
                   Sku = "VER49171",
                   SrcLatitude = 35.4444M,
                   SrcLongitude = -97.6164M,
                   SrcZipCode = "73128",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = "EA",
                   WarehouseId = 26,
                   WarehouseName = "013",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 1303516,
                   Sku = "VER49171",
                   SrcLatitude = 43.1624M,
                   SrcLongitude = -87.9898M,
                   SrcZipCode = "53223",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = null,
                   WarehouseId = 37,
                   WarehouseName = "025",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 97,

                   GTINPLUS = 1000023942491712,
                   Id = 1303517,
                   Sku = "VER49171",
                   SrcLatitude = 39.9046M,
                   SrcLongitude = -82.9703M,
                   SrcZipCode = "43207",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = "EA",
                   WarehouseId = 60,
                   WarehouseName = "009",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 77,

                   GTINPLUS = 1000023942491712,
                   Id = 1306207,
                   Sku = "VER49171",
                   SrcLatitude = 47.4483M,
                   SrcLongitude = -122.2731M,
                   SrcZipCode = "98188",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = "EA",
                   WarehouseId = 40,
                   WarehouseName = "028",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 136,

                   GTINPLUS = 1000023942491712,
                   Id = 1306208,
                   Sku = "VER49171",
                   SrcLatitude = 33.4564M,
                   SrcLongitude = -112.1284M,
                   SrcZipCode = "85009",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = "EA",
                   WarehouseId = 49,
                   WarehouseName = "036",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 1307670,
                   Sku = "VER49171",
                   SrcLatitude = 42.2768M,
                   SrcLongitude = -83.3758M,
                   SrcZipCode = "48184",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = null,
                   WarehouseId = 45,
                   WarehouseName = "032",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 1308043,
                   Sku = "VER49171",
                   SrcLatitude = 41.9099M,
                   SrcLongitude = -72.6029M,
                   SrcZipCode = "06088",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = null,
                   WarehouseId = 27,
                   WarehouseName = "014",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 179,

                   GTINPLUS = 1000023942491712,
                   Id = 1308044,
                   Sku = "VER49171",
                   SrcLatitude = 33.9938M,
                   SrcLongitude = -117.5236M,
                   SrcZipCode = "91752",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = "EA",
                   WarehouseId = 44,
                   WarehouseName = "031",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 112,

                   GTINPLUS = 1000023942491712,
                   Id = 1309010,
                   Sku = "VER49171",
                   SrcLatitude = 39.9683M,
                   SrcLongitude = -74.9533M,
                   SrcZipCode = "08057",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = "EA",
                   WarehouseId = 35,
                   WarehouseName = "023",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 149,

                   GTINPLUS = 1000023942491712,
                   Id = 1311371,
                   Sku = "VER49171",
                   SrcLatitude = 35.2522M,
                   SrcLongitude = -80.8265M,
                   SrcZipCode = "28206",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = "EA",
                   WarehouseId = 33,
                   WarehouseName = "002",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 82,

                   GTINPLUS = 1000023942491712,
                   Id = 1311372,
                   Sku = "VER49171",
                   SrcLatitude = 39.8682M,
                   SrcLongitude = -86.2123M,
                   SrcZipCode = "46268",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = "EA",
                   WarehouseId = 36,
                   WarehouseName = "024",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 1311373,
                   Sku = "VER49171",
                   SrcLatitude = 43.0734M,
                   SrcLongitude = -76.0558M,
                   SrcZipCode = "13057",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = null,
                   WarehouseId = 41,
                   WarehouseName = "029",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 54,

                   GTINPLUS = 1000023942491712,
                   Id = 1311374,
                   Sku = "VER49171",
                   SrcLatitude = 40.7498M,
                   SrcLongitude = -111.9260M,
                   SrcZipCode = "84104",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = "EA",
                   WarehouseId = 46,
                   WarehouseName = "033",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 41,

                   GTINPLUS = 1000023942491712,
                   Id = 1311375,
                   Sku = "VER49171",
                   SrcLatitude = 41.4886M,
                   SrcLongitude = -74.3450M,
                   SrcZipCode = "10941",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = "EA",
                   WarehouseId = 50,
                   WarehouseName = "037",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 1311662,
                   Sku = "VER49171",
                   SrcLatitude = 38.6406M,
                   SrcLongitude = -121.4440M,
                   SrcZipCode = "95838",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = null,
                   WarehouseId = 43,
                   WarehouseName = "030",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 42,

                   GTINPLUS = 1000023942491712,
                   Id = 1315402,
                   Sku = "VER49171",
                   SrcLatitude = 45.0132M,
                   SrcLongitude = -93.0297M,
                   SrcZipCode = "55109",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = "EA",
                   WarehouseId = 23,
                   WarehouseName = "010",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 46,

                   GTINPLUS = 1000023942491712,
                   Id = 1315403,
                   Sku = "VER49171",
                   SrcLatitude = 25.8856M,
                   SrcLongitude = -80.2292M,
                   SrcZipCode = "33167",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = "EA",
                   WarehouseId = 42,
                   WarehouseName = "003",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 1315404,
                   Sku = "VER49171",
                   SrcLatitude = 26.2729M,
                   SrcLongitude = -80.2603M,
                   SrcZipCode = "33065",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = null,
                   WarehouseId = 55,
                   WarehouseName = "046",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 59,

                   GTINPLUS = 1000023942491712,
                   Id = 1315541,
                   Sku = "VER49171",
                   SrcLatitude = 33.4816M,
                   SrcLongitude = -86.8590M,
                   SrcZipCode = "35211",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = "EA",
                   WarehouseId = 29,
                   WarehouseName = "016",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 151,

                   GTINPLUS = 1000023942491712,
                   Id = 1315542,
                   Sku = "VER49171",
                   SrcLatitude = 39.1484M,
                   SrcLongitude = -76.7922M,
                   SrcZipCode = "20794",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = "EA",
                   WarehouseId = 32,
                   WarehouseName = "019",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 680,

                   GTINPLUS = 1000023942491712,
                   Id = 1315543,
                   Sku = "VER49171",
                   SrcLatitude = 27.9951M,
                   SrcLongitude = -82.4046M,
                   SrcZipCode = "33610",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = "EA",
                   WarehouseId = 53,
                   WarehouseName = "004",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 1316605,
                   Sku = "VER49171",
                   SrcLatitude = 30.3415M,
                   SrcLongitude = -81.7358M,
                   SrcZipCode = "32254",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = null,
                   WarehouseId = 39,
                   WarehouseName = "027",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 2169635,
                   Sku = "VER49171",
                   SrcLatitude = 33.7866M,
                   SrcLongitude = -118.2987M,
                   SrcZipCode = "91749",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = null,
                   WarehouseId = 69,
                   WarehouseName = "COI",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 2169636,
                   Sku = "VER49171",
                   SrcLatitude = 38.8933M,
                   SrcLongitude = -89.4052M,
                   SrcZipCode = "62246",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = null,
                   WarehouseId = 88,
                   WarehouseName = "STL",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 111,

                   GTINPLUS = 1000023942491712,
                   Id = 2178328,
                   Sku = "VER49171",
                   SrcLatitude = 35.2072M,
                   SrcLongitude = -80.9568M,
                   SrcZipCode = "28278",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = "EA",
                   WarehouseId = 67,
                   WarehouseName = "CNC",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 2178329,
                   Sku = "VER49171",
                   SrcLatitude = 40.7498M,
                   SrcLongitude = -111.9260M,
                   SrcZipCode = "84104",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = null,
                   WarehouseId = 87,
                   WarehouseName = "SLK",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 177,

                   GTINPLUS = 1000023942491712,
                   Id = 2179807,
                   Sku = "VER49171",
                   SrcLatitude = 39.1551M,
                   SrcLongitude = -76.7215M,
                   SrcZipCode = "21076",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = "EA",
                   WarehouseId = 63,
                   WarehouseName = "BAL",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 2179808,
                   Sku = "VER49171",
                   SrcLatitude = 40.3039M,
                   SrcLongitude = -74.5065M,
                   SrcZipCode = "08512",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = null,
                   WarehouseId = 68,
                   WarehouseName = "CNJ",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 2181404,
                   Sku = "VER49171",
                   SrcLatitude = 29.9511M,
                   SrcLongitude = -90.2060M,
                   SrcZipCode = "70123",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = null,
                   WarehouseId = 79,
                   WarehouseName = "NOL",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 2181539,
                   Sku = "VER49171",
                   SrcLatitude = 39.8682M,
                   SrcLongitude = -86.2123M,
                   SrcZipCode = "46268",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = null,
                   WarehouseId = 75,
                   WarehouseName = "IDY",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 2195377,
                   Sku = "VER49171",
                   SrcLatitude = 39.7388M,
                   SrcLongitude = -104.4083M,
                   SrcZipCode = "80238",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = null,
                   WarehouseId = 70,
                   WarehouseName = "DCO",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 2195800,
                   Sku = "VER49171",
                   SrcLatitude = 32.8267M,
                   SrcLongitude = -96.9633M,
                   SrcZipCode = "75061",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = null,
                   WarehouseId = 71,
                   WarehouseName = "DFW",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 2203247,
                   Sku = "VER49171",
                   SrcLatitude = 29.5395M,
                   SrcLongitude = -98.4194M,
                   SrcZipCode = "78217",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = null,
                   WarehouseId = 86,
                   WarehouseName = "SAT",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 2203248,
                   Sku = "VER49171",
                   SrcLatitude = 40.7498M,
                   SrcLongitude = -111.9260M,
                   SrcZipCode = "84104",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = null,
                   WarehouseId = 96,
                   WarehouseName = "SLK",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 2203883,
                   Sku = "VER49171",
                   SrcLatitude = 42.3501M,
                   SrcLongitude = -73.8199M,
                   SrcZipCode = "12051",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = null,
                   WarehouseId = 61,
                   WarehouseName = "ALB",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 2213590,
                   Sku = "VER49171",
                   SrcLatitude = 43.0073M,
                   SrcLongitude = -85.7255M,
                   SrcZipCode = "49544",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = null,
                   WarehouseId = 73,
                   WarehouseName = "GMI",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 2213591,
                   Sku = "VER49171",
                   SrcLatitude = 33.3917M,
                   SrcLongitude = -111.9249M,
                   SrcZipCode = "85282",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = null,
                   WarehouseId = 82,
                   WarehouseName = "PAZ",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 2213592,
                   Sku = "VER49171",
                   SrcLatitude = 36.1398M,
                   SrcLongitude = -96.0297M,
                   SrcZipCode = "74158",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = null,
                   WarehouseId = 90,
                   WarehouseName = "TUL",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 58,

                   GTINPLUS = 1000023942491712,
                   Id = 5044249,
                   Sku = "PG9605",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 27069,
                   TotalAvailable = 72,
                   Unit = "EA",
                   WarehouseId = 7184,
                   WarehouseName = "20",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 20,

                   GTINPLUS = 1000023942491712,
                   Id = 5056140,
                   Sku = "PG9605",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 27069,
                   TotalAvailable = 72,
                   Unit = "EA",
                   WarehouseId = 7183,
                   WarehouseName = "10",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 5068399,
                   Sku = "PG9605",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 27069,
                   TotalAvailable = 72,
                   Unit = null,
                   WarehouseId = 7187,
                   WarehouseName = "80",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 7960914,
                   Sku = "10912867",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 29281,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 7190,
                   WarehouseName = "A03",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 7960915,
                   Sku = "10912867",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 29281,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 7193,
                   WarehouseName = "A06",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 149,

                   GTINPLUS = 1000023942491712,
                   Id = 8165690,
                   Sku = "VER49171",
                   SrcLatitude = 39.7340M,
                   SrcLongitude = -105.0259M,
                   SrcZipCode = "80204",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = "EA",
                   WarehouseId = 24,
                   WarehouseName = "011",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 92,

                   GTINPLUS = 1000023942491712,
                   Id = 8165691,
                   Sku = "VER49171",
                   SrcLatitude = 35.0231M,
                   SrcLongitude = -89.8492M,
                   SrcZipCode = "38141",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = "EA",
                   WarehouseId = 57,
                   WarehouseName = "006",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 103,

                   GTINPLUS = 1000023942491712,
                   Id = 8165816,
                   Sku = "VER49171",
                   SrcLatitude = 29.5790M,
                   SrcLongitude = -98.2778M,
                   SrcZipCode = "78154",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = "EA",
                   WarehouseId = 28,
                   WarehouseName = "015",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 73,

                   GTINPLUS = 1000023942491712,
                   Id = 8169142,
                   Sku = "VER49171",
                   SrcLatitude = 38.7229M,
                   SrcLongitude = -90.4474M,
                   SrcZipCode = "63043",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = "EA",
                   WarehouseId = 31,
                   WarehouseName = "018",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 107,

                   GTINPLUS = 1000023942491712,
                   Id = 8171858,
                   Sku = "VER49171",
                   SrcLatitude = 33.0033M,
                   SrcLongitude = -96.8820M,
                   SrcZipCode = "75007",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = "EA",
                   WarehouseId = 56,
                   WarehouseName = "005",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 66,

                   GTINPLUS = 1000023942491712,
                   Id = 8175076,
                   Sku = "VER49171",
                   SrcLatitude = 39.1222M,
                   SrcLongitude = -94.5487M,
                   SrcZipCode = "64120",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = "EA",
                   WarehouseId = 58,
                   WarehouseName = "007",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 8178643,
                   Sku = "VER49171",
                   SrcLatitude = 40.3039M,
                   SrcLongitude = -74.5065M,
                   SrcZipCode = "08512",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = null,
                   WarehouseId = 54,
                   WarehouseName = "044",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 93,

                   GTINPLUS = 1000023942491712,
                   Id = 8199105,
                   Sku = "VER49171",
                   SrcLatitude = 42.0076M,
                   SrcLongitude = -87.9931M,
                   SrcZipCode = "60007",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = "EA",
                   WarehouseId = 48,
                   WarehouseName = "035",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 45,

                   GTINPLUS = 1000023942491712,
                   Id = 8199106,
                   Sku = "VER49171",
                   SrcLatitude = 42.8802M,
                   SrcLongitude = -85.5352M,
                   SrcZipCode = "49512",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = "EA",
                   WarehouseId = 52,
                   WarehouseName = "039",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 73,

                   GTINPLUS = 1000023942491712,
                   Id = 8212671,
                   Sku = "VER49171",
                   SrcLatitude = 29.7971M,
                   SrcLongitude = -95.4958M,
                   SrcZipCode = "77055",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = "EA",
                   WarehouseId = 59,
                   WarehouseName = "008",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 75,

                   GTINPLUS = 1000023942491712,
                   Id = 8233646,
                   Sku = "VER49171",
                   SrcLatitude = 36.1901M,
                   SrcLongitude = -86.8053M,
                   SrcZipCode = "37228",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = "EA",
                   WarehouseId = 38,
                   WarehouseName = "026",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 58,

                   GTINPLUS = 1000023942491712,
                   Id = 8252846,
                   Sku = "VER49171",
                   SrcLatitude = 42.7717M,
                   SrcLongitude = -71.5132M,
                   SrcZipCode = "03063",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = "EA",
                   WarehouseId = 47,
                   WarehouseName = "034",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 8965354,
                   Sku = "VER49171",
                   SrcLatitude = 42.4829M,
                   SrcLongitude = -71.1574M,
                   SrcZipCode = "01801",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = null,
                   WarehouseId = 91,
                   WarehouseName = "WOB",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 34,

                   GTINPLUS = 1000023942491712,
                   Id = 9059778,
                   Sku = "VER49171",
                   SrcLatitude = 47.4483M,
                   SrcLongitude = -122.2731M,
                   SrcZipCode = "98188",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = "EA",
                   WarehouseId = 89,
                   WarehouseName = "SWA",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 9061281,
                   Sku = "VER49171",
                   SrcLatitude = 40.4344M,
                   SrcLongitude = -80.0248M,
                   SrcZipCode = "15086",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = null,
                   WarehouseId = 84,
                   WarehouseName = "PTS",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 246,

                   GTINPLUS = 1000023942491712,
                   Id = 9061282,
                   Sku = "VER49171",
                   SrcLatitude = 38.6865M,
                   SrcLongitude = -121.3494M,
                   SrcZipCode = "95842",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = "EA",
                   WarehouseId = 85,
                   WarehouseName = "SAC",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 9061283,
                   Sku = "VER49171",
                   SrcLatitude = 40.3039M,
                   SrcLongitude = -74.5065M,
                   SrcZipCode = "08512",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = null,
                   WarehouseId = 93,
                   WarehouseName = "CNJ",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 9061284,
                   Sku = "VER49171",
                   SrcLatitude = 47.4483M,
                   SrcLongitude = -122.2731M,
                   SrcZipCode = "98188",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = null,
                   WarehouseId = 97,
                   WarehouseName = "SWA",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 9075921,
                   Sku = "VER49171",
                   SrcLatitude = 35.0231M,
                   SrcLongitude = -89.8492M,
                   SrcZipCode = "38141",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = null,
                   WarehouseId = 78,
                   WarehouseName = "MTN",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 9090661,
                   Sku = "VER49171",
                   SrcLatitude = 39.1479M,
                   SrcLongitude = -94.5680M,
                   SrcZipCode = "64116",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = null,
                   WarehouseId = 76,
                   WarehouseName = "KAN",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 99,

                   GTINPLUS = 1000023942491712,
                   Id = 9131665,
                   Sku = "VER49171",
                   SrcLatitude = 36.0127M,
                   SrcLongitude = -86.5600M,
                   SrcZipCode = "37086",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = "EA",
                   WarehouseId = 80,
                   WarehouseName = "NSH",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 9132510,
                   Sku = "VER49171",
                   SrcLatitude = 41.3289M,
                   SrcLongitude = -81.4559M,
                   SrcZipCode = "44087",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = null,
                   WarehouseId = 66,
                   WarehouseName = "CLV",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 54,

                   GTINPLUS = 1000023942491712,
                   Id = 9137313,
                   Sku = "VER49171",
                   SrcLatitude = 44.8471M,
                   SrcLongitude = -93.1543M,
                   SrcZipCode = "55121",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = "EA",
                   WarehouseId = 77,
                   WarehouseName = "MIN",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 9137314,
                   Sku = "VER49171",
                   SrcLatitude = 45.4840M,
                   SrcLongitude = -122.6365M,
                   SrcZipCode = "97202",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = null,
                   WarehouseId = 83,
                   WarehouseName = "POR",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 9137315,
                   Sku = "VER49171",
                   SrcLatitude = 35.0231M,
                   SrcLongitude = -89.8492M,
                   SrcZipCode = "38141",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = null,
                   WarehouseId = 95,
                   WarehouseName = "MTN",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 9150323,
                   Sku = "VER49171",
                   SrcLatitude = 26.0480M,
                   SrcLongitude = -80.3749M,
                   SrcZipCode = "33331",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = null,
                   WarehouseId = 72,
                   WarehouseName = "FTL",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 139,

                   GTINPLUS = 1000023942491712,
                   Id = 9150324,
                   Sku = "VER49171",
                   SrcLatitude = 29.8744M,
                   SrcLongitude = -95.5278M,
                   SrcZipCode = "77040",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = "EA",
                   WarehouseId = 74,
                   WarehouseName = "HOU",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 9180163,
                   Sku = "VER49171",
                   SrcLatitude = 39.9648M,
                   SrcLongitude = -83.1260M,
                   SrcZipCode = "43228",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = null,
                   WarehouseId = 64,
                   WarehouseName = "CBS",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 9180164,
                   Sku = "VER49171",
                   SrcLatitude = 39.8682M,
                   SrcLongitude = -86.2123M,
                   SrcZipCode = "46268",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = null,
                   WarehouseId = 94,
                   WarehouseName = "IDY",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 9212558,
                   Sku = "VER49171",
                   SrcLatitude = 28.4522M,
                   SrcLongitude = -81.4678M,
                   SrcZipCode = "32819",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = null,
                   WarehouseId = 81,
                   WarehouseName = "ORL",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 9262979,
                   Sku = "VER49171",
                   SrcLatitude = 34.0425M,
                   SrcLongitude = -84.0262M,
                   SrcZipCode = "30024",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = null,
                   WarehouseId = 62,
                   WarehouseName = "ATL",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 11670682,
                   Sku = "768643",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 15499,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = null,
                   WarehouseName = null,
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 11915287,
                   Sku = "PG9605",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 27069,
                   TotalAvailable = 72,
                   Unit = null,
                   WarehouseId = 7185,
                   WarehouseName = "30",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 11929375,
                   Sku = "VER49171",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 25499,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = null,
                   WarehouseName = null,
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 12597826,
                   Sku = "PG9605",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 27069,
                   TotalAvailable = 72,
                   Unit = null,
                   WarehouseId = 7186,
                   WarehouseName = "40",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 13850342,
                   Sku = "10912867",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 29281,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 7192,
                   WarehouseName = "A05",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 13917527,
                   Sku = "10912867",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 29281,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 7188,
                   WarehouseName = "A01",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 13917528,
                   Sku = "10912867",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 29281,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 7191,
                   WarehouseName = "A04",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 13917529,
                   Sku = "10912867",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 29281,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 7194,
                   WarehouseName = "A07",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 14395712,
                   Sku = "10912867",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 29281,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 7189,
                   WarehouseName = "A02",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 14624431,
                   Sku = "Q349171",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 29539,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = null,
                   WarehouseName = null,
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 165,

                   GTINPLUS = 1000023942491712,
                   Id = 14679797,
                   Sku = "VER49171",
                   SrcLatitude = 33.8631M,
                   SrcLongitude = -84.5382M,
                   SrcZipCode = "30082",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = "EA",
                   WarehouseId = 22,
                   WarehouseName = "001",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 111,

                   GTINPLUS = 1000023942491712,
                   Id = 14679798,
                   Sku = "VER49171",
                   SrcLatitude = 29.9871M,
                   SrcLongitude = -90.1695M,
                   SrcZipCode = "70001",
                   SupplierId = 7,
                   TotalAvailable = 3343,
                   Unit = "EA",
                   WarehouseId = 30,
                   WarehouseName = "017",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942491712,
                   Id = 14738531,
                   Sku = "VER49171",
                   SrcLatitude = 41.9178M,
                   SrcLongitude = -88.1370M,
                   SrcZipCode = "60188",
                   SupplierId = 8,
                   TotalAvailable = 864,
                   Unit = null,
                   WarehouseId = 65,
                   WarehouseName = "CHI",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 4431,
                   Sku = "964297",
                   SrcLatitude = 38.7809M,
                   SrcLongitude = -90.3669M,
                   SrcZipCode = "63042",
                   SupplierId = 3,
                   TotalAvailable = 1,
                   Unit = null,
                   WarehouseId = 2,
                   WarehouseName = "MO",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 5292,
                   Sku = "964297",
                   SrcLatitude = 40.2417M,
                   SrcLongitude = -77.1983M,
                   SrcZipCode = "17013",
                   SupplierId = 3,
                   TotalAvailable = 1,
                   Unit = null,
                   WarehouseId = 3,
                   WarehouseName = "PA",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 5648,
                   Sku = "964297",
                   SrcLatitude = 36.6207M,
                   SrcLongitude = -119.7308M,
                   SrcZipCode = "93725",
                   SupplierId = 3,
                   TotalAvailable = 1,
                   Unit = null,
                   WarehouseId = 1,
                   WarehouseName = "CA",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 1,

                   GTINPLUS = 1000023942986645,
                   Id = 6292,
                   Sku = "964297",
                   SrcLatitude = 32.7678M,
                   SrcLongitude = -96.6082M,
                   SrcZipCode = "75149",
                   SupplierId = 3,
                   TotalAvailable = 1,
                   Unit = "EA",
                   WarehouseId = 4,
                   WarehouseName = "TX",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 81678,
                   Sku = "4357150",
                   SrcLatitude = 38.8867M,
                   SrcLongitude = -77.4457M,
                   SrcZipCode = "20151",
                   SupplierId = 4,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 17,
                   WarehouseName = "9",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 84154,
                   Sku = "4357150",
                   SrcLatitude = 32.9462M,
                   SrcLongitude = -96.7058M,
                   SrcZipCode = "75081",
                   SupplierId = 4,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 13,
                   WarehouseName = "5",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 84658,
                   Sku = "4357150",
                   SrcLatitude = 36.2115M,
                   SrcLongitude = -115.1241M,
                   SrcZipCode = "89030",
                   SupplierId = 4,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 10,
                   WarehouseName = "2",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 85529,
                   Sku = "4357150",
                   SrcLatitude = 37.5176M,
                   SrcLongitude = -121.9287M,
                   SrcZipCode = "94539",
                   SupplierId = 4,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 11,
                   WarehouseName = "3",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 87172,
                   Sku = "4357150",
                   SrcLatitude = 25.7735M,
                   SrcLongitude = -80.3572M,
                   SrcZipCode = "33172",
                   SupplierId = 4,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 9,
                   WarehouseName = "16",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 89506,
                   Sku = "4357150",
                   SrcLatitude = 40.5192M,
                   SrcLongitude = -74.3021M,
                   SrcZipCode = "08832",
                   SupplierId = 4,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 16,
                   WarehouseName = "8",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 89952,
                   Sku = "4357150",
                   SrcLatitude = 39.8814M,
                   SrcLongitude = -83.0839M,
                   SrcZipCode = "43123",
                   SupplierId = 4,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 8,
                   WarehouseName = "14",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 90428,
                   Sku = "4357150",
                   SrcLatitude = 41.9205M,
                   SrcLongitude = -88.0793M,
                   SrcZipCode = "60139",
                   SupplierId = 4,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 14,
                   WarehouseName = "6",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 90668,
                   Sku = "4357150",
                   SrcLatitude = 33.8193M,
                   SrcLongitude = -118.2325M,
                   SrcZipCode = "90810",
                   SupplierId = 4,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 7,
                   WarehouseName = "12",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 90669,
                   Sku = "4357150",
                   SrcLatitude = 33.9381M,
                   SrcLongitude = -84.1972M,
                   SrcZipCode = "30071",
                   SupplierId = 4,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 12,
                   WarehouseName = "4",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 91356,
                   Sku = "4357150",
                   SrcLatitude = 45.4560M,
                   SrcLongitude = -122.7996M,
                   SrcZipCode = "97008",
                   SupplierId = 4,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 5,
                   WarehouseName = "10",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 92390,
                   Sku = "4357150",
                   SrcLatitude = 34.9441M,
                   SrcLongitude = -89.8544M,
                   SrcZipCode = "38654",
                   SupplierId = 4,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 15,
                   WarehouseName = "7",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 93825,
                   Sku = "4357150",
                   SrcLatitude = 34.0183M,
                   SrcLongitude = -117.8546M,
                   SrcZipCode = "91789",
                   SupplierId = 4,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 6,
                   WarehouseName = "11",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 1134672,
                   Sku = "VER98664",
                   SrcLatitude = 41.6790M,
                   SrcLongitude = -88.1403M,
                   SrcZipCode = "60490",
                   SupplierId = 5,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 20,
                   WarehouseName = "Chicago",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 1140236,
                   Sku = "VER98664",
                   SrcLatitude = 33.3958M,
                   SrcLongitude = -84.7121M,
                   SrcZipCode = "30265",
                   SupplierId = 5,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 18,
                   WarehouseName = "Atlanta",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 1157431,
                   Sku = "VER98664",
                   SrcLatitude = 36.6207M,
                   SrcLongitude = -119.7308M,
                   SrcZipCode = "93725",
                   SupplierId = 5,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 19,
                   WarehouseName = "California",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 4,

                   GTINPLUS = 1000023942986645,
                   Id = 1302855,
                   Sku = "VER98664",
                   SrcLatitude = 35.4444M,
                   SrcLongitude = -97.6164M,
                   SrcZipCode = "73128",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = "EA",
                   WarehouseId = 26,
                   WarehouseName = "013",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 1302856,
                   Sku = "VER98664",
                   SrcLatitude = 36.2019M,
                   SrcLongitude = -79.7101M,
                   SrcZipCode = "27214",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = null,
                   WarehouseId = 34,
                   WarehouseName = "022",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 1302857,
                   Sku = "VER98664",
                   SrcLatitude = 26.2729M,
                   SrcLongitude = -80.2603M,
                   SrcZipCode = "33065",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = null,
                   WarehouseId = 55,
                   WarehouseName = "046",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 1304289,
                   Sku = "VER98664",
                   SrcLatitude = 43.1624M,
                   SrcLongitude = -87.9898M,
                   SrcZipCode = "53223",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = null,
                   WarehouseId = 37,
                   WarehouseName = "025",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 4,

                   GTINPLUS = 1000023942986645,
                   Id = 1304290,
                   Sku = "VER98664",
                   SrcLatitude = 35.0231M,
                   SrcLongitude = -89.8492M,
                   SrcZipCode = "38141",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = "EA",
                   WarehouseId = 57,
                   WarehouseName = "006",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 8,

                   GTINPLUS = 1000023942986645,
                   Id = 1304291,
                   Sku = "VER98664",
                   SrcLatitude = 39.9046M,
                   SrcLongitude = -82.9703M,
                   SrcZipCode = "43207",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = "EA",
                   WarehouseId = 60,
                   WarehouseName = "009",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 15,

                   GTINPLUS = 1000023942986645,
                   Id = 1306566,
                   Sku = "VER98664",
                   SrcLatitude = 39.7340M,
                   SrcLongitude = -105.0259M,
                   SrcZipCode = "80204",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = "EA",
                   WarehouseId = 24,
                   WarehouseName = "011",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 1309188,
                   Sku = "VER98664",
                   SrcLatitude = 43.0734M,
                   SrcLongitude = -76.0558M,
                   SrcZipCode = "13057",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = null,
                   WarehouseId = 41,
                   WarehouseName = "029",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 20,

                   GTINPLUS = 1000023942986645,
                   Id = 1309189,
                   Sku = "VER98664",
                   SrcLatitude = 25.8856M,
                   SrcLongitude = -80.2292M,
                   SrcZipCode = "33167",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = "EA",
                   WarehouseId = 42,
                   WarehouseName = "003",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 3,

                   GTINPLUS = 1000023942986645,
                   Id = 1309190,
                   Sku = "VER98664",
                   SrcLatitude = 42.7717M,
                   SrcLongitude = -71.5132M,
                   SrcZipCode = "03063",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = "EA",
                   WarehouseId = 47,
                   WarehouseName = "034",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 1309191,
                   Sku = "VER98664",
                   SrcLatitude = 40.4322M,
                   SrcLongitude = -80.1021M,
                   SrcZipCode = "15205",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = null,
                   WarehouseId = 51,
                   WarehouseName = "038",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 1309192,
                   Sku = "VER98664",
                   SrcLatitude = 42.8802M,
                   SrcLongitude = -85.5352M,
                   SrcZipCode = "49512",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = null,
                   WarehouseId = 52,
                   WarehouseName = "039",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 13,

                   GTINPLUS = 1000023942986645,
                   Id = 1309193,
                   Sku = "VER98664",
                   SrcLatitude = 39.1222M,
                   SrcLongitude = -94.5487M,
                   SrcZipCode = "64120",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = "EA",
                   WarehouseId = 58,
                   WarehouseName = "007",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 23,

                   GTINPLUS = 1000023942986645,
                   Id = 1310657,
                   Sku = "VER98664",
                   SrcLatitude = 38.6406M,
                   SrcLongitude = -121.4440M,
                   SrcZipCode = "95838",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = "EA",
                   WarehouseId = 43,
                   WarehouseName = "030",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 32,

                   GTINPLUS = 1000023942986645,
                   Id = 1310658,
                   Sku = "VER98664",
                   SrcLatitude = 27.9951M,
                   SrcLongitude = -82.4046M,
                   SrcZipCode = "33610",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = "EA",
                   WarehouseId = 53,
                   WarehouseName = "004",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 8,

                   GTINPLUS = 1000023942986645,
                   Id = 1310659,
                   Sku = "VER98664",
                   SrcLatitude = 33.0033M,
                   SrcLongitude = -96.8820M,
                   SrcZipCode = "75007",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = "EA",
                   WarehouseId = 56,
                   WarehouseName = "005",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 1310967,
                   Sku = "VER98664",
                   SrcLatitude = 38.7229M,
                   SrcLongitude = -90.4474M,
                   SrcZipCode = "63043",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = null,
                   WarehouseId = 31,
                   WarehouseName = "018",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 7,

                   GTINPLUS = 1000023942986645,
                   Id = 1314788,
                   Sku = "VER98664",
                   SrcLatitude = 45.0132M,
                   SrcLongitude = -93.0297M,
                   SrcZipCode = "55109",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = "EA",
                   WarehouseId = 23,
                   WarehouseName = "010",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 1314789,
                   Sku = "VER98664",
                   SrcLatitude = 37.5748M,
                   SrcLongitude = -77.4267M,
                   SrcZipCode = "23222",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = null,
                   WarehouseId = 25,
                   WarehouseName = "012",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 1314790,
                   Sku = "VER98664",
                   SrcLatitude = 36.1901M,
                   SrcLongitude = -86.8053M,
                   SrcZipCode = "37228",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = null,
                   WarehouseId = 38,
                   WarehouseName = "026",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 13,

                   GTINPLUS = 1000023942986645,
                   Id = 1315112,
                   Sku = "VER98664",
                   SrcLatitude = 47.4483M,
                   SrcLongitude = -122.2731M,
                   SrcZipCode = "98188",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = "EA",
                   WarehouseId = 40,
                   WarehouseName = "028",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 30,

                   GTINPLUS = 1000023942986645,
                   Id = 1315113,
                   Sku = "VER98664",
                   SrcLatitude = 33.4564M,
                   SrcLongitude = -112.1284M,
                   SrcZipCode = "85009",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = "EA",
                   WarehouseId = 49,
                   WarehouseName = "036",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 1318430,
                   Sku = "VER98664",
                   SrcLatitude = 41.4886M,
                   SrcLongitude = -74.3450M,
                   SrcZipCode = "10941",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = null,
                   WarehouseId = 50,
                   WarehouseName = "037",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 21,

                   GTINPLUS = 1000023942986645,
                   Id = 1318657,
                   Sku = "VER98664",
                   SrcLatitude = 29.5790M,
                   SrcLongitude = -98.2778M,
                   SrcZipCode = "78154",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = "EA",
                   WarehouseId = 28,
                   WarehouseName = "015",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 1318658,
                   Sku = "VER98664",
                   SrcLatitude = 29.9871M,
                   SrcLongitude = -90.1695M,
                   SrcZipCode = "70001",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = null,
                   WarehouseId = 30,
                   WarehouseName = "017",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 2172631,
                   Sku = "VER98664",
                   SrcLatitude = 39.8682M,
                   SrcLongitude = -86.2123M,
                   SrcZipCode = "46268",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = null,
                   WarehouseId = 94,
                   WarehouseName = "IDY",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 2180191,
                   Sku = "VER98664",
                   SrcLatitude = 39.7388M,
                   SrcLongitude = -104.4083M,
                   SrcZipCode = "80238",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = null,
                   WarehouseId = 70,
                   WarehouseName = "DCO",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 2180192,
                   Sku = "VER98664",
                   SrcLatitude = 47.4483M,
                   SrcLongitude = -122.2731M,
                   SrcZipCode = "98188",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = null,
                   WarehouseId = 97,
                   WarehouseName = "SWA",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 2184136,
                   Sku = "VER98664",
                   SrcLatitude = 26.0480M,
                   SrcLongitude = -80.3749M,
                   SrcZipCode = "33331",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = null,
                   WarehouseId = 72,
                   WarehouseName = "FTL",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 2184137,
                   Sku = "VER98664",
                   SrcLatitude = 33.3917M,
                   SrcLongitude = -111.9249M,
                   SrcZipCode = "85282",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = null,
                   WarehouseId = 82,
                   WarehouseName = "PAZ",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 2186318,
                   Sku = "VER98664",
                   SrcLatitude = 39.1479M,
                   SrcLongitude = -94.5680M,
                   SrcZipCode = "64116",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = null,
                   WarehouseId = 76,
                   WarehouseName = "KAN",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 71,

                   GTINPLUS = 1000023942986645,
                   Id = 2194545,
                   Sku = "VER98664",
                   SrcLatitude = 40.3039M,
                   SrcLongitude = -74.5065M,
                   SrcZipCode = "08512",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = "EA",
                   WarehouseId = 68,
                   WarehouseName = "CNJ",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 2194546,
                   Sku = "VER98664",
                   SrcLatitude = 45.4840M,
                   SrcLongitude = -122.6365M,
                   SrcZipCode = "97202",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = null,
                   WarehouseId = 83,
                   WarehouseName = "POR",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 16,

                   GTINPLUS = 1000023942986645,
                   Id = 2197048,
                   Sku = "VER98664",
                   SrcLatitude = 29.8744M,
                   SrcLongitude = -95.5278M,
                   SrcZipCode = "77040",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = "EA",
                   WarehouseId = 74,
                   WarehouseName = "HOU",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 2197049,
                   Sku = "VER98664",
                   SrcLatitude = 36.1398M,
                   SrcLongitude = -96.0297M,
                   SrcZipCode = "74158",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = null,
                   WarehouseId = 90,
                   WarehouseName = "TUL",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 2197304,
                   Sku = "VER98664",
                   SrcLatitude = 42.3501M,
                   SrcLongitude = -73.8199M,
                   SrcZipCode = "12051",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = null,
                   WarehouseId = 61,
                   WarehouseName = "ALB",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 32,

                   GTINPLUS = 1000023942986645,
                   Id = 2197305,
                   Sku = "VER98664",
                   SrcLatitude = 39.1551M,
                   SrcLongitude = -76.7215M,
                   SrcZipCode = "21076",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = "EA",
                   WarehouseId = 63,
                   WarehouseName = "BAL",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 2197306,
                   Sku = "VER98664",
                   SrcLatitude = 39.9648M,
                   SrcLongitude = -83.1260M,
                   SrcZipCode = "43228",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = null,
                   WarehouseId = 64,
                   WarehouseName = "CBS",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 59,

                   GTINPLUS = 1000023942986645,
                   Id = 2205993,
                   Sku = "VER98664",
                   SrcLatitude = 41.9178M,
                   SrcLongitude = -88.1370M,
                   SrcZipCode = "60188",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = "EA",
                   WarehouseId = 65,
                   WarehouseName = "CHI",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 2205994,
                   Sku = "VER98664",
                   SrcLatitude = 36.0127M,
                   SrcLongitude = -86.5600M,
                   SrcZipCode = "37086",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = null,
                   WarehouseId = 80,
                   WarehouseName = "NSH",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 2205995,
                   Sku = "VER98664",
                   SrcLatitude = 28.4522M,
                   SrcLongitude = -81.4678M,
                   SrcZipCode = "32819",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = null,
                   WarehouseId = 81,
                   WarehouseName = "ORL",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 4736254,
                   Sku = "VER98664",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 25499,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = null,
                   WarehouseName = null,
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 12,

                   GTINPLUS = 1000023942986645,
                   Id = 5056337,
                   Sku = "YN6008",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 27069,
                   TotalAvailable = 37,
                   Unit = "EA",
                   WarehouseId = 7184,
                   WarehouseName = "20",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 19,

                   GTINPLUS = 1000023942986645,
                   Id = 5097533,
                   Sku = "YN6008",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 27069,
                   TotalAvailable = 37,
                   Unit = "EA",
                   WarehouseId = 7183,
                   WarehouseName = "10",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 5110595,
                   Sku = "YN6008",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 27069,
                   TotalAvailable = 37,
                   Unit = null,
                   WarehouseId = 7187,
                   WarehouseName = "80",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 7873914,
                   Sku = "11648855",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 29281,
                   TotalAvailable = 7,
                   Unit = null,
                   WarehouseId = 7189,
                   WarehouseName = "A02",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 8015424,
                   Sku = "VER98664",
                   SrcLatitude = 36.6207M,
                   SrcLongitude = -119.7308M,
                   SrcZipCode = "93725",
                   SupplierId = 5,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 92,
                   WarehouseName = "California",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 8030668,
                   Sku = "VER98664",
                   SrcLatitude = 40.2910M,
                   SrcLongitude = -76.8203M,
                   SrcZipCode = "17109",
                   SupplierId = 5,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 21,
                   WarehouseName = "Harrisburg",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 24,

                   GTINPLUS = 1000023942986645,
                   Id = 8154229,
                   Sku = "VER98664",
                   SrcLatitude = 33.9938M,
                   SrcLongitude = -117.5236M,
                   SrcZipCode = "91752",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = "EA",
                   WarehouseId = 44,
                   WarehouseName = "031",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 8154230,
                   Sku = "VER98664",
                   SrcLatitude = 40.3039M,
                   SrcLongitude = -74.5065M,
                   SrcZipCode = "08512",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = null,
                   WarehouseId = 54,
                   WarehouseName = "044",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 8167158,
                   Sku = "VER98664",
                   SrcLatitude = 42.2768M,
                   SrcLongitude = -83.3758M,
                   SrcZipCode = "48184",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = null,
                   WarehouseId = 45,
                   WarehouseName = "032",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 8177150,
                   Sku = "VER98664",
                   SrcLatitude = 41.9099M,
                   SrcLongitude = -72.6029M,
                   SrcZipCode = "06088",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = null,
                   WarehouseId = 27,
                   WarehouseName = "014",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 3,

                   GTINPLUS = 1000023942986645,
                   Id = 8177151,
                   Sku = "VER98664",
                   SrcLatitude = 39.9683M,
                   SrcLongitude = -74.9533M,
                   SrcZipCode = "08057",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = "EA",
                   WarehouseId = 35,
                   WarehouseName = "023",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 29,

                   GTINPLUS = 1000023942986645,
                   Id = 8179491,
                   Sku = "VER98664",
                   SrcLatitude = 42.0076M,
                   SrcLongitude = -87.9931M,
                   SrcZipCode = "60007",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = "EA",
                   WarehouseId = 48,
                   WarehouseName = "035",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 12,

                   GTINPLUS = 1000023942986645,
                   Id = 8179855,
                   Sku = "VER98664",
                   SrcLatitude = 35.2522M,
                   SrcLongitude = -80.8265M,
                   SrcZipCode = "28206",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = "EA",
                   WarehouseId = 33,
                   WarehouseName = "002",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 8,

                   GTINPLUS = 1000023942986645,
                   Id = 8180122,
                   Sku = "VER98664",
                   SrcLatitude = 40.7498M,
                   SrcLongitude = -111.9260M,
                   SrcZipCode = "84104",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = "EA",
                   WarehouseId = 46,
                   WarehouseName = "033",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 8182469,
                   Sku = "VER98664",
                   SrcLatitude = 33.4816M,
                   SrcLongitude = -86.8590M,
                   SrcZipCode = "35211",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = null,
                   WarehouseId = 29,
                   WarehouseName = "016",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 8182470,
                   Sku = "VER98664",
                   SrcLatitude = 30.3415M,
                   SrcLongitude = -81.7358M,
                   SrcZipCode = "32254",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = null,
                   WarehouseId = 39,
                   WarehouseName = "027",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 4,

                   GTINPLUS = 1000023942986645,
                   Id = 8200132,
                   Sku = "VER98664",
                   SrcLatitude = 33.8631M,
                   SrcLongitude = -84.5382M,
                   SrcZipCode = "30082",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = "EA",
                   WarehouseId = 22,
                   WarehouseName = "001",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 5,

                   GTINPLUS = 1000023942986645,
                   Id = 8200133,
                   Sku = "VER98664",
                   SrcLatitude = 39.1484M,
                   SrcLongitude = -76.7922M,
                   SrcZipCode = "20794",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = "EA",
                   WarehouseId = 32,
                   WarehouseName = "019",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 8213473,
                   Sku = "VER98664",
                   SrcLatitude = 39.8682M,
                   SrcLongitude = -86.2123M,
                   SrcZipCode = "46268",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = null,
                   WarehouseId = 36,
                   WarehouseName = "024",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 8,

                   GTINPLUS = 1000023942986645,
                   Id = 8213474,
                   Sku = "VER98664",
                   SrcLatitude = 29.7971M,
                   SrcLongitude = -95.4958M,
                   SrcZipCode = "77055",
                   SupplierId = 7,
                   TotalAvailable = 297,
                   Unit = "EA",
                   WarehouseId = 59,
                   WarehouseName = "008",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 28,

                   GTINPLUS = 1000023942986645,
                   Id = 9027214,
                   Sku = "VER98664",
                   SrcLatitude = 42.4829M,
                   SrcLongitude = -71.1574M,
                   SrcZipCode = "01801",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = "EA",
                   WarehouseId = 91,
                   WarehouseName = "WOB",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 9091562,
                   Sku = "VER98664",
                   SrcLatitude = 39.8682M,
                   SrcLongitude = -86.2123M,
                   SrcZipCode = "46268",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = null,
                   WarehouseId = 75,
                   WarehouseName = "IDY",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 9091563,
                   Sku = "VER98664",
                   SrcLatitude = 40.7498M,
                   SrcLongitude = -111.9260M,
                   SrcZipCode = "84104",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = null,
                   WarehouseId = 87,
                   WarehouseName = "SLK",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 33,

                   GTINPLUS = 1000023942986645,
                   Id = 9106773,
                   Sku = "VER98664",
                   SrcLatitude = 34.0425M,
                   SrcLongitude = -84.0262M,
                   SrcZipCode = "30024",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = "EA",
                   WarehouseId = 62,
                   WarehouseName = "ATL",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 9136247,
                   Sku = "VER98664",
                   SrcLatitude = 29.5395M,
                   SrcLongitude = -98.4194M,
                   SrcZipCode = "78217",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = null,
                   WarehouseId = 86,
                   WarehouseName = "SAT",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 12,

                   GTINPLUS = 1000023942986645,
                   Id = 9138546,
                   Sku = "VER98664",
                   SrcLatitude = 38.6865M,
                   SrcLongitude = -121.3494M,
                   SrcZipCode = "95842",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = "EA",
                   WarehouseId = 85,
                   WarehouseName = "SAC",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 9138547,
                   Sku = "VER98664",
                   SrcLatitude = 40.3039M,
                   SrcLongitude = -74.5065M,
                   SrcZipCode = "08512",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = null,
                   WarehouseId = 93,
                   WarehouseName = "CNJ",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 9138775,
                   Sku = "VER98664",
                   SrcLatitude = 43.0073M,
                   SrcLongitude = -85.7255M,
                   SrcZipCode = "49544",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = null,
                   WarehouseId = 73,
                   WarehouseName = "GMI",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 28,

                   GTINPLUS = 1000023942986645,
                   Id = 9151234,
                   Sku = "VER98664",
                   SrcLatitude = 33.7866M,
                   SrcLongitude = -118.2987M,
                   SrcZipCode = "91749",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = "EA",
                   WarehouseId = 69,
                   WarehouseName = "COI",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 9151235,
                   Sku = "VER98664",
                   SrcLatitude = 29.9511M,
                   SrcLongitude = -90.2060M,
                   SrcZipCode = "70123",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = null,
                   WarehouseId = 79,
                   WarehouseName = "NOL",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 3,

                   GTINPLUS = 1000023942986645,
                   Id = 9181217,
                   Sku = "VER98664",
                   SrcLatitude = 32.8267M,
                   SrcLongitude = -96.9633M,
                   SrcZipCode = "75061",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = "EA",
                   WarehouseId = 71,
                   WarehouseName = "DFW",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 9181218,
                   Sku = "VER98664",
                   SrcLatitude = 35.0231M,
                   SrcLongitude = -89.8492M,
                   SrcZipCode = "38141",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = null,
                   WarehouseId = 78,
                   WarehouseName = "MTN",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 9181219,
                   Sku = "VER98664",
                   SrcLatitude = 38.8933M,
                   SrcLongitude = -89.4052M,
                   SrcZipCode = "62246",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = null,
                   WarehouseId = 88,
                   WarehouseName = "STL",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 9181220,
                   Sku = "VER98664",
                   SrcLatitude = 47.4483M,
                   SrcLongitude = -122.2731M,
                   SrcZipCode = "98188",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = null,
                   WarehouseId = 89,
                   WarehouseName = "SWA",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 9181221,
                   Sku = "VER98664",
                   SrcLatitude = 40.7498M,
                   SrcLongitude = -111.9260M,
                   SrcZipCode = "84104",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = null,
                   WarehouseId = 96,
                   WarehouseName = "SLK",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 9213816,
                   Sku = "VER98664",
                   SrcLatitude = 40.4344M,
                   SrcLongitude = -80.0248M,
                   SrcZipCode = "15086",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = null,
                   WarehouseId = 84,
                   WarehouseName = "PTS",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 9213817,
                   Sku = "VER98664",
                   SrcLatitude = 35.0231M,
                   SrcLongitude = -89.8492M,
                   SrcZipCode = "38141",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = null,
                   WarehouseId = 95,
                   WarehouseName = "MTN",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 34,

                   GTINPLUS = 1000023942986645,
                   Id = 9347634,
                   Sku = "VER98664",
                   SrcLatitude = 41.3289M,
                   SrcLongitude = -81.4559M,
                   SrcZipCode = "44087",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = "EA",
                   WarehouseId = 66,
                   WarehouseName = "CLV",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 9347635,
                   Sku = "VER98664",
                   SrcLatitude = 35.2072M,
                   SrcLongitude = -80.9568M,
                   SrcZipCode = "28278",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = null,
                   WarehouseId = 67,
                   WarehouseName = "CNC",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 9347636,
                   Sku = "VER98664",
                   SrcLatitude = 44.8471M,
                   SrcLongitude = -93.1543M,
                   SrcZipCode = "55121",
                   SupplierId = 8,
                   TotalAvailable = 326,
                   Unit = null,
                   WarehouseId = 77,
                   WarehouseName = "MIN",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 20,

                   GTINPLUS = 1000023942986645,
                   Id = 11733199,
                   Sku = "YN6008",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 27069,
                   TotalAvailable = 37,
                   Unit = "EA",
                   WarehouseId = 7186,
                   WarehouseName = "40",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 10,

                   GTINPLUS = 1000023942986645,
                   Id = 12062397,
                   Sku = "YN6008",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 27069,
                   TotalAvailable = 37,
                   Unit = "EA",
                   WarehouseId = 7185,
                   WarehouseName = "30",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 13725984,
                   Sku = "11648855",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 29281,
                   TotalAvailable = 7,
                   Unit = null,
                   WarehouseId = 7194,
                   WarehouseName = "A07",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 14192828,
                   Sku = "11648855",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 29281,
                   TotalAvailable = 7,
                   Unit = null,
                   WarehouseId = 7188,
                   WarehouseName = "A01",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 14192829,
                   Sku = "11648855",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 29281,
                   TotalAvailable = 7,
                   Unit = null,
                   WarehouseId = 7191,
                   WarehouseName = "A04",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 14561807,
                   Sku = "11648855",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 29281,
                   TotalAvailable = 7,
                   Unit = null,
                   WarehouseId = 7190,
                   WarehouseName = "A03",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000023942986645,
                   Id = 14561808,
                   Sku = "11648855",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 29281,
                   TotalAvailable = 7,
                   Unit = null,
                   WarehouseId = 7193,
                   WarehouseName = "A06",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 7,

                   GTINPLUS = 1000023942986645,
                   Id = 15077284,
                   Sku = "11648855",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 29281,
                   TotalAvailable = 7,
                   Unit = "EA",
                   WarehouseId = 7192,
                   WarehouseName = "A05",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 129353,
                   Sku = "2519613",
                   SrcLatitude = 37.5176M,
                   SrcLongitude = -121.9287M,
                   SrcZipCode = "94539",
                   SupplierId = 4,
                   TotalAvailable = 3,
                   Unit = null,
                   WarehouseId = 11,
                   WarehouseName = "3",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 129830,
                   Sku = "2519613",
                   SrcLatitude = 33.9381M,
                   SrcLongitude = -84.1972M,
                   SrcZipCode = "30071",
                   SupplierId = 4,
                   TotalAvailable = 3,
                   Unit = null,
                   WarehouseId = 12,
                   WarehouseName = "4",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 130063,
                   Sku = "2519613",
                   SrcLatitude = 25.7735M,
                   SrcLongitude = -80.3572M,
                   SrcZipCode = "33172",
                   SupplierId = 4,
                   TotalAvailable = 3,
                   Unit = null,
                   WarehouseId = 9,
                   WarehouseName = "16",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 3,

                   GTINPLUS = 1000043859543403,
                   Id = 130064,
                   Sku = "2519613",
                   SrcLatitude = 34.9441M,
                   SrcLongitude = -89.8544M,
                   SrcZipCode = "38654",
                   SupplierId = 4,
                   TotalAvailable = 3,
                   Unit = "EA",
                   WarehouseId = 15,
                   WarehouseName = "7",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 131155,
                   Sku = "2519613",
                   SrcLatitude = 36.2115M,
                   SrcLongitude = -115.1241M,
                   SrcZipCode = "89030",
                   SupplierId = 4,
                   TotalAvailable = 3,
                   Unit = null,
                   WarehouseId = 10,
                   WarehouseName = "2",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 568898,
                   Sku = "2519613",
                   SrcLatitude = 39.8814M,
                   SrcLongitude = -83.0839M,
                   SrcZipCode = "43123",
                   SupplierId = 4,
                   TotalAvailable = 3,
                   Unit = null,
                   WarehouseId = 8,
                   WarehouseName = "14",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 570063,
                   Sku = "2519613",
                   SrcLatitude = 38.8867M,
                   SrcLongitude = -77.4457M,
                   SrcZipCode = "20151",
                   SupplierId = 4,
                   TotalAvailable = 3,
                   Unit = null,
                   WarehouseId = 17,
                   WarehouseName = "9",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 571027,
                   Sku = "2519613",
                   SrcLatitude = 32.9462M,
                   SrcLongitude = -96.7058M,
                   SrcZipCode = "75081",
                   SupplierId = 4,
                   TotalAvailable = 3,
                   Unit = null,
                   WarehouseId = 13,
                   WarehouseName = "5",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 573419,
                   Sku = "2519613",
                   SrcLatitude = 40.5192M,
                   SrcLongitude = -74.3021M,
                   SrcZipCode = "08832",
                   SupplierId = 4,
                   TotalAvailable = 3,
                   Unit = null,
                   WarehouseId = 16,
                   WarehouseName = "8",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 574774,
                   Sku = "2519613",
                   SrcLatitude = 34.0183M,
                   SrcLongitude = -117.8546M,
                   SrcZipCode = "91789",
                   SupplierId = 4,
                   TotalAvailable = 3,
                   Unit = null,
                   WarehouseId = 6,
                   WarehouseName = "11",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 574775,
                   Sku = "2519613",
                   SrcLatitude = 41.9205M,
                   SrcLongitude = -88.0793M,
                   SrcZipCode = "60139",
                   SupplierId = 4,
                   TotalAvailable = 3,
                   Unit = null,
                   WarehouseId = 14,
                   WarehouseName = "6",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 575264,
                   Sku = "2519613",
                   SrcLatitude = 45.4560M,
                   SrcLongitude = -122.7996M,
                   SrcZipCode = "97008",
                   SupplierId = 4,
                   TotalAvailable = 3,
                   Unit = null,
                   WarehouseId = 5,
                   WarehouseName = "10",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 575265,
                   Sku = "2519613",
                   SrcLatitude = 33.8193M,
                   SrcLongitude = -118.2325M,
                   SrcZipCode = "90810",
                   SupplierId = 4,
                   TotalAvailable = 3,
                   Unit = null,
                   WarehouseId = 7,
                   WarehouseName = "12",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 1148002,
                   Sku = "FEL3312501",
                   SrcLatitude = 40.2910M,
                   SrcLongitude = -76.8203M,
                   SrcZipCode = "17109",
                   SupplierId = 5,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 21,
                   WarehouseName = "Harrisburg",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 1158021,
                   Sku = "FEL3312501",
                   SrcLatitude = 36.6207M,
                   SrcLongitude = -119.7308M,
                   SrcZipCode = "93725",
                   SupplierId = 5,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 19,
                   WarehouseName = "California",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 1516838,
                   Sku = "FEL3312501",
                   SrcLatitude = 30.3415M,
                   SrcLongitude = -81.7358M,
                   SrcZipCode = "32254",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = null,
                   WarehouseId = 39,
                   WarehouseName = "027",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 2,

                   GTINPLUS = 1000043859543403,
                   Id = 1516839,
                   Sku = "FEL3312501",
                   SrcLatitude = 33.9938M,
                   SrcLongitude = -117.5236M,
                   SrcZipCode = "91752",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = "EA",
                   WarehouseId = 44,
                   WarehouseName = "031",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 1517113,
                   Sku = "FEL3312501",
                   SrcLatitude = 29.9871M,
                   SrcLongitude = -90.1695M,
                   SrcZipCode = "70001",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = null,
                   WarehouseId = 30,
                   WarehouseName = "017",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 1517114,
                   Sku = "FEL3312501",
                   SrcLatitude = 43.0734M,
                   SrcLongitude = -76.0558M,
                   SrcZipCode = "13057",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = null,
                   WarehouseId = 41,
                   WarehouseName = "029",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 1,

                   GTINPLUS = 1000043859543403,
                   Id = 1519863,
                   Sku = "FEL3312501",
                   SrcLatitude = 35.0231M,
                   SrcLongitude = -89.8492M,
                   SrcZipCode = "38141",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = "EA",
                   WarehouseId = 57,
                   WarehouseName = "006",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 1522286,
                   Sku = "FEL3312501",
                   SrcLatitude = 25.8856M,
                   SrcLongitude = -80.2292M,
                   SrcZipCode = "33167",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = null,
                   WarehouseId = 42,
                   WarehouseName = "003",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 1524866,
                   Sku = "FEL3312501",
                   SrcLatitude = 42.8802M,
                   SrcLongitude = -85.5352M,
                   SrcZipCode = "49512",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = null,
                   WarehouseId = 52,
                   WarehouseName = "039",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 1,

                   GTINPLUS = 1000043859543403,
                   Id = 1526625,
                   Sku = "FEL3312501",
                   SrcLatitude = 33.4816M,
                   SrcLongitude = -86.8590M,
                   SrcZipCode = "35211",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = "EA",
                   WarehouseId = 29,
                   WarehouseName = "016",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 3,

                   GTINPLUS = 1000043859543403,
                   Id = 1526626,
                   Sku = "FEL3312501",
                   SrcLatitude = 27.9951M,
                   SrcLongitude = -82.4046M,
                   SrcZipCode = "33610",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = "EA",
                   WarehouseId = 53,
                   WarehouseName = "004",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 2,

                   GTINPLUS = 1000043859543403,
                   Id = 1529171,
                   Sku = "FEL3312501",
                   SrcLatitude = 35.4444M,
                   SrcLongitude = -97.6164M,
                   SrcZipCode = "73128",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = "EA",
                   WarehouseId = 26,
                   WarehouseName = "013",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 1530461,
                   Sku = "FEL3312501",
                   SrcLatitude = 41.9099M,
                   SrcLongitude = -72.6029M,
                   SrcZipCode = "06088",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = null,
                   WarehouseId = 27,
                   WarehouseName = "014",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 2,

                   GTINPLUS = 1000043859543403,
                   Id = 1530462,
                   Sku = "FEL3312501",
                   SrcLatitude = 39.9046M,
                   SrcLongitude = -82.9703M,
                   SrcZipCode = "43207",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = "EA",
                   WarehouseId = 60,
                   WarehouseName = "009",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 1530604,
                   Sku = "FEL3312501",
                   SrcLatitude = 36.1901M,
                   SrcLongitude = -86.8053M,
                   SrcZipCode = "37228",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = null,
                   WarehouseId = 38,
                   WarehouseName = "026",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 1,

                   GTINPLUS = 1000043859543403,
                   Id = 1531697,
                   Sku = "FEL3312501",
                   SrcLatitude = 35.2522M,
                   SrcLongitude = -80.8265M,
                   SrcZipCode = "28206",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = "EA",
                   WarehouseId = 33,
                   WarehouseName = "002",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 3,

                   GTINPLUS = 1000043859543403,
                   Id = 1537912,
                   Sku = "FEL3312501",
                   SrcLatitude = 39.1484M,
                   SrcLongitude = -76.7922M,
                   SrcZipCode = "20794",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = "EA",
                   WarehouseId = 32,
                   WarehouseName = "019",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 3,

                   GTINPLUS = 1000043859543403,
                   Id = 1537913,
                   Sku = "FEL3312501",
                   SrcLatitude = 38.6406M,
                   SrcLongitude = -121.4440M,
                   SrcZipCode = "95838",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = "EA",
                   WarehouseId = 43,
                   WarehouseName = "030",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 18,

                   GTINPLUS = 1000043859543403,
                   Id = 2321335,
                   Sku = "FEL3312501",
                   SrcLatitude = 33.7866M,
                   SrcLongitude = -118.2987M,
                   SrcZipCode = "91749",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = "EA",
                   WarehouseId = 69,
                   WarehouseName = "COI",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 2,

                   GTINPLUS = 1000043859543403,
                   Id = 2327070,
                   Sku = "FEL3312501",
                   SrcLatitude = 39.7388M,
                   SrcLongitude = -104.4083M,
                   SrcZipCode = "80238",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = "EA",
                   WarehouseId = 70,
                   WarehouseName = "DCO",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 2330817,
                   Sku = "FEL3312501",
                   SrcLatitude = 40.4344M,
                   SrcLongitude = -80.0248M,
                   SrcZipCode = "15086",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = null,
                   WarehouseId = 84,
                   WarehouseName = "PTS",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 2330818,
                   Sku = "FEL3312501",
                   SrcLatitude = 35.0231M,
                   SrcLongitude = -89.8492M,
                   SrcZipCode = "38141",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = null,
                   WarehouseId = 95,
                   WarehouseName = "MTN",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 6,

                   GTINPLUS = 1000043859543403,
                   Id = 2333063,
                   Sku = "FEL3312501",
                   SrcLatitude = 29.8744M,
                   SrcLongitude = -95.5278M,
                   SrcZipCode = "77040",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = "EA",
                   WarehouseId = 74,
                   WarehouseName = "HOU",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 2343355,
                   Sku = "FEL3312501",
                   SrcLatitude = 35.2072M,
                   SrcLongitude = -80.9568M,
                   SrcZipCode = "28278",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = null,
                   WarehouseId = 67,
                   WarehouseName = "CNC",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 18,

                   GTINPLUS = 1000043859543403,
                   Id = 2343356,
                   Sku = "FEL3312501",
                   SrcLatitude = 38.6865M,
                   SrcLongitude = -121.3494M,
                   SrcZipCode = "95842",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = "EA",
                   WarehouseId = 85,
                   WarehouseName = "SAC",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 2,

                   GTINPLUS = 1000043859543403,
                   Id = 2343357,
                   Sku = "FEL3312501",
                   SrcLatitude = 38.8933M,
                   SrcLongitude = -89.4052M,
                   SrcZipCode = "62246",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = "EA",
                   WarehouseId = 88,
                   WarehouseName = "STL",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 2,

                   GTINPLUS = 1000043859543403,
                   Id = 2345005,
                   Sku = "FEL3312501",
                   SrcLatitude = 39.9648M,
                   SrcLongitude = -83.1260M,
                   SrcZipCode = "43228",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = "EA",
                   WarehouseId = 64,
                   WarehouseName = "CBS",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 1,

                   GTINPLUS = 1000043859543403,
                   Id = 2346610,
                   Sku = "FEL3312501",
                   SrcLatitude = 26.0480M,
                   SrcLongitude = -80.3749M,
                   SrcZipCode = "33331",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = "EA",
                   WarehouseId = 72,
                   WarehouseName = "FTL",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 2346611,
                   Sku = "FEL3312501",
                   SrcLatitude = 39.8682M,
                   SrcLongitude = -86.2123M,
                   SrcZipCode = "46268",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = null,
                   WarehouseId = 94,
                   WarehouseName = "IDY",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 6,

                   GTINPLUS = 1000043859543403,
                   Id = 2355205,
                   Sku = "FEL3312501",
                   SrcLatitude = 33.3917M,
                   SrcLongitude = -111.9249M,
                   SrcZipCode = "85282",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = "EA",
                   WarehouseId = 82,
                   WarehouseName = "PAZ",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 2355206,
                   Sku = "FEL3312501",
                   SrcLatitude = 40.3039M,
                   SrcLongitude = -74.5065M,
                   SrcZipCode = "08512",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = null,
                   WarehouseId = 93,
                   WarehouseName = "CNJ",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 2355207,
                   Sku = "FEL3312501",
                   SrcLatitude = 47.4483M,
                   SrcLongitude = -122.2731M,
                   SrcZipCode = "98188",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = null,
                   WarehouseId = 97,
                   WarehouseName = "SWA",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 2355813,
                   Sku = "FEL3312501",
                   SrcLatitude = 43.0073M,
                   SrcLongitude = -85.7255M,
                   SrcZipCode = "49544",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = null,
                   WarehouseId = 73,
                   WarehouseName = "GMI",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 2355814,
                   Sku = "FEL3312501",
                   SrcLatitude = 36.1398M,
                   SrcLongitude = -96.0297M,
                   SrcZipCode = "74158",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = null,
                   WarehouseId = 90,
                   WarehouseName = "TUL",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 2356136,
                   Sku = "FEL3312501",
                   SrcLatitude = 29.5395M,
                   SrcLongitude = -98.4194M,
                   SrcZipCode = "78217",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = null,
                   WarehouseId = 86,
                   WarehouseName = "SAT",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 2367773,
                   Sku = "FEL3312501",
                   SrcLatitude = 29.9511M,
                   SrcLongitude = -90.2060M,
                   SrcZipCode = "70123",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = null,
                   WarehouseId = 79,
                   WarehouseName = "NOL",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 13,

                   GTINPLUS = 1000043859543403,
                   Id = 2371050,
                   Sku = "FEL3312501",
                   SrcLatitude = 34.0425M,
                   SrcLongitude = -84.0262M,
                   SrcZipCode = "30024",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = "EA",
                   WarehouseId = 62,
                   WarehouseName = "ATL",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 6,

                   GTINPLUS = 1000043859543403,
                   Id = 2371051,
                   Sku = "FEL3312501",
                   SrcLatitude = 32.8267M,
                   SrcLongitude = -96.9633M,
                   SrcZipCode = "75061",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = "EA",
                   WarehouseId = 71,
                   WarehouseName = "DFW",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 4706555,
                   Sku = "959210",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 15499,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = null,
                   WarehouseName = null,
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 4738494,
                   Sku = "FEL3312501",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 25499,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = null,
                   WarehouseName = null,
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 5117221,
                   Sku = "LE5732",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 27069,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 7184,
                   WarehouseName = "20",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 5117222,
                   Sku = "LE5732",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 27069,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 7187,
                   WarehouseName = "80",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 5164315,
                   Sku = "LE5732",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 27069,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 7185,
                   WarehouseName = "30",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 1,

                   GTINPLUS = 1000043859543403,
                   Id = 7854758,
                   Sku = "10197879",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 29281,
                   TotalAvailable = 2,
                   Unit = "EA",
                   WarehouseId = 7192,
                   WarehouseName = "A05",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 7874244,
                   Sku = "10197879",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 29281,
                   TotalAvailable = 2,
                   Unit = null,
                   WarehouseId = 7190,
                   WarehouseName = "A03",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 7874245,
                   Sku = "10197879",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 29281,
                   TotalAvailable = 2,
                   Unit = null,
                   WarehouseId = 7193,
                   WarehouseName = "A06",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 7997596,
                   Sku = "FEL3312501",
                   SrcLatitude = 41.6790M,
                   SrcLongitude = -88.1403M,
                   SrcZipCode = "60490",
                   SupplierId = 5,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 20,
                   WarehouseName = "Chicago",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 8003529,
                   Sku = "FEL3312501",
                   SrcLatitude = 33.3958M,
                   SrcLongitude = -84.7121M,
                   SrcZipCode = "30265",
                   SupplierId = 5,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 18,
                   WarehouseName = "Atlanta",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 8028085,
                   Sku = "FEL3312501",
                   SrcLatitude = 36.6207M,
                   SrcLongitude = -119.7308M,
                   SrcZipCode = "93725",
                   SupplierId = 5,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 92,
                   WarehouseName = "California",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 8357865,
                   Sku = "FEL3312501",
                   SrcLatitude = 40.7498M,
                   SrcLongitude = -111.9260M,
                   SrcZipCode = "84104",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = null,
                   WarehouseId = 46,
                   WarehouseName = "033",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 1,

                   GTINPLUS = 1000043859543403,
                   Id = 8357866,
                   Sku = "FEL3312501",
                   SrcLatitude = 40.4322M,
                   SrcLongitude = -80.1021M,
                   SrcZipCode = "15205",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = "EA",
                   WarehouseId = 51,
                   WarehouseName = "038",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 2,

                   GTINPLUS = 1000043859543403,
                   Id = 8374880,
                   Sku = "FEL3312501",
                   SrcLatitude = 42.0076M,
                   SrcLongitude = -87.9931M,
                   SrcZipCode = "60007",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = "EA",
                   WarehouseId = 48,
                   WarehouseName = "035",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 3,

                   GTINPLUS = 1000043859543403,
                   Id = 8385962,
                   Sku = "FEL3312501",
                   SrcLatitude = 29.7971M,
                   SrcLongitude = -95.4958M,
                   SrcZipCode = "77055",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = "EA",
                   WarehouseId = 59,
                   WarehouseName = "008",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 1,

                   GTINPLUS = 1000043859543403,
                   Id = 8386985,
                   Sku = "FEL3312501",
                   SrcLatitude = 33.8631M,
                   SrcLongitude = -84.5382M,
                   SrcZipCode = "30082",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = "EA",
                   WarehouseId = 22,
                   WarehouseName = "001",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 8387999,
                   Sku = "FEL3312501",
                   SrcLatitude = 36.2019M,
                   SrcLongitude = -79.7101M,
                   SrcZipCode = "27214",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = null,
                   WarehouseId = 34,
                   WarehouseName = "022",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 5,

                   GTINPLUS = 1000043859543403,
                   Id = 8388000,
                   Sku = "FEL3312501",
                   SrcLatitude = 47.4483M,
                   SrcLongitude = -122.2731M,
                   SrcZipCode = "98188",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = "EA",
                   WarehouseId = 40,
                   WarehouseName = "028",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 8388001,
                   Sku = "FEL3312501",
                   SrcLatitude = 26.2729M,
                   SrcLongitude = -80.2603M,
                   SrcZipCode = "33065",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = null,
                   WarehouseId = 55,
                   WarehouseName = "046",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 2,

                   GTINPLUS = 1000043859543403,
                   Id = 8389762,
                   Sku = "FEL3312501",
                   SrcLatitude = 39.7340M,
                   SrcLongitude = -105.0259M,
                   SrcZipCode = "80204",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = "EA",
                   WarehouseId = 24,
                   WarehouseName = "011",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 1,

                   GTINPLUS = 1000043859543403,
                   Id = 8389763,
                   Sku = "FEL3312501",
                   SrcLatitude = 38.7229M,
                   SrcLongitude = -90.4474M,
                   SrcZipCode = "63043",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = "EA",
                   WarehouseId = 31,
                   WarehouseName = "018",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 8398485,
                   Sku = "FEL3312501",
                   SrcLatitude = 37.5748M,
                   SrcLongitude = -77.4267M,
                   SrcZipCode = "23222",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = null,
                   WarehouseId = 25,
                   WarehouseName = "012",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 2,

                   GTINPLUS = 1000043859543403,
                   Id = 8398486,
                   Sku = "FEL3312501",
                   SrcLatitude = 42.7717M,
                   SrcLongitude = -71.5132M,
                   SrcZipCode = "03063",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = "EA",
                   WarehouseId = 47,
                   WarehouseName = "034",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 4,

                   GTINPLUS = 1000043859543403,
                   Id = 8398998,
                   Sku = "FEL3312501",
                   SrcLatitude = 33.4564M,
                   SrcLongitude = -112.1284M,
                   SrcZipCode = "85009",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = "EA",
                   WarehouseId = 49,
                   WarehouseName = "036",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 2,

                   GTINPLUS = 1000043859543403,
                   Id = 8398999,
                   Sku = "FEL3312501",
                   SrcLatitude = 33.0033M,
                   SrcLongitude = -96.8820M,
                   SrcZipCode = "75007",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = "EA",
                   WarehouseId = 56,
                   WarehouseName = "005",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 2,

                   GTINPLUS = 1000043859543403,
                   Id = 8404748,
                   Sku = "FEL3312501",
                   SrcLatitude = 39.1222M,
                   SrcLongitude = -94.5487M,
                   SrcZipCode = "64120",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = "EA",
                   WarehouseId = 58,
                   WarehouseName = "007",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 8407027,
                   Sku = "FEL3312501",
                   SrcLatitude = 41.4886M,
                   SrcLongitude = -74.3450M,
                   SrcZipCode = "10941",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = null,
                   WarehouseId = 50,
                   WarehouseName = "037",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 8459480,
                   Sku = "FEL3312501",
                   SrcLatitude = 43.1624M,
                   SrcLongitude = -87.9898M,
                   SrcZipCode = "53223",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = null,
                   WarehouseId = 37,
                   WarehouseName = "025",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 2,

                   GTINPLUS = 1000043859543403,
                   Id = 8472116,
                   Sku = "FEL3312501",
                   SrcLatitude = 39.9683M,
                   SrcLongitude = -74.9533M,
                   SrcZipCode = "08057",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = "EA",
                   WarehouseId = 35,
                   WarehouseName = "023",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 8472117,
                   Sku = "FEL3312501",
                   SrcLatitude = 42.2768M,
                   SrcLongitude = -83.3758M,
                   SrcZipCode = "48184",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = null,
                   WarehouseId = 45,
                   WarehouseName = "032",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 8472118,
                   Sku = "FEL3312501",
                   SrcLatitude = 40.3039M,
                   SrcLongitude = -74.5065M,
                   SrcZipCode = "08512",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = null,
                   WarehouseId = 54,
                   WarehouseName = "044",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 2,

                   GTINPLUS = 1000043859543403,
                   Id = 8476374,
                   Sku = "FEL3312501",
                   SrcLatitude = 45.0132M,
                   SrcLongitude = -93.0297M,
                   SrcZipCode = "55109",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = "EA",
                   WarehouseId = 23,
                   WarehouseName = "010",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 1,

                   GTINPLUS = 1000043859543403,
                   Id = 8476375,
                   Sku = "FEL3312501",
                   SrcLatitude = 29.5790M,
                   SrcLongitude = -98.2778M,
                   SrcZipCode = "78154",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = "EA",
                   WarehouseId = 28,
                   WarehouseName = "015",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 1,

                   GTINPLUS = 1000043859543403,
                   Id = 8476376,
                   Sku = "FEL3312501",
                   SrcLatitude = 39.8682M,
                   SrcLongitude = -86.2123M,
                   SrcZipCode = "46268",
                   SupplierId = 7,
                   TotalAvailable = 49,
                   Unit = "EA",
                   WarehouseId = 36,
                   WarehouseName = "024",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 6,

                   GTINPLUS = 1000043859543403,
                   Id = 9147570,
                   Sku = "FEL3312501",
                   SrcLatitude = 41.3289M,
                   SrcLongitude = -81.4559M,
                   SrcZipCode = "44087",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = "EA",
                   WarehouseId = 66,
                   WarehouseName = "CLV",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 11,

                   GTINPLUS = 1000043859543403,
                   Id = 9147571,
                   Sku = "FEL3312501",
                   SrcLatitude = 40.3039M,
                   SrcLongitude = -74.5065M,
                   SrcZipCode = "08512",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = "EA",
                   WarehouseId = 68,
                   WarehouseName = "CNJ",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 9208290,
                   Sku = "FEL3312501",
                   SrcLatitude = 36.0127M,
                   SrcLongitude = -86.5600M,
                   SrcZipCode = "37086",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = null,
                   WarehouseId = 80,
                   WarehouseName = "NSH",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 9232894,
                   Sku = "FEL3312501",
                   SrcLatitude = 42.3501M,
                   SrcLongitude = -73.8199M,
                   SrcZipCode = "12051",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = null,
                   WarehouseId = 61,
                   WarehouseName = "ALB",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 14,

                   GTINPLUS = 1000043859543403,
                   Id = 9246464,
                   Sku = "FEL3312501",
                   SrcLatitude = 41.9178M,
                   SrcLongitude = -88.1370M,
                   SrcZipCode = "60188",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = "EA",
                   WarehouseId = 65,
                   WarehouseName = "CHI",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 3,

                   GTINPLUS = 1000043859543403,
                   Id = 9253530,
                   Sku = "FEL3312501",
                   SrcLatitude = 42.4829M,
                   SrcLongitude = -71.1574M,
                   SrcZipCode = "01801",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = "EA",
                   WarehouseId = 91,
                   WarehouseName = "WOB",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 9269942,
                   Sku = "FEL3312501",
                   SrcLatitude = 39.1479M,
                   SrcLongitude = -94.5680M,
                   SrcZipCode = "64116",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = null,
                   WarehouseId = 76,
                   WarehouseName = "KAN",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 6,

                   GTINPLUS = 1000043859543403,
                   Id = 9269943,
                   Sku = "FEL3312501",
                   SrcLatitude = 44.8471M,
                   SrcLongitude = -93.1543M,
                   SrcZipCode = "55121",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = "EA",
                   WarehouseId = 77,
                   WarehouseName = "MIN",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 1,

                   GTINPLUS = 1000043859543403,
                   Id = 9269944,
                   Sku = "FEL3312501",
                   SrcLatitude = 28.4522M,
                   SrcLongitude = -81.4678M,
                   SrcZipCode = "32819",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = "EA",
                   WarehouseId = 81,
                   WarehouseName = "ORL",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 9284948,
                   Sku = "FEL3312501",
                   SrcLatitude = 45.4840M,
                   SrcLongitude = -122.6365M,
                   SrcZipCode = "97202",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = null,
                   WarehouseId = 83,
                   WarehouseName = "POR",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 8,

                   GTINPLUS = 1000043859543403,
                   Id = 9287519,
                   Sku = "FEL3312501",
                   SrcLatitude = 47.4483M,
                   SrcLongitude = -122.2731M,
                   SrcZipCode = "98188",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = "EA",
                   WarehouseId = 89,
                   WarehouseName = "SWA",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 9332608,
                   Sku = "FEL3312501",
                   SrcLatitude = 40.7498M,
                   SrcLongitude = -111.9260M,
                   SrcZipCode = "84104",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = null,
                   WarehouseId = 96,
                   WarehouseName = "SLK",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 2,

                   GTINPLUS = 1000043859543403,
                   Id = 9386359,
                   Sku = "FEL3312501",
                   SrcLatitude = 39.8682M,
                   SrcLongitude = -86.2123M,
                   SrcZipCode = "46268",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = "EA",
                   WarehouseId = 75,
                   WarehouseName = "IDY",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 4,

                   GTINPLUS = 1000043859543403,
                   Id = 9386360,
                   Sku = "FEL3312501",
                   SrcLatitude = 35.0231M,
                   SrcLongitude = -89.8492M,
                   SrcZipCode = "38141",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = "EA",
                   WarehouseId = 78,
                   WarehouseName = "MTN",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 14,

                   GTINPLUS = 1000043859543403,
                   Id = 9421044,
                   Sku = "FEL3312501",
                   SrcLatitude = 39.1551M,
                   SrcLongitude = -76.7215M,
                   SrcZipCode = "21076",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = "EA",
                   WarehouseId = 63,
                   WarehouseName = "BAL",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 3,

                   GTINPLUS = 1000043859543403,
                   Id = 9421045,
                   Sku = "FEL3312501",
                   SrcLatitude = 40.7498M,
                   SrcLongitude = -111.9260M,
                   SrcZipCode = "84104",
                   SupplierId = 8,
                   TotalAvailable = 144,
                   Unit = "EA",
                   WarehouseId = 87,
                   WarehouseName = "SLK",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 12041927,
                   Sku = "LE5732",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 27069,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 7183,
                   WarehouseName = "10",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 12041928,
                   Sku = "LE5732",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 27069,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = 7186,
                   WarehouseName = "40",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = 1,

                   GTINPLUS = 1000043859543403,
                   Id = 13847325,
                   Sku = "10197879",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 29281,
                   TotalAvailable = 2,
                   Unit = "EA",
                   WarehouseId = 7191,
                   WarehouseName = "A04",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 13950340,
                   Sku = "K73312501",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 29539,
                   TotalAvailable = null,
                   Unit = null,
                   WarehouseId = null,
                   WarehouseName = null,
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 14233146,
                   Sku = "10197879",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 29281,
                   TotalAvailable = 2,
                   Unit = null,
                   WarehouseId = 7189,
                   WarehouseName = "A02",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 14250955,
                   Sku = "10197879",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 29281,
                   TotalAvailable = 2,
                   Unit = null,
                   WarehouseId = 7194,
                   WarehouseName = "A07",
                   WarehouseRating = null,



               },
               new Inventory
               {
                   Available = null,

                   GTINPLUS = 1000043859543403,
                   Id = 14621107,
                   Sku = "10197879",
                   SrcLatitude = null,
                   SrcLongitude = null,
                   SrcZipCode = null,
                   SupplierId = 29281,
                   TotalAvailable = 2,
                   Unit = null,
                   WarehouseId = 7188,
                   WarehouseName = "A01",
                   WarehouseRating = null,



               }
                #endregion
                    };
               return _inventories;
            }
        }

        public static List<Price> Matches
        {
            get
            {
                if (_matches == null)
                    _matches = new List<Price>
                    {
                        #region matches
                        new Price
        {
            Cost = 24.0300m,
            CountryOfOrigin = "JP",
            GTINPLUS = 1000013803134971,
            Id = 465978,
            LeadTime = 2,
            LowestPrice = true,
            LowestTAAPrice = true,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 16917,
            Sku = "S15208B001",
            SupplierId = 29539,
            SupplierName = "Office Max",
            SupplierRating = 3,
            TAA = true,
            Unit = "EA",



        },
        new Price
        {
            Cost = 24.9400m,
            CountryOfOrigin = "JP",
            GTINPLUS = 1000013803134971,
            Id = 588242,
            LeadTime = 2,
            LowestPrice = null,
            LowestTAAPrice = null,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 16917,
            Sku = "3097682",
            SupplierId = 4,
            SupplierName = "Synnex Corporation",
            SupplierRating = 3,
            TAA = true,
            Unit = "EA",



        },
        new Price
        {
            Cost = 25.4200m,
            CountryOfOrigin = "JP",
            GTINPLUS = 1000013803134971,
            Id = 605800,
            LeadTime = 2,
            LowestPrice = null,
            LowestTAAPrice = null,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 16917,
            Sku = "CL241XL",
            SupplierId = 5,
            SupplierName = "D&H Distributing",
            SupplierRating = 5,
            TAA = true,
            Unit = "EA",



        },
        new Price
        {
            Cost = 26.4700m,
            CountryOfOrigin = "JP",
            GTINPLUS = 1000013803134971,
            Id = 850331,
            LeadTime = 2,
            LowestPrice = null,
            LowestTAAPrice = null,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 16917,
            Sku = "CNM5208B001",
            SupplierId = 8,
            SupplierName = "United Stationers, Inc.",
            SupplierRating = 5,
            TAA = true,
            Unit = "EA",



        },
        new Price
        {
            Cost = 24.9900m,
            CountryOfOrigin = "CA",
            GTINPLUS = 1000013803134971,
            Id = 881157,
            LeadTime = 2,
            LowestPrice = null,
            LowestTAAPrice = null,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 16917,
            Sku = "804397",
            SupplierId = 15499,
            SupplierName = "Office Depot, Inc.",
            SupplierRating = 1,
            TAA = true,
            Unit = "EA",



        },
        new Price
        {
            Cost = 25.3700m,
            CountryOfOrigin = "JP",
            GTINPLUS = 1000013803134971,
            Id = 913792,
            LeadTime = 3,
            LowestPrice = null,
            LowestTAAPrice = null,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 16917,
            Sku = "CNMCL241XL",
            SupplierId = 7,
            SupplierName = "S.P. Richards Company",
            SupplierRating = 5,
            TAA = true,
            Unit = "EA",



        },
        new Price
        {
            Cost = 25.2800m,
            CountryOfOrigin = "JP",
            GTINPLUS = 1000013803134971,
            Id = 971877,
            LeadTime = 3,
            LowestPrice = null,
            LowestTAAPrice = null,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 16917,
            Sku = "501447",
            SupplierId = 3,
            SupplierName = "Supplies Network",
            SupplierRating = 5,
            TAA = true,
            Unit = "EA",



        },
        new Price
        {
            Cost = 11.7400m,
            CountryOfOrigin = null,
            GTINPLUS = 1000023942491712,
            Id = 12713,
            LeadTime = 2,
            LowestPrice = null,
            LowestTAAPrice = null,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 25377,
            Sku = "Q349171",
            SupplierId = 29539,
            SupplierName = "Office Max",
            SupplierRating = 3,
            TAA = null,
            Unit = "EA",



        },
        new Price
        {
            Cost = 6.2300m,
            CountryOfOrigin = "CN",
            GTINPLUS = 1000023942491712,
            Id = 45684,
            LeadTime = 2,
            LowestPrice = null,
            LowestTAAPrice = null,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 25377,
            Sku = "VER49171",
            SupplierId = 8,
            SupplierName = "United Stationers, Inc.",
            SupplierRating = 5,
            TAA = false,
            Unit = "EA",



        },
        new Price
        {
            Cost = 5.5700m,
            CountryOfOrigin = null,
            GTINPLUS = 1000023942491712,
            Id = 213238,
            LeadTime = 7,
            LowestPrice = null,
            LowestTAAPrice = null,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 25377,
            Sku = "10912867",
            SupplierId = 29281,
            SupplierName = "Tech Data Corporation",
            SupplierRating = 4,
            TAA = null,
            Unit = "EA",



        },
        new Price
        {
            Cost = 6.6900m,
            CountryOfOrigin = "CA",
            GTINPLUS = 1000023942491712,
            Id = 225951,
            LeadTime = 2,
            LowestPrice = null,
            LowestTAAPrice = true,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 25377,
            Sku = "768643",
            SupplierId = 15499,
            SupplierName = "Office Depot, Inc.",
            SupplierRating = 1,
            TAA = true,
            Unit = "EA",



        },
        new Price
        {
            Cost = 5.0000m,
            CountryOfOrigin = "CN",
            GTINPLUS = 1000023942491712,
            Id = 343750,
            LeadTime = 3,
            LowestPrice = true,
            LowestTAAPrice = null,
            MinimumQty = 10,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 25377,
            Sku = "VER49171",
            SupplierId = 25499,
            SupplierName = "TriMega Purchasing Association",
            SupplierRating = 3,
            TAA = false,
            Unit = "EA",



        },
        new Price
        {
            Cost = 5.2100m,
            CountryOfOrigin = null,
            GTINPLUS = 1000023942491712,
            Id = 732793,
            LeadTime = 2,
            LowestPrice = null,
            LowestTAAPrice = null,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 25377,
            Sku = "3729282",
            SupplierId = 4,
            SupplierName = "Synnex Corporation",
            SupplierRating = 3,
            TAA = null,
            Unit = "EA",



        },
        new Price
        {
            Cost = 5.0000m,
            CountryOfOrigin = "CN",
            GTINPLUS = 1000023942491712,
            Id = 782885,
            LeadTime = 3,
            LowestPrice = true,
            LowestTAAPrice = null,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 25377,
            Sku = "912417",
            SupplierId = 3,
            SupplierName = "Supplies Network",
            SupplierRating = 5,
            TAA = false,
            Unit = "EA",



        },
        new Price
        {
            Cost = 6.3500m,
            CountryOfOrigin = null,
            GTINPLUS = 1000023942491712,
            Id = 935054,
            LeadTime = 3,
            LowestPrice = null,
            LowestTAAPrice = null,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 25377,
            Sku = "PG9605",
            SupplierId = 27069,
            SupplierName = "Ingram Micro Inc.",
            SupplierRating = 4,
            TAA = null,
            Unit = "EA",



        },
        new Price
        {
            Cost = 7.3800m,
            CountryOfOrigin = "TW",
            GTINPLUS = 1000023942491712,
            Id = 962122,
            LeadTime = 3,
            LowestPrice = null,
            LowestTAAPrice = null,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 25377,
            Sku = "VER49171",
            SupplierId = 7,
            SupplierName = "S.P. Richards Company",
            SupplierRating = 5,
            TAA = true,
            Unit = "EA",



        },
        new Price
        {
            Cost = 24.9200m,
            CountryOfOrigin = "TW",
            GTINPLUS = 1000023942986645,
            Id = 14913,
            LeadTime = 2,
            LowestPrice = null,
            LowestTAAPrice = null,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 40502,
            Sku = "4357150",
            SupplierId = 4,
            SupplierName = "Synnex Corporation",
            SupplierRating = 3,
            TAA = true,
            Unit = "EA",



        },
        new Price
        {
            Cost = 27.4400m,
            CountryOfOrigin = "TW",
            GTINPLUS = 1000023942986645,
            Id = 55305,
            LeadTime = 2,
            LowestPrice = null,
            LowestTAAPrice = null,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 40502,
            Sku = "VER98664",
            SupplierId = 8,
            SupplierName = "United Stationers, Inc.",
            SupplierRating = 5,
            TAA = true,
            Unit = "EA",



        },
        new Price
        {
            Cost = 27.6500m,
            CountryOfOrigin = "TW",
            GTINPLUS = 1000023942986645,
            Id = 180446,
            LeadTime = 3,
            LowestPrice = null,
            LowestTAAPrice = null,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 40502,
            Sku = "VER98664",
            SupplierId = 7,
            SupplierName = "S.P. Richards Company",
            SupplierRating = 5,
            TAA = true,
            Unit = "EA",



        },
        new Price
        {
            Cost = 25.3900m,
            CountryOfOrigin = null,
            GTINPLUS = 1000023942986645,
            Id = 268079,
            LeadTime = 3,
            LowestPrice = null,
            LowestTAAPrice = null,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 40502,
            Sku = "YN6008",
            SupplierId = 27069,
            SupplierName = "Ingram Micro Inc.",
            SupplierRating = 4,
            TAA = null,
            Unit = "EA",



        },
        new Price
        {
            Cost = 24.0000m,
            CountryOfOrigin = "TW",
            GTINPLUS = 1000023942986645,
            Id = 317748,
            LeadTime = 3,
            LowestPrice = true,
            LowestTAAPrice = true,
            MinimumQty = 4,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 40502,
            Sku = "VER98664",
            SupplierId = 25499,
            SupplierName = "TriMega Purchasing Association",
            SupplierRating = 3,
            TAA = true,
            Unit = "EA",



        },
        new Price
        {
            Cost = 24.5100m,
            CountryOfOrigin = "TW",
            GTINPLUS = 1000023942986645,
            Id = 745945,
            LeadTime = 3,
            LowestPrice = null,
            LowestTAAPrice = null,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 40502,
            Sku = "964297",
            SupplierId = 3,
            SupplierName = "Supplies Network",
            SupplierRating = 5,
            TAA = true,
            Unit = "EA",



        },
        new Price
        {
            Cost = 24.8100m,
            CountryOfOrigin = "CN",
            GTINPLUS = 1000023942986645,
            Id = 904672,
            LeadTime = 2,
            LowestPrice = null,
            LowestTAAPrice = null,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 40502,
            Sku = "VER98664",
            SupplierId = 5,
            SupplierName = "D&H Distributing",
            SupplierRating = 5,
            TAA = false,
            Unit = "EA",



        },
        new Price
        {
            Cost = 26.7700m,
            CountryOfOrigin = null,
            GTINPLUS = 1000023942986645,
            Id = 967557,
            LeadTime = 9,
            LowestPrice = null,
            LowestTAAPrice = null,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 40502,
            Sku = "11648855",
            SupplierId = 29281,
            SupplierName = "Tech Data Corporation",
            SupplierRating = 4,
            TAA = null,
            Unit = "EA",



        },
        new Price
        {
            Cost = 447.4500m,
            CountryOfOrigin = null,
            GTINPLUS = 1000043859543403,
            Id = 198068,
            LeadTime = 13,
            LowestPrice = null,
            LowestTAAPrice = null,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 228346,
            Sku = "10197879",
            SupplierId = 29281,
            SupplierName = "Tech Data Corporation",
            SupplierRating = 4,
            TAA = null,
            Unit = "EA",



        },
        new Price
        {
            Cost = 591.9600m,
            CountryOfOrigin = "CA",
            GTINPLUS = 1000043859543403,
            Id = 564938,
            LeadTime = 2,
            LowestPrice = null,
            LowestTAAPrice = true,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 228346,
            Sku = "959210",
            SupplierId = 15499,
            SupplierName = "Office Depot, Inc.",
            SupplierRating = 1,
            TAA = true,
            Unit = "EA",



        },
        new Price
        {
            Cost = 675.9900m,
            CountryOfOrigin = "CN",
            GTINPLUS = 1000043859543403,
            Id = 710124,
            LeadTime = 2,
            LowestPrice = null,
            LowestTAAPrice = null,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 228346,
            Sku = "K73312501",
            SupplierId = 29539,
            SupplierName = "Office Max",
            SupplierRating = 3,
            TAA = false,
            Unit = "EA",



        },
        new Price
        {
            Cost = 434.9600m,
            CountryOfOrigin = "CN",
            GTINPLUS = 1000043859543403,
            Id = 926953,
            LeadTime = 3,
            LowestPrice = true,
            LowestTAAPrice = null,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 228346,
            Sku = "FEL3312501",
            SupplierId = 25499,
            SupplierName = "TriMega Purchasing Association",
            SupplierRating = 3,
            TAA = false,
            Unit = "EA",



        },
        new Price
        {
            Cost = 509.5500m,
            CountryOfOrigin = null,
            GTINPLUS = 1000043859543403,
            Id = 927326,
            LeadTime = 15,
            LowestPrice = null,
            LowestTAAPrice = null,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 228346,
            Sku = "LE5732",
            SupplierId = 27069,
            SupplierName = "Ingram Micro Inc.",
            SupplierRating = 4,
            TAA = null,
            Unit = "EA",



        },
        new Price
        {
            Cost = 476.6100m,
            CountryOfOrigin = "CN",
            GTINPLUS = 1000043859543403,
            Id = 952682,
            LeadTime = 3,
            LowestPrice = null,
            LowestTAAPrice = null,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 228346,
            Sku = "FEL3312501",
            SupplierId = 7,
            SupplierName = "S.P. Richards Company",
            SupplierRating = 5,
            TAA = false,
            Unit = "EA",



        },
        new Price
        {
            Cost = 462.7300m,
            CountryOfOrigin = "CN",
            GTINPLUS = 1000043859543403,
            Id = 967270,
            LeadTime = 2,
            LowestPrice = null,
            LowestTAAPrice = null,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 228346,
            Sku = "FEL3312501",
            SupplierId = 8,
            SupplierName = "United Stationers, Inc.",
            SupplierRating = 5,
            TAA = false,
            Unit = "EA",



        },
        new Price
        {
            Cost = 453.0800m,
            CountryOfOrigin = "CN",
            GTINPLUS = 1000043859543403,
            Id = 971997,
            LeadTime = 2,
            LowestPrice = null,
            LowestTAAPrice = null,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 228346,
            Sku = "FEL3312501",
            SupplierId = 5,
            SupplierName = "D&H Distributing",
            SupplierRating = 5,
            TAA = false,
            Unit = "EA",



        },
        new Price
        {
            Cost = 457.6100m,
            CountryOfOrigin = null,
            GTINPLUS = 1000043859543403,
            Id = 996288,
            LeadTime = 2,
            LowestPrice = null,
            LowestTAAPrice = null,
            MinimumQty = 1,
            PackPrice = null,
            PackQty = null,
            PackRequired = false,
            PackSku = null,
            PackUnit = null,
            ReferenceId = 228346,
            Sku = "2519613",
            SupplierId = 4,
            SupplierName = "Synnex Corporation",
            SupplierRating = 3,
            TAA = null,
            Unit = "EA",



        }
        #endregion
                    };
                return _matches;
            }

        }



    }
}

