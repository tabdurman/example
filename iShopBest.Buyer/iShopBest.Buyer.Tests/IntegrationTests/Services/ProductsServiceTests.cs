﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using iShopBest.Buyer.AppTools;
using iShopBest.Buyer.Domain.Model.ServiceContracts;
using CapSoft.Infrastructure.Domain;
using iShopBest.Buyer.Domain.Model.DataContracts.ProductsService;
using System.Collections.Generic;
using System.Linq.Expressions;
using iShopBest.Buyer.Domain.Model.Entities;
using CapSoft.Infrastructure.QueryObject;
using AutoMapper;
using iShopBest.Buyer.Presentation.Api.Config;

namespace iShopBest.Buyer.Tests.IntegrationTests.Services
{
    [TestClass]
    public class ProductsServiceTests
    {
        [TestInitialize]
        public void Initialize()
        {
            Bootstrapper.Init("iShopBest.Buyer.App", "name=BuyerCatalogEntities");
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<BuyerPresentationApiProfile>();
            });
        }


        [TestMethod]
        public void GetProductsByCategoryId()
        {
            Product product = new Product();
            bool ascending = true;
            string sortByField = "Id";

            /*
            List<Expression<Func<Product, SortableProperty>>> orderBy = new List<Expression<Func<Product, SortableProperty>>>();
            orderBy.Add(x => new SortableProperty(x.GetType().GetProperty(sortByField).GetValue(x, null), ascending));
            sortByField = "ProductTitle";
            orderBy.Add(x => new SortableProperty(x.GetType().GetProperty(sortByField).GetValue(x, null), ascending));
            */

            string[] orderBy = "Id,ProductTitle".Split(',');

            GetProductsRequest request = new GetProductsRequest()
            {
                CategoryId = 43211509,
                PageNumber = 1,
                PageSize = 50,
                //Ascending = true,
                OrderBy = orderBy
            };


            IProductsService _testingService= ValueObjectBase.Container.GetExportedValue<IProductsService>();
            GetProductsResponse response = _testingService.GetProducts(request);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetProductsByKeyword()
        {
            Product product = new Product();
            bool ascending = true;
            string sortByField = "Id";
            string[] orderBy = "Id,ProductTitle".Split(',');

            GetProductsRequest request = new GetProductsRequest()
            {
                Keywords="toner",
                PageNumber = 1,
                PageSize = 50,
                //Ascending = true,
                OrderBy = orderBy
            };


            IProductsService _testingService = ValueObjectBase.Container.GetExportedValue<IProductsService>();
            GetProductsResponse response = _testingService.GetProducts(request);
            Assert.IsNotNull(response);
        }
    }
}
