﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using iShopBest.Buyer.Domain.Model.ServiceContracts;
using iShopBest.Buyer.AppTools;
using CapSoft.Infrastructure.Domain;
using iShopBest.Buyer.Domain.Model.DataContracts.ShoppingCartService;

namespace iShopBest.Buyer.Tests.IntegrationTests.Services
{
    [TestClass]
    public class ShoppingCartServiceTests
    {
        private IShoppingCartService _testingService;

        [TestInitialize]
        public void Init()
        {
            Bootstrapper.Init("iShopBest.Buyer.App", "name=BuyerCatalogEntities");
            _testingService = ValueObjectBase.Container.GetExportedValue<IShoppingCartService>();
        }


        [TestMethod]
        public void GetCartsListCall()
        {
            var resp = _testingService.GetCartsList(new GetCartsListRequest() { ForUserId = 1 });
            Assert.IsNotNull(resp);
        }
    }
}
