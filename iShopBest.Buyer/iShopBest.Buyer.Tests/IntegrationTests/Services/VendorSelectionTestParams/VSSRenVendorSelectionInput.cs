﻿using iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService;

namespace iShopBest.Buyer.Tests.IntegrationTests.Services.VendorSelectionTestParams
{
    public class VSSRunVendorSelectionInput
    {
        private RunVendorSelectionRequest _request;

        public VSSRunVendorSelectionInput()
        {
            Request = new iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService.RunVendorSelectionRequest
            {
                IncludeInventory = false,
                Preferences = new System.Collections.Generic.List<iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService.Parameter>
                {
                    new iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService.Parameter
                    {
                        Name = "DeliverySpeedImportance",
                        Value = "50"
                    },
                    new iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService.Parameter
                    {
                        Name = "FreightCostImportance",
                        Value = "50"
                    },
                    new iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService.Parameter
                    {
                        Name = "SingleSupplierImportance",
                        Value = "50"
                    }
                },
                ShoppingCart = new System.Collections.Generic.List<iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService.ItemRequirement>
                {
                    new iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService.ItemRequirement
                    {
                        DestLatitude = 0m,
                        DestLongitude = 0m,
                        DestZipCode = "33324",
                        GtinPlus = 1000829160493879,
                        Quantity = 1
                    },
                    new iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService.ItemRequirement
                    {
                        DestLatitude = 0m,
                        DestLongitude = 0m,
                        DestZipCode = "33324",
                        GtinPlus = 1000829160493886,
                        Quantity = 1
                    },
                    new iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService.ItemRequirement
                    {
                        DestLatitude = 0m,
                        DestLongitude = 0m,
                        DestZipCode = "33324",
                        GtinPlus = 1000829160493893,
                        Quantity = 1
                    },
                    new iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService.ItemRequirement
                    {
                        DestLatitude = 0m,
                        DestLongitude = 0m,
                        DestZipCode = "33324",
                        GtinPlus = 1000829160493909,
                        Quantity = 1
                    }
                }
            };
        }

        public RunVendorSelectionRequest GetSecondRequest()
        {
            Request = new iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService.RunVendorSelectionRequest
            {
                IncludeInventory = false,
                Preferences = new System.Collections.Generic.List<iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService.Parameter>
                {
                    new iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService.Parameter
                    {
                        Name = "DeliverySpeedImportance",
                        Value = "50"
                    },
                    new iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService.Parameter
                    {
                        Name = "FreightCostImportance",
                        Value = "50"
                    },
                    new iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService.Parameter
                    {
                        Name = "SingleSupplierImportance",
                        Value = "10"
                    }
                },
                ShoppingCart = new System.Collections.Generic.List<iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService.ItemRequirement>
                {
                    new iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService.ItemRequirement
                    {
                        DestLatitude = 0m,
                        DestLongitude = 0m,
                        DestZipCode = "33324",
                        GtinPlus = 1000808736558136,
                        Quantity = 1
                    },
                    new iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService.ItemRequirement
                    {
                        DestLatitude = 0m,
                        DestLongitude = 0m,
                        DestZipCode = "33324",
                        GtinPlus = 1000886111124879,
                        Quantity = 1
                    }
                }
            };
            return Request;
        }

        public RunVendorSelectionRequest Request { get => _request; set => _request = value; }
    }
}