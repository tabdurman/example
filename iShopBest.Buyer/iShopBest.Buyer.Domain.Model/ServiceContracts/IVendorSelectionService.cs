﻿using CapSoft.Infrastructure.Domain;
using iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService;
using System;
using System.ServiceModel;

namespace iShopBest.Buyer.Domain.Model.ServiceContracts
{
    [ServiceContract]
    public interface IVendorSelectionService : IDisposable
    {
        [FaultContract(typeof(ServiceFault))]
        [OperationContract]
        RunVendorSelectionResponse RunVendorSelection(RunVendorSelectionRequest request);

        /// <summary>
        /// Will load matches for each line item
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [FaultContract(typeof(ServiceFault))]
        [OperationContract]
        GetMatchesResponse GetMatches(GetMatchesRequest request);

        [FaultContract(typeof(ServiceFault))]
        [OperationContract]
        GenerateRoutesResponse GenerateRoutes(GenerateRoutesRequest request);

    }
}
