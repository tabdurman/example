﻿using CapSoft.Infrastructure.Domain;
using iShopBest.Buyer.Domain.Model.DataContracts.ProductsService;
using System;
using System.ServiceModel;

namespace iShopBest.Buyer.Domain.Model.ServiceContracts
{
    [ServiceContract]
    public interface IProductsService : IDisposable
    {
        [FaultContract(typeof(ServiceFault))]
        [OperationContract]
        GetProductsResponse GetProducts(GetProductsRequest request);

        [FaultContract(typeof(ServiceFault))]
        [OperationContract]
        GetProductDetailsResponse GetProductDetails(GetProductDetailsRequest request);

        [FaultContract(typeof(ServiceFault))]
        [OperationContract]
        GetProductImageResponse GetProductImage(GetProductImageRequest request);
    }
}
