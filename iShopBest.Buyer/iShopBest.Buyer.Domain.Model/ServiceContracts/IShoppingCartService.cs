﻿using CapSoft.Infrastructure.Domain;
using iShopBest.Buyer.Domain.Model.DataContracts.ShoppingCartService;
using System;
using System.ServiceModel;

namespace iShopBest.Buyer.Domain.Model.ServiceContracts
{
    [ServiceContract]
    public interface IShoppingCartService : IDisposable
    {
        [FaultContract(typeof(ServiceFault))]
        [OperationContract]
        SaveCartResponse SaveCart(SaveCartRequest request);

        [FaultContract(typeof(ServiceFault))]
        [OperationContract]
        LoadCartResponse LoadCart(LoadCartRequest request);

        [FaultContract(typeof(ServiceFault))]
        [OperationContract]
        GetCartsListResponse GetCartsList(GetCartsListRequest request);
    }
}
