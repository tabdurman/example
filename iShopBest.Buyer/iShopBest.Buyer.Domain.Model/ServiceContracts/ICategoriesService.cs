﻿using CapSoft.Infrastructure.Domain;
using iShopBest.Buyer.Domain.Model.DataContracts.CategoriesService;
using iShopBest.Buyer.Domain.Model.Entities;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace iShopBest.Buyer.Domain.Model.ServiceContracts
{
    [ServiceContract]
    public interface ICategoriesService: IDisposable
    {
        /// <summary>
        /// Will return list of categories per specified parent id
        /// <para>Send null as <paramref name="parentCategoryId"/> for root level categories</para>
        /// </summary>
        /// <param name="parentCategoryId">64 bit integer value of parent caregory id</param>
        /// <returns>List of child categories</returns>
        [FaultContract(typeof(ServiceFault))]
        [OperationContract]
        GetCategoriesResponse GetCategories(Int64? parentCategoryId);


        /// <summary>
        /// Will return list of brand/series/models per specified parent id
        /// <para>Send null as <paramref name="parentCategoryId"/> for root level categories</para>
        /// </summary>
        /// <param name="parentCategoryId">64 bit integer value of parent caregory id</param>
        /// <returns>List of child categories</returns>
        [FaultContract(typeof(ServiceFault))]
        [OperationContract]
        List<CatalogCategory> GetCatalogCategories(Int64? parentCategoryId);

        /// <summary>
        /// Will return products under specified category
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [FaultContract(typeof(ServiceFault))]
        [OperationContract]
        GetProductsByCategoryResponse GetProductsByCategory(GetProductsByCategoryRequest request);
    }
}
