﻿using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.ProductsService
{
    [DataContract(IsReference=true)]
    public class ManufacturerSummary
    {
        [DataMember]
        public long ManufacturerId { get; set; }

        [DataMember]
        public string ManufacturerName { get; set; }

        [DataMember]
        public long ProductsCount { get; set; }

    }
}
