﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.ProductsService
{
    [DataContract]
    public class GetProductDetailsResponse 
    {
        [DataMember]
        public ProductView ProductDetails { get; set; }

        [DataMember]
        public IList<AvailabilityView> AvailableFrom { get; set; } 

    }
}
