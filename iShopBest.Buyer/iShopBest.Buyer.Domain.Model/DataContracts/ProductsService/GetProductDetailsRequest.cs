﻿using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.ProductsService
{
    [DataContract]
    public class GetProductDetailsRequest
    {
        [DataMember]
        public long ProductId { get; set; }
    }
}
