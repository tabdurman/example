﻿using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.ProductsService
{
    [DataContract(IsReference=true)]
    public class GetProductImageRequest
    {
        [DataMember]
        public long ProductId { get; set; } 
    }
}
