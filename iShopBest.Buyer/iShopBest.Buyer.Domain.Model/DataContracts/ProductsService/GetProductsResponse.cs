﻿using CapSoft.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.ProductsService
{
    [DataContract(IsReference=true)]
    public class GetProductsResponse : CommonPagedResponse<ProductView>
    {
        [DataMember]
        public decimal? MinPrice { get; set; }
        [DataMember]
        public decimal? MaxPrice { get; set; }
        [DataMember]
        public List<ManufacturerSummary> Manufacturers { get; set; }
    }
}
