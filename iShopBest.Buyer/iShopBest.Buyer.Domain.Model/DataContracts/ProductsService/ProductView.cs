﻿using System;
using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.ProductsService
{
    [DataContract]
    public class ProductView
    {
        [DataMember] 
        public long ProductId { get; set; }
        [DataMember]
        public long ReferenceId { get; set; }

        [DataMember]
        public string ManufacturerPartNumber { get; set; }
        [DataMember]
        public string ProductTitle { get; set; }
        [DataMember]
        public string ShortDescription { get; set; }
        [DataMember]
        public string LongDescription { get; set; }
        [DataMember]
        public string Keywords { get; set; }
        [DataMember]
        public string GTIN { get; set; }
        [DataMember]
        public string Dimensions { get; set; }
        [DataMember]
        public Nullable<decimal> Weight { get; set; }
        [DataMember]
        public Nullable<decimal> ListPrice { get; set; }
        [DataMember]
        public Nullable<byte> ProductRating { get; set; }
        [DataMember]
        public string ManufacturerName { get; set; }
        [DataMember]
        public string ManufacturerId { get; set; }
        [DataMember]
        public Nullable<byte> ManufacturerRating { get; set; }
        [DataMember]
        public string Unit { get; set; }
        [DataMember]
        public string CountryOfOrigin { get; set; }
        [DataMember]
        public Nullable<long> UNSPSC { get; set; }
        [DataMember]
        public string ImageUrl { get; set; }
        [DataMember]
        public decimal PriceMin { get; set; }
        [DataMember]
        public decimal PriceMax { get; set; }

    }
}
