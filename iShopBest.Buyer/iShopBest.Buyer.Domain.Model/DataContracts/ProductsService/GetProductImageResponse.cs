﻿using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.ProductsService
{
    [DataContract(IsReference=true)]
    public class GetProductImageResponse
    {
        [DataMember]
        public byte[] DataBin { get; set; }
        [DataMember]
        public string MediaType { get; set; }


    }
}
