﻿using CapSoft.Infrastructure.Domain;
using System;
using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.ProductsService
{
    [DataContract(IsReference =true)]
    public class GetProductsRequest : CommonPagingRequest<ProductView>
    {
        [DataMember]
        public long? CategoryId { get; set; }
        [DataMember]
        public string Keywords { get; set; }
        [DataMember]
        public Nullable<decimal> PriceMin { get; set; }
        [DataMember]
        public Nullable<decimal> PriceMax { get; set; }
        [DataMember] 
        public long[] Manufacturers { get; set; }

    }
}
