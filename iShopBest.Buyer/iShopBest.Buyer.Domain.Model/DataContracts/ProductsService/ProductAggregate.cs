﻿using iShopBest.Buyer.Domain.Model.Entities;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.ProductsService
{
    [DataContract(IsReference=true)]
    public class ProductAggregate
    {
        [DataMember]
        public Product Product { get; set; }
        [DataMember]
        public IList<Price> Prices { get; set; }
    }
}
