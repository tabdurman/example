﻿using iShopBest.Buyer.Domain.Model.Entities;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.ShoppingCartService
{
    [DataContract(IsReference=true)]
    public class SaveCartResponse
    {
        [DataMember]
        public ShoppingCart Cart { get; set; }
    }
}
