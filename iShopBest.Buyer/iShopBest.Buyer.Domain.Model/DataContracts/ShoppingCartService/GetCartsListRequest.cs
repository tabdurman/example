﻿using System;
using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.ShoppingCartService
{
    [DataContract(IsReference = true)]
    public class GetCartsListRequest
    {
        [DataMember]
        public Int64 ForUserId { get; set; }
    }
}
