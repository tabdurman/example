﻿using iShopBest.Buyer.Domain.Model.Entities;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.ShoppingCartService
{
    [DataContract(IsReference=true)]
    public class GetCartsListResponse
    {
        [DataMember]
        public IList<ShoppingCart> ShoppingCarts { get; set; }
    }
}
