﻿using iShopBest.Buyer.Domain.Model.Entities;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.ShoppingCartService
{
    [DataContract(IsReference = true)]
    public class SaveCartRequest
    {
        [DataMember]
        public string CartName { get; set; }
        [DataMember]
        public bool OverrideExisting { get; set; }
        [DataMember]
        public long UserId { get; set; }
        [DataMember]
        public IList<ShoppingCartItem> ShoppingCartItems { get; set; }
    }
}
