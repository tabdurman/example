﻿using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.ShoppingCartService
{
    [DataContract(IsReference=true)]
    public class LoadCartRequest
    {
        [DataMember]
        public long ForUserId { get; set; }
        [DataMember]
        public long CartId { get; set; }
    }
}
