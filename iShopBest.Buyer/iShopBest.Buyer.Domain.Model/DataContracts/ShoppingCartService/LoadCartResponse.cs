﻿using iShopBest.Buyer.Domain.Model.Entities;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.ShoppingCartService
{
    [DataContract(IsReference =true)]
    public class LoadCartResponse : SaveCartResponse
    {
        [DataMember]
        public List<ShoppingCartItem> ShoppingCartItems { get; set;  }
    }
}
