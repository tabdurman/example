﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService
{
    [DataContract(IsReference = true)]
    public class Order
    {
        private decimal _score;

        [DataMember]
        public double TotalDistance {
            get
            {
                return OrderLines.Sum(x => x.Distance);
            }
        }
        [DataMember]
        public decimal MaxDeliveryDays
        {
            get
            {
                return OrderLines.Max(x => x.DeliveryDays);
            }
        }
        [DataMember]
        public decimal TotalEstimatedFreight
        {
            get
            {
                return OrderLines.Sum(x => x.EstimatedFreight);
            }
        }
        [DataMember]
        public decimal Score {
            get
            {
                return Math.Round(_score, 2);
            }
            set
            {
                _score = value;
            }
        }
        [DataMember]
        public List<OrderLine> OrderLines { get; set; }
        [DataMember]
        public decimal OrderTotal
        {
            get
            {
                decimal val = 0M;
                if (OrderLines != null && OrderLines.Count > 0)
                    foreach (var line in OrderLines)
                        val += line.Price * line.Quantity;
                return val;
            }
        }

        private decimal _weightedCostTotal;

        [DataMember]
        public decimal WeightedCostTotal
        {
            get
            {
                /*
                _weightedCostTotal = 0M;
                decimal cof1 = 0M,cof2=0M;
                if (OrderLines != null && OrderLines.Count > 0)
                {
                    var maxVendors = OrderLines.Select(x => x.SupplierId).Distinct().Count();
                    var maxFreightCost = OrderLines.Max(x => x.EstimatedFreight);

#if DEBUG
                    Debug.WriteLine("maxVendors {0}, maxFreightCost {1}.", maxVendors, maxFreightCost);
#endif

                    foreach (var line in OrderLines)
                    {
                        cof1 += line.WeightedCost * (
                            1 +
                            (decimal)(line.Distance / MaxDistance) * decimal.Parse(Preferences.Where(x => x.Name.Contains("DeliverySpeed")).FirstOrDefault().Value)/100M +
                            (line.EstimatedFreight / maxFreightCost) * decimal.Parse(Preferences.Where(x => x.Name.Contains("FreightCost")).FirstOrDefault().Value) / 100M
                            );
                    }/*
                    cof2 += 1*(1+
                        (OrderLines.Select(x=>x.SupplierId).Distinct().Count()/ maxVendors) *decimal.Parse(Preferences.Where(x => x.Name.Contains("SingleSupplier")).FirstOrDefault().Value) / 100M
                        );*/
                /*}
                _weightedCostTotal = cof1 + cof2;
                Debug.WriteLine("_weightedCostTotal {0}", _weightedCostTotal);
                */
                return _weightedCostTotal;

            }
            set
            {
                _weightedCostTotal = value;
            }
        }
        [DataMember]
        public List<string> OrderingVendors
        {
            get
            {
                return OrderLines.Select(x=>x.SupplierName).Distinct().ToList();
            }
        }

        [DataMember]
        public List<Parameter> Preferences { get; set; }

    }
}
