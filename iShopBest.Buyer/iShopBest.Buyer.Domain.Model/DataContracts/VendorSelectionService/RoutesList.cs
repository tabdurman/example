﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService
{
    [DataContract(IsReference =true)]
    public class RoutesList
    {
        [DataMember]
        public Int64 GtinPlus { get; set; }
        [DataMember]
        public List<Route> Routes { get; set; }
        [DataMember]
        public int BestRouteSequenceNumber { get; set; }

    }
}
