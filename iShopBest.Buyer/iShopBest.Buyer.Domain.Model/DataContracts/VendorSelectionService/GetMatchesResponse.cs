﻿using iShopBest.Buyer.Domain.Model.Entities;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService
{
    [DataContract(IsReference =true)]
    public class GetMatchesResponse 
    {
        [DataMember]
        public List<Product> Products { get; set; }
        [DataMember]
        public List<Price> Matches { get; set; }
        [DataMember]
        public List<Inventory> Inventories { get; set; }
        [DataMember]
        public int ExecutionSpeedTicks { get; set; }
    }
}
