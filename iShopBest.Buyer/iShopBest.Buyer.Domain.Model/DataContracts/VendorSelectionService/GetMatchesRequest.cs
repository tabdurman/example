﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService
{
    [DataContract(IsReference =true)]
    public class GetMatchesRequest
    {
        [DataMember]
        public List<ItemRequirement> ShoppingCart { get; set; }
        [DataMember]
        public bool IncludeInventory { get; set; }
    }
}
