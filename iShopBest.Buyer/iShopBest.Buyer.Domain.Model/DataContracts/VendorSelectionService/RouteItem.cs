﻿using iShopBest.Buyer.Domain.Model.Entities;
using System;
using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService
{
    /// <summary>
    /// Class to hold info about routing item inside of route
    /// </summary>
    [DataContract(IsReference =true)]
    public class RouteItem
    {
        [DataMember]
        public int SequenceNumber { get; set; }
        [DataMember]
        public Price MatchPrice { get; set; }
        [DataMember]
        public ItemRequirement Requirement { get; set; }
        [DataMember]
        public Inventory MatchInventory { get; set; }
        [DataMember]
        public int OrderedQuantity { get; set; }
        public double Distance
        {
            get
            {
                double dist=-1;
                if (MatchInventory != null && Requirement != null)
                {
                    if (MatchInventory.SrcLatitude != null && MatchInventory.SrcLongitude != null && Requirement.DestLatitude != 0 && Requirement.DestLongitude != 0)
                    {
                        double rlat1 = Math.PI * (double)MatchInventory.SrcLatitude / 180;
                        double rlat2 = Math.PI * (double)Requirement.DestLatitude / 180;
                        double theta = (double)MatchInventory.SrcLongitude - (double)Requirement.DestLongitude;
                        double rtheta = Math.PI * theta / 180;
                        dist =
                            Math.Sin(rlat1) * Math.Sin(rlat2) + Math.Cos(rlat1) *
                            Math.Cos(rlat2) * Math.Cos(rtheta);
                        dist = Math.Acos(dist);
                        dist = dist * 180 / Math.PI;
                        dist = dist * 60 * 1.1515;
                    }
                }
                if (MatchPrice.LeadTime!=null && MatchInventory.WarehouseName=="N/A")
                {
                    //use lead time in cases when distance can not be calculated
                    dist = (double) (MatchPrice.LeadTime * 250) - 1.00;
                }
                return dist;
            }
        }
        [DataMember]
        public decimal SubTotal
        {
            get
            {
                return OrderedQuantity * MatchPrice.Cost;
            }
        }
        /// <summary>
        /// Units of cost per 250 miles each
        /// </summary>
        [DataMember]
        public decimal FreigthCost
        {
            get
            {
                return (1 + (int)Distance / 250) * OrderedQuantity;
            }
        }
        [DataMember]
        public int BackOrderedQuantity {
            get
            {
                return Requirement.Quantity - OrderedQuantity;
            }
        }

    }
}
