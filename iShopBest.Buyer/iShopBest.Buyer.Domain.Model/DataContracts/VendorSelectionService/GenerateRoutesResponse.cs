﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService
{
    [DataContract(IsReference=true)]
    public class GenerateRoutesResponse
    {
        [DataMember]
        public List<RoutesList> RoutesPerGtin { get; set; }
        [DataMember]
        public int ExecutionSpeedTicks { get; set; }
    }
}
