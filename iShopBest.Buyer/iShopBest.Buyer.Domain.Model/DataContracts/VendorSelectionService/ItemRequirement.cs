﻿using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService
{
    [DataContract(IsReference=true)]
    public class ItemRequirement
    {
        [DataMember]
        public long GtinPlus { get; set; }
        [DataMember]
        public int Quantity { get; set; }
        [DataMember]
        public string DestZipCode { get; set; }
        [DataMember]
        public decimal DestLongitude { get; set; }
        [DataMember]
        public decimal DestLatitude { get; set; }

    }
}
