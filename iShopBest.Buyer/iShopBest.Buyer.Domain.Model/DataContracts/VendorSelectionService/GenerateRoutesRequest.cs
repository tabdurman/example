﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService
{
    [DataContract]
    public class GenerateRoutesRequest : GetMatchesRequest
    {
        [DataMember]
        public GetMatchesResponse MatchResults { get; set; }
        [DataMember]
        public List<Parameter> Preferences { get; set; }

    }
}
