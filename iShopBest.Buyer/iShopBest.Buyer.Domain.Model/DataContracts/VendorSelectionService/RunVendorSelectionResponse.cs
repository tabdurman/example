﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService
{
    [DataContract(IsReference = true)]
    public class RunVendorSelectionResponse : GetMatchesResponse
    {
        [DataMember]
        public IList<Order> Orders { get; set; }
    }
}
