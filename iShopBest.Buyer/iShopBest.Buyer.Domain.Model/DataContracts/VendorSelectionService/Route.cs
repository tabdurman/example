﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService
{
    /// <summary>
    /// Class to hold info about whole route
    /// </summary>
    [DataContract(IsReference=true)]
    public class Route
    {
        [DataMember]
        public int SequenceNumber { get; set; }

        [DataMember]
        public List<RouteItem> Path { get; set; }
        [DataMember]
        public decimal RouteCost
        {
            get
            {
                decimal val = 0M;
                if (Path!=null)
                    foreach(var path in Path)
                        val += path.SubTotal;
                return val;
            }
        }
        [DataMember]
        public decimal RouteFreightCost
        {
            get
            {
                decimal val = 0M;
                if (Path != null)
                    foreach (var path in Path)
                        val += path.FreigthCost;
                return val;
            }
        }

        [DataMember]
        public int RequiredQuantity
        {
            get
            {
                int val = 0;
                if (Path != null && Path.Count>0)
                   val = Path[0].Requirement.Quantity;
                return val;
            }
        }

        [DataMember]
        public int OrderedQuantity
        {
            get
            {
                int val = 0;
                if (Path != null)
                    foreach (var path in Path)
                        val += path.OrderedQuantity;
                return val;
            }
        }

        /// <summary>
        /// will show if ordered from warehouses versus from totalavailable quantity
        /// vendors who do not supply by warehouse stocks, will use this option
        /// </summary>
        [DataMember]
        public bool NoWarehousDetails { get; set; }

        /// <summary>
        /// vendors who do not have stock information at all will have that set to true
        /// </summary>
        [DataMember]
        public bool NoStockAvailable { get; set; }

    }
}
