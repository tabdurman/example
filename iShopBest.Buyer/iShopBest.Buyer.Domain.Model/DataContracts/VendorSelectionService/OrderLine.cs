﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService
{
    [DataContract(IsReference = true)]
    public class OrderLine
    {
        [DataMember]
        public long GTINPLUS { get; set; }
        [DataMember]
        public byte ProductRating { get; set; }

        [DataMember]
        public Int64 SupplierId { get; set; }
        [DataMember]
        public string SupplierName { get; set; }
        [DataMember]
        public byte SupplierRating { get; set; }

        [DataMember]
        public string Sku { get; set; }
        [DataMember]
        public string Coo { get; set; }

        [DataMember]
        public Int64? WarehouseId { get; set; }
        [DataMember]
        public string WarehouseName { get; set; }
        [DataMember]
        public byte? WarehouseRating { get; set; }

        [DataMember]
        public int RequestedQuantity { get; set; }
        [DataMember]
        public int Quantity { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        [DataMember]
        public decimal EstimatedFreight { get; set; }
        [DataMember]
        public double Distance { get; set; }

        [DataMember]
        public int DeliveryDays
        {
            get
            {
                return (int)(Distance / 250) + 1;

            }
        }
        [DataMember]
        public decimal Weighter { get; set; }
        [DataMember]
        public decimal WeightedCost
        {
            get
            {
                return Weighter * Quantity * Price;
            }
        }
    }
}
