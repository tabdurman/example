﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService
{
    [DataContract]
    public class RunVendorSelectionRequest : GetMatchesRequest
    {
        [DataMember]
        public List<Parameter> Preferences { get; set; }
    }
}
