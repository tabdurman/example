﻿using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService
{
    [DataContract(IsReference =true, Namespace ="")]
    public class Parameter
    {
        /// <summary>
        /// Parameter name
        /// </summary>
        [DataMember]
        public string Name { get; set; }
        /// <summary>
        /// Parameter value
        /// </summary>
        [DataMember]
        public string Value { get; set; }
    }
}
