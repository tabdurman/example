﻿using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.VendorSelectionService
{
    [DataContract(IsReference = true)]
    public class RouteIndex
    {
        [DataMember]
        public decimal WeightedCost { get; set; }
        [DataMember]
        public int RouteAxisI { get; set; }
        [DataMember]
        public int RouteAxisJ { get; set; }

        [DataMember]
        public decimal Score { get; set; }

    }
}
