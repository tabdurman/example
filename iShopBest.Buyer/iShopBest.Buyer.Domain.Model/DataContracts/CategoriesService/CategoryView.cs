﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.CategoriesService
{
    [DataContract(IsReference =true)]
    public class CategoryView
    {
        [DataMember]
        //public SortedDictionary<Int64,string> Path { get; set; }
        public IList<CategoryView> Path { get; set; }
        [DataMember]
        public Int64? ParentCategoryId { get; set; }
        [DataMember]
        public Int64 CategoryId { get; set; }
        [DataMember]
        public string CategoryDescription { get; set; }
        [DataMember]
        public string CategoryImage { get; set; }
        [DataMember]
        public int ProductCount { get; set; }
    }
}
