﻿using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.CategoriesService
{
    [DataContract(IsReference=true)]
    public class GetProductsByCategoryRequest
    {
        [DataMember]
        public long CategoryId { get; set; }
    }
}
