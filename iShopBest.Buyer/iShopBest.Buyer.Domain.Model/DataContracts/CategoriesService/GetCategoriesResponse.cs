﻿using iShopBest.Buyer.Domain.Model.Entities;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.CategoriesService
{
    [DataContract(IsReference=true)]
    public class GetCategoriesResponse
    {
        [DataMember]
        public IList<CategoryView> Path { get; set; }
        [DataMember]
        public IList<CategoryView> Categories { get; set; }
    }
}
