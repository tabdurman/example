﻿using iShopBest.Buyer.Domain.Model.DataContracts.ProductsService;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace iShopBest.Buyer.Domain.Model.DataContracts.CategoriesService
{
    [DataContract(IsReference=true)]
    public class GetProductsByCategoryResponse
    {
        [DataMember]
        public List<string> CatgoryPath { get; set; }
        [DataMember]
        public List<ProductAggregate> Products { get; set; }
    }
}
