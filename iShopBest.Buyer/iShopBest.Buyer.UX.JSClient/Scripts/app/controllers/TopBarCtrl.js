﻿(function (angular) {

    var app;

    try {
        app = angular.module('rootApp');
    }
    catch (err) {
        angular.module("rootApp", ["ui.router", "ngResource"]);
        app = angular.module('rootApp');
    }

    app.controller('TopBarCtrl',
        ['$scope', '$rootScope', '$stateParams', '$http', '$log', '$filter', 'infrastructure',
            function ($scope, $rootScope, $stateParams, $http, $log, $filter, infrastructure) {
                var that = this;
            }
        ]);
})(window.angular);