﻿(function (angular) {

    var app;

    try {
        app = angular.module('rootApp');
    }
    catch (err) {
        angular.module("rootApp", ["ui.router", "ngResource"]);
        app = angular.module('rootApp');
    }

    app.controller('MainViewCtrl',
        ['$scope', '$rootScope', '$stateParams', '$http', '$log', '$filter', 'infrastructure', 'buyerWebSiteService',
    function ($scope, $rootScope, $stateParams, $http, $log, $filter, infrastructure, buyerWebSiteService) {
                var that = this;

                var handleError = function (reason,handler) {
                    $scope.responseData = undefined;
                    $scope.responseError = reason;
                    if (typeof handler !== 'undefined')
                        handler(reason);
                }

                var handleSuccess = function (data, handler) {
                    $scope.responseData = data;
                    $scope.responseError = undefined;
                    if (typeof handler !== 'undefined')
                        handler(data);
                }

                $scope.doLogin = function () {
                    buyerWebSiteService.loginUser(
                        { email: 'demouser@capitolsupply.com', password: 'iShopBest2017_', confirmPassword: "", userName: "demouser@capitolsupply.com", grant_type: "password" },

                        function (data) {
                            $scope.accessToken = data.access_token;
                            handleSuccess(data);
                        },
                        handleError
                    );
                }
        
                $scope.doLogout = function () {
                    buyerWebSiteService.logoutUser(null,

                        function (data) {
                            $scope.accessToken = undefined;
                            handleSuccess(data);
                        },

                        handleError
                    );
                };
                
                $scope.getCats = function () {
                    buyerWebSiteService.getCategories({rootCategoryId: null},
                        handleSuccess,
                        handleError
                    );
                };
                
                $scope.getUserInfo = function () {
                    buyerWebSiteService.getUserInfo({},
                        handleSuccess,
                        handleError
                    );
                };

                $scope.getManageInfo = function () {
                    buyerWebSiteService.getManageInfo({ returnUrl: window.location.href, generateState: true},
                        handleSuccess,
                        handleError
                    );

                };

                $scope.getExternalLogin = function () {
                    buyerWebSiteService.getExternalLogin({ provider: 'self', error: null },
                        handleSuccess,
                        handleError
                    );
                };
    }
        ]);
})(window.angular);