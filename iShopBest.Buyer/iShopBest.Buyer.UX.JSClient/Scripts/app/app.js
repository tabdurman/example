﻿(function (angular) {

    'use strict';

    angular.module("rootApp", ["ui.router", "ngResource", "ui.bootstrap", "ngCookies"]);
    var app = angular.module("rootApp");
    app.value('runningMode', 'GLOBAL');

    app.value('runningMode', 'LOCAL');

    app.factory('quickInterceptor', function ($q, $injector) {

        return {
            'request': function (config) {
                $('#grayout').show();
                $('#loading').show();
                return config;
            },
            'requestError': function (rejection) {
                var $http = $injector.get('$http');
                if ($http.pendingRequests.length < 1) {
                    $('#loading').hide();
                    $('#grayout').hide();
                }
                return $q.reject(rejection);
            },
            'response': function (response) {
                var $http = $injector.get('$http');
                if ($http.pendingRequests.length < 1) {
                    $('#loading').hide();
                    $('#grayout').hide();
                }
                return response;
            },
            'responseError': function (rejection) {
                var $http = $injector.get('$http');
                if ($http.pendingRequests.length < 1) {
                    $('#loading').hide();
                    $('#grayout').hide();
                }
                return $q.reject(rejection);

            }
        };

    });

    app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function ($stateProvider, $urlRouterProvider, $httpProvider) {
        //        app.config(['$stateProvider', '$urlRouterProvider', '$qProvider', '$httpProvider', function ($stateProvider, $urlRouterProvider, $qProvider,$httpProvider) {

        //1.5.9 fix for possible unhandled rejection
        //$qProvider.errorOnUnhandledRejections(false);

        //route definitions
        $urlRouterProvider
                .when('/', 's')
                .when('//', 's')
                .otherwise('');

        $stateProvider.state('home', {
            //url: '/s/:rootCategoryId/:keywords',
            url: '/s/:keywords',
            views: {
                'topBar': {
                    templateUrl: '/Scripts/app/views/topBarView.html',
                    controller: 'TopBarCtrl'
                },
                'mainView': {

                    templateUrl: '/Scripts/app/views/mainView.html',
                    controller: 'MainViewCtrl'
                },
                'bottomBar': {
                    controller: 'BottomBarCtrl',
                    templateUrl: '/Scripts/app/views/bottomBarView.html'
                }
            }
        });

        $httpProvider.interceptors.push('quickInterceptor');
        $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
        // Override $http service's default transformRequest
        
        $httpProvider.defaults.transformRequest = [function (data) {
            /**
             * The workhorse; converts an object to x-www-form-urlencoded serialization.
             * @param {Object} obj
             * @return {String}
             */
            var param = function (obj) {
                var query = '';
                var name, value, fullSubName, subName, subValue, innerObj, i;
                for (name in obj) {
                    value = obj[name];
                    if (value instanceof Array) {
                        for (i = 0; i < value.length; ++i) {
                            subValue = value[i];
                            fullSubName = name + '[' + i + ']';
                            innerObj = {};
                            innerObj[fullSubName] = subValue;
                            query += param(innerObj) + '&';
                        }
                    }
                    else if (value instanceof Object) {
                        for (subName in value) {
                            subValue = value[subName];
                            fullSubName = name + '[' + subName + ']';
                            innerObj = {};
                            innerObj[fullSubName] = subValue;
                            query += param(innerObj) + '&';
                        }
                    }
                    else if (value !== undefined && value !== null) {
                        query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
                    }
                }
                return query.length ? query.substr(0, query.length - 1) : query;
            };
            return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
        }];

    }]);



    app.run(['$state', '$stateParams', '$rootScope', function ($state, $stateParams, $rootScope) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        $state.go('home');
    }]);


})(window.angular);
