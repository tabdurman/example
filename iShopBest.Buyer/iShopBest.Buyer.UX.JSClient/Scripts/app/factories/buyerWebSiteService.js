﻿(function (angular) {
    'use strict';
    var app = angular.module('rootApp');
    app.factory('buyerWebSiteService', ['$resource', 'buyerWebSiteRepository', function ($resource, buyerWebSiteRepository) {

        var getHeaders = function () {
            if (accessToken)
                return { "Authorization": "Bearer " + accessToken };
        };

        var accessToken = "";


        var service = {
            loginUser:  function (userData,success,failure) {
                userData.userName = userData.email;
                buyerWebSiteRepository
                    .service(getHeaders())
                    .Login(userData
                    ,function (data) {
                        if (data && data.access_token)
                            accessToken = data.access_token;

                        success(data)
                    }
                    , function (reason) {
                        failure(reason)
                    });
            },
            getCategories: function (params,success,failure) {
                buyerWebSiteRepository
                    .service(getHeaders())
                    .Categories(params
                    , function (data) {
                        if (data && data.access_token)
                            accessToken = data.access_token;

                        success(data)
                    }
                    , function (reason) {
                        failure(reason)
                    });

            },
            logoutUser: function (params, success, failure) {
                buyerWebSiteRepository
                    .service(getHeaders())
                    .Logout(params
                    , function (data) {
                        accessToken = "";
                        success(data)
                    }
                    , function (reason) {
                        failure(reason)
                    });
            },

            getUserInfo: function (params, success, failure) {
                buyerWebSiteRepository
                    .service(getHeaders())
                    .UserInfo(params
                    , function (data) {
                        success(data)
                    }
                    , function (reason) {
                        failure(reason)
                    });
            },
            getManageInfo: function (params, success, failure) {
            buyerWebSiteRepository
                .service(getHeaders())
                .ManageInfo(params
                , function (data) {
                    success(data)
                }
                , function (reason) {
                    failure(reason)
                });
            },
            getExternalLogin: function (params, success, failure) {
                buyerWebSiteRepository
                    .service(getHeaders())
                    .ExternalLogin(params
                    , function (data) {
                        success(data)
                    }
                    , function (reason) {
                        failure(reason)
                    });
            }

        };

        return service;
    }]);
})(window.angular);