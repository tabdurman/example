﻿(function (angular) {

    var app = angular.module('rootApp');

    app.factory('buyerWebSiteRepository', function ($resource) {


        var baseUrl = 'https://buyer.ishopbest.com';

        return { 
            service: function (headers) {
                return $resource(baseUrl, {},
                    {
                        'Login': {
                            method: "POST"
                            , isArray: false
                            , url: baseUrl + '/api/Account/Token'
                            , headers: headers
                        },
                        'Logout': {
                            method: "POST"
                            , isArray: false
                            , url: baseUrl + '/api/Account/Logout'
                            , headers: headers
                        },
                        'UserInfo' : {
                            method: "GET"
                            , isArray: false
                            , url: baseUrl + '/api/Account/UserInfo'
                            , headers: headers
                        },
                        'ManageInfo': {
                            method: "GET"
                            , isArray: false
                            , url: baseUrl + '/api/Account/ManageInfo'
                            , headers: headers
                        },
                        'ExternalLogin': {
                            method: "GET"
                            , isArray: false
                            , url: baseUrl + '/api/Account/ExternalLogin'
                            , headers: headers
                        },
                        'Categories': {
                            method: "GET"
                            , isArray: false
                            , url: baseUrl + '/api/Categories'
                            , headers: headers
                        }

                    });
            }
        };
    });

})(window.angular);