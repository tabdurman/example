# README #

Click or tap Source folder, left menu
FIRST THING YOU WANT TO SEE: iShopBest.Buyer.UX.Andromeda UX code under iShopBest.Buyer
all API controllers are under iShopBest.Buyer.Presentation.Api 
all MVC controllers are under iShopBest.Buyer.Presentation.MVC
1. iShopBest.Buyer is a folder with sample code for buyer fronent, 
accessable by capitolsupply.ishopbest.com online,
it will not compile because of missing doman, repository and infrastructure layers
you can get idea about my code writing style in C#
2. There are examples of websites i can show you as an examples
Production stage is kctalents.com to show my HTML5/CSS3/JavaScript skills
Others are beta stages, but they have examples of AngularJS usage
To see JS code, use F12 button while browsing websites, and if you are using chrome,
check source tab for JS code, its is usually under app folder

### What is this repository for? ###

* You are browsing example repository
* Version 1
* [Tair Abdurman](https://bitbucket.org/tabdurman/example)

### How do I get set up? ###

* Example project includes only Presentation and User Experience layers
* Domain/Infrastructure/Repository layers available upon request
* you will not be able completely compile these layers
* use Visual Studio 2017 to view source

### Contribution guidelines ###

* Tests not included


### Who do I talk to? ###

* tabdurman@gmail.com  for any questions you have